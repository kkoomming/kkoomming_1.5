//
//  MyPageVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/21.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Promises

class MyPageVC: BaseVC {
    let disposeBag = DisposeBag()
    
    var pageType:MyFeedType = .post
    
    let topView = TopView(type: .mypage)
    
    lazy var scrollView = UIScrollView().then{
        $0.contentInsetAdjustmentBehavior = .never
        $0.delegate = self
    }
    
    let stackView = UIStackView().then{
        $0.alignment = .fill
        $0.axis = .vertical
        $0.distribution = .fill
        $0.spacing = 30
    }
    
    let topProfileView = UIView()
    
    let profileBackgroundView = UIView().then{
        $0.backgroundColor = RGB(red: 215, green: 243, blue: 246)
    }
    
    let profileView = ProfileView(isMyProfile: true)
    
    lazy var pagerVC = MyPagePagerVC().then{
        $0.selectorDelegate = self
        
        $0.scriptRefreshHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.updateViewHeight(index:2)
            print("MyPageVC scriptRefreshHandler")
        }
        
        $0.likeRefreshHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.updateViewHeight(index:1)
            print("MyPageVC likeRefreshHandler")
        }
    }
    lazy var pagerView:UIView! = pagerVC.view
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.updateViewHeight()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getMyProfile()
        
        AlertManager.shared.updateStatus(view: topView)
    }
    
    private func setupUI() {
        scrollView.addSubViews([
            stackView,
        ])
        
        topProfileView.addSubViews([
            profileBackgroundView,
            topView,
            profileView,
        ])
        
        stackView.addArrangedSubview(topProfileView)
        stackView.addArrangedSubview(pagerView)
        addChild(pagerVC)
        
        self.view.addSubview(scrollView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        scrollView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
        
        profileBackgroundView.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(profileView.getBackgroundHeight())
        }
        
        topView.snp.makeConstraints{
            $0.top.equalTo(self.stackView.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        profileView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(297)
        }
    
        profileView.layoutIfNeeded()
        profileView.snp.updateConstraints{
            $0.height.equalTo(profileView.getProfileViewHeight())
        }
        
        let profileCotentHeight = profileView.getCotentHeight()
        topProfileView.snp.makeConstraints{
            $0.height.equalTo(profileCotentHeight)
        }
        
        pagerView.snp.makeConstraints{
            $0.height.equalTo(1)
        }
        
    }
    
    private func updateViewHeight(index:Int = 0) {
        profileView.layoutIfNeeded()
        profileView.profileImageView.layoutIfNeeded()
        profileView.profileContentView.layoutIfNeeded()
        
        let profileViewHeight = profileView.getCotentHeight()
        
        topProfileView.snp.updateConstraints{
            $0.height.equalTo(profileViewHeight)
        }
        
        let safeTop = AppManager.shared.getSafeAreaInsets().top
        let profileImageTop = profileView.profileImageView.frame.origin.y
        let profileDividedHeight = (profileView.profileImageView.frame.size.height / 2)
        let contentHeight = profileView.profileContentView.frame.height
        let backgroundHeight = (contentHeight / 2) + 65 + safeTop + profileImageTop + profileDividedHeight
        
        print("TEST : safeTop : ", safeTop)
        print("TEST : profileImageTop : ", profileImageTop)
        print("TEST : profileDividedHeight : ", profileDividedHeight)
        print("TEST : contentHeight : ", contentHeight)
        print("TEST : backgroundHeight : ", backgroundHeight)
        
        //파란색 뒷 배경 영역
        profileBackgroundView.snp.updateConstraints{
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(backgroundHeight)
        }
        
        topProfileView.layoutIfNeeded()
        
        //내 글, 좋아요, 스크랩의 각 뷰의 높이 설정
        pagerVC.updateUI(index: index)
    }
}

extension MyPageVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.frame.origin = CGPoint(x: 0, y: 0)
        } else {
            let y = scrollView.contentOffset.y
            let viewHeight = self.scrollView.frame.height
            let contentHeight = self.pagerVC.view.frame.height
            
            if ((contentHeight - y) - 100) <= (viewHeight / 2) {

                //좋아요
                if self.pageType == .like {
                    guard !self.pagerVC.myLikeVC.isPaging else { return }
                    self.pagerVC.myLikeVC.getMyFeed(isFirst: false)
                }

                //스크랩
                if self.pageType == .scrap {
                    guard !self.pagerVC.myScriptVC.isPaging else { return }
                    self.pagerVC.myScriptVC.getMyFeed(isFirst: false)
                }
            }
        }
    }
}

extension MyPageVC: MyPageSelectorDelegate {
    func selectTab(index: Int) {
        if index == 0 {
            self.pageType = .post
        } else if index == 1 {
            self.pageType = .like
        } else {
            self.pageType = .scrap
        }
    }
}

extension MyPageVC {
    func getMyProfile() {
        DispatchQueue.main.async {
            API.getMyProfile() { model in
                guard let model = model, let data = model.data else { return }
                print("nickname : ", data.nickname)
                
                if model.success {
                    self.configuration(item: data)
                    
                    //내 글 조회
                    self.pagerVC.myWriteVC.getMyFeed(isFirst: true)
                    
                    //좋아요 조회
                    self.pagerVC.myLikeVC.getMyFeed(isFirst: true)
                    
                    //스크랩 조회
                    self.pagerVC.myScriptVC.getMyFeed(isFirst: true)
                } else {
                    AlertManager.shared.show(text: "잠시 후 다시 시도해 주세요.", type: .one)
                }
            }
        }
    }
    
    private func configuration(item:User){
        UserManager.shared.update(user: item)
        profileView.configuration(isMyProfile: true,item: item)
    }
}
