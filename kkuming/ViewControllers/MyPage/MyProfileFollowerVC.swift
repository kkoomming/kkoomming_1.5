//
//  MyProfileFollowerVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/12.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip
import Alamofire

class MyProfileFollowerVC: BaseVC, IndicatorInfoProvider {
    var itemInfo = IndicatorInfo(title: "팔로워")
    var items:[User] = []
    var userId:Int = 0
    
    var titleRefreshHandler:(()->Void)? = nil
    
    //MARK: 페이징
    var page:Int = 1
    
    let profileAndFollowListView = ProfileAndFollowListView()
    
    var emptyView = ListEmptyView().then{
        $0.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    private func setupUI() {
        addSubViews([
            profileAndFollowListView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileAndFollowListView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension MyProfileFollowerVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        profileAndFollowListView.isHidden = bool
    }
}

extension MyProfileFollowerVC {
    public func configuration(items:[User]) {
        self.items = items
        if self.items.count == 0 {
            //빈 화면 처리
            isEmpty(true)
        } else {
            //빈 화면 숨김 처리
            isEmpty(false)
            profileAndFollowListView.configuration(items: self.items)
            
            self.profileAndFollowListView.moreHandler = { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.getSomeoneFollower(userId: strongSelf.userId, isFirst: false)
            }
        }
    }
    
    public func add(items:[User]) {
        self.items += items
        self.profileAndFollowListView.configuration(items: self.items)
    }
    
    //마이꾸밍 > 팔로워
    public func getMyFollowing(isFirst:Bool) {
        API.getMyFollower() {[weak self] model in
            guard let strongSelf = self, let model = model else { return }
            strongSelf.updateUI(model: model, isFirst: isFirst)
        }
    }
    
    //타인 > 팔로워
    public func getSomeoneFollower(userId:Int, isFirst:Bool) {
        self.userId = userId
        
        let param:Parameters = [
            "userId" : userId,
            "offset" : page
        ]
        
        API.getSomeoneFollower(param:param) { [weak self] model in
            guard let strongSelf = self, let model = model else {
                return
            }
            
            strongSelf.updateUI(model: model, isFirst:isFirst)
        }
    }
    
    private func updateUI(model:ProfileFollowerData, isFirst:Bool){
        guard let items = model.data?.follower?.users,
                let meta = model.data?.follower?.metaData else { return }
        
        if model.success {
            if isFirst {
                //테스트용
//                    let users = TestFactory.createModelList(type: User.self)
//                    strongSelf.configuration(items: users)
                
                //실제
                self.configuration(items: items)
            } else {
                self.add(items: items)
            }
            
            if self.page >= meta.totalPages {
                self.profileAndFollowListView.isPaging = true
            } else {
                self.profileAndFollowListView.isPaging = false
            }
            
            self.page += 1
            
            //개수 표기
            let titleString = "팔로워 " + meta.totalRecords.toKilo
            self.itemInfo = IndicatorInfo(title: titleString)
            self.titleRefreshHandler?()
        } else {
            AlertManager.shared.show(text: "프로필 팔로워 에러", type: .one)
        }
    }
}
