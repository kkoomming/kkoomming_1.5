//
//  MyProfileFollowingVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/12.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip
import Kingfisher
import Alamofire

class MyProfileFollowingVC: BaseVC, IndicatorInfoProvider {
    var itemInfo = IndicatorInfo(title: "팔로잉")
    var items:[User] = []
    var userId:Int = 0
    
    var titleRefreshHandler:(()->Void)? = nil
    
    //페이징
    var page = 1
    var isPaging = false
    var isMain = true
    
    var emptyView = ListEmptyView().then{
        $0.isHidden = true
    }
    
    //내껀지 타인껀지 확인
    var isMy:Bool = false
    
    lazy var tableView = UITableView().then{
        $0.allowsSelection = false
        $0.register(ProfileFollowingViewCell.self, forCellReuseIdentifier: ProfileFollowingViewCell.identifier)
        $0.delegate = self
        $0.dataSource = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    private func setupUI() {
        addSubViews([
            tableView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension MyProfileFollowingVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.items.count == 0 {
            //빈 화면 처리
            isEmpty(true)
        } else {
            //빈 화면 숨김 처리
            isEmpty(false)
        }
        
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProfileFollowingViewCell.identifier, for: indexPath) as! ProfileFollowingViewCell
        let index = indexPath.row
        let item = items[index]
        
        //팔로잉 상태
        cell.configuration(item: item)
        
        cell.followingButton.rx.tap.bind{ [weak self] in
            guard let strongSelf = self, let followingId = item.id else { return }
            strongSelf.followReact(followingId: followingId, index:index)
        }.disposed(by: cell.disposeBag)
        
        cell.thumbButton.rx.tap.bind{
            let id = item.id ?? 0
            
            if UserManager.shared.getUserId() == id {
                //본인
                AppManager.shared.popToRoot()
                AppManager.shared.moveToTab(index: 4)
                return
            } else {
                //타인
                App.push(ProfileVC(userId: id))
            }
        }.disposed(by: cell.disposeBag)
        
        cell.nameButton.rx.tap.bind{
            let id = item.id ?? 0
            
            if UserManager.shared.getUserId() == id {
                //본인
                AppManager.shared.popToRoot()
                AppManager.shared.moveToTab(index: 4)
                return
            } else {
                //타인
                App.push(ProfileVC(userId: id))
            }
        }.disposed(by: cell.disposeBag)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.items.count - 1) {
            guard !isPaging else { return }
            self.isPaging = true
            
            if isMy {
                self.getMyFollowing()
            } else {
                self.getSomeoneFollowing(userId: userId)
            }
            
            //My인지 타인인지 BOOL로 체크후 각자 API
//            self.getEventsList()
        }
    }
}

extension MyProfileFollowingVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        tableView.isHidden = bool
    }
}

extension MyProfileFollowingVC {
    public func configuration(items:[User]) {
        self.items = items
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    public func add(items:[User]) {
        self.items += items
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    //마이꾸밍 > 팔로잉
    public func getMyFollowing() {
        API.getMyFollowing(offset:page) { model in
            guard let model = model,
                let items = model.data?.following?.users,
            let meta = model.data?.following?.metaData else { return }
            
            if model.success {
                if self.isMain {
                    //테스트
//                    self.configuration(items: [])
                    
                    //실제
                    self.configuration(items: items)
                    
                    self.isMain = false
                    self.isMy = true
                } else {
                    self.add(items: items)
                }
                
                if self.page >= meta.totalPages {
                    self.isPaging = true
                } else {
                    self.isPaging = false
                }
                
                self.page += 1
                
                //개수 표기
                let titleString = "팔로잉 " + meta.totalRecords.toKilo
                self.itemInfo = IndicatorInfo(title: titleString)
                self.titleRefreshHandler?()
            } else {
                AlertManager.shared.show(text: "에러 발생", type: .one)
            }
        }
    }
    
    //타인 > 팔로잉
    public func getSomeoneFollowing(userId:Int) {
        let param:Parameters = [
            "userId" : userId,
            "offset" : page
        ]
        
        self.userId = userId
        
        API.getSomeoneFollowing(param:param) { model in
            guard
                let model = model,
                let items = model.data?.following?.users,
                let meta = model.data?.following?.metaData
            else { return }
            
            if model.success {
                if self.isMain {
                    //테스트용
//                    let users = TestFactory.createModelList(type: User.self)
//                    self.configuration(items: [])
                    
                    //실제
                    self.configuration(items: items)
                    
                    self.isMain = false
                    self.isMy = false
                } else {
                    self.add(items: items)
                }
                
                if self.page >= meta.totalPages {
                    self.isPaging = true
                } else {
                    self.isPaging = false
                }
                
                self.page += 1
                
                //개수 표기
                let titleString = "팔로잉 " + meta.totalRecords.toKilo
                self.itemInfo = IndicatorInfo(title: titleString)
                self.titleRefreshHandler?()
            } else {
                AlertManager.shared.show(text: "에러 발생", type: .one)
            }
        }
    }
    
    //팔로우
    func followReact(followingId:Int, index:Int) {
        API.followReact(followingId: followingId) { [weak self] model, data in
            guard let strongSelf = self, let model = model else {
                if let data = data {
                    JSON.decode(data: data) { errorData in
                        guard let errorData = errorData else {
                            return
                        }
                        
                        if errorData.data.code == "7003" {
                            AlertManager.shared.show(text: "본인은 팔로우 할 수 없습니다.", type: .one)
                        }
                    }
                }
                return
            }

            if model.success {
                strongSelf.items[index].isFollowed = model.data.isFollowed
                DispatchQueue.main.async {
                    strongSelf.tableView.reloadData()
                }
            } else {
                AlertManager.shared.show(text: "에러 발생", type: .one)
            }
        }
    }
}
