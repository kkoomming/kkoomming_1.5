//
//  ProfileView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/29.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class ProfileView: UIView {
    let disposeBag = DisposeBag()
    var profileVC: MyProfileCommonVC!
    var isMyProfile:Bool = false
    var user:User!
    
    //MARK: 팔로우
    var isFollowed = false {
        didSet {
            self.updateButtonStatus()
        }
    }
    
    //새로고침 전용
    var refreshHandler:(()->Void)? = nil
    
    //클릭시 링크 이동을 위해 저장
    var linkShare:String = ""
    
    let followButtonBorderView = BackgroundBorderView(RGB(red: 112, green: 112, blue: 112).withAlphaComponent(0.29))
    
    let followView = UIView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 13
        $0.backgroundColor = .mainColor
    }
    let followLabel = UILabel().then{
        $0.font = getFont(size: 12, type: .bold)
        $0.textColor = .white
        $0.text = "팔로우"
    }
    lazy var followButton = UIButton().then{
        $0.rx.tap.bind{ [weak self] in
            guard let strongSelf = self, let followingId = strongSelf.user.id else {return}
            strongSelf.followReact(followingId: followingId)
        }.disposed(by: disposeBag)
    }
    
    //MARK: 일반
    let profileImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 83/2
        $0.contentMode = .scaleToFill
    }
    
    //이게 그림자 뷰임
    let profileContentView = UIView().then{
        $0.clipsToBounds = true
        $0.layer.cornerRadius = 20
        $0.backgroundColor = .white
        $0.layer.shadowColor = UIColor.black.withAlphaComponent(0.6).cgColor
        $0.layer.shadowRadius = 4
        $0.layer.shadowOpacity = 0.5
        $0.layer.shadowOffset = CGSize(width: 1, height: 1)
        $0.layer.masksToBounds = false
    }
    
    let editImageView = UIImageView().then{
        $0.image = UIImage(named: "ic_profile")
    }
    
    lazy var editButton = UIButton().then{
        $0.rx.tap.bind{
            print("editButton")
            
            //자신의 프로필만 가능하게
            App.push(MyProfileUpdateVC())
        }.disposed(by: disposeBag)
    }
    
    let profileStackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.spacing = 16
    }
    
    let profileNameLabel = UILabel().then{
        $0.font = getFont(size: 20, type: .EBold)
        $0.textAlignment = .center
        $0.textColor = .black
    }
    
    let profileIntroduceLabel = UILabel().then{
        $0.numberOfLines = 4
        $0.font = getFont(size: 11)
        $0.textAlignment = .center
        $0.textColor = RGB(red: 114, green: 114, blue: 114)
    }
    
    lazy var snsLinkLabel = UILabel().then{
        $0.numberOfLines = 2
        $0.font = getFont(size: 11)
        $0.textAlignment = .center
        $0.textColor = RGB(red: 114, green: 114, blue: 114)
        $0.isUserInteractionEnabled = true
    }

    let followingParentStackView = UIStackView()
    let followingStackView = UIStackView().then{
        $0.axis = .horizontal
        $0.alignment = .fill
        $0.distribution = .fill
        $0.spacing = 10
        $0.layoutMargins = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 40)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    //팔로워 999k
    let followingLeftView = UIView()
    let followingLeftLabel = UILabel().then{
        let textString = "팔로워 999k"
        let attributes:[NSAttributedString.Key:Any] = [
            NSAttributedString.Key.font:getFont(size: 14, type: .bold),
            NSAttributedString.Key.foregroundColor:UIColor.black
        ]
        let attributedText = NSMutableAttributedString(string: textString, attributes: attributes)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 14, type: .regular), range: (textString as NSString).range(of: "팔로워"))
        
//        $0.attributedText = attributedText
        $0.textAlignment = .right
    }
    lazy var followingLeftButton = UIButton().then{
        $0.rx.tap.bind{ [weak self] in
            print("followingLeftButton")
            guard let strongSelf = self, let userId = strongSelf.user.id else {
                return
            }
            
            strongSelf.profileVC = MyProfileCommonVC(isFollwer: true, isMyProfile: strongSelf.isMyProfile, userId: userId)
            
            App.push(strongSelf.profileVC)
        }.disposed(by: disposeBag)
    }
    
    let followingBorderView = BackgroundBorderView(RGB(red: 112, green: 112, blue: 112).withAlphaComponent(0.29))
    
    //팔로잉 999k
    let followingRightView = UIView()
    let followingRightLabel = UILabel().then{
        let textString = "팔로잉 999k"
        let attributes:[NSAttributedString.Key:Any] = [
            NSAttributedString.Key.font:getFont(size: 14, type: .bold),
            NSAttributedString.Key.foregroundColor:UIColor.black
        ]
        let attributedText = NSMutableAttributedString(string: textString, attributes: attributes)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 14, type: .regular), range: (textString as NSString).range(of: "팔로잉"))
        
//        $0.attributedText = attributedText
        $0.textAlignment = .left
    }
    lazy var followingRightButton = UIButton().then{
        $0.rx.tap.bind{ [weak self] in
            print("followingRightButton")
            guard let strongSelf = self, let userId = strongSelf.user.id else {
                return
            }
            
            strongSelf.profileVC = MyProfileCommonVC(isFollwer: false, isMyProfile: strongSelf.isMyProfile, userId: userId)
            
            App.push(strongSelf.profileVC)
        }.disposed(by: disposeBag)
    }
    
    init(isMyProfile:Bool) {
        super.init(frame: CGRect.zero)
        self.isMyProfile = isMyProfile
        
        setupUI()
    }
    
    public func configuration(isMyProfile:Bool, item:User, refreshHandler:(()->Void)? = nil) {
        self.isMyProfile = isMyProfile
        self.refreshHandler = refreshHandler
        self.user = item
        if let isFollowed = item.isFollowed {
            self.isFollowed = isFollowed
        }
        
        profileNameLabel.text = item.nickname
        profileIntroduceLabel.text = item.bio
        snsLinkLabel.text = item.linkShare
        self.linkShare = item.linkShare ?? ""
        
        if let totalFollower = item.totalFollower {
            let string = totalFollower.toKilo
            
            var textString = "팔로워 \(string)"
            if !isMyProfile {
                let count = string.replacingOccurrences(of: "(", with: "")
                    .replacingOccurrences(of: ")", with: "")
                textString = "팔로워 \(count)"
            }
            
            let attributes:[NSAttributedString.Key:Any] = [
                NSAttributedString.Key.font:getFont(size: 14, type: .bold),
                NSAttributedString.Key.foregroundColor:UIColor.black
            ]
            let attributedText = NSMutableAttributedString(string: textString, attributes: attributes)
            attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 14, type: .regular), range: (textString as NSString).range(of: "팔로워"))
            
            followingLeftLabel.attributedText = attributedText
            followingLeftLabel.textAlignment = .right
        }
        
        if let totalFollowing = item.totalFollowing {
            let string = totalFollowing.toKilo
            
            var textString = "팔로잉 \(string)"
            if !isMyProfile {
                let count = string.replacingOccurrences(of: "(", with: "")
                    .replacingOccurrences(of: ")", with: "")
                textString = "팔로잉 \(count)"
            }
            
            let attributes:[NSAttributedString.Key:Any] = [
                NSAttributedString.Key.font:getFont(size: 14, type: .bold),
                NSAttributedString.Key.foregroundColor:UIColor.black
            ]
            let attributedText = NSMutableAttributedString(string: textString, attributes: attributes)
            attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 14, type: .regular), range: (textString as NSString).range(of: "팔로잉"))
            
            followingRightLabel.attributedText = attributedText
            
            if isMyProfile {
                followingRightLabel.textAlignment = .left
            } else {
                followingRightLabel.textAlignment = .center
            }
        }
        
        if let url = item.avatar?.toURL {
            profileImageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
        }
        
        //프로필 수정 편집 가능 여부
        setEditView(false)
        if let userId = user.id {
            if UserManager.shared.getUserId() == userId {
                setEditView(true)
            }
        }
    }
    
    public func getProfileViewHeight() -> CGFloat {
        var height:CGFloat = 0
        
        profileContentView.layoutIfNeeded()
        profileImageView.layoutIfNeeded()
        
        height += AppManager.shared.getSafeAreaInsets().top
        height += 65 //topView Height
        height += profileImageView.frame.origin.y //top 마진
        height += profileImageView.frame.size.height / 2 //프로필 높이 절반
        height += profileContentView.frame.size.height
        height -= 30
        
        return height
    }
    
    public func getCotentHeight() -> CGFloat {
        var height:CGFloat = 0
        
        profileContentView.layoutIfNeeded()
        profileImageView.layoutIfNeeded()
        
        height += 65 //topView Height
        height += profileImageView.frame.origin.y //top 마진
        height += profileContentView.frame.size.height
        height += profileImageView.frame.size.height / 2
        height += 30 //bottom 마진(profileView의 ContentView bottom 마진)
        height += AppManager.shared.getSafeAreaInsets().top
        
        return height
    }
    
    public func getBackgroundHeight() -> CGFloat {
        var height:CGFloat = 0
        
        profileContentView.layoutIfNeeded()
        profileImageView.layoutIfNeeded()
        
        height += 65 //topView Height
        height += profileImageView.frame.origin.y
        height += 30
        
        return height
    }
    
    private func setupUI() {
        //SNS 링크 활성화
        addGestureSNSLink()
        
        setupProfileEditButton()
        setupProfileStackView()
        setupFollowingAndFollowerView()
        
        addSubViews([
            profileContentView,
            profileStackView,
            profileImageView,
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(36)
            $0.centerX.equalToSuperview()
            $0.width.height.equalTo(83)
        }
        
        profileStackView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(133)
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        profileContentView.snp.makeConstraints{
            $0.top.equalTo(profileStackView.snp.top).offset(-51)
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
            $0.bottom.equalTo(profileStackView.snp.bottom).offset(30)
        }
    }
    
    private func setupProfileEditButton() {
        profileContentView.addSubViews([
            editImageView,
            editButton,
        ])
        
        setupProfileEditButtonConstraints()
    }
    
    private func setupProfileEditButtonConstraints() {
        editImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(12)
            $0.trailing.equalToSuperview().offset(-12)
            $0.width.height.equalTo(30)
        }
        
        editButton.snp.makeConstraints{
            $0.top.equalToSuperview().offset(12)
            $0.trailing.equalToSuperview().offset(-12)
            $0.width.height.equalTo(50)
        }
    }
    
    private func setupProfileStackView() {
        [
            profileNameLabel,
            profileIntroduceLabel,
            snsLinkLabel
        ].forEach{
            profileStackView.addArrangedSubview($0)
        }
    }
    
    private func setupFollowingAndFollowerView() {
        followingParentStackView.addArrangedSubview(followingStackView)
        profileStackView.addArrangedSubview(followingParentStackView)
        
        setupFollowingAndFollowerViewConstraints()
    }
    
    private func setupFollowingAndFollowerViewConstraints() {
        followingStackView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        
        followingLeftView.addSubViews([
            followingLeftLabel,
            followingLeftButton
        ])
        
        followingRightView.addSubViews([
            followingRightLabel,
            followingRightButton
        ])
        
        [
            followingLeftView,
            followingBorderView,
            followingRightView
        ].forEach {
            followingStackView.addArrangedSubview($0)
        }
        
        followingBorderView.snp.makeConstraints{
            $0.width.equalTo(1)
        }
        
        followingLeftView.snp.makeConstraints{
            $0.width.equalTo(followingRightView.snp.width)
        }
        
        followingLeftLabel.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        followingLeftButton.snp.makeConstraints{
            $0.top.left.trailing.bottom.equalToSuperview()
        }
        
        followingRightLabel.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        followingRightButton.snp.makeConstraints{
            $0.top.left.trailing.bottom.equalToSuperview()
        }
        
        //MARK: 타인 프로필 일 때만 설정
        if !isMyProfile {
            setupFollowView()
            
            [
                followButtonBorderView,
                followView
            ].forEach {
                followingStackView.addArrangedSubview($0)
            }
            
            followButtonBorderView.snp.makeConstraints{
                $0.width.equalTo(1)
            }
            
            followView.snp.makeConstraints{
                $0.width.equalTo(72)
            }
        }
    }
    
    private func setupFollowView() {
        followView.addSubViews([
            followLabel,
            followButton
        ])
        
        setupFollowViewConstraints()
    }
    
    private func setupFollowViewConstraints(){
        followLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        followButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setEditView(_ bool:Bool) {
        self.editButton.isHidden = !bool
        self.editImageView.isHidden = !bool
    }
    
    private func addGestureSNSLink() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onTouchSNSLink(sender:)))
        gesture.numberOfTapsRequired = 1
        snsLinkLabel.addGestureRecognizer(gesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ProfileView {
    //MARK: ProfileBlockVC 에서 profileView 에서 사용
    public func removeProfileViewForBlock(){
        editButton.removeFromSuperview()
        editImageView.removeFromSuperview()
        followingStackView.removeFromSuperview()
    }
}

extension ProfileView {
    //프로필 링크 활성화
    @objc
    func onTouchSNSLink(sender: UIButton) {
        openSafari(url: self.linkShare)
    }
}

//MARK: - Follow
extension ProfileView {
    public func updateButtonStatus(){
        if self.isFollowed {
            followLabel.text = "팔로잉"
            followLabel.textColor = .mainColor
            followView.backgroundColor = UIColor.white
            followView.layer.borderWidth = 1
            followView.layer.borderColor = UIColor.mainColor.cgColor
        } else {
            followLabel.text = "팔로우"
            followLabel.textColor = UIColor.white
            followView.backgroundColor = .mainColor
            followView.layer.borderWidth = 0
        }
    }
    
    func followReact(followingId:Int) {
        guard let followingId = user.id else {return}
        API.followReact(followingId: followingId) { [weak self] model, data in
            guard let strongSelf = self, let model = model else {
                if let data = data {
                    JSON.decode(data: data) { errorData in
                        guard let errorData = errorData else {
                            return
                        }
                        
                        if errorData.data.code == "7003" {
                            AlertManager.shared.show(text: "본인은 팔로우 할 수 없습니다.", type: .one)
                        }
                    }
                }
                return
            }
            
            if model.success {
                strongSelf.isFollowed = model.data.isFollowed
                strongSelf.refreshHandler?()
            } else {
                AlertManager.shared.show(text: "에러 발생", type: .one)
            }
        }
    }
}
