//
//  SelectDeleteView.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/03.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class SelectDeleteView: UIView {
    let disposeBag = DisposeBag()
    var selectHandler:(()->Void)? = nil
    var cancelHandler:(()->Void)? = nil
    
    lazy var selectButton = UIButton().then{
        $0.setTitle("선택삭제", for: .normal)
        $0.setTitleColor(.mainColor, for: .normal)
        $0.titleLabel?.font = getFont(size: 12)
        $0.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else { return }
            AlertManager.shared.show(text: "삭제 하시겠습니까?", type: .two){
                if strongSelf.selectHandler != nil {
                    strongSelf.selectHandler!()
                }
            }
        }.disposed(by: disposeBag)
    }
    
    lazy var cancelButton = UIButton().then{
        $0.setTitle("취소", for: .normal)
        $0.setTitleColor(.black, for: .normal)
        $0.titleLabel?.font = getFont(size: 12)
        $0.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.cancelHandler != nil {
                strongSelf.cancelHandler!()
            }
        }.disposed(by: disposeBag)
    }
    
    init(selectHandler:(()->Void)? = nil, cancelHandler:(()->Void)? = nil) {
        super.init(frame: CGRect.zero)
        self.selectHandler = selectHandler
        self.cancelHandler = cancelHandler
        
        setupUI()
    }
    
    private func setupUI() {
        addSubViews([
            selectButton,
            cancelButton
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        cancelButton.snp.makeConstraints{
            $0.top.trailing.bottom.equalToSuperview()
            $0.width.equalTo(30)
        }
        
        selectButton.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.trailing.equalTo(cancelButton.snp.leading).offset(-10)
            $0.width.equalTo(60)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
