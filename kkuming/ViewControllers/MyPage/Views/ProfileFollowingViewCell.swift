//
//  ProfileFollowingViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/12.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class ProfileFollowingViewCell: UITableViewCell {
    static let identifier = String(describing: ProfileFollowingViewCell.self)
    var isFollowing:Bool = false
    var item:User? = nil
    
    var disposeBag = DisposeBag()
    
    let thumbImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 77/2
    }
    
    let thumbButton = UIButton()
    
    let nameLabel = UILabel().then{
        $0.font = getFont(size: 14, type: .EBold)
        $0.textColor = .black
    }
    
    let nameButton = UIButton()
    
    let introduceLabel = UILabel().then{
        $0.numberOfLines = 2
        $0.font = getFont(size: 11)
        $0.textColor = RGB(red: 116, green: 116, blue: 116)
    }
    let addressLabel = UILabel().then{
        $0.font = getFont(size: 9)
        $0.textColor = RGB(red: 206, green: 206, blue: 206)
    }
    
    let followingButtonView = UIView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 13
        $0.layer.borderColor = RGB(red: 56, green: 196, blue: 207).cgColor
        $0.layer.borderWidth = 1
    }

    let followingLabel = UILabel().then{
        $0.font = getFont(size: 11, type: .bold)
        $0.text = "팔로잉"
    }
    
    lazy var followingButton = UIButton()
    
    var completion:(()->Void)? = nil
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.disposeBag = DisposeBag()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        setupProperties()
        setupUI()
    }
    
    public func configuration(item:User) {
        self.item = item
        
        self.isFollowing = item.isFollowed ?? false
        
        if let avatar = item.avatar {
            thumbImageView.kf.setImage(with: avatar.toURL, options: [.loadDiskFileSynchronously])
        }
        
        if let nickname = item.nickname {
            nameLabel.text = nickname
        }
        
        if let bio = item.bio {
            introduceLabel.text = bio
        }
        
        if let linkShare = item.linkShare {
            addressLabel.text = linkShare
        }
        
        updateFollowingStatus()
    }
    
    private func setupProperties() {
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = 15
        backgroundColor = .white
    }
    
    private func setupUI() {
        createFollowingButton()
        
        contentView.addSubViews([
            thumbImageView,
            thumbButton,
            nameLabel,
            nameButton,
            introduceLabel,
            addressLabel,
            followingButtonView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        thumbImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(20)
            $0.leading.equalToSuperview().offset(25)
            $0.width.height.equalTo(77)
            $0.bottom.equalToSuperview().offset(-20)
        }
        
        thumbButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalTo(thumbImageView)
        }
        
        nameLabel.snp.makeConstraints{
            $0.top.equalTo(thumbImageView)
            $0.leading.equalTo(thumbImageView.snp.trailing).offset(20)
            $0.trailing.equalTo(followingButtonView.snp.leading).offset(-5)
        }
        
        nameButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalTo(nameLabel)
        }
        
        followingButtonView.snp.makeConstraints{
            $0.top.equalTo(thumbImageView.snp.top)
            $0.trailing.equalToSuperview().offset(-25)
            $0.width.equalTo(72)
            $0.height.equalTo(25)
        }
        
        introduceLabel.snp.makeConstraints{
            $0.centerY.equalTo(thumbImageView).offset(5)
            $0.leading.equalTo(nameLabel.snp.leading)
            $0.trailing.equalToSuperview().offset(-10)
        }
        
        addressLabel.snp.makeConstraints{
            $0.leading.equalTo(nameLabel.snp.leading)
            $0.trailing.equalToSuperview().offset(-10)
            $0.bottom.equalTo(thumbImageView).offset(-5)
        }
    }
    
    private func createFollowingButton() {
        followingButtonView.addSubViews([
            followingLabel,
            followingButton
        ])
        
        followingLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        followingButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func updateFollowingStatus() {
        if isFollowing {
            followingLabel.text = "팔로잉"
            followingLabel.textColor = RGB(red: 56, green: 196, blue: 207)
            followingButtonView.backgroundColor = UIColor.white
            followingButtonView.layer.borderWidth = 1
            
        } else {
            followingLabel.text = "팔로우"
            followingButtonView.layer.borderWidth = 0
            
            if UserManager.shared.getUserId() == self.item?.id ?? 0 {
                //본인
                followingLabel.textColor = RGB(red: 51, green: 51, blue: 51)
                followingButtonView.backgroundColor = RGB(red: 237, green: 237, blue: 237)
            } else {
                followingLabel.textColor = UIColor.white
                followingButtonView.backgroundColor = RGB(red: 56, green: 196, blue: 207)
            }
        }
        
        if self.completion != nil {
            self.completion!()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
