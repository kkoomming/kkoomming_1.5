//
//  ProfileAndFollowListView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/09.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class ProfileAndFollowListView: UIView {
    var items:[User] = []
    
    //페이징
    var isPaging = false
    var moreHandler:(()->Void)? = nil
    
    let topMargin:CGFloat = 32
    let leftMargin:CGFloat = 25
    let rightMargin:CGFloat = 25
    lazy var width:CGFloat = 100
    lazy var height:CGFloat = 159
    let lineSpacing:CGFloat = 16
    
    lazy var layout = UICollectionViewFlowLayout().then{
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .vertical
    }
    
    lazy var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(ProfileAndFollowListViewCell.self, forCellWithReuseIdentifier: ProfileAndFollowListViewCell.identifier)
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = UIEdgeInsets(top: topMargin, left: leftMargin, bottom: 0, right: rightMargin)
        $0.isPagingEnabled = false
    }
    
    public func configuration(items:[User]) {
        self.items = items
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    private func setupUI() {
        addSubViews([
            collectionView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension ProfileAndFollowListView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileAndFollowListViewCell.identifier, for: indexPath) as! ProfileAndFollowListViewCell
        let index = indexPath.item
        let item = items[index]
        
        cell.item = item
        cell.profileImageView.kf.setImage(with: item.avatar?.toURL, options: [.loadDiskFileSynchronously])
        cell.profileLabel.text = item.nickname
        
        cell.configuration(isFollowing: item.isFollowed ?? false)
        
        cell.followButton.button.rx.tap.bind{ [weak self] in
            guard let strongSelf = self, let followingId = item.id else { return }
            print("followButton cell 클릭")
            strongSelf.followReact(followingId: followingId, index:index)
        }.disposed(by: cell.disposeBag)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        let item = items[index]
        let id = item.id ?? 0
        
        if UserManager.shared.getUserId() == id {
            //본인
            AppManager.shared.popToRoot()
            AppManager.shared.moveToTab(index: 4)
            return
        } else {
            //타인
            App.push(ProfileVC(userId: id))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.item == self.items.count - 1) {
            guard !isPaging else { return }
            self.isPaging = true
            
            self.moreHandler?()
        }
    }
}


class ProfileAndFollowListViewCell: UICollectionViewCell {
    static let identifier = String(describing: ProfileAndFollowListViewCell.self)
    
    var isFollowing:Bool = false
    var completion:(()->Void)? = nil
    var disposeBag = DisposeBag()
    var item:User? = nil {
        didSet {
            followButton.item = self.item
        }
    }
    
    let profileImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 100 / 2
        
        $0.contentMode = .scaleToFill
    }
    
    let profileLabel = UILabel().then{
        $0.font = getFont(size: 12, type: .bold)
        $0.textAlignment = .center
    }
    
    lazy var followButton = ButtonView(type: .follow).then{
        $0.setButtonTitle(onString: "팔로잉", offString: "팔로우")
    }
    
    public func configuration(isFollowing:Bool, completion:(()->Void)? = nil) {
        self.isFollowing = isFollowing
        self.completion = completion
        
        updateFollowingStatus()
    }
    
    private func updateFollowingStatus() {
        followButton.updateButtonStatus(isFollowing)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.disposeBag = DisposeBag()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        contentView.addSubViews([
            profileImageView,
            profileLabel,
            followButton
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileImageView.snp.makeConstraints{
            $0.height.equalTo(100)
            $0.leading.trailing.top.equalToSuperview()
        }
        
        profileLabel.snp.makeConstraints{
            $0.top.equalTo(profileImageView.snp.bottom).offset(11)
            $0.leading.trailing.equalToSuperview()
        }
        
        followButton.snp.makeConstraints{
            $0.top.equalTo(profileLabel.snp.bottom).offset(10)
            $0.centerX.equalToSuperview()
            $0.width.equalTo(72)
            $0.height.equalTo(25)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ProfileAndFollowListView {
    func followReact(followingId:Int, index:Int) {
        API.followReact(followingId: followingId) { [weak self] model, data in
            guard let strongSelf = self, let model = model else {
                if let data = data {
                    JSON.decode(data: data) { errorData in
                        guard let errorData = errorData else {
                            return
                        }
                        
                        if errorData.data.code == "7003" {
                            AlertManager.shared.show(text: "본인은 팔로우 할 수 없습니다.", type: .one)
                        }
                    }
                }
                return
            }

            if model.success {
                strongSelf.items[index].isFollowed = model.data.isFollowed
                DispatchQueue.main.async {
                    strongSelf.collectionView.reloadData()
                }
            } else {
                AlertManager.shared.show(text: "에러 발생", type: .one)
            }
        }
    }
}
