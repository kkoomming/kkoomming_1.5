//
//  ContentSelectView.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/07.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Promises

enum ContentSelectViewType {
    case SCRIPT
}

class ContentSelectView: UIView {
    var type:VariousListType!
    
    var items:[Post] = []
    var selectedItems:[Post] = []
    var meta:MetaData!
    var pageType:MyFeedType!
    
    //MARK: 페이징
    var page = 1
    var isPaging = false
    
    var scriptRefreshHandler:(()->Void)? = nil
    var likeRefreshHandler:(()->Void)? = nil
    
    let leftMargin:CGFloat = 25
    let rightMargin:CGFloat = 25
    lazy var width:CGFloat = 0
    lazy var height:CGFloat = 0
    let lineSpacing:CGFloat = 10
    var isUseDeleteMode:Bool = false {
        didSet {
            collectionView.reloadData()
        }
    }
    
    lazy var layout = UICollectionViewFlowLayout().then{
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .vertical
    }
    
    lazy var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(VariousListViewCell.self, forCellWithReuseIdentifier: VariousListViewCell.identifier)
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
        $0.isScrollEnabled = false
        $0.isPagingEnabled = false
    }
    
    init(type:VariousListType) {
        super.init(frame: CGRect.zero)
        self.type = type
        
        calculateWidthAndHeight()
        setupUI()
    }
    
    init(pageType:MyFeedType) {
        super.init(frame: CGRect.zero)
        self.pageType = pageType
        
        switch pageType{
        case .post: //내 글
            //TODO: 설정 필요
            break
        case .like: //좋아요
            self.type = .three
            break
        case .scrap: //스크랩
            self.type = .three
            break
        }
        
        calculateWidthAndHeight()
        setupUI()
    }
    
    public func add(items:[Post]) {
        self.items += items
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    public func configuration(items:[Post], meta:MetaData) {
        self.items = items
        self.meta = meta
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    public func deleteItemsAll() {
        var postIdList:[Int] = []
        
        for item in items {
            if let _ = self.selectedItems.firstIndex(where: { $0.id == item.id }) {
                postIdList.append(item.id)
            }
        }
        
        switch pageType {
        case .post:
            break
        case .like:
            unLikeSelect(postIdList: postIdList)
            break
        case .scrap:
            unScrapSelect(postIdList: postIdList)
            break
        default:
            print("deleteItemsAll default")
        }
    }
    
    public func deleteSelectedItems() {
        self.selectedItems.removeAll()
    }
    
    public func reload() {
        collectionView.reloadData()
    }
    
    public func getTwoLineHeight() -> CGFloat {
        return height * 2 + lineSpacing
    }
    
    private func calculateWidthAndHeight() {
        switch type{
        case .two:
            self.width = (UIScreen.main.bounds.width - leftMargin - rightMargin - 10) / 2
        case .three:
            self.width = (UIScreen.main.bounds.width - leftMargin - rightMargin - 20) / 3
        case .none:
            self.width = 0
        }
        self.height = width
    }
    
    private func setupUI() {
        addSubview(collectionView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ContentSelectView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isUseDeleteMode {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VariousListViewCell.identifier, for: indexPath) as! VariousListViewCell
            let item = self.items[indexPath.item]
            
            cell.configuration(item: item)
            cell.isVisibleCheckBox(true)
            
            if let _ = selectedItems.firstIndex(where: {$0.id == item.id}) {
                cell.isSelected(true)
            } else {
                cell.isSelected(false)
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VariousListViewCell.identifier, for: indexPath) as! VariousListViewCell
            let item = items[indexPath.item]
            
            cell.configuration(item: item)
            cell.isVisibleCheckBox(false)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("VariousListView index : ", indexPath.item)
        
        if isUseDeleteMode {
            let item = self.items[indexPath.item]
            
            if let _ = selectedItems.firstIndex(where: {$0.id == item.id}) {
                selectedItems.removeAll(where: { $0.id == item.id })
            } else {
                selectedItems.append(item)
            }
            
            collectionView.reloadData()
        } else {
            let item = self.items[indexPath.item]
            let vc = ContentDetailVC()
            let postData = Post(id: item.id)
            vc.configuration(postData: postData, postType: .NONE)
            vc.getDetailPost() {
                App.push(vc)
            }
        }
    }
}

//MARK: - API
extension ContentSelectView {
    private func unLikeSelect(postIdList:[Int]) {
        API.unLikeSelect(postIdList: postIdList) { model in
            guard let model = model else {
                return
            }
            
            if model.success {
                AlertManager.shared.show(text: "삭제되었습니다.", type: .one) {
                    //[Post]의 개수 삭제
                    Promise<Void>(on: .main){
                        for item in self.items {
                            if let _ = self.selectedItems.firstIndex(where: { $0.id == item.id }) {
                                self.items.removeAll(where: { $0.id == item.id })
                            }
                        }
                    }.then{
                        self.deleteSelectedItems()
                    }
                    
                    //MetaData 개수 삭제
                    self.meta.totalRecords -= postIdList.count
                    
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                        self.likeRefreshHandler?()
                    }
                }
            } else {
                AlertManager.shared.show(text: "삭제 실패", type: .one)
            }
        }
    }
    
    private func unScrapSelect(postIdList:[Int]) {
        API.unScrapSelect(postIdList: postIdList) { model in
            guard let model = model else {
                return
            }
            
            if model.success {
                AlertManager.shared.show(text: "삭제되었습니다.", type: .one) {
                    //[Post]의 개수 삭제
                    Promise<Void>(on: .main){
                        for item in self.items {
                            if let _ = self.selectedItems.firstIndex(where: { $0.id == item.id }) {
                                self.items.removeAll(where: { $0.id == item.id })
                            }
                        }
                    }.then{
                        self.deleteSelectedItems()
                    }
                    
                    //MetaData 개수 삭제
                    self.meta.totalRecords -= postIdList.count
                    
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                        self.scriptRefreshHandler?()
                    }
                }
            } else {
                AlertManager.shared.show(text: "삭제 실패", type: .one)
            }
        }
    }
}
