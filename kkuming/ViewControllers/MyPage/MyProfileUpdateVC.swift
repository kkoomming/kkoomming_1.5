//
//  MyProfileUpdateVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/08.
//

import UIKit
import Then
import SnapKit
import Kingfisher
import Alamofire
import RxSwift
import RxCocoa

class MyProfileUpdateVC: BaseVC {
    lazy var topView = TopView(type: .profileUpdate) { [weak self] in
        guard let strongSelf = self else {return}
        strongSelf.updateMyProfile()
    }
    let disposeBag = DisposeBag()
    
    var user:User? = nil
    var originalSNSViewHeight:CGFloat = 40

    //업로드 시 이거가 있으면 프로필 변경으로 취급, 없으면 변경 안함
    var profileImage:UIImage? = nil
    
    let scrollView = UIScrollView()
    let stackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.layoutMargins = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    let profileAreaView = UIView()
    
    let profileImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 75
        $0.image = UIImage(named: "coinExample")
    }
    let cameraButton = UIButton().then{
        $0.setImage(UIImage(named: "ic_camera"), for: .normal)
    }
    
    lazy var nickNameFieldView = TextFieldView(cornerRadius: 10).then{
        $0.textField.font = getFont(size: 16, type: .EBold)
        $0.textField.text = "초보다꾸러람찌"
        $0.textField.returnKeyType = .next
        $0.textField.delegate = self
        $0.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    let nickNameHintLabel = UILabel().then{
        $0.font = getFont(size: 12)
        $0.textColor = RGB(red: 114, green: 114, blue: 114)
        $0.textAlignment = .center
        $0.text = "닉네임을 입력해주세요 (7/12)"
    }
    
    lazy var introduceTextView = UITextView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 10
        $0.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        $0.backgroundColor = RGB(red: 247, green: 247, blue: 247)
        
        $0.text = "소개글"
        $0.font = getFont(size: 12)
        $0.delegate = self
    }
    let introduceHintLabel = UILabel().then{
        $0.font = getFont(size: 12)
        $0.textColor = RGB(red: 114, green: 114, blue: 114)
        $0.textAlignment = .center
        $0.text = "소개글을 입력해주세요 (26/100)"
    }
    
    lazy var snsAddressTextView = UITextView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 10
        $0.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        $0.backgroundColor = RGB(red: 247, green: 247, blue: 247)
        
        $0.font = getFont(size: 12)
        $0.textContainer.lineBreakMode = .byCharWrapping
        $0.returnKeyType = .done
        $0.delegate = self
    }
    
    let snsAddressHintLabel = UILabel().then{
        $0.font = getFont(size: 12)
        $0.textColor = RGB(red: 114, green: 114, blue: 114)
        $0.textAlignment = .center
        $0.text = "SNS 주소를 입력해주세요"
    }
    
    deinit{
        print("MyProfileUpdateVC deinit")
        self.registerNotification(false)
        GalleryManager.shared.clear()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = UserManager.shared.getUserData().user {
            self.user = user
        }
        
        self.registerNotification(true)
        
        setupUI()
        setupRx()
        configuration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        KkumingKeyboardManager.shared.isUseKeyboardLibrary(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        KkumingKeyboardManager.shared.isUseKeyboardLibrary(false)
    }
    
    private func setupUI() {
        setupStackViewToScrollView()
        setupStackView()
        
        addSubViews([
            topView,
            scrollView
        ])
        
        setupConstraints()
    }
    
    private func setupStackViewToScrollView() {
        scrollView.addSubview(stackView)
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalToSuperview()
        }
    }
    
    private func setupStackView() {
        setupProfileAreaView()
        
        [
            profileAreaView,
            nickNameFieldView,
            nickNameHintLabel,
            introduceTextView,
            introduceHintLabel,
            snsAddressTextView,
            snsAddressHintLabel
        ].forEach {
            stackView.addArrangedSubview($0)
        }
        
        setupStackViewConstraints()
    }
    
    private func setupStackViewConstraints() {
        profileAreaView.snp.makeConstraints{
            $0.height.equalTo(150)
        }
        
        stackView.setCustomSpacing(30, after: profileAreaView)
        
        nickNameFieldView.snp.makeConstraints{
            $0.height.equalTo(40)
        }
        
        stackView.setCustomSpacing(10, after: nickNameFieldView)
        stackView.setCustomSpacing(30, after: nickNameHintLabel)
        
        introduceTextView.snp.makeConstraints{
            $0.height.equalTo(126)
        }
        
        stackView.setCustomSpacing(10, after: introduceTextView)
        stackView.setCustomSpacing(30, after: introduceHintLabel)
        
        snsAddressTextView.snp.makeConstraints{
            $0.height.equalTo(originalSNSViewHeight)
        }
        
        stackView.setCustomSpacing(10, after: snsAddressTextView)
    }
    
    private func setupProfileAreaView() {
        profileAreaView.addSubViews([
            profileImageView,
            cameraButton
        ])
        
        profileImageView.snp.makeConstraints{
            $0.width.height.equalTo(150)
            $0.centerY.centerX.equalToSuperview()
        }
        
        cameraButton.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.trailing.equalTo(profileImageView.snp.trailing)
            $0.bottom.equalTo(profileImageView.snp.bottom).offset(-10)
        }
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        scrollView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupRx() {
        cameraButton.rx.tap.bind{ [weak self] in
            print("cameraButton 클릭")
            let selectAlbumView = AlertButtonView(title: "앨범에서 선택", buttonType: .mainColor){
                print("앨범에서 선택")
                GalleryManager.shared.requestAlbum()
            }
            
            let selectCameraView = AlertButtonView(title: "카메라 촬영", buttonType: .mainColor){ [weak self] in
                print("카메라 촬영")
                guard let strongSelf = self else {
                    return
                }
                
                if GalleryManager.shared.isRequestCamera() {
                    GalleryManager.shared.startCamera(strongSelf){ [weak self] in
                        guard let strongSelf = self else {
                            return
                        }
                        
                        let vc = GallerySelectedVC()
                        vc.isProfileUpdateVC = true //프로필 수정 뷰
                        
                        let images = strongSelf.createHighQualityImages(images: GalleryManager.shared.getSelectedImages())
                        
                        vc.profileUpdateHandler = {[weak self] data in
                            guard let strongSelf = self else {
                                return
                            }
                            print("profileUpdateHandler")
                            
                            strongSelf.profileImage = data?.image
                            strongSelf.profileImageView.image = data?.image
                        }
                        
                        vc.images = images
                        
                        GalleryManager.shared.clear()
                        
                        App.push(vc)
                    }
                }
            }
            
            AlertManager.shared.showAlertView(buttonList: [
                selectAlbumView,
                selectCameraView
            ])
        }.disposed(by: disposeBag)
    }
    
    private func registerNotification(_ bool:Bool){
        if bool {
            NotificationCenter.default.addObserver(self, selector: #selector(self.moveToAlbum), name: .requestAlbum, object: nil)
        } else {
            NotificationCenter.default.removeObserver(self, name: .requestAlbum, object: nil)
        }
    }
    
    private func configuration() {
        guard let user = user else { return }
        
        //이미지
        profileImageView.kf.setImage(with: user.avatar?.toURL, options: [.loadDiskFileSynchronously])
        
        //닉네임
        nickNameFieldView.textField.text = user.nickname
        
        //소개글
        introduceTextView.text = user.bio
        
        //인스타 주소
        snsAddressTextView.text = user.linkShare
    }
    
    //앨범 선택으로 이동
    @objc
    private func moveToAlbum() {
        DispatchQueue.main.async {
            let pickerVC = GalleryPickerVC(isMulti: false)
            pickerVC.profileUpdateHandler = {[weak self] data in
                guard let strongSelf = self else {
                    return
                }
                print("profileUpdateHandler")
                
                strongSelf.profileImage = data?.image
                strongSelf.profileImageView.image = data?.image
            }
            App.push(pickerVC)
        }
    }

    private func createHighQualityImages(images:[GalleryData?]) -> [GalleryData?] {
        var temp:[GalleryData?] = []
        for item in images {
            if let data = item {
                LocalImageManager.shared.requestImage(with: data.asset) { result in
                    temp.append(result)
                }
            }
        }
        return temp
    }
}

extension MyProfileUpdateVC: UITextFieldDelegate, UITextViewDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if nickNameFieldView.textField == textField {
            introduceTextView.becomeFirstResponder()
        }
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == nickNameFieldView.textField {
            if let text = textField.text {
                let count = text.count
                if count > 12 {
                    let startIndex = text.index(text.startIndex, offsetBy: 0)
                    let endIndex = text.index(text.startIndex, offsetBy: 12)
                    let string = String(text[startIndex..<endIndex])

                    nickNameFieldView.textField.text = string
                    nickNameHintLabel.text = "닉네임을 입력해주세요 (12/12)"
                } else {
                    nickNameHintLabel.text = "닉네임을 입력해주세요 (\(count)/12)"
                }
            } else {
                nickNameHintLabel.text = "닉네임을 입력해주세요 (0/12)"
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == nickNameFieldView.textField {
            //스페이스
            if string == " " {
                return false
            }
            
            //지우기
            if string == "" {
                return true
            }
            
            return string.isValidation()
        }
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == introduceTextView {
            //4줄 검사
            let fourView = UITextView()
            fourView.frame.size = CGSize(width: textView.textContainer.size.width, height: 400)
            fourView.font = getFont(size: 12)
            fourView.text = """
            1
            2
            3
            4
            """
            fourView.sizeToFit()
            
            //텍스트가 입력이 완료 됐을 때
            let willBeView = UITextView()
            willBeView.frame.size = CGSize(width: textView.textContainer.size.width, height: 400)
            willBeView.font = getFont(size: 12)
            willBeView.text = textView.text + text
            willBeView.sizeToFit()
            
            let maximumHeight:CGFloat = fourView.frame.height
            let currentHeight:CGFloat = willBeView.frame.height
            
            if currentHeight > maximumHeight {
                return false
            }
        } else if textView == snsAddressTextView {
            if text == "\n" {
                textView.resignFirstResponder()
            }
            
            //1줄 (테스트 필요 : textView.textContainer.size)
            let oneView = UITextView()
            oneView.frame.size = CGSize(width: textView.textContainer.size.width, height: 400)
            oneView.font = getFont(size: 12)
            oneView.text = """
            1
            """
            oneView.sizeToFit()
            
            //2줄 검사용
            let twoView = UITextView()
            twoView.frame.size = CGSize(width: textView.textContainer.size.width, height: 400)
            twoView.font = getFont(size: 12)
            twoView.text = """
            1
            2
            """
            twoView.sizeToFit()
            
            //텍스트가 입력이 완료 됐을 때
            let willBeView = UITextView()
            willBeView.frame.size = CGSize(width: textView.textContainer.size.width, height: 400)
            willBeView.font = getFont(size: 12)
            willBeView.text = textView.text + text
            willBeView.sizeToFit()
            
            let minimumHeight:CGFloat = oneView.frame.height
            let maximumHeight:CGFloat = twoView.frame.height
            let currentHeight:CGFloat = willBeView.frame.height
            let otherHeight:CGFloat = originalSNSViewHeight - minimumHeight

            if currentHeight > maximumHeight {
                return false
            }
            
            snsAddressTextView.snp.updateConstraints{
                $0.height.equalTo(otherHeight + currentHeight)
            }
        }
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == introduceTextView {
            if let text = textView.text {
                let count = text.count
                
                //100줄 검사
                if count > 100 {
                    let startIndex = text.index(text.startIndex, offsetBy: 0)
                    let endIndex = text.index(text.startIndex, offsetBy: 100)
                    let string = String(text[startIndex..<endIndex])

                    introduceTextView.text = string
                    introduceHintLabel.text = "소개글을 입력해주세요 (100/100)"
                } else {
                    introduceHintLabel.text = "소개글을 입력해주세요 (\(count)/100)"
                }
                
            } else {
                introduceHintLabel.text = "소개글을 입력해주세요 (0/100)"
            }
        }
    }
}

//MARK: - API
extension MyProfileUpdateVC {
    func updateMyProfile() {
        var files:[Data] = []
        var param:Parameters = [:]
        
        //등록할 사진 (등록시에만 작성)
        if profileImage != nil {
            if let data = profileImage?.jpegData(compressionQuality: 1.0) {
                files.append(data)
            }
        }
        
        //닉네임
        if let nickname = nickNameFieldView.textField.text {
            param.updateValue(nickname, forKey: "nickname")
        }
        
        //소개글
        if let bio = introduceTextView.text {
            param.updateValue(bio, forKey: "bio")
        }
        
        //SNS 주소
        if let linkShare = snsAddressTextView.text, linkShare != "" {
            if linkShare.contains("http://") || linkShare.contains("https://") {
                param.updateValue(linkShare, forKey: "linkShare")
            } else {
                AlertManager.shared.show(text: "링크 형태가 아닙니다.", type: .one)
                return
            }
        } else {
            param.updateValue("", forKey: "linkShare")
        }
        
        API.updateMyProfile(param: param, files: files) { model, data in
            guard let model = model else {
                if let data = data {
                    JSON.decode(data: data) { errorData in
                        guard let errorData = errorData else {
                            return
                        }
                        
                        if errorData.data.code == Errors.e1019.rawValue {
                            AlertManager.shared.show(text: "이미 사용중인 닉네임입니다.", type: .one)
                        }
                    }
                }
                return
            }
            
            print("model : ", model)
            
            if model.success {
                print("업데이트 완료")
                App.pop()
            } else {
                if let data = data {
                    JSON.decode(data: data) { errorData in
                        guard let errorData = errorData else {
                            return
                        }
                        
                        if errorData.data.code == Errors.e1019.rawValue {
                            AlertManager.shared.show(text: "이미 사용중인 닉네임입니다.", type: .one)
                        }
                    }
                }
            }
            
            //MARK: 업데이트 완료되면 UserManager.shared에서 컨트롤 하는 것이 있다면 UserManager에도 저장시켜줘야함
        }
    }
}
