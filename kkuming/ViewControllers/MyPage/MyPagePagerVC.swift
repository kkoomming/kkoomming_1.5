//
//  MyPagePagerVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/03.
//

import UIKit
import Then
import SnapKit
import XLPagerTabStrip

protocol MyPageSelectorDelegate {
    func selectTab(index:Int)
}

class MyPagePagerVC: PagerVC {
    var scriptRefreshHandler:(()->Void)? = nil
    var likeRefreshHandler:(()->Void)? = nil
    
    var selectorDelegate:MyPageSelectorDelegate? = nil
    
    lazy var myWriteVC = MyWriteVC().then{
        $0.titleRefreshHandler = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.buttonBarView.reloadData()
        }
        
        $0.updateHeightHandler = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.updateUI(index: 0)
        }
    }
    
    lazy var myLikeVC = MyLikeVC().then{
        $0.titleRefreshHandler = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.buttonBarView.reloadData()
        }
    }
    
    lazy var myScriptVC = MyScriptVC().then{
        $0.titleRefreshHandler = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.buttonBarView.reloadData()
        }
    }
    
    var pagerViewControllers:[UIViewController] = []
    
    let borderView = BorderView()
    
    override func viewDidLoad() {
        configuration()
        
        super.viewDidLoad()
        
        self.delegate = self
        self.containerView.isPagingEnabled = false
        self.containerView.isScrollEnabled = false
        
        self.view.addSubview(borderView)
        borderView.snp.makeConstraints{
            $0.bottom.equalTo(buttonBarView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(1)
        }
    }

    private func isZeroCollectionView(index:Int) -> Bool {
        var height:CGFloat = 0
        switch index {
        case 0:
            height = myWriteVC.threeListView.collectionView.contentSize.height
            break
        case 1:
            height = myLikeVC.threeListView.collectionView.contentSize.height
            break
        case 2:
            height = myScriptVC.threeListView.collectionView.contentSize.height
            break
        default:
            break
        }
        
        if height == 0 {
            return true
        } else {
            return false
        }
    }
    
    public func getHeight(index:Int) -> CGFloat {
        self.buttonBarView.layoutIfNeeded()
        self.borderView.layoutIfNeeded()
        let height = self.buttonBarView.frame.height + self.borderView.frame.height + 10
        let deleteAreaHeight:CGFloat = 39.0
        if index == 0 {
            myWriteVC.threeListView.layoutIfNeeded()
            myWriteVC.threeListView.collectionView.layoutIfNeeded()
            
            if myWriteVC.threeListView.items.count <= 1 {
                return 400
            } else {
                let viewHeight = myWriteVC.threeListView.collectionView.contentSize.height + height
                print("myWriteVC viewHeight : ", viewHeight)
                
                return viewHeight
            }
    
        } else if index == 1 {
            myLikeVC.threeListView.layoutIfNeeded()
            myLikeVC.threeListView.collectionView.layoutIfNeeded()
            
            if myLikeVC.threeListView.items.count <= 1 {
                return 400
            } else {
                let viewHeight = myLikeVC.threeListView.collectionView.contentSize.height + height + deleteAreaHeight
                print("myLikeVC viewHeight : ", viewHeight)
                
                return viewHeight
            }
        } else if index == 2 {
            myScriptVC.threeListView.layoutIfNeeded()
            myScriptVC.threeListView.collectionView.layoutIfNeeded()
            
            if myScriptVC.threeListView.items.count <= 1 {
                return 400
            } else {
                let viewHeight = myScriptVC.threeListView.collectionView.contentSize.height + height + deleteAreaHeight
                print("myScriptVC viewHeight : ", viewHeight)
                
                return viewHeight
            }
        }
        
        return 0
    }
    
    private func configuration() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = selectedColor
        settings.style.buttonBarItemFont = getFont(size: 15, type: .bold)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = unSelectedColor
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        myLikeVC.likeRefreshHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.likeRefreshHandler?()
            strongSelf.updateUI(index: 1)
        }
        
        myScriptVC.scriptRefreshHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.scriptRefreshHandler?()
            strongSelf.updateUI(index: 2)
        }

        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            guard let strongSelf = self else { return }
            
            strongSelf.updateTitleLabel(oldCell: oldCell, newCell: newCell)
        }
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        self.pagerViewControllers = [myWriteVC, myLikeVC, myScriptVC]
        return self.pagerViewControllers
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        
        if progressPercentage >= 0.1 {
            updateUI(index: toIndex)
        }
        
    }
    
    public func updateUI(index:Int) {
        selectorDelegate?.selectTab(index: index)
        
        let pagerHeight = self.getHeight(index: index)
        if isZeroCollectionView(index: index) {
            self.view.snp.updateConstraints{
                $0.height.equalTo(270)
            }
        } else {
            self.view.snp.updateConstraints{
                $0.height.equalTo(pagerHeight)
            }
        }
    }
}
