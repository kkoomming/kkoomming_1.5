//
//  MyScriptVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/03.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip

class MyScriptVC: BaseVC, IndicatorInfoProvider {
    var itemInfo = IndicatorInfo(title: "스크랩")
    
    //MARK: 페이징
    var page = 1
    var isPaging = false
    
    //viewWillAppear와 Disappear에서 사용
    var isVisibleEmpty:Bool = false
    
    let disposeBag = DisposeBag()
    
    var titleRefreshHandler:(()->Void)? = nil
    var scriptRefreshHandler:(()->Void)? = nil
    
    var isSelectedDeleteView:Bool = false {
        didSet {
            isSelectDeleteViewHidden(isSelectedDeleteView)
        }
    }
    
    lazy var deleteButton = UIButton().then{
        $0.setImage(UIImage(named: "ic_delete"), for: .normal)
        $0.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.isSelectedDeleteView = !strongSelf.isSelectedDeleteView
            print("deleteButton")
        }.disposed(by: disposeBag)
    }
    
    lazy var selectDeleteView = SelectDeleteView(selectHandler: {
        print("선택삭제")
        self.isSelectedDeleteView = true
        self.threeListView.deleteItemsAll()
    }, cancelHandler: {
        print("취소")
        self.isSelectedDeleteView = true
        self.threeListView.deleteSelectedItems()
    }).then{
        $0.isHidden = true
    }
    
    lazy var threeListView = ContentSelectView(pageType: .scrap).then{
        $0.collectionView.isScrollEnabled = false
        $0.collectionView.isPagingEnabled = false
        $0.collectionView.contentInset = UIEdgeInsets(top: 10, left: $0.leftMargin, bottom: 10, right: $0.rightMargin)
        $0.scriptRefreshHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.refreshUI()
        }
    }
    
    let emptyView = ListEmptyView().then{
        $0.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.emptyView.isHidden = self.isVisibleEmpty
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.emptyView.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func refreshUI() {
        self.scriptRefreshHandler?()
        let isEmpty = self.threeListView.items.count == 0
        self.isEmpty(isEmpty)
        
        let titleString = "스크랩 " + self.threeListView.meta.totalRecords.toKilo
        self.itemInfo = IndicatorInfo(title: titleString)
        self.titleRefreshHandler?()
    }
    
    private func setupUI() {
        addSubViews([
            deleteButton,
            selectDeleteView,
            threeListView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        deleteButton.snp.makeConstraints{
            $0.top.equalToSuperview().offset(9)
            $0.trailing.equalToSuperview().offset(-15)
            $0.width.height.equalTo(30)
        }
        
        selectDeleteView.snp.makeConstraints{
            $0.height.equalTo(20)
            $0.centerY.equalTo(deleteButton.snp.centerY)
            $0.leading.equalToSuperview().offset(10)
            $0.trailing.equalTo(deleteButton.snp.trailing)
        }
        
        threeListView.snp.makeConstraints{
            $0.top.equalTo(deleteButton.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func isSelectDeleteViewHidden(_ bool:Bool) {
        self.deleteButton.isHidden = !bool
        self.selectDeleteView.isHidden = bool
        
        if !bool {
            self.threeListView.isUseDeleteMode = true
        } else {
            self.threeListView.isUseDeleteMode = false
        }
        
        threeListView.reload()
    }
}

extension MyScriptVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    func isEmpty(_ bool:Bool) {
        self.isVisibleEmpty = !bool
        emptyView.isHidden = !bool
        threeListView.isHidden = bool
        deleteButton.isHidden = bool
    }
}

//MARK: - API
extension MyScriptVC {
    
    //스크랩
    func getMyFeed(isFirst:Bool, completion:((MetaData)->Void)? = nil) {
        if isFirst {
            page = 1
            self.isPaging = false
        }
        
        self.isPaging = true
        API.getMyFeed(type: .scrap, offset: page) { model in
            guard let model = model,
                  let posts = model.data?.posts,
                  let meta = model.data?.metaData
            else {
                return
            }
            
            if isFirst {
                self.threeListView.configuration(items: posts, meta: meta)
                completion?(meta)
            } else {
                self.threeListView.add(items: posts)
            }
            
            DispatchQueue.main.async {
                self.threeListView.scriptRefreshHandler?()
            }
            
            let count = meta.totalPages
            if count == 0 {
                self.isEmpty(true)
            } else {
                self.isEmpty(false)
            }
            
            let titleString = "스크랩 " + meta.totalRecords.toKilo
            self.itemInfo = IndicatorInfo(title: titleString)
            self.titleRefreshHandler?()
            
            if self.page >= meta.totalPages {
                self.isPaging = true
            } else {
                self.isPaging = false
            }
            
            self.page += 1
        }
    }
}
