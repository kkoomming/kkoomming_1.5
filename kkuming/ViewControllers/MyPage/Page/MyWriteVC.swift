//
//  MyWriteVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/03.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip

class MyWriteVC: BaseVC, IndicatorInfoProvider {
    var itemInfo = IndicatorInfo(title: "내 글")
    var items:[Post] = []
    
    var titleRefreshHandler:(()->Void)? = nil
    var updateHeightHandler:(()->Void)? = nil
    
    //MARK: 페이징
    var page:Int = 1
    
    lazy var threeListView = VariousListView(type: .three).then{
        $0.collectionView.isScrollEnabled = false
        $0.collectionView.isPagingEnabled = false
        $0.collectionView.contentInset = UIEdgeInsets(top: 10, left: $0.leftMargin, bottom: 10, right: $0.rightMargin)
        $0.updateHeightHandler = self.updateHeightHandler
    }
    
    let emptyView = ListEmptyView().then{
        $0.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func updateUI() {
        //내 글, 좋아요, 스크랩 영역의 컨텐츠 높이 변경
        self.updateHeightHandler?()
    }
    
    public func configuration(items:[Post]) {
        self.items = items
        if self.items.count == 0 {
            //빈 화면 처리
            isEmpty(true)
        } else {
            //빈 화면 숨김 처리
            isEmpty(false)
            threeListView.configuration(items: self.items)
            self.threeListView.moreHandler = { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                
                strongSelf.getMyFeed(isFirst: false)
            }
        }
    }
    
    public func add(items:[Post]){
        self.items += items
        self.threeListView.add(items: items)
    }
    
    private func setupUI() {
        addSubViews([
            threeListView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        threeListView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension MyWriteVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        threeListView.isHidden = bool
    }
}

//MARK: - API
extension MyWriteVC {
    func getMyFeed(isFirst:Bool) {
        self.threeListView.isPaging = true
        
        if isFirst {
            page = 1
            self.threeListView.isPaging = false
        }
        
        API.getMyFeed(type: .post, offset: page) { model in
            guard let model = model,
                  let posts = model.data?.posts,
                  let meta = model.data?.metaData else {
                return
            }
            
            if model.success {
                if isFirst {
                    self.configuration(items: posts)
                } else {
                    self.add(items: posts)
                }
                
                if self.page >= meta.totalPages {
                    self.threeListView.isPaging = true
                } else {
                    self.threeListView.isPaging = false
                }
                
                self.page += 1
                
                let titleString = "내 글 " + meta.totalRecords.toKilo
                self.itemInfo = IndicatorInfo(title: titleString)
                self.titleRefreshHandler?()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    self.updateHeightHandler?()
                }
            } else {
                AlertManager.shared.show(text: "잠시 후 다시 시도해 주세요.", type: .one)
            }
        }
    }
}
