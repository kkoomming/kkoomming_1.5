//
//  MyProfileCommonVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/12.
//

import UIKit
import Then
import SnapKit

class MyProfileCommonVC: BaseVC {
    var isFollwer: Bool = false
    var isMyProfile: Bool = false
    var userId: Int = 0
    
    lazy var topView = TopView(type: .back, title: "프로필")
    
    var myprofileVC: MyProfilePagerVC!
    
    init(isFollwer:Bool, isMyProfile:Bool, userId:Int){
        super.init(nibName: nil, bundle: nil)
        
        self.isFollwer = isFollwer
        self.isMyProfile = isMyProfile
        self.userId = userId
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myprofileVC = MyProfilePagerVC(isFollwer: isFollwer)
        
        setupUI()
        setupFollowerAndFollowing()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AlertManager.shared.updateStatus(view: topView)
    }
    
    private func setupUI() {
        
        addSubViews([
            topView,
            myprofileVC.view
        ])
        
        addChild(myprofileVC)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        myprofileVC.view.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupFollowerAndFollowing() {
        //마이꾸밍의 프로필인지 타인의 프로필인지
        if isMyProfile {
            myprofileVC.followerVC.getMyFollowing(isFirst: true)
            myprofileVC.followingVC.getMyFollowing()
        } else {
            myprofileVC.followerVC.getSomeoneFollower(userId:userId, isFirst: true)
            myprofileVC.followingVC.getSomeoneFollowing(userId:userId)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
