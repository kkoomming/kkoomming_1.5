//
//  MainVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/21.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class MainVC: BaseVC {
    let disposeBag = DisposeBag()
    var data:MainListData?
    
    var bannerTimer:Timer!
    
    let topView = TopView(type: .main)
    
    lazy var scrollView = UIScrollView().then{
        $0.refreshControl = refreshControl
    }
    
    lazy var refreshControl = UIRefreshControl().then{
        $0.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
    }
    
    let stackView = UIStackView().then{
        $0.alignment = .fill
        $0.axis = .vertical
        $0.distribution = .equalSpacing
        $0.spacing = 10
    }
    
    lazy var topBigBannerView = BigBannerView().then{
        $0.delegate = self
    }
    
    lazy var bannerPageControlAreaView = UIView().then{
        $0.addSubview(bannerPageControlView)
    }
    lazy var bannerPageControlView = BannerPageControlView().then{
        $0.type = .EVENT
    }
    
    lazy var bannerPageLineAreaView = UIView().then{
        $0.addSubview(bannerPageLineView)
    }
    lazy var bannerPageLineView = BannerPageLineView()
    
    let todayDecorateLabel = PaddingLabel(left: 25).then{
        $0.textColor = .black
        $0.font = getFont(size: 18)
        let textString = "오늘의 꾸미기"
        let attributedText = NSMutableAttributedString(string: textString)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 18, type: .EBold), range: (textString as NSString).range(of: "꾸미기"))
        $0.attributedText = attributedText
    }
    
    let todayDecorateListView = VariousListView(type: .two)
    
    lazy var todayDecorateButtonAreaView = UIView().then{
        $0.addSubview(todayDecorateButtonView)
    }
    
    let todayDecorateButtonView = ButtonView(type:.gray){
        print("todayDecorateButtonView")
        App.push(tabType: .feed)
    }.then{
        let textString = "오늘의 꾸미기 더보기"
        let attributedText = NSMutableAttributedString(string: textString)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 13, type: .EBold), range: (textString as NSString).range(of: "더보기"))
        $0.setAttributedText(attributedText)
    }
    
    let bestDecorateLabel = PaddingLabel(left: 25).then{
        $0.textColor = .black
        $0.font = getFont(size: 18)
        let textString = "베스트 꾸미기"
        let attributedText = NSMutableAttributedString(string: textString)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 18, type: .EBold), range: (textString as NSString).range(of: "꾸미기"))
        $0.attributedText = attributedText
    }
    
    let bestDecorateListView = VariousListView(type: .three)
    
    lazy var bestDecorateButtonAreaView = UIView().then{
        $0.addSubview(bestDecorateButtonView)
    }
    
    let bestDecorateButtonView = ButtonView(type:.gray){
        print("bestDecorateButtonView")
        App.push(tabType: .feedPopular)
    }.then{
        let textString = "베스트 꾸미기 더보기"
        let attributedText = NSMutableAttributedString(string: textString)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 13, type: .EBold), range: (textString as NSString).range(of: "더보기"))
        $0.setAttributedText(attributedText)
    }
    
    let kkumingChallengeLabel = PaddingLabel(left: 25).then {
        $0.textColor = .black
        $0.font = getFont(size: 18)
        let textString = "꾸밍 챌린지"
        let attributedText = NSMutableAttributedString(string: textString)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 18, type: .EBold), range: (textString as NSString).range(of: "챌린지"))
        $0.attributedText = attributedText
    }
    
    lazy var kkumingChallengeBannerView = DepthBannerView().then{
        $0.delegate = self
        $0.addSubview(kkumingChallengePageControlView)
    }
    
    lazy var kkumingChallengePageControlView = BannerPageControlView().then{
        $0.type = .CHALLENGE
    }
    
    let kkumingTagLabel = PaddingLabel(left: 25).then {
        $0.textColor = .black
        $0.font = getFont(size: 18)
        let textString = "꾸밍 태그"
        let attributedText = NSMutableAttributedString(string: textString)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 18, type: .EBold), range: (textString as NSString).range(of: "태그"))
        $0.attributedText = attributedText
    }
    
    let kkumingTopTagView = MainTagView().then{
        $0.setUIEdgeInsets(UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0))
    }
    
    let kkumingBottomTagView = MainTagView().then{
        $0.isReverse = true
        $0.setUIEdgeInsets(UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0))
    }
    
    var nowFloatingView = NowFloatingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("accessToken : ", UserManager.shared.getUserData().accessToken)

        setupUI()
        self.getMainAll(isFirst:true)
        self.startBigBannerTimer()
        
        //메인 실행 했다는 것을 알림
        AppManager.shared.isRunMain = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AlertManager.shared.updateStatus(view: topView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if RefreshManager.shared.data.isMain {
            self.getMainAll()
            RefreshManager.shared.data.isMain = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let height = (UIScreen.main.bounds.width * 230 / 375) - 30
        kkumingChallengePageControlView.snp.updateConstraints{
            $0.top.equalTo(kkumingChallengeBannerView.snp.top).offset(height)
        }
    }
    
    public func configuration(items:MainListData) {
        self.data = items
        guard let data = data else { return }
        if let banner = data.banner?.slides {
            topBigBannerView.configuration(items: banner)
            bannerPageControlView.configuration(items: banner)
            bannerPageLineView.configuration(items: banner)
        }
        
        if let today = data.today, let posts = today.posts {
            todayDecorateListView.configuration(items: posts, postType: .POST_TODAY_DECORATING)
        }
        
        if let best = data.best, let posts = best.posts {
            bestDecorateListView.configuration(items: posts, postType: .POST_TODAY_DECORATING)
        }
        
        if let challenge = data.challenge {
            kkumingChallengeBannerView.configuration(items: challenge)
            kkumingChallengePageControlView.configuration(items: challenge)
        }
        
        if let recommendTag = data.recommendTag, let items = recommendTag.manageTag {
            do {
                var firstList:[Tag] = []
                var secondList:[Tag] = []
                let count = items.count
                let dividedCount = count/2
                
                for index in 0..<dividedCount {
                    firstList.append(items[index])
                }
                
                for index in dividedCount..<count {
                    secondList.append(items[index])
                }
                
                kkumingTopTagView.configuration(items: firstList)
                kkumingBottomTagView.configuration(items: secondList)
            } catch {
                print("tag error")
            }
        }
        
        if let userList = data.userList {
            nowFloatingView.configuration(items: userList)
        }
    }
    
    private func setupUI() {
        //MARK: 뷰 추가하는 부분
        [
            topBigBannerView,
            bannerPageControlAreaView,
            bannerPageLineAreaView,
            createSpacingView(height: 10),
            todayDecorateLabel,
            createSpacingView(height: 0),
            todayDecorateListView,
            createSpacingView(height: 10),
            todayDecorateButtonAreaView,
            createSpacingView(height: 25),
            bestDecorateLabel,
            createSpacingView(height: 0),
            bestDecorateListView,
            createSpacingView(height: 10),
            bestDecorateButtonAreaView,
            createSpacingView(height: 25),
            kkumingChallengeLabel,
            createSpacingView(height: 0),
            kkumingChallengeBannerView,
            createSpacingView(height: 25),
            kkumingTagLabel,
            createSpacingView(height: 0),
            kkumingTopTagView,
            kkumingBottomTagView,
            createSpacingView(height: 25),
        ].forEach{
            stackView.addArrangedSubview($0)
        }
        
        stackView.addArrangedSubview(nowFloatingView)
        
        scrollView.addSubViews([
            stackView
        ])
        
        addSubViews([
            topView,
            scrollView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        scrollView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
        
        setupStackViewConstraints()
    }
    
    private func setupStackViewConstraints() {
        //비율은 327 * 294px, 654 * 588
        let width = UIScreen.main.bounds.width - 50 //-50은 left와 right마진 합친 값
        let height = width * 588 / 654
        topBigBannerView.snp.makeConstraints{
            $0.height.equalTo(height)
        }
        
        bannerPageControlAreaView.snp.makeConstraints{
            $0.height.equalTo(20)
        }
        
        bannerPageControlView.snp.makeConstraints{
            $0.width.equalTo(60)
            $0.height.equalTo(20)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-40)
        }
        
        bannerPageLineAreaView.snp.makeConstraints{
            $0.height.equalTo(2)
        }
        
        bannerPageLineView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(40)
            $0.trailing.equalToSuperview().offset(-40)
        }
        
        todayDecorateListView.snp.makeConstraints{
            $0.height.equalTo(todayDecorateListView.getTwoLineHeight())
        }
        
        todayDecorateButtonAreaView.snp.makeConstraints{
            $0.height.equalTo(43)
        }
        
        todayDecorateButtonView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-25)
        }
        
        bestDecorateListView.snp.makeConstraints{
            $0.height.equalTo(bestDecorateListView.getTwoLineHeight())
        }
        
        bestDecorateButtonAreaView.snp.makeConstraints{
            $0.height.equalTo(43)
        }
        
        bestDecorateButtonView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-25)
        }
        
        //꾸밍 챌린지 (하단 영역이 마진값 : 20, 높이는 70으로 총 90 잡아먹음)
        let challengeHeight = (UIScreen.main.bounds.width * 230 / 375) + 90
        kkumingChallengeBannerView.snp.makeConstraints{
            $0.height.equalTo(challengeHeight)
        }
        
        kkumingChallengePageControlView.snp.makeConstraints{
            $0.width.equalTo(60)
            $0.height.equalTo(19)
            
            //MARK: viewDidLayoutSubviews에서 높이 update함
            $0.top.equalTo(kkumingChallengeBannerView.snp.top).offset(200)
            $0.trailing.equalTo(kkumingChallengeBannerView.snp.trailing).offset(-10)
        }
        
        //꾸밍 태그
        kkumingTopTagView.snp.makeConstraints{
            $0.height.equalTo(29)
        }
        
        kkumingBottomTagView.snp.makeConstraints{
            $0.height.equalTo(29)
        }
    }
}

extension MainVC: BannerViewDelegate{
    @objc func refresh(_ sender: AnyObject) {
       print("새로고침")
        self.getMainAll() {
            self.refreshControl.endRefreshing()
        }
    }
    
    /*
     메인 최상단 배너 분기처리
     1. 외부 URL -> Safari
     2. 내부 URL -> 내부 WebView
     3. 꾸밍 챌린지 상세
     4. 이벤트 상세와 같은 기능
     */
    func selectBannerHandler(data:BannerData) {
        if let dataType = data.type, let type = BannerType(rawValue: dataType) {
            switch type {
            case .BANNER_WEB_VIEW, .WEB_VIEW:
                if let linkUrl = data.linkUrl {
                    let vc = WebVC(title: data.webViewTitle ?? "", url: linkUrl)
                    AppManager.shared.push(vc)
                }
                break
            case .CHALLENGE:
                if let id = data.eventsId {
                    let vc = TagDetailVC(topTitleString: data.webViewTitle ?? "",
                                         id: id,
                                         type:.CHALLENGE,
                                         isUseBanner: true)
                    AppManager.shared.push(vc)
                }
                
                break
            case .URL:
                if let linkUrl = data.linkUrl {
                    openSafari(url: linkUrl)
                }
                break
            case .EVENT:
                if let eventsId = data.eventsId {
                    API.eventsDetail(id: eventsId, type: .EVENT) { model in
                        guard let model = model, let events = model.data?.events  else { return }
                        
                        if model.success {
                            if let linkUrl = events.linkUrl {
                                let vc = WebVC(title: data.webViewTitle ?? "", url: linkUrl)
                                AppManager.shared.push(vc)
                            }
                        }
                    }
                }
                break
            }
        }
    }
    
    func didSelectItemAt(index: Int) {
        print("꾸밍 챌린지 선택 인덱스 : ", index)
        if let items = self.data?.challenge {
            let item = items[index]
            let id = item.id
            let vc = TagDetailVC(topTitleString: item.title ?? "",
                                 id: id,
                                 type:.CHALLENGE,
                                 isUseBanner: true)
            print("꾸밍 챌린지 선택 id : \(id)")
            AppManager.shared.push(vc)
        }
    }
    
    func didSelectBigBanner(index: Int) {
        guard let data = data else { return }
        if let bannerList = data.banner?.slides {
            let banner = bannerList[index]
            selectBannerHandler(data: banner)
        }
    }
    
    func scrollViewWillEndDragging(index: Int) {
        
    }
    
    func scrollViewWillEndDragging(view: BannerView, index: Int) {
        if view == topBigBannerView {
            bannerPageControlView.updateCurrentNumber(index: index)
            bannerPageLineView.updateCurrentLine(index: index)
            
            stopBigBannerTimer()
            startBigBannerTimer()
        }
    }
    
    func scrollViewWillEndDragging(depthView: DepthBannerView, index: Int) {
        if kkumingChallengeBannerView == depthView {
            print("index : ", index)
            kkumingChallengePageControlView.updateCurrentNumber(index: index)
        }
    }
}

//기타 기능
extension MainVC {
    private func startBigBannerTimer() {
        bannerTimer = Timer.scheduledTimer(withTimeInterval: 5.0, repeats: true) { [weak self] _ in
            guard let strongSelf = self else {
                return
            }
            var index:Int = Int(strongSelf.topBigBannerView.currentIndex)
            let totalCount = strongSelf.topBigBannerView.numberOfItems
            
            //만약 30번 정도 남으면 초기화
            if (index > (totalCount - 30)) {
                let estimatedIndex = CGFloat(strongSelf.topBigBannerView.numberOfItems % strongSelf.topBigBannerView.items.count) + CGFloat(strongSelf.topBigBannerView.numberOfItems / 2)
                strongSelf.topBigBannerView.currentIndex = estimatedIndex
                strongSelf.moveToScroll(index: Int(estimatedIndex))
            } else {
                index += 1
                strongSelf.moveToScroll(index: index)
            }
        }
    }
    
    private func stopBigBannerTimer() {
        bannerTimer.invalidate()
        bannerTimer = nil
    }
    
    private func moveToScroll(index:Int) {
        let count = self.topBigBannerView.items.count
        
        //0개에 대한 예외처리
        if count == 0 {
            return
        }
        
        let currentIndex = index % count
        let indexPath = IndexPath(row: Int(index) , section: 0)
        
        topBigBannerView.currentIndex = CGFloat(index)
        
        topBigBannerView.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        bannerPageControlView.updateCurrentNumber(index: currentIndex)
        bannerPageLineView.updateCurrentLine(index: currentIndex)
    }
}

//MARK: - API
extension MainVC {
    private func getMainAll(isFirst:Bool = false, completion:(()->Void)? = nil) {
        API.getMainALL(){ [weak self] response in
            guard let strongSelf = self,
                  let model = response,
                  let data = model.data
            else { return }
            
            strongSelf.configuration(items: data)
            
            completion?()
            
            if isFirst {
                strongSelf.getNoticePopup()
            }
        }
    }
    
    private func getNoticePopup() {
        let savedDate = UserDefaultsUtil.readMainPopupDate()
        let currentDate = Date().dateFormat()
        
        //오늘 하루 그만 보기 체크
        if savedDate == currentDate {
            return
        }
        
        //가져오기
        API.getBannerList(exposureType: .POPUP, periodType: .ING) { model in
            guard let model = model, let data = model.data.slides else {
                return
            }
            
            if data.count != 0 {
                let popupView = NoticePopupView()
                popupView.items = data
                AlertManager.shared.showWindows(view: popupView)
            }
            
            print("getNoticePopup data count : ", data.count)
        }
    }
}
