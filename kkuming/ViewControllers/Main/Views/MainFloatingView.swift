//
//  MainFloatingView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/26.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class MainFloatingView: UIView {
    var isFollowing:Bool = false {
        didSet {
            self.updateFollowingStatus()
        }
    }
    var item:User?
    
    let disposeBag = DisposeBag()
    
    lazy var backgroundTouchButton = UIButton().then{
        $0.rx.tap.bind{ [weak self] in
            let id = self?.item?.id ?? 0
            if id == UserManager.shared.getUserId() {
                AppManager.shared.moveToTab(index: 4)
                return
            }
            
            App.push(ProfileVC(userId: id))
        }.disposed(by: disposeBag)
    }
    let thumbImageView = UIImageView()
    let nameLabel = UILabel().then{
        $0.font = getFont(size: 14, type: .EBold)
        $0.textColor = .black
    }
    let introduceLabel = UILabel().then{
        $0.numberOfLines = 2
        $0.font = getFont(size: 11)
        $0.textColor = RGB(red: 116, green: 116, blue: 116)
    }
    let addressLabel = UILabel().then{
        $0.font = getFont(size: 9)
        $0.textColor = RGB(red: 206, green: 206, blue: 206)
        $0.isUserInteractionEnabled = true
    }
    
    let followingButtonView = UIView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 10
        $0.layer.borderColor = RGB(red: 56, green: 196, blue: 207).cgColor
        $0.layer.borderWidth = 1
    }

    let followingLabel = UILabel().then{
        $0.font = getFont(size: 11, type: .bold)
        $0.text = "팔로잉"
    }
    
    lazy var followingButton = UIButton().then{
        $0.rx.tap.bind{ [weak self] in
            guard let strongSelf = self, let followingId = strongSelf.item?.id else { return }
            print("followingButton")
            strongSelf.followReact(followingId: followingId)
        }.disposed(by: self.disposeBag)
    }
    
    var completion:(()->Void)? = nil
    
    init(completion:(()->Void)? = nil) {
        super.init(frame: CGRect.zero)
        self.completion = completion
        
        setupProperties()
        setupUI()
    }
    
    public func configuration(item:User) {
        self.item = item
        if let isFollowing = item.isFollowed {
            self.isFollowing = isFollowing
        }
        if let urlString = item.avatar, let url = URL(string: urlString) {
            thumbImageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
        }
        nameLabel.text = item.nickname
        introduceLabel.text = item.bio
        addressLabel.text = item.linkShare
        
        updateFollowingStatus()
    }
    
    private func setupProperties() {
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = 15
        backgroundColor = .white
    }
    
    private func setupUI() {
        addGestureSNSLink()
        createFollowingButton()
        
        addSubViews([
            backgroundTouchButton,
            thumbImageView,
            nameLabel,
            introduceLabel,
            addressLabel,
            followingButtonView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        backgroundTouchButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        thumbImageView.snp.makeConstraints{
            $0.leading.top.bottom.equalToSuperview()
            $0.width.equalTo(95)
        }
        
        nameLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(17)
            $0.leading.equalTo(thumbImageView.snp.trailing).offset(20)
            $0.trailing.equalTo(followingButtonView.snp.leading).offset(-5)
        }
        
        followingButtonView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(13)
            $0.trailing.equalToSuperview().offset(-10)
            $0.width.equalTo(56)
            $0.height.equalTo(23)
        }
        
        introduceLabel.snp.makeConstraints{
            $0.top.equalTo(nameLabel.snp.bottom).offset(7)
            $0.leading.equalTo(nameLabel.snp.leading)
            $0.trailing.equalToSuperview().offset(-10)
        }
        
        addressLabel.snp.makeConstraints{
            $0.top.equalTo(introduceLabel.snp.bottom).offset(6)
            $0.leading.equalTo(nameLabel.snp.leading)
            $0.trailing.equalToSuperview().offset(-10)
        }
    }
    
    private func createFollowingButton() {
        followingButtonView.addSubViews([
            followingLabel,
            followingButton
        ])
        
        followingLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        followingButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func updateFollowingStatus() {
        if isFollowing {
            followingLabel.text = "팔로잉"
            followingLabel.textColor = RGB(red: 56, green: 196, blue: 207)
            followingButtonView.backgroundColor = UIColor.white
            followingButtonView.layer.borderWidth = 1
        } else {
            if UserManager.shared.getUserId() == self.item?.id {
                //본인
                followingLabel.text = "팔로우"
                followingLabel.textColor = RGB(red: 51, green: 51, blue: 51)
                followingButtonView.backgroundColor = RGB(red: 237, green: 237, blue: 237)
                followingButtonView.layer.borderWidth = 0
            } else {
                followingLabel.text = "팔로우"
                followingLabel.textColor = UIColor.white
                followingButtonView.backgroundColor = RGB(red: 56, green: 196, blue: 207)
            }
        }
        
        self.completion?()
    }
    
    private func addGestureSNSLink() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(onTouchSNSLink(sender:)))
        gesture.numberOfTapsRequired = 1
        addressLabel.addGestureRecognizer(gesture)
    }
    
    //프로필 링크 활성화
    @objc
    func onTouchSNSLink(sender: UIButton) {
        openSafari(url: addressLabel.text ?? "")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MainFloatingView {
    func followReact(followingId:Int) {
        guard let item = item, let followingId = item.id else {return}
        API.followReact(followingId: followingId) { [weak self] model, data in
            guard let strongSelf = self, let model = model else {
                if let data = data {
                    JSON.decode(data: data) { errorData in
                        guard let errorData = errorData else {
                            return
                        }
                        
                        if errorData.data.code == "7003" {
                            AlertManager.shared.show(text: "본인은 팔로우 할 수 없습니다.", type: .one)
                        }
                    }
                }
                return
            }
            
            if model.success {
                strongSelf.isFollowing = model.data.isFollowed
            } else {
                AlertManager.shared.show(text: "에러 발생", type: .one)
            }
        }
    }
}
