//
//  VariousListView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/25.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class VariousListView: UIView {
    var type:VariousListType!
    
    var items:[Post] = []
    var postType:PostDetailType = .NONE
    var selectedItems:[Post] = []
    
    let leftMargin:CGFloat = 25
    let rightMargin:CGFloat = 25
    lazy var width:CGFloat = 0
    lazy var height:CGFloat = 0
    let lineSpacing:CGFloat = 10
    var isUseDeleteMode:Bool = false {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var scrollEndHandler:(()->Void)? = nil
    var updateHeightHandler:(()->Void)? = nil
    
    //페이징
    var isPaging = false
    var moreHandler:(()->Void)? = nil
    
    lazy var layout = UICollectionViewFlowLayout().then{
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .vertical
    }
    
    lazy var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(VariousListViewCell.self, forCellWithReuseIdentifier: VariousListViewCell.identifier)
//        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
        $0.isScrollEnabled = false
        $0.isPagingEnabled = false
    }
    
    init(type:VariousListType) {
        super.init(frame: CGRect.zero)
        self.type = type
        
        calculateWidthAndHeight()
        setupUI()
    }
    
    public func configuration(items:[Post], postType:PostDetailType = .NONE) {
        self.items = items
        self.postType = postType

        if self.items.count != 0 {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.1, animations: {
                    self.collectionView.reloadData()
                }, completion: {_ in 
                    self.updateHeightHandler?()
                })
            }
        }
    }
    
    public func add(items:[Post]) {
        self.items += items

        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    public func reload() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    public func getTwoLineHeight() -> CGFloat {
        return height * 2 + lineSpacing
    }
    
    private func calculateWidthAndHeight() {
        switch type{
        case .two:
            self.width = (UIScreen.main.bounds.width - leftMargin - rightMargin - 10) / 2
        case .three:
            self.width = (UIScreen.main.bounds.width - leftMargin - rightMargin - 20) / 3
        case .none:
            self.width = 0
        }
        self.height = width
    }
    
    private func setupUI() {
        addSubview(collectionView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VariousListView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isUseDeleteMode {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VariousListViewCell.identifier, for: indexPath) as! VariousListViewCell
            let item = self.items[indexPath.item]
            cell.configuration(item: item)
            cell.isVisibleCheckBox(true)
            
            if let _ = selectedItems.firstIndex(where: {$0.id == item.id}) {
                cell.isSelected(true)
            } else {
                cell.isSelected(false)
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VariousListViewCell.identifier, for: indexPath) as! VariousListViewCell
            let item = self.items[indexPath.item] as! Post
            
            cell.configuration(item: item)
            cell.isVisibleCheckBox(false)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.items[indexPath.item]
        
        if isUseDeleteMode {
            if let _ = selectedItems.firstIndex(where: {$0.id == item.id}) {
                selectedItems.removeAll(where: { $0.id == item.id })
            } else {
                selectedItems.append(item)
            }
            
            collectionView.reloadData()
        } else {
            let vc = ContentDetailVC()
            vc.configuration(postData: item, postType: postType)
            vc.getDetailPost() {
                App.push(vc)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.item == self.items.count - 1) {
            guard !isPaging else { return }
            self.isPaging = true
            
            if self.items.count >= 10 {
                self.moreHandler?()
            }
        }
    }
}
