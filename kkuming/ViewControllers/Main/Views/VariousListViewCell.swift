//
//  VariousListViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/25.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class VariousListViewCell: UICollectionViewCell {
    static var identifier = String(describing: VariousListViewCell.self)
    var disposeBag = DisposeBag()
    let checkOffImage = UIImage(named: "check_btn")!
    let checkOnImage = UIImage(named: "check_btn_over")!
    var item:Any!
    
    let imageView = UIImageView().then{
        $0.contentMode = .scaleAspectFill
    }
    
    lazy var checkBoxButton = UIButton().then{
        $0.isHidden = true
        $0.setImage(checkOffImage, for: .normal)
        $0.isUserInteractionEnabled = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.disposeBag = DisposeBag()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
        setupUI()
    }
    
    public func configuration(item:Post) {
        self.item = item
        
        if let urlString = item.thumbnailImage?.encodingQuery, let url = URL(string: urlString) {
            imageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
        }
    }
    
    public func isVisibleCheckBox(_ bool:Bool) {
        checkBoxButton.isHidden = !bool
    }
    
    public func isSelected(_ bool:Bool) {
        if bool {
            checkBoxButton.setImage(checkOnImage, for: .normal)
        } else {
            checkBoxButton.setImage(checkOffImage, for: .normal)
        }
    }
    
    private func commonInit() {
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 15
    }
    
    private func setupUI() {
        contentView.addSubViews([
            imageView,
            checkBoxButton
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        imageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        checkBoxButton.snp.makeConstraints{
            $0.width.height.equalTo(20)
            $0.top.equalToSuperview().offset(2.5)
            $0.trailing.equalToSuperview().offset(-2.5)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
