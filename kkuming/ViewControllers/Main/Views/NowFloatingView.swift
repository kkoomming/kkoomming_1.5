import UIKit
import SnapKit
import Then

class NowFloatingView: UIView {
    var items:[User] = []
    
    let nowFloatingLabel = PaddingLabel().then{
        $0.textColor = .black
        $0.font = getFont(size: 18)
        let textString = "지금 뜨고 있어요"
        let attributedText = NSMutableAttributedString(string: textString)
        attributedText.addAttribute(NSAttributedString.Key.font, value: getFont(size: 18, type: .EBold), range: (textString as NSString).range(of: "뜨고 있어요"))
        $0.attributedText = attributedText
    }
    
    let nowFloatingStackView = UIStackView().then{
        $0.alignment = .fill
        $0.distribution = .fill
        $0.axis = .vertical
        $0.layoutMargins = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    init() {
        super.init(frame: CGRect.zero)
        self.backgroundColor = RGB(red: 247, green: 247, blue: 247)
        
        setupUI()
    }
    
    private func setupUI() {
        [
            createSpacingView(height: 45),
            nowFloatingLabel,
            createSpacingView(height: 22)
        ].forEach{
            nowFloatingStackView.addArrangedSubview($0)
        }
        
        self.addSubview(nowFloatingStackView)
        
        setupConstraints()
    }
    
    public func configuration(items:[User]) {
        removeAll()
        
        /*
         45 : 위아래 여백
         Int(nowFloatingLabel.intrinsicContentSize.height) : 타이틀 높이
         22 : 타이틀과 아이템 사이의 여백
         96 : 아이템 높이
         15 : 아이템 사이 spacing
         */
        let height = 45 + Int(nowFloatingLabel.intrinsicContentSize.height) + 22 + ((96 + 15) * items.count) + 45
        
        var index = 0
        for item in items {
            createNowFloatingView(item: item, index: index, stackView: nowFloatingStackView)
            index += 1
        }
        
        let bottomSpacingView = UIView().then{
            $0.tag = 9999
        }
        
        self.nowFloatingStackView.addArrangedSubview(bottomSpacingView)
        bottomSpacingView.snp.makeConstraints{
            $0.height.equalTo(45)
        }
        
        self.snp.updateConstraints{
            $0.height.equalTo(height)
        }
    }
    
    public func removeAll() {
        for item in nowFloatingStackView.arrangedSubviews {
            if item is MainFloatingView || item.tag == 9999 {
                nowFloatingStackView.removeArrangedSubview(item)
                item.removeFromSuperview()
            }
        }
    }
    
    private func setupConstraints() {
        self.snp.makeConstraints{
            $0.height.equalTo(1)
        }
        
        nowFloatingStackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func createNowFloatingView(item:User, index:Int, stackView:UIStackView) {
        let mainFloatingView = MainFloatingView(){
            
        }.then{
            $0.configuration(item: item)
        }
        
        stackView.addArrangedSubview(mainFloatingView)
        stackView.setCustomSpacing(15, after: mainFloatingView)
        
        mainFloatingView.snp.makeConstraints{
            $0.height.equalTo(96)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
