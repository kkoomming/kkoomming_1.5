//
//  MainTagView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/26.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class MainTagView: UIView {
    let colors:[UIColor] = [
        RGB(red: 227, green: 247, blue: 248),
        RGB(red: 255, green: 245, blue: 201)
    ]
    
    var tagList:[Tag] = []
    var isReverse:Bool = false
    
    let layout = UICollectionViewFlowLayout().then{
        $0.scrollDirection = .horizontal
        $0.minimumInteritemSpacing = 6
    }
    
    lazy var collectionView = UICollectionView(
        frame: CGRect.zero,
        collectionViewLayout: layout
    ).then{
        $0.showsHorizontalScrollIndicator = false
        $0.alwaysBounceHorizontal = true
        $0.register(MainTagViewCell.self, forCellWithReuseIdentifier: MainTagViewCell.identifier)
        $0.dataSource = self
        $0.delegate = self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    public func configuration(items:[Tag]){
        self.tagList = items
        collectionView.reloadData()
    }

    public func setUIEdgeInsets(_ insets:UIEdgeInsets){
        self.collectionView.contentInset = insets
    }
    
    private func setupUI() {
        addSubViews([
            collectionView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MainTagView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MainTagViewCell.identifier, for: indexPath) as! MainTagViewCell
        let index = indexPath.item
        
        if isReverse {
            cell.backgroundColor = colors.reversed()[index % 2]
        } else {
            cell.backgroundColor = colors[index % 2]
        }
        cell.titleLabel.text = tagList[index].tagName
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let leftMargin:CGFloat = 28
        let rightMargin:CGFloat = 28
        let index = indexPath.item
        let size = getLabelSize(text: self.tagList[index].tagName, font: getFont(size: 11, type: .bold))
        let width = leftMargin + size.width + rightMargin
        
        return CGSize(width: width, height: 29)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        let item = self.tagList[index]
        
        print("태그명 : ", item.tagName)

        App.push(TagDetailVC(topTitleString: item.tagName))
    }
}
