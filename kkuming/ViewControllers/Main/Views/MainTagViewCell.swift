//
//  MainTagViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/26.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class MainTagViewCell: UICollectionViewCell {
    static var identifier = String(describing: MainTagViewCell.self)
    
    let titleLabel = UILabel().then{
        $0.font = getFont(size: 11, type: .bold)
        $0.textColor = .black
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    public func configuration(text:String){
        titleLabel.text = text
    }
    
    private func setupUI() {
        setupProperties()
        
        contentView.addSubview(titleLabel)
        
        setupConstraints()
    }
    
    private func setupProperties() {
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 12
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
