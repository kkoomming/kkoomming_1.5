//
//  LoginVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/21.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import NaverThirdPartyLogin
import Alamofire
import KakaoSDKUser
import GoogleSignIn
import FirebaseCore
import FirebaseAuth
import AuthenticationServices
import FirebaseMessaging
import RxGesture

enum LoginType:String {
    case NAVER = "NAVER"
    case KAKAO = "KAKAO"
    case GOOGLE = "GOOGLE"
    case APPLE = "APPLE"
    case NORMAL = "NORMAL"
}

class LoginVC: BaseVC{
    let disposeBag = DisposeBag()
    
    let naverLoginInstance = NaverThirdPartyLoginConnection.getSharedInstance()
    
    let stackView = UIStackView().then{
        $0.alignment = .fill
        $0.axis = .vertical
        $0.distribution = .fill
    }
    
    let kkumingTopLetterLabel = UILabel().then{
        $0.text =
        """
        평범한 오늘을
        특별하게 꾸미다
        """
        $0.font = getFont(size: 18, type: .light)
        $0.textColor = .white
        $0.numberOfLines = 2
    }
    
    let kkumingLetterAndImageView:UIView = {
        let view = UIView()
        let kkumingLetterImageView = UIImageView()
        let kkumingImageView = UIImageView()
        kkumingLetterImageView.image = UIImage(named: "kkuming_letter")
        kkumingImageView.image = UIImage(named: "main_character")
        kkumingImageView.contentMode = .scaleAspectFit
        
        view.addSubViews([
            kkumingLetterImageView,
            kkumingImageView
        ])
        
        kkumingLetterImageView.snp.makeConstraints{
            $0.width.equalTo(85)
            $0.height.equalTo(45)
            $0.leading.equalToSuperview()
            $0.top.equalToSuperview().offset(15)
        }
        
        kkumingImageView.snp.makeConstraints{
            $0.width.equalTo(160)
            $0.height.equalTo(110)
            $0.top.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        
        
        return view
    }()
    var naverView:UIView!
    var kakaoView:UIView!
    var googleView:UIView!
    var appleView:UIView!
    
    lazy var borderAndDefaultTitleView:UIView = {
        let view = UIView()
        let borderView = UIView()
        borderView.backgroundColor = .white
        
        let defaultTitleLabel = UILabel()
        defaultTitleLabel.font = getFont(size: 14, type: .regular)
        defaultTitleLabel.text = "[교보북클럽 회원 대상]SNS 간편 로그인 전환 안내 >"
        defaultTitleLabel.textColor = .white
        
        view.addSubview(borderView)
        view.addSubview(defaultTitleLabel)
        
        borderView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(1)
        }
        
        defaultTitleLabel.snp.makeConstraints{
            $0.top.equalTo(borderView.snp.bottom).offset(20)
            $0.centerX.equalToSuperview()
        }
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupLoginView()
        setupUI()
        setBindings()
    }
    
    private func setupLoginView() {
        setupNaverLogin()
        setupKakaoLogin()
        setupGoogleLogin()
        setupAppleLogin()
    }
    
    private func setupUI() {
        self.view.backgroundColor = .mainColor
        
        [
            kkumingTopLetterLabel,
            kkumingLetterAndImageView,
            naverView,
            createSpacingView(height: 12),
            kakaoView,
            createSpacingView(height: 12),
            googleView,
            createSpacingView(height: 12),
            appleView,
            createSpacingView(height: 12),
            borderAndDefaultTitleView
        ].forEach{
            stackView.addArrangedSubview($0)
        }

        addSubViews([
            stackView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        stackView.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(40)
            $0.trailing.equalToSuperview().offset(-40)
        }
        
        kkumingLetterAndImageView.snp.makeConstraints{
            $0.height.equalTo(110)
        }
        
        setupSNSConstraints()
    }
    
    private func setupSNSConstraints() {
        naverView.snp.makeConstraints{
            $0.height.equalTo(45)
        }
        
        kakaoView.snp.makeConstraints{
            $0.height.equalTo(45)
        }
        googleView.snp.makeConstraints{
            $0.height.equalTo(45)
        }
        appleView.snp.makeConstraints{
            $0.height.equalTo(45)
        }
        
        borderAndDefaultTitleView.snp.makeConstraints{
            $0.height.equalTo(50)
        }
    }
    
    private func setBindings() {
        borderAndDefaultTitleView.rx.tapGesture().skip(1)
            .observe(on: MainScheduler.instance)
            .bind{ _ in
                print("💙 교보북클럽 로그인 전환")
                let urlString = "https://kkoomming.net/login-test.html"
                openSafari(url: urlString)
            }.disposed(by: disposeBag)
    }
    
    func createSNSView(type:SNSType, completion:(()->Void)? = nil) -> UIView {
        let view = UIView().then {
            $0.layer.cornerRadius = 6
        }
        let iconImageView = UIImageView()
        let titleLabel = UILabel()
        lazy var button = UIButton().then{
            $0.rx.tap.bind{
                if completion != nil {
                    completion!()
                }
            }.disposed(by: disposeBag)
        }
        
        switch type {
        case .naver:
            view.backgroundColor = RGB(red: 3, green: 199, blue: 90)
            iconImageView.image = UIImage(named: "ic_naver")
            titleLabel.text = "네이버로 시작하기"
            titleLabel.textColor = .white
            titleLabel.font = UIFont.systemFont(ofSize: 15)
        case .kakao:
            view.backgroundColor = RGB(red: 254, green: 229, blue: 0)
            iconImageView.image = UIImage(named: "ic_kakao")
            titleLabel.text = "카카오로 시작하기"
            titleLabel.textColor = .black
            titleLabel.font = UIFont.systemFont(ofSize: 15)
        case .google:
            let textString = "Google로 시작하기"
            let attributeedString = NSMutableAttributedString(string: textString)
            attributeedString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15, weight: .bold), range: (textString as NSString).range(of: "Google"))
            
            view.backgroundColor = .white
            iconImageView.image = UIImage(named: "ic_google")
            titleLabel.textColor = .black
            titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            titleLabel.attributedText = attributeedString
        case .apple:
            let textString = "Apple로 시작하기"
            let attributeedString = NSMutableAttributedString(string: textString)
            attributeedString.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 15, weight: .bold), range: (textString as NSString).range(of: "Apple"))
            
            view.backgroundColor = .black
            iconImageView.image = UIImage(named: "ic_apple")
            titleLabel.text = "Apple로 시작하기"
            titleLabel.textColor = .white
            titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            titleLabel.attributedText = attributeedString
        case .default:
            view.backgroundColor = .white
            iconImageView.image = UIImage(named: "ic_kyobo")
            titleLabel.text = "교보북클럽(꾸밍) 로그인"
            titleLabel.textColor = .black
            titleLabel.font = UIFont.systemFont(ofSize: 15)
        }
        
        view.addSubViews([
            iconImageView,
            titleLabel,
            button
        ])
        
        iconImageView.snp.makeConstraints{
            $0.width.height.equalTo(30)
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(8)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview().offset(10)
        }
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        return view
    }
}

//MARK: - SNS
//네이버
extension LoginVC: NaverThirdPartyLoginConnectionDelegate {
    func setupNaverLogin() {
        naverView = createSNSView(type: .naver){
            print("네이버")
            self.doLoginNaver()
        }
        
        naverLoginInstance?.delegate = self
    }
    
    func doLoginNaver() {
        naverLoginInstance?.requestThirdPartyLogin()
    }
    
    func doLogoutNaver() {
        naverLoginInstance?.requestDeleteToken()
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithAuthCode() {
        print("네이버 로그인 성공")
        
        getNaverInfo()
    }
    
    func oauth20ConnectionDidFinishRequestACTokenWithRefreshToken() {
        print("네이버 토큰\(naverLoginInstance?.accessToken)")
        getNaverInfo()
    }
    
    func oauth20ConnectionDidFinishDeleteToken() {
        print("네이버 로그아웃")
        doLogoutNaver()
    }
    
    func oauth20Connection(_ oauthConnection: NaverThirdPartyLoginConnection!, didFailWithError error: Error!) {
        print("에러 = \(error.localizedDescription)")
    }
    
    private func getNaverInfo() {
        guard let isValidAccessToken = naverLoginInstance?.isValidAccessTokenExpireTimeNow() else { return }
        
        if !isValidAccessToken {
          return
        }
        
        guard let tokenType = naverLoginInstance?.tokenType else { return }
        guard let accessToken = naverLoginInstance?.accessToken else { return }
        let urlStr = "https://openapi.naver.com/v1/nid/me"
        let url = URL(string: urlStr)!
        
        let authorization = "\(tokenType) \(accessToken)"
        
        print("authorization : ", authorization)
                
        let req = AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: ["Authorization": authorization])
        
        req.responseJSON { response in
            guard let result = response.value as? [String: Any] else { return }
            guard let object = result["response"] as? [String: Any] else { return }
            print("object : ", object)
            guard let id = object["id"] as? String else { return }
//            guard let name = object["name"] as? String else { return }
//            guard let email = object["email"] as? String else { return }
//            guard let nickname = object["nickname"] as? String else { return }

            self.login(clientId: id, loginMethod: .NAVER)
        }
    }
}

//카카오
extension LoginVC {
    func setupKakaoLogin() {
        kakaoView = createSNSView(type: .kakao){
            print("카카오")
            self.doLoginKakao()
        }
    }
    
    func doLoginKakao() {
        if (UserApi.isKakaoTalkLoginAvailable()) {

            UserApi.shared.loginWithKakaoTalk {(oauthToken, error) in
                if let error = error {
                    print(error)
                }
                else {
                    print("loginWithKakaoTalk() success.")
                    
                    let token = oauthToken
                    
                    self.getInfoFromKakao()
                }
            }
        } else {
            UserApi.shared.loginWithKakaoAccount {(oauthToken, error) in
                    if let error = error {
                        print(error)
                    }
                    else {
                        print("loginWithKakaoAccount() success.")

                        //do something
                        let token = oauthToken
                        
                        self.getInfoFromKakao()
                    }
                }
        }
    }
    
    func getInfoFromKakao() {
        UserApi.shared.me() {(user, error) in
            if let error = error {
                print(error)
            }
            else {
                print("me() success.")
                
                if let info = user, let id = info.id {
                    print("id : ", id)
                    self.login(clientId: String(id), loginMethod: .KAKAO)
                } else {
                    AlertManager.shared.show(text: "잠시 후 다시 시도해 주세요.", type: .one)
                }
            }
        }
    }
}

//구글
extension LoginVC {
    func setupGoogleLogin() {
        googleView = createSNSView(type: .google){
            print("구글")
            self.doLoginGoogle()
        }
    }
    
    func doLoginGoogle() {
        guard let clientID = FirebaseApp.app()?.options.clientID else { return }
        let config = GIDConfiguration(clientID: clientID)
        
        GIDSignIn.sharedInstance.signIn(with: config, presenting: self) { [unowned self] user, error in

            if let error = error {
                return
            }

//            guard let authentication = user?.authentication,
//                  let idToken = authentication.idToken else {
//                return
//            }
            
            if let userId = user?.userID {
                self.login(clientId: userId, loginMethod: .GOOGLE)
            }

//            let credential = GoogleAuthProvider.credential(withIDToken: idToken,
//                                                         accessToken: authentication.accessToken)
//
//            print("credential : ", credential)
          // ...
        }
    }
}

//애플
extension LoginVC: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    func setupAppleLogin() {
        appleView = createSNSView(type: .apple){
            print("애플")
            self.doLoginApple()
        }
    }
    
    func doLoginApple() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    func doWithdrawalApple() -> Void {
        if let accessToken = UserManager.shared.getUserData().accessToken {
            
            let p8 = """
-----BEGIN PRIVATE KEY-----
MIGTAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBHkwdwIBAQQgzUUFbWN/Amke86m5
Xn6SG9vwVf2sRVJ5yyzSeN8eyI6gCgYIKoZIzj0DAQehRANCAAR9MJ42yoGNjUP8
Qd1oK+y3gVbRSKDJQqcal80kOSQMxJivuoJlFAGQXfWKSfckLcWQHsIfDUCieSzl
9olIa8lE
-----END PRIVATE KEY-----
"""
            
            let keyID = "3Z82QZ9BFM"
            let teamID = "3VP3AWQK4L"
            let clientID = "kr.kkoomming"
            
            let jwt = JWT(keyID: keyID,
                          teamID: teamID,
                          issueDate: Date(),
                          expireDuration: 60 * 60,
                          bundleId: clientID,
                          appleAddress: "https://appleid.apple.com"
            )
            do {
                let clientSecret = try jwt.sign(with: p8)

                var parameters:Parameters = [
                    "client_id" : clientID,
                    "client_secret" : clientSecret,
                    "code" : accessToken,
                    "grant_type" : "authorization_code",
                ]
                
                let headers:HTTPHeaders = [
                    "Content-Type": "application/x-www-form-urlencoded"
                ]
                
                AF.request(
                    "https://appleid.apple.com/auth/token",
                    method: .post,
                    parameters: parameters,
                    encoding: URLEncoding.default,
                    headers:headers
                ).responseJSON { response in
                    var responseDictionary : Dictionary<String, Any> = [String : Any]()
                    do {
                        responseDictionary = try JSONSerialization.jsonObject(with: Data(response.data!), options: []) as! [String:Any]
                        
                        if responseDictionary["error"] == nil {
                            if let token = responseDictionary["access_token"] {
                                parameters = [
                                    "client_id" : clientID,
                                    "client_secret" : clientSecret,
                                    "token": token
                                ]
                                
                                AF.request(
                                    "http://appleid.apple.com/auth/revoke",
                                    method: .post,
                                    parameters: parameters,
                                    encoding: URLEncoding.default,
                                    headers: headers
                                ).response { response in
                                    switch response.result {
                                        case .success(_):
                                            print("success")
                                        case .failure(let err):
                                            print("fail \(err)")
                                    }
                                }
                            } else { print("fail tokenGenerateError") }
                        } else { print("fail \(responseDictionary["error"] as? String ?? "")") }
                    } catch { print("fail responseJSONClassCastError") }
                }
            } catch { print("fail JWTGenerateError") }
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        switch authorization.credential {
            case let appleIDCredential as ASAuthorizationAppleIDCredential:
                let userIdentifier = appleIDCredential.user
                let fullName = appleIDCredential.fullName
                let email = appleIDCredential.email
                
//                let userData = UserData(
//                    error: nil,
//                    user: nil,
//                    accessToken: String(data: appleIDCredential.authorizationCode!, encoding: .utf8),
//                    refreshToken: ""
//                )
//
//                UserManager.shared.set(userData: userData)
//
//                print("userIdentifier : \(userIdentifier), fullName : \(fullName), email : \(email)")
//
//                App.setNavi(MainTabBarController())
//            case let passwordCredential as ASPasswordCredential:
//                let username = passwordCredential.user
//                let password = passwordCredential.password
//
//                print("username : \(username), password : \(password)")

                self.login(clientId: userIdentifier, loginMethod: .APPLE)
            default:
                break
            }
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}

extension LoginVC {
    //로그인 토큰
    private func login(clientId:String, loginMethod:LoginType) {
        AppManager.shared.login(clientId: clientId, loginMethod: loginMethod)
    }
}
