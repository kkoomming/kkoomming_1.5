//
//  FeedRecentVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/27.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip
import Alamofire

class FeedRecentVC: BaseVC, IndicatorInfoProvider {
    let itemInfo = IndicatorInfo(title: "최신")
    var items:[Post] = []
    var page:Int = 1
    
    lazy var twoListView = VariousListView(type: .two).then{
        $0.collectionView.isScrollEnabled = true
        $0.collectionView.contentInset = UIEdgeInsets(top: 25, left: $0.leftMargin, bottom: 10, right: $0.rightMargin)
    }
    
    lazy var refreshControl = UIRefreshControl().then{
        $0.addTarget(self, action: #selector(getFeed), for: .valueChanged)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        twoListView.collectionView.refreshControl = refreshControl
        getFeed()
    }
    
    public func configuration(items:[Post]) {
        self.items = items
        twoListView.configuration(items: self.items, postType: .POST_FEED)
        
        twoListView.moreHandler = { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.getFeedMoreData()
        }
    }
    
    public func add(items:[Post]) {
        self.items += items
        twoListView.add(items: items)
    }
    
    private func setupUI() {
        addSubViews([
            twoListView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        twoListView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func clearFeed() {
        page = 1
        twoListView.isPaging = false
        refreshControl.endRefreshing()
    }
    
    @objc
    private func getFeed() {
        clearFeed()
        
        let param:Parameters = [
            "offset" : page,
            "orderBy" : FeedType.POST_LATEST.rawValue
        ]
        
        API.getPostInFeed(param: param) { [weak self] model in
            guard let strongSelf = self,
                  let model = model,
                  let posts = model.data?.posts,
                  let meta = model.data?.metaData else { return }
            if model.success {
                strongSelf.configuration(items: posts)
                
                if strongSelf.page >= meta.totalPages {
                    strongSelf.twoListView.isPaging = true
                } else {
                    strongSelf.twoListView.isPaging = false
                }
                
                strongSelf.page += 1
            }
        }
    }
    
    private func getFeedMoreData() {
        let param:Parameters = [
            "offset" : page,
            "orderBy" : FeedType.POST_LATEST.rawValue
        ]
        
        API.getPostInFeed(param: param) { [weak self] model in
            guard let strongSelf = self,
                  let model = model,
                  let posts = model.data?.posts,
                  let meta = model.data?.metaData
            else {
                print("getSomeoneFollowingPost 데이터 확인 필요")
                return
            }
            
            strongSelf.add(items: posts)
            
            if strongSelf.page >= meta.totalPages {
                strongSelf.twoListView.isPaging = true
            } else {
                strongSelf.twoListView.isPaging = false
            }
            
            strongSelf.page += 1
        }
    }
}

extension FeedRecentVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
