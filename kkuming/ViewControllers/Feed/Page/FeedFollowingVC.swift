//
//  FeedFollowingVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/27.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip

class FeedFollowingVC: BaseVC, IndicatorInfoProvider {
    let itemInfo = IndicatorInfo(title: "팔로잉")
    
    lazy var followingListView = FollowingListView()
    
    lazy var refreshControl = UIRefreshControl().then{
        $0.addTarget(self, action: #selector(getSomeoneFollowingPost), for: .valueChanged)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        getSomeoneFollowingPost()
    }
    
    private func setupUI() {
        followingListView.collectionView.refreshControl = refreshControl
        
        addSubViews([
            followingListView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        followingListView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension FeedFollowingVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension FeedFollowingVC {
    @objc
    func getSomeoneFollowingPost() {
        refreshControl.endRefreshing()
        
        API.getSomeoneFollowingPost() { [weak self] model in
            print("model : ", model)
            print("model.data : ", model?.data)
            guard let strongSelf = self,
                  let model = model,
                  let data = model.data,
                  let users = data.following?.users,
                  let userMeta = data.following?.metaData,
                  let posts = data.posts?.posts,
                  let postsMeta = data.posts?.metaData
            else {
                print("getSomeoneFollowingPost 데이터 확인 필요")
                return
            }
            
            if model.success {
                strongSelf.followingListView.configuration(users: users, userMeta: userMeta,items: posts, meta:postsMeta)
            } else {
                AlertManager.shared.show(text: "잠시 후 다시 시도해 주세요.", type: .one)
            }
        }
    }
}
