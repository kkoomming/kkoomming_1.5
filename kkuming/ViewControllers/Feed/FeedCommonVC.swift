//
//  FeedCommonVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/28.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip

class FeedCommonVC: BaseVC {
    let disposeBag = DisposeBag()
    
    var type:TabBarType! {
        didSet {
            moveToViewController()
        }
    }
    
    let topView = TopView(type: .feed)
    let contentView = FeedPagerVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AlertManager.shared.updateStatus(view: topView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if type != nil {
            moveToViewController()
            type = nil
        }
    }
    
    private func setupNotification() {
        print("setupNotification")
        NotificationCenter.default.addObserver(self, selector: #selector(changePageToFeed), name: .showFeed, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(changePageToFollowing), name: .showFeedFollowing, object: nil)
    }
    
    private func setupUI() {
        addSubViews([
            topView,
            contentView.view
        ])
        
        addChild(contentView)
        contentView.loadViewIfNeeded()
        contentView.view.layoutIfNeeded()
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        contentView.view.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
    
}

//MARK: - objc
extension FeedCommonVC {
    @objc
    private func changePageToFeed() {
        print("changePageToFeed")
        type = .feed
        moveToViewController()
    }
    
    @objc
    private func changePageToPopular() {
        print("changePageToPopular")
        type = .feedPopular
        moveToViewController()
    }
    
    @objc
    private func changePageToFollowing() {
        print("changePageToFollowing")
        type = .feedFollowing
    }
    
    func moveToViewController() {
        if type == .feed {
            DispatchQueue.main.async {
                self.contentView.moveToViewController(at: 0)
            }
        } else
        if type == .feedPopular {
            DispatchQueue.main.async {
                self.contentView.moveToViewController(at: 1)
            }
        } else
        if type == .feedFollowing {
            DispatchQueue.main.async {
                self.contentView.moveToViewController(at: 2)
            }
        }
    }
}
