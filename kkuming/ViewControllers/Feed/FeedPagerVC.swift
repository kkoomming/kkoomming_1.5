//
//  FeedPagerVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/27.
//

import UIKit
import Then
import SnapKit
import XLPagerTabStrip

class FeedPagerVC: PagerVC {
    let borderView = BorderView()
    
    override func viewDidLoad() {
        configuration()
        
        super.viewDidLoad()
        
        self.view.addSubview(borderView)
        borderView.snp.makeConstraints{
            $0.bottom.equalTo(buttonBarView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(1)
        }
        
    }
    
    private func configuration() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = selectedColor
        settings.style.buttonBarItemFont = getFont(size: 15, type: .bold)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = unSelectedColor
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            guard let strongSelf = self else { return }
            
            oldCell?.label.textColor = strongSelf.unSelectedColor
            newCell?.label.textColor = strongSelf.selectedColor
            
            if newCell?.label.text == "최신" {
                App.push(tabType: .feed)
                return
            } else
            if newCell?.label.text == "팔로잉" {
                App.push(tabType: .feedFollowing)
                return
            } else {
                strongSelf.moveToViewController(at: 1)
            }
        }
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let recentVC = FeedRecentVC()
        let popularVC = FeedPopularVC()
        let followingVC = FeedFollowingVC()
        return [recentVC, popularVC, followingVC]
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.item != currentIndex else { return }

        buttonBarView.moveTo(index: indexPath.item, animated: true, swipeDirection: .none, pagerScroll: .yes)

        let oldIndexPath = IndexPath(item: currentIndex, section: 0)
        let newIndexPath = IndexPath(item: indexPath.item, section: 0)

        let cells = cellForItems(at: [oldIndexPath, newIndexPath], reloadIfNotVisible: true)

        if pagerBehaviour.isProgressiveIndicator {
            if let changeCurrentIndexProgressive = changeCurrentIndexProgressive {
                changeCurrentIndexProgressive(cells.first!, cells.last!, 1, true, true)
            }
        } else {
            if let changeCurrentIndex = changeCurrentIndex {
                changeCurrentIndex(cells.first!, cells.last!, true)
            }
        }
    }
}

extension FeedPagerVC {
    private func cellForItems(at indexPaths: [IndexPath], reloadIfNotVisible reload: Bool = true) -> [ButtonBarViewCell?] {
        let cells = indexPaths.map { buttonBarView.cellForItem(at: $0) as? ButtonBarViewCell }

        if reload {
            let indexPathsToReload = cells.enumerated()
                .compactMap { (arg) -> IndexPath? in
                    let (index, cell) = arg
                    return cell == nil ? indexPaths[index] : nil
                }
                .compactMap { (indexPath: IndexPath) -> IndexPath? in
                    return (indexPath.item >= 0 && indexPath.item < buttonBarView.numberOfItems(inSection: indexPath.section)) ? indexPath : nil
                }

            if !indexPathsToReload.isEmpty {
                buttonBarView.reloadItems(at: indexPathsToReload)
            }
        }

        return cells
    }
}
