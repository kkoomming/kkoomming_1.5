//
//  FollowingContentListViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/28.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class FollowingListViewCell: UICollectionViewCell {
    var item:Post? = nil
    
    var disposeBag = DisposeBag()
    let socialStackView = SocialStackView()
    
    static let identifier = String(describing: FollowingListViewCell.self)
    
    let profileImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 39 / 2
        $0.contentMode = .scaleToFill
    }
    
    let profileButton = UIButton()
    
    let profileNameLabel = UILabel().then{
        $0.font = getFont(size: 13, type: .EBold)
        $0.textColor = .black
    }
    
    let contentImageView = UIImageView().then{
        $0.contentMode = .scaleAspectFit
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 15
    }
    
    let contentImageViewButton = UIButton()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.disposeBag = DisposeBag()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        self.contentView.addSubViews([
            profileImageView,
            profileNameLabel,
            profileButton,
            socialStackView,
            contentImageView,
            contentImageViewButton
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileImageView.snp.makeConstraints{
            $0.top.leading.equalToSuperview()
            $0.width.height.equalTo(39)
        }
        
        profileButton.snp.makeConstraints{
            $0.top.leading.bottom.equalTo(profileImageView)
            $0.trailing.equalTo(profileNameLabel)
        }
        
        profileNameLabel.snp.makeConstraints{
            $0.centerY.equalTo(profileImageView.snp.centerY)
            $0.leading.equalTo(profileImageView.snp.trailing).offset(10)
        }
        
        socialStackView.snp.makeConstraints{
            $0.trailing.equalToSuperview()
            $0.centerY.equalTo(profileImageView.snp.centerY)
            $0.height.equalTo(20)
        }
        
        //기존엔 정사각형이였지만, 높이를 가변적으로 설정
        contentImageView.snp.makeConstraints{
            $0.top.equalTo(profileImageView.snp.bottom).offset(10)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        contentImageViewButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalTo(contentImageView)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FollowingListViewCell {
    func likeReact(postId:Int, completion:((Bool, Int) -> Void)? = nil) {
        API.likeReact(postId: postId) { model in
            guard let model = model else { return }
            let isLiked = model.data.isLiked
            
            if model.success {
                if let totalLikes = model.data.totalLikes {
                    completion?(isLiked, totalLikes)
                }
            } else {
                AlertManager.shared.show(text: "좋아요 에러 발생", type: .one)
            }
        }
    }
    
    func scrapReact(postId:Int, completion:((Bool) -> Void)? = nil) {
        API.scrapReact(postId: postId) { model in
            guard let model = model else { return }
            let isScrap = model.data.isScrap
            
            if model.success {
                completion?(isScrap)
            } else {
                AlertManager.shared.show(text: "스크랩 에러 발생", type: .one)
            }
        }
    }
}
