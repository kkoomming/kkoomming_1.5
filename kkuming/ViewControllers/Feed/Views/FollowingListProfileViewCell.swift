//
//  FollowingListProfileViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/22.
//

import Foundation
import UIKit
import SnapKit
import Then

class FollowingListProfileViewCell: UICollectionViewCell {
    static let identifier = "FollowingListProfileViewCell"
    
    lazy var profileListView = ProfileListView()
    
    let borderView = BorderView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        [
            profileListView,
            borderView
        ].forEach{
            self.contentView.addSubview($0)
        }
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileListView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(107.5)
        }
        
        borderView.snp.makeConstraints{
            $0.top.equalTo(profileListView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(1)
            $0.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
