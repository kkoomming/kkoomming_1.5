//
//  FollowingListView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/28.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class FollowingListView: UIView {
    var users:[User] = []
    var userMeta:MetaData? = nil
    var items:[Post] = []
    var meta:MetaData? = nil
    
    //페이징
    var page = 1
    var isPaging = false
    
    var leftMargin:CGFloat = 25
    var rightMargin:CGFloat = 25
    
    let layout = UICollectionViewFlowLayout().then{
        $0.scrollDirection = .vertical
        $0.minimumInteritemSpacing = 40
        $0.minimumLineSpacing = 40
    }
    
    lazy var collectionView = UICollectionView(
        frame: CGRect.zero,
        collectionViewLayout: layout
    ).then{
        $0.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        $0.showsVerticalScrollIndicator = false
        $0.isScrollEnabled = true
        $0.register(FollowingListProfileViewCell.self, forCellWithReuseIdentifier: FollowingListProfileViewCell.identifier)
        $0.register(FollowingListViewCell.self, forCellWithReuseIdentifier: FollowingListViewCell.identifier)
        $0.dataSource = self
        $0.delegate = self
    }
    
    var emptyView = ListEmptyView().then{
        $0.isHidden = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    public func configuration(users:[User], userMeta:MetaData, items:[Post], meta:MetaData) {
        clear()
        
        self.users = users
        self.userMeta = userMeta
        
        //MARK: 상단 영역을 위해 추가
        let post = Post(id: 0)
        var list:[Post] = []
        list.append(post)
        list += items
        
        self.items = list
        self.meta = meta
        
        self.page += 1
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    private func clear() {
        page = 1
        isPaging = false
    }
    
    public func add(items:[Post], meta:MetaData) {
        self.items += items
        self.meta = meta
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    
    func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        collectionView.isHidden = bool
    }
    
    private func setupUI() {
        addSubViews([
            collectionView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func updateLike(index:Int, status:Bool, totalLikes:Int){
        self.items[index].isLike = status
        self.items[index].totalLikes = totalLikes
    }
    
    private func updateScrap(index:Int, status:Bool){
        self.items[index].isScrap = status
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension FollowingListView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //FollowingListProfileViewCell 때문에 0이 아닌 1로 해야됨
        if items.count == 1 {
            isEmpty(true)
        } else {
            isEmpty(false)
        }
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let index = indexPath.item
        let item = items[index]
        
        if index == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FollowingListProfileViewCell.identifier, for: indexPath) as! FollowingListProfileViewCell
            
            cell.profileListView.configuration(items: self.users, meta: self.userMeta!)
            
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FollowingListViewCell.identifier, for: indexPath) as! FollowingListViewCell
        
        //아이템 설정
        cell.item = item
        
        //프로필 이미지
        cell.profileImageView.kf.setImage(with: item.owner?.avatar?.toURL, options: [.loadDiskFileSynchronously])
        
        //프로필 이름
        cell.profileNameLabel.text = item.owner?.nickname
        
        //프로필 선택
        cell.profileButton.rx.tap.bind{
            print("profileButton")
            App.push(ProfileVC(userId: item.owner?.id ?? 0 ))
        }.disposed(by: cell.disposeBag)
        
        //소셜 영역 설정
        cell.socialStackView.configuration(item: item)
        
        //좋아요 클릭
        cell.socialStackView.likeButton.rx.tap.bind{ [weak self] in
            print("likeButton")
            cell.likeReact(postId: item.id) { [weak self] status, totalLikes in
                self?.updateLike(index: index, status: status, totalLikes: totalLikes)
                self?.collectionView.reloadData()
            }
        }.disposed(by: cell.disposeBag)
        
        //스크랩 클릭
        cell.socialStackView.bookmarkButton.rx.tap.bind{ [weak self] in
            print("bookmarkButton")
            cell.scrapReact(postId: item.id) { [weak self] bool in
                self?.updateScrap(index: index, status: bool)
                self?.collectionView.reloadData()
            }
        }.disposed(by: cell.disposeBag)
        
        //컨텐츠 이미지
        cell.contentImageView.kf.setImage(with: item.thumbnailImage?.toURL, options: [.loadDiskFileSynchronously])
        
        //컨텐츠 선택
        cell.contentImageViewButton.rx.tap.bind{
            print("contentImageViewButton")
            let vc = ContentDetailVC()
            vc.configuration(postData: item, postType: .POST_FOLLOWING)
            vc.getDetailPost() {
                App.push(vc)
            }
        }.disposed(by: cell.disposeBag)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if indexPath.item == 0 {
            return CGSize(width: collectionView.frame.width, height: 107.5)
        } else {
            //이미지 비율과 맞춰서 진행하기로함
            let width = collectionView.frame.width - (leftMargin + rightMargin)
            var height:CGFloat = 0
            
            let imageView = UIImageView()
            imageView.kf.setImage(with: self.items[indexPath.item].thumbnailImage?.toURL, options: [.loadDiskFileSynchronously], completionHandler: { _ in
                let image = imageView.image
                if let image = image {
                    //49는 프로필과 소셜 영역
                    height = 49 + ((width * image.size.height) / image.size.width )
                }
            })
            
            return CGSize(width: collectionView.frame.width - (leftMargin + rightMargin), height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == self.items.count - 1 {
            guard !isPaging else { return }
            self.isPaging = true
            
            self.getFollowingMoreData()
        }
    }
}

//MARK: - API
extension FollowingListView {
    func getFollowingMoreData() {
        API.getSomeoneFollowingPost(offset: page) { [weak self] model in
            guard let strongSelf = self,
                  let model = model,
                  let data = model.data,
                  let posts = data.posts?.posts,
                  let postsMeta = data.posts?.metaData
            else {
                print("getSomeoneFollowingPost 데이터 확인 필요")
                return
            }
            
            strongSelf.add(items: posts, meta: postsMeta)
            
            if strongSelf.page >= postsMeta.totalPages {
                strongSelf.isPaging = true
            } else {
                strongSelf.isPaging = false
            }
            
            strongSelf.page += 1
        }
    }
}

