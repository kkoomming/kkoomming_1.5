//
//  ProfileListView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/28.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class ProfileListView: UIView {
    var items:[User] = []
    var meta:MetaData? = nil
    
    //페이징
    var page = 1
    var isPaging = false
    
    let layout = UICollectionViewFlowLayout().then{
        $0.scrollDirection = .horizontal
        $0.minimumInteritemSpacing = 10
    }
    
    lazy var collectionView = UICollectionView(
        frame: CGRect.zero,
        collectionViewLayout: layout
    ).then{
        $0.contentInset = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
        $0.showsHorizontalScrollIndicator = false
        $0.register(ProfileListViewCell.self, forCellWithReuseIdentifier: ProfileListViewCell.identifier)
        $0.dataSource = self
        $0.delegate = self
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    public func configuration(items:[User], meta:MetaData){
        self.items = items
        self.meta = meta
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    public func add(items:[User], meta:MetaData) {
        self.items += items
        self.meta = meta
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    private func setupUI() {
        addSubViews([
            collectionView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ProfileListView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileListViewCell.identifier, for: indexPath) as! ProfileListViewCell
        let index = indexPath.item
        let item = items[index]
        
        cell.profileImageView.kf.setImage(with: item.avatar?.toURL, options: [.loadDiskFileSynchronously])
        cell.profileNameLabel.text = item.nickname
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        let item = items[index]
        
        App.push(ProfileVC(userId: item.id ?? 0))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 69, height: 107.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == self.items.count - 1 {
            print("페이징 처리")
            guard !isPaging else { return }
            self.isPaging = true
            
            if self.items.count >= 10 {
                self.getFollowingMoreData()
            }
        }
    }
}

//MARK: - API
extension ProfileListView {
    func getFollowingMoreData() {
        API.getSomeoneFollowingPost(offset: page) { [weak self] model in
            guard let strongSelf = self,
                  let model = model,
                  let data = model.data,
                  let users = data.following?.users,
                  let userMeta = data.following?.metaData
            else {
                print("getSomeoneFollowingPost 데이터 확인 필요")
                return
            }
            
            strongSelf.add(items: users, meta: userMeta)
            
            if strongSelf.page >= userMeta.totalPages {
                strongSelf.isPaging = true
            } else {
                strongSelf.isPaging = false
            }
            
            strongSelf.page += 1
        }
    }
}
