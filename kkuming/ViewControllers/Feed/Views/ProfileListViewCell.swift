//
//  ProfileListViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/28.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class ProfileListViewCell: UICollectionViewCell {
    static let identifier = String(describing: ProfileListViewCell.self)
    
    let profileImageView = UIImageView().then{
        $0.contentMode = .scaleToFill
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 69/2
    }
    
    let profileNameLabel = UILabel().then{
        $0.font = getFont(size: 10, type: .EBold)
        $0.textColor = .black
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }

    private func setupUI() {
        contentView.addSubViews([
            profileImageView,
            profileNameLabel
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileImageView.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(69)
        }
        
        profileNameLabel.snp.makeConstraints{
            $0.top.equalTo(profileImageView.snp.bottom).offset(8)
            $0.centerX.equalTo(profileImageView.snp.centerX)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
