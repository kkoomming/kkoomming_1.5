//
//  WebVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/09.
//

import UIKit
import WebKit
import SnapKit
import Then

class WebVC: BaseVC {
    var titleString:String = ""
    var urlString:String = ""
    var isTopHidden:Bool = false
    
    ///나중에 사파리로 열 때 사용
    var isLoginTest:Bool = false

    var topView:TopView!

    let preferences = WKPreferences().then{
        $0.javaScriptCanOpenWindowsAutomatically = true
    }

    lazy var contentController = WKUserContentController().then{
        let controller = $0
        [
            "login"
        ].forEach{
            controller.add(self, name: $0)
        }
    }

    lazy var config = WKWebViewConfiguration().then{
        $0.processPool = App.processPool
        $0.preferences = preferences
        $0.userContentController = contentController
    }

    lazy var webView = WKWebView(frame: CGRect.zero, configuration: config).then{
        $0.allowsBackForwardNavigationGestures = true
        $0.navigationDelegate = self
        $0.uiDelegate = self
    }

    init(title titleString:String,url urlString:String, isTopHidden:Bool = false) {
        super.init(nibName: nil, bundle: nil)
        self.titleString = titleString
        self.urlString = urlString
        self.isTopHidden = isTopHidden
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()

        if urlString != "" {
            load(urlString: urlString)
        }

        if isTopHidden {
            topView.isHidden = true
            topView.snp.updateConstraints{
                $0.height.equalTo(0)
            }
        }
    }

    public func load(urlString:String){
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)

            webView.load(request)
            
            //MARK: 임시 로그인 할 때 사용, smemno은 갱신형 토큰이므로 임시 로그인 하려면 새로 발급 받아서 하는게 안전함
            //토큰 발급 링크 : https://mobile.kyobobook.co.kr/login?partnerCode=KMG
//            login(smemno: "NdaGHe64INQnrLoZNabd#g==")
        }
    }

    private func setupUI() {
        setupView()

        addSubViews([
            topView,
            webView
        ])

        setupConstraints()
    }

    private func setupView(){
        topView = TopView(type: .back, title: titleString)
    }

    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }

        webView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    private func setupRx() {

    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func login(smemno clientId:String, loginMethod:LoginType = .NORMAL) {
        AppManager.shared.login(clientId: clientId, loginMethod: loginMethod)
    }
}

extension WebVC: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let bridgeName = message.name

        switch bridgeName {
        case "login":
            let smemno = message.body as! String
            //MARK: 실서버 연동시 여기서 smemno 받음 (브릿지 사용하면 이거로 사용됨)
            login(smemno: smemno)
            break
        default:
            print("userContentController default")
            break
        }
    }


}

extension WebVC: WKUIDelegate, WKNavigationDelegate {
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        
        return nil
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {

            if url.scheme == "kkoomming" {
                if (url.host ?? "" == "auth" && url.path == "/login") {
                    App.pop()
                }
            }

        }
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let url = webView.url?.absoluteString, url == "https://mobile.kyobobook.co.kr/login?partnerCode=KMG" {
            
            //스크립트 활성화
            let script = """
            function removeElementsByClass(className){
                const elements = document.getElementsByClassName(className);
                while(elements.length > 0){
                    elements[0].parentNode.removeChild(elements[0]);
                }
            }
            
            removeElementsByClass('user_svc')
            """
            
            webView.evaluateJavaScript(script)
        }
        
        //MARK: 로그인 외부 페이지 예외 처리
        //isLoginTest가 true고, 링크가  https://mobile.kyobobook.co.kr/login?partnerCode=KMG 가 아니라면 실행
        if let url = webView.url?.absoluteString, url != "https://mobile.kyobobook.co.kr/login?partnerCode=KMG" && isLoginTest {
            self.isLoginTest = false
            App.pop()
            openSafari(url: url)
        }
        
        if let url = webView.url?.absoluteString, url == "https://kkoomming.net/login-test.html" {
            self.isLoginTest = true
        }
        
        let urlString = "https://kkoomming.co.kr/login/callback?smemno="
        if let url = webView.url?.absoluteString, url.contains(urlString) {
            if let smemno = url.replacingOccurrences(of: urlString, with: "")
                .removingPercentEncoding {
                login(smemno: smemno)
            }
        }
    }
}
