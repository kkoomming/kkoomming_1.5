//
//  KCDoingVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/26.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip

class KCDoingVC: BaseVC, IndicatorInfoProvider {
    let itemInfo = IndicatorInfo(title: "진행중")
    
    let emptyView = UIView().then{
        $0.isHidden = true
        let imageView = UIImageView(image: UIImage(named: "emptyKM"))
        
        $0.addSubview(imageView)
        imageView.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
            $0.width.height.equalTo(164)
        }
    }
    
    let listView = KCListView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        listView.getEventsList()
    }
    
    private func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        listView.isHidden = bool
    }
    
    private func setupUI() {
        addSubViews([
            listView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        listView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension KCDoingVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
