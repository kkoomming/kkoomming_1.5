//
//  KCListView.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/09.
//

import UIKit
import SnapKit
import Then
import Kingfisher

class KCListView: UIView {
    var items:[EventData] = []
    var isDone:Bool = false
    
    var periodType:PeriodType!
    
    //MARK: 페이징
    var page = 1
    var isPaging = false
    var isMain = true
    
    lazy var tableView = UITableView().then{
        $0.register(KCListViewCell.self, forCellReuseIdentifier: KCListViewCell.identifier)
        $0.delegate = self
        $0.dataSource = self
        $0.showsVerticalScrollIndicator = false
        $0.separatorStyle = .none
        $0.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 35))
    }
    
    init(isDone:Bool = false) {
        super.init(frame: CGRect.zero)
        self.isDone = isDone
        
        if isDone {
            periodType = .CLOSE
        } else {
            periodType = .ING
        }
        
        setupUI()
    }
    
    public func configuration(items:[EventData]) {
        self.items = items
        tableView.reloadData()
    }
    
    public func add(items:[EventData]) {
        self.items += items
        tableView.reloadData()
    }
    
    private func setupUI() {
        self.addSubview(tableView)
        setupConstraints()
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension KCListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: KCListViewCell.identifier, for: indexPath) as! KCListViewCell
        let index = indexPath.row
        let item = items[index]
        
        //진행중, 마감 분기처리
        if isDone {
            cell.dimView.isHidden = false
        } else {
            cell.dimView.isHidden = true
        }
        
        //썸네일 지정
        if let files = item.files {
            cell.thumbImageView.kf.setImage(with: files.url.toURL, options: [.loadDiskFileSynchronously])
        }
        
        //이벤트 기간 지정
        let startString = dateToFormat(string: item.startAt, dateFormat: .post)
        let endString = dateToFormat(string: item.endAt, dateFormat: .post)
        cell.dateLabel.text = startString + " ~ " + endString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !isDone {
            let item = items[indexPath.item]
            let id = item.id
            let vc = TagDetailVC(topTitleString: item.title ?? "",
                                 id: id,
                                 type:.CHALLENGE,
                                 isUseBanner: true)
            App.push(vc)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.items.count - 1 {
            guard !isPaging else { return }
            self.isPaging = true
            
            self.getEventsList()
        }
    }
}

extension KCListView {
    //리스트 가져오기
    func getEventsList() {
        //MARK: 테스트용
        /*
        if let data = TestFactory.createModel(type: EventData.self) {
            self.configuration(items: [data])
        }
        return
         */
        API.getEventsList(type: .CHALLENGE, periodType: self.periodType, offset: page) { model in
            guard let items = model?.data?.events, let meta = model?.data?.metaData else { return }
            
            if self.isMain {
                self.configuration(items: items)
                self.isMain = false
            } else {
                self.add(items: items)
            }
            
            if self.page >= meta.totalPages {
                self.isPaging = true
            } else {
                self.isPaging = false
            }
            
            self.page += 1
        }
    }
}
