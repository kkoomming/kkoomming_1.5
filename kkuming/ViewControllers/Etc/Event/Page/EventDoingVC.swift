//
//  EventDoingVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/08.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip

class EventDoingVC: BaseVC, IndicatorInfoProvider {
    let itemInfo = IndicatorInfo(title: "진행중")
    var items:[BannerData] = []
    
    let emptyView = UIView().then{
        $0.isHidden = true
        let imageView = UIImageView(image: UIImage(named: "emptyKM"))
        
        $0.addSubview(imageView)
        imageView.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
            $0.width.height.equalTo(164)
        }
    }
    
    let listView = EventListView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        listView.getEventsList(){ meta in
            if meta.totalPages == 0 {
                self.isEmpty(true)
            } else {
                self.isEmpty(false)
            }
        }
    }
    
    private func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        listView.isHidden = bool
    }
    
    private func setupUI() {
        addSubViews([
            listView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        listView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(30)
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-25)
            $0.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension EventDoingVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}
