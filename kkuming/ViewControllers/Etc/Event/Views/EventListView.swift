//
//  EventListView.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/08.
//

import UIKit
import SnapKit
import Then
import Kingfisher

class EventListView: UIView {
    var items:[BannerData] = []
    var isDone:Bool = false
    
    var periodType:PeriodType!
    
    //MARK: 페이징
    var page = 1
    var isPaging = false
    var isMain = true
    
    lazy var tableView = UITableView().then{
        $0.register(EventListViewCell.self, forCellReuseIdentifier: EventListViewCell.identifier)
        $0.delegate = self
        $0.dataSource = self
        $0.showsVerticalScrollIndicator = false
        $0.separatorStyle = .none
    }
    
    init(isDone:Bool = false) {
        super.init(frame: CGRect.zero)
        self.isDone = isDone
        
        if isDone {
            periodType = .CLOSE
        } else {
            periodType = .ING
        }
        
        setupUI()
    }
    
    public func configuration(items:[BannerData]) {
        self.items = items
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    public func add(items:[BannerData]) {
        self.items += items
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    private func setupUI() {
        
        self.addSubview(tableView)
        setupConstraints()
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension EventListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EventListViewCell.identifier, for: indexPath) as! EventListViewCell
        let index = indexPath.row
        let item = items[index]
        
        if isDone {
            cell.dimView.isHidden = false
        } else {
            cell.dimView.isHidden = true
        }
        
        //썸네일 지정
        if let url = item.subImage?.url {
            cell.thumbImageView.kf.setImage(with: url.toURL, options: [.loadDiskFileSynchronously])
        }
        
        //이벤트 기간 지정
        let startString = dateToFormat(string: item.startAt, dateFormat: .post)
        let endString = dateToFormat(string: item.endAt, dateFormat: .post)
        cell.dateLabel.text = startString + " ~ " + endString
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == self.items.count - 1) {
            guard !isPaging else { return }
            self.isPaging = true
            self.getEventsList()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.items[indexPath.row]
        
        if let linkUrl = data.linkUrl {
            let vc = WebVC(title: data.webViewTitle ?? "", url: linkUrl)
            AppManager.shared.push(vc)
        }
    }
}

extension EventListView {
    //리스트 가져오기
    func getEventsList(completion:((MetaData)->Void)? = nil) {
        API.getBannerList(exposureType: .BANNER, periodType: self.periodType, offset: page) { model in
            guard let items = model?.data.slides,
                  let meta = model?.data.metaData else { return }
            
            if self.isMain {
                self.configuration(items: items)
                self.isMain = false
            } else {
                self.add(items: items)
            }
            
            if self.page >= meta.totalPages {
                self.isPaging = true
            } else {
                self.isPaging = false
            }
            
            self.page += 1
            
            completion?(meta)
        }
    }
}
