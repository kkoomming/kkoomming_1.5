//
//  EventListViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/08.
//

import UIKit
import SnapKit
import Then

class EventListViewCell: UITableViewCell {
    static let identifier = "EventListViewCell"
    
    let thumbImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 15
    }
    
    let dimView = UIView().then{
        $0.isHidden = true
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 15
        $0.backgroundColor = .black.withAlphaComponent(0.5)
    }
    
    let dateLabel = UILabel().then{
        $0.textColor = RGB(red: 154, green: 154, blue: 154)
        $0.font = getFont(size: 10)
        $0.text = "2023.04.26 - 2023.09.26"
    }
    
    let spacingView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
    }
    
    private func setupUI() {
        self.selectionStyle = .none
        
        contentView.addSubViews([
            thumbImageView,
            dimView,
            dateLabel,
            spacingView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        let width = (UIScreen.main.bounds.width - 50)
        let height = width * 90 / 327
        thumbImageView.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(height)
        }
        
        dimView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalTo(thumbImageView)
        }
        
        dateLabel.snp.makeConstraints{
            $0.top.equalTo(thumbImageView.snp.bottom).offset(8)
            $0.leading.trailing.equalToSuperview()
        }
        
        spacingView.snp.makeConstraints{
            $0.height.equalTo(30)
            $0.top.equalTo(dateLabel.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
