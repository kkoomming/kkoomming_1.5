//
//  EventVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/08.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class EventVC: BaseVC {
    let disposeBag = DisposeBag()
    
    let topView = TopView(type: .back, title: "이벤트")
    
    let pagerVC = EventPagerVC()
    lazy var pagerView:UIView! = pagerVC.view
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AlertManager.shared.updateStatus(view: topView)
    }
    
    private func setupUI() {
        addSubViews([
            topView,
            pagerView
        ])
        addChild(pagerVC)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        pagerView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
}
