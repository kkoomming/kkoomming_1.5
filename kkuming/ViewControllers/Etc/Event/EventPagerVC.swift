//
//  EventPagerVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/08.
//

import UIKit
import Then
import SnapKit
import XLPagerTabStrip

class EventPagerVC: PagerVC {
    let doingVC = EventDoingVC()
    let doneVC = EventDoneVC()
    
    let borderView = BorderView()
    
    override func viewDidLoad() {
        configuration()
        
        super.viewDidLoad()
        
        self.view.addSubview(borderView)
        borderView.snp.makeConstraints{
            $0.bottom.equalTo(buttonBarView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(1)
        }
    }
    
    private func configuration() {
        settings.style.buttonBarBackgroundColor = .white
        settings.style.buttonBarItemBackgroundColor = .white
        settings.style.selectedBarBackgroundColor = selectedColor
        settings.style.buttonBarItemFont = getFont(size: 15, type: .bold)
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = unSelectedColor
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0

        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            guard let strongSelf = self else { return }
            
            oldCell?.label.textColor = strongSelf.unSelectedColor
            newCell?.label.textColor = UIColor.black
        }
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [doingVC, doneVC]
    }
}
