//
//  TagDetailVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/06.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Alamofire
import Kingfisher

class TagDetailVC: BaseVC {
    let disposeBag = DisposeBag()
    
    //페이징
    var page = 1
    var isPaging = false
    var isMain = true
    
    //최상단 배너 사용 여부
    var isUseBanner:Bool = false
    
    private var topTitleString:String = ""
    
    private var items:[Post] = []
    private var id:Int = 0
    private var type:BannerPageType!
    
    lazy var topView = TopView(type: .feed, title: topTitleString)
    lazy var scrollView = UIScrollView().then{
        $0.delegate = self
    }
    let stackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
    }
    
    let twoListView = VariousListView(type: .two).then{
        $0.collectionView.isScrollEnabled = false
        $0.collectionView.isPagingEnabled = false
        $0.collectionView.contentInset = UIEdgeInsets(top: 10, left: $0.leftMargin, bottom: 10, right: $0.rightMargin)
    }
    
    init(topTitleString:String, isUseBanner:Bool = false){
        super.init(nibName: nil, bundle: nil)
        self.topTitleString = topTitleString
        self.isUseBanner = isUseBanner
        
    }
    
    init(topTitleString:String, id:Int, type:BannerPageType,isUseBanner:Bool) {
        super.init(nibName: nil, bundle: nil)
        self.topTitleString = topTitleString
        self.id = id
        self.type = type
        self.isUseBanner = isUseBanner
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AlertManager.shared.updateStatus(view: topView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isUseBanner {
            //꾸밍챌린지 상세
            getEventDetail(id: id)
        } else {
            getList(tag: topTitleString)
        }
        
        setupUI()
    }
    
    private func configuration(items:[Post], postType:PostDetailType = .POST_BY_TAG) {
        twoListView.configuration(items: items, postType: postType)
        
        updateTwoListViewHeight()
    }
    
    private func add(items:[Post]) {
        twoListView.add(items: items)
        twoListView.collectionView.reloadData()
        updateTwoListViewHeight()
    }
    
    private func updateTwoListViewHeight() {
        twoListView.layoutIfNeeded()
        let height = twoListView.collectionView.contentSize.height
        twoListView.snp.remakeConstraints{
            $0.height.equalTo(height)
        }
    }
    
    private func setupUI() {
        addSubViews([
            topView,
            scrollView,
        ])
        
        setupConstraints()
        setupStackView()
        
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        scrollView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupStackView() {
        scrollView.addSubview(stackView)
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
    }
    
    private func setupListView() {
        [
            twoListView
        ].forEach{
            stackView.addArrangedSubview($0)
        }
        
        setupListViewConstraints()
    }
    
    private func setupListViewConstraints() {
        twoListView.snp.makeConstraints{
            $0.height.equalTo(0)
        }
    }
    
    private func setupTopBanner(item:EventData) {
        stackView.spacing = 35
        
        let bannerStackView = UIStackView().then{
            $0.axis = .vertical
            $0.alignment = .fill
            $0.distribution = .fill
            $0.spacing = 20
        }
        
        let moreStackView = UIStackView().then{
            $0.axis = .vertical
            $0.alignment = .fill
            $0.distribution = .fill
            $0.layoutMargins = UIEdgeInsets(top: 0, left: 25, bottom: 0, right: 25)
            $0.isLayoutMarginsRelativeArrangement = true
        }
        
        //배너 영역
        let bannerView = UIView()
        let bannerImageView = UIImageView().then{
            if let files = item.files2 {
                $0.kf.setImage(with: files.url.toURL, options: [.loadDiskFileSynchronously])
            }
        }
        let bannerButton = UIButton().then{
            $0.rx.tap.bind{
                print("bannerButton")
            }.disposed(by: disposeBag)
        }
        
        //관련 이벤트 더보기
        let moreView = UIView().then{
            $0.clipsToBounds = true
            $0.layer.masksToBounds = true
            $0.layer.cornerRadius = 10
            $0.layer.borderWidth = 1
            $0.layer.borderColor = RGB(red: 210, green: 210, blue: 210).cgColor
        }
        let moreLabel = UILabel().then{
            $0.text = "관련 이벤트 더보기"
        }
        
        lazy var moreButton = UIButton().then{
            $0.rx.tap.bind{
                print("moreButton")
                let vc = WebVC(title: "", url: item.linkUrl ?? "")
                App.push(vc)
            }.disposed(by: disposeBag)
        }
        
        [
            bannerImageView,
            bannerButton
        ].forEach {
            bannerView.addSubview($0)
        }
        
        [
            moreLabel,
            moreButton
        ].forEach {
            moreView.addSubview($0)
        }
        
        bannerImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        bannerButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        moreLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        moreButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        moreStackView.addArrangedSubview(moreView)
        bannerStackView.addArrangedSubview(bannerView)
        bannerStackView.addArrangedSubview(moreStackView)
        
        let challengeHeight = (UIScreen.main.bounds.width * 230 / 375)
        bannerView.snp.makeConstraints{
            $0.height.equalTo(challengeHeight)
        }
        
        moreView.snp.makeConstraints{
            $0.height.equalTo(43)
        }
        
        stackView.addArrangedSubview(bannerStackView)
    }
}

extension TagDetailVC:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        let contentSizeHeight = scrollView.contentSize.height
        let currentViewHeight = scrollView.frame.height
        
        if position > (contentSizeHeight - currentViewHeight - 100) {
            guard !isPaging else { return }
            self.isPaging = true
            print("paging")
            
            if self.isUseBanner {
                self.getEventDetail(id: self.id)
            } else {
                self.getList(tag: self.topTitleString)
            }
        }
    }
}

extension TagDetailVC {
    //일반 태그 가져오기
    func getList(tag:String) {
        self.isPaging = true
        let keyword = tag.trimmingCharacters(in: ["#"])
        let param:Parameters = [
           "offset" : page,
           "textSearch" : keyword
        ]
        
        print("getList 현재 페이지 : ", page)
        
        API.getTagList(param: param) { model in
            guard let model = model, let posts = model.data?.posts, let meta = model.data?.metaData else {
                print("getTagList nil")
                return
            }
            
            if self.isMain {
                self.setupListView()
                self.configuration(items: posts)
                self.isMain = false
            } else {
                self.add(items: posts)
            }
            
            if self.page >= meta.totalPages {
                self.isPaging = true
            } else {
                self.isPaging = false
            }
            
            self.page += 1
        }
    }
    
    //꾸밍챌린지 상세
    func getEventDetail(id:Int) {
        self.isPaging = true
        API.eventsDetail(id: id, type: .CHALLENGE, offset: page) { model in
            guard let events = model?.data?.events,
                  let posts = model?.data?.postsList?.posts,
                  let meta = model?.data?.postsList?.metaData else { return }
            
            if self.isMain {
                self.setupTopBanner(item: events)
                self.setupListView()
                self.configuration(items: posts)
                self.isMain = false
            } else {
                self.add(items: posts)
            }
            
            if self.page >= meta.totalPages {
                self.isPaging = true
            } else {
                self.isPaging = false
            }
            
            self.page += 1
        }
    }
}
