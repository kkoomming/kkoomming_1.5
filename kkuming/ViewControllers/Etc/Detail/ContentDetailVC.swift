//
//  ContentDetailVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/06.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Alamofire
import FirebaseDynamicLinks

class ContentDetailVC: BaseVC {
    let disposeBag = DisposeBag()
    //ContentDetailVC를 Load하기 전에 configuration에서 미리 설정할 때 사용
    var postData:Post? = nil
    var postType:PostDetailType = .NONE
    
    //처음 들어왔는지 확인
    var isFirst:Bool = true
    
    //페이징
    var page = 2 //MARK: getDetailPost하면 댓글이 무조건 1페이지를 불러오기 때문에 2로 설정
    var isPaging = false
    
    var item:PostData? = nil {
        didSet{
            configuration()
        }
    }
    
    var updateItemHandler:((Post) -> ())? = nil
    
    //입력 창 원래 높이
    var originalInputViewHeight:CGFloat = 81
    
    lazy var bannerItems:[UIImage] = []
    
    var imageHeight:CGFloat = UIScreen.main.bounds.width
    var HEIGHT:CGFloat = 0 {
        didSet {
            commentListView.snp.updateConstraints{
                $0.height.equalTo(self.HEIGHT)
            }
        }
    }
    
    lazy var topView = TopView(type: .detail).then{
        $0.delegate = self
    }
    
    lazy var scrollView = UIScrollView().then{
        $0.delegate = self
    }
    let contentStackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
    }
    
    lazy var inputCommentView = InputCommentView().then{
        $0.commentTextView.delegate = self
        $0.commentTextView.textColor = UIColor.placeholderColor
        $0.commentTextView.text = $0.placeholderString
    }
    let bottomSafeAreaView = BackgroundView(RGB(red: 234, green: 234, blue: 234))
    
    lazy var imageListView = DirectionBannerView().then{
        $0.delegate = self
    }
    
    lazy var profileView = DetailProfileView().then{
        $0.shareHandler = { [weak self] in
            self?.createDynamicLink(postId: self?.postData?.id ?? 0)
        }
    }
    
    let borderStateView = UIView()
    let socialStackView = SocialStackView()
    let dateTimeLabel = UILabel().then{
        $0.font = getFont(size: 12)
        $0.text = "2022.02.18"
        $0.textColor = RGB(red: 185, green: 185, blue: 185)
    }
    
    let contentLabel = PaddingLabel(top: 0, left: 25, right: 25, bottom: 0).then{
        $0.lineBreakMode = .byCharWrapping
        $0.numberOfLines = 0
        $0.font = getFont(size: 12)
        $0.text = "비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸비비드한 오늘의 다꾸"
        $0.textColor = .textGrayColor
    }
    
    let tagView = TagView().then{
        $0.isDetail = true
        $0.cornerRadius = 0
        $0.textColor = .textGrayColor
        $0.textFont = getFont(size: 10, type: .bold)
        $0.borderWidth = 0
        $0.tagBackgroundColor = RGB(red: 250, green: 250, blue: 250)
        $0.cellHeight = 24
        $0.leftMargin = 25
        $0.rightMargin = 25
        $0.lineSpacing = 6
    }
    
    let borderContentAndCommentView = BorderView()
    
    lazy var commentListTitleStackView = UIStackView().then{
        $0.addArrangedSubview(commentListTitleLabel)
        $0.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    let commentListTitleLabel = UILabel().then{
        $0.font = getFont(size: 14, type: .EBold)
    }
    
    let commentListView = CommentListView()
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tagView.updateTagViewLayout()
    }
    
    deinit {
        print("ContentDetailVC deinit")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //답변 화면 때문에 여기에 설정
        KkumingKeyboardManager.shared.setKeyboardNotification(
            parentView: view,
            keyboardView: inputCommentView,
            scrollView: scrollView,
            registKey: "commentDetail"
        )
        
        commentListView.tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        inputCommentView.commentTextView.text = inputCommentView.placeholderString
        inputCommentView.commentTextView.textColor = UIColor.placeholderColor
        inputCommentView.commentTextView.resignFirstResponder()
        
        if !isFirst {
            getDetailPost()
        } else {
            isFirst = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //답변 화면 때문에 여기에 설정
        KkumingKeyboardManager.shared.removeKeyboardNotification(registKey: "commentDetail")
        commentListView.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){
            if let _ = change?[.newKey] {
                let contentHeight: CGFloat = commentListView.tableView.contentSize.height
                self.HEIGHT = contentHeight
            }
        }
    }
}
//MARK: - Public
extension ContentDetailVC {
    public func configuration() {
        guard let item = item, let data = item.data else {
            print("ContentDetailVC configuration item과 data가 올바르지 않습니다.")
            return
        }
        bannerItems.removeAll()
        
        //상단 이미지 배너
        if let urls = data.post.album {
            DispatchQueue.global().async {
                for url in urls {
                    do{
                        let data = try Data(contentsOf: URL(string: url)!)
                        let image = UIImage(data: data)!
                        self.bannerItems.append(image)
                    }catch{
                        print("ContentDetailVC configuration ERROR")
                    }
                }
                self.imageListView.configuration(items: self.bannerItems)
            }
        }
        
        //프로필 영역
        profileView.configuration(item: data)
        
        //소셜 영역
        socialStackView.configuration(item: data.post) {[weak self] post in
            self?.item?.data?.post = post
        }
        
        //날짜
        if let createdAt = data.post.createdAt {
            dateTimeLabel.text = dateToFormat(string: createdAt, dateFormat: .post)
        }
        
        //컨텐츠 내용
        if let description = data.post.description {
            contentLabel.text = description
        }
        
        //태그
        if let tags = data.post.hashTags {
            tagView.configuration(items: tags)
        }
        
        //댓글 제목
//        let metaData = data.commentsList.metaData
//        let replyMetaData = data.commentsList.replysCommentsMetaData
//        let count = metaData.totalRecords + replyMetaData.totalRecords
        if let count = data.post.totalComments {
            commentListTitleLabel.text = "댓글 \(count)"
        }
        
        //댓글 영역
        let comments = data.commentsList.comments
        commentListView.refreshHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.getDetailPost()
        }
        commentListView.configuration(items: comments)
    }
    
    //메인에서 쓰고있음.
    public func configuration(postData:Post, postType:PostDetailType) {
        self.postData = postData
        self.postType = postType
        
        //좋아요, 북마크를 위해 추가
//        self.socialStackView.postId = postData.id
    }
    
}

//MARK: - UI
extension ContentDetailVC {
    private func setupUI() {
        //MARK: 여기서 스택 뷰 설정
        [
            imageListView,
            profileView,
            borderStateView,
            createSpacingView(height: 22),
            contentLabel,
            createSpacingView(height: 37),
            tagView,
            createSpacingView(height: 55),
            borderContentAndCommentView,
            createSpacingView(height: 30),
            commentListTitleStackView,
            createSpacingView(height: 10),
            commentListView
        ].forEach{
            contentStackView.addArrangedSubview($0)
        }
        scrollView.addSubview(contentStackView)
        
        addSubViews([
            topView,
            scrollView,
            inputCommentView,
            bottomSafeAreaView
        ])
    
        setupBoardStateView()
        setupConstraints()
        setupRx()
        setupStackViewContraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        scrollView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
        }
        
        contentStackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
        
        inputCommentView.snp.makeConstraints{
            $0.top.equalTo(scrollView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(self.view.safeAreaLayoutGuide)
            $0.height.equalTo(self.originalInputViewHeight)
        }
        
        bottomSafeAreaView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupRx() {
        inputCommentView.commentInputButton.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else { return }
            guard let postData = strongSelf.postData else {
                print("getDetailPost item 설정 안됨")
                return
            }
            
            let content = strongSelf.inputCommentView.getText()
            print("댓글 입력한 텍스트 : ", content)
            
            guard content != "" else {
                AlertManager.shared.show(text: "댓글을 입력해 주세요", type: .one)
                return
            }
            
            let param:Parameters = [
                "postId" : postData.id,
                "content" : content,
            ]
            
            API.postComment(param: param) { model in
                guard let model = model else {
                    return
                }
                
                if model.success {
                    print("success : ", model.data?.content)
                    strongSelf.clearText()
                    strongSelf.getDetailPost() {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3){
                            //스크롤 최하단 처리
                            strongSelf.view.endEditing(true)
                            let y = strongSelf.scrollView.contentSize.height - strongSelf.scrollView.frame.height
                            strongSelf.scrollView.setContentOffset(CGPoint(x: 0, y: y), animated: true)
                        }
                    }
                }
            }
        }.disposed(by: disposeBag)
        
        //소셜 좋아요 클릭
        socialStackView.likeButton.rx.tap.bind{ [weak self] in
            print("likeButton")
            self?.socialStackView.likeReact(postId: self?.postData?.id ?? 0)
        }.disposed(by: disposeBag)
        
        //스크랩 클릭
        socialStackView.bookmarkButton.rx.tap.bind{ [weak self] in
            print("bookmarkButton")
            self?.socialStackView.scrapReact(postId: self?.postData?.id ?? 0)
        }.disposed(by: disposeBag)
    }
    
    private func setupStackViewContraints() {
        imageListView.snp.makeConstraints{
            $0.height.equalTo(imageHeight)
        }
        
        profileView.snp.makeConstraints{
            $0.height.equalTo(70)
        }
        
        borderStateView.snp.makeConstraints{
            $0.height.equalTo(32.5)
        }
        
        tagView.snp.makeConstraints{
            $0.height.equalTo(100)
        }
        
        borderContentAndCommentView.snp.makeConstraints{
            $0.height.equalTo(1)
        }
        
        //MARK: viewDidLayoutSubviews에서 또 수정함
        commentListView.snp.makeConstraints{
            $0.height.equalTo(commentListView.getListViewHeight())
        }
    }
    
    private func setupBoardStateView() {
        borderStateView.addSubViews([
            socialStackView,
            dateTimeLabel
        ])
        
        setupBoardStateViewConstraints()
    }
    
    private func setupBoardStateViewConstraints() {
        socialStackView.snp.makeConstraints{
            $0.top.equalTo(profileView.snp.bottom).offset(12.5)
            $0.leading.equalTo(25)
            $0.height.equalTo(20)
        }
        
        dateTimeLabel.snp.makeConstraints{
            $0.trailing.equalToSuperview().offset(-25)
            $0.bottom.equalTo(socialStackView.snp.bottom)
        }
    }
    
    private func clearText() {
        self.inputCommentView.clearText()
        
        inputCommentView.snp.updateConstraints{
            $0.height.equalTo(originalInputViewHeight)
        }
    }
    
}

//MARK: - API
extension ContentDetailVC {
    @objc
    public func getDetailPost(completion:(()->Void)? = nil) {
        self.isPaging = false
        self.page = 2 //MARK: getDetailPost하면 댓글이 무조건 1페이지를 불러오기 때문에 2로 설정
        
        guard let postData = postData else {
            print("getDetailPost item 설정 안됨")
            return
        }
        
        let param:Parameters = [
            "id": postData.id,
            "type": self.postType.rawValue
        ]
        
        API.getDetailPost(param: param) { response, data in
            if let model = response, let ownerId = model.data?.post.owner?.id {
                self.item = model
                
                if model.success == true {
                    if ownerId == UserManager.shared.getUserId() {
                        //작성자
                        self.topView = TopView(type: .myDetail).then{
                            $0.delegate = self
                        }
                    } else {
                        //일반
                        self.topView = TopView(type: .detail).then{
                            $0.delegate = self
                        }
                    }
                    
                    if completion != nil {
                        completion!()
                    }
                } else {
                    AlertManager.shared.show(text: "잠시 후 다시 시도해 주세요.", type: .one)
                }
            } else {
                if let data = data {
                    do {
                        let decoder = JSONDecoder()
                        let error = try decoder.decode(ErrorData.self, from: data)
                        
                        switch error.data.code {
                        case Errors.e3005.rawValue:
                            AlertManager.shared.show(text: "삭제된 글입니다.", type: .one) {
                                if App.getWindowVCLast() is ContentDetailVC {
                                    App.pop()
                                }
                            }
                            break
                        case Errors.e3006.rawValue:
                            AlertManager.shared.show(text: "이 게시물을 볼 수 없습니다.", type: .one) {
                                if App.getWindowVCLast() is ContentDetailVC {
                                    App.pop()
                                }
                            }
                            break
                        case Errors.e3014.rawValue:
                            AlertManager.shared.show(text: "이 게시물의 소유자는 현재 잠겨 있어 이 요청을 처리할 수 없습니다.", type: .one) {
                                if App.getWindowVCLast() is ContentDetailVC {
                                    App.pop()
                                }
                            }
                            break
                        case Errors.e3015.rawValue:
                            AlertManager.shared.show(text: "차단된 사용자는 게시물에 접근할 수 없습니다.", type: .one) {
                                if App.getWindowVCLast() is ContentDetailVC {
                                    App.pop()
                                }
                            }
                            break
                        default:
                            AlertManager.shared.show(text: "잠시 후 다시 시도해 주세요.", type: .one) {
                                if App.getWindowVCLast() is ContentDetailVC {
                                    App.pop()
                                }
                            }
                            break
                        }
                    } catch(_) {
                        AlertManager.shared.show(text: "에러 발생", type: .one)
                    }
                }
            }
        }
    }
    
    public func getComment() {
        guard let postId = postData?.id else { return }
        let param:Parameters = [
            "postId" : postId,
            "offset" : self.page
        ]
        
        API.getComment(param: param) { model in
            guard let model = model,
                  let data = model.data,
                  let comments = data.comments,
                  let meta = data.metaData
            else {
                print("ContentDetailVC configuration item과 data가 올바르지 않습니다.")
                return
            }
            
            self.item?.data?.commentsList.comments += comments
            
            if self.page >= meta.totalPages {
                self.isPaging = true
            } else {
                self.isPaging = false
            }
            
            self.page += 1
            
//            self.configuration()
        }
    }
}

//MARK: - Etc
extension ContentDetailVC: BannerViewDelegate, TopViewDelegate, UIScrollViewDelegate {
    /*
     수정할 때 참조 링크 : https://medium.com/hcleedev/ios-%EC%95%B1%EC%97%90%EC%84%9C-firebase-dynamic-link-%EC%83%9D%EC%84%B1%ED%95%98%EA%B3%A0-%EC%88%98%EC%8B%A0%ED%95%98%EA%B8%B0-e357e95343c9
     */
    //"https://kkoomming.page.link/?link=https://kkoomming.com/init/post-detail/\(postId)&apn=kr.kkoomming&isi=1574307909&ibi=kr.kkoomming"
    func createDynamicLink(postId:Int) {
        AlertManager.shared.show(isLoading: true)
        
        let link = URL(string: "https://kkoomming.com/init/post-detail/\(postId)")
                let referralLink = DynamicLinkComponents(link: link!, domainURIPrefix: "https://kkoomming.page.link")
                let bundleId = "kr.kkoomming"
                
                // iOS 설정
                referralLink?.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleId)
                referralLink?.iOSParameters?.appStoreID = "1574307909"
                    
                // Android 설정
                referralLink?.androidParameters = DynamicLinkAndroidParameters(packageName: bundleId)
        
        // 단축 URL 생성
        referralLink?.shorten { (shortURL, warnings, error) in
            if let error = error {
                print(error.localizedDescription)
                AlertManager.shared.show(isLoading: false)
                return
            }
            if let urlString = shortURL?.absoluteString {
                let objectsToShare:[String] = [urlString]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                        
                // 공유하기 기능 중 제외할 기능이 있을 때 사용
                //activityVC.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.addToReadingList]
                self.present(activityVC, animated: true, completion: nil)
                print("shortURL : ", urlString)
                AlertManager.shared.show(isLoading: false)
            }
        }
    }
    
    func selectProfile() {
        guard let item = item else { return }
        print("selectProfile")
        App.push(ProfileVC(userId: item.data?.post.owner?.id ?? 0))
    }
    
    func selectEdit() {
        print("selectEdit")
        guard let postData = postData else { return }
        let vc = WriteVC(id: postData.id)
        //TODO: 수정 완료 됐을 때 새로고침 로직 필요
        vc.editHandler = {
            print("새로고침 해줘")
        }
        App.push(vc)
    }
    
    func selectReport() {
        print("selectReport")
        guard let item = item else { return }
        let view = ReportView(isPost: true)
        
        view.completion = {
            view.reportPost(postId: item.data?.post.id ?? 0) {
                AppManager.shared.pop()
            }
        }
        
        AlertManager.shared.showWindows(view: view)
    }
    
    func selectDelete() {
        print("selectDelete")
        AlertManager.shared.show(text: "삭제 하시겠습니까?", type: .two) { [weak self] in
            guard let strongSelf = self, let postData = strongSelf.postData else { return }
            let param:Parameters = [
                "id" : postData.id
            ]
            
            API.deletePost(param: param) { model in
                guard let model = model else {
                    return
                }

                if model.success {
                    App.pop()
                }
            }
        }
    }
    
    func selectBlock() {
        print("selectBlock")
    }
    
    func didSelectItemAt(index: Int) {
        print("didSelectItemAt index : ", index)
        
        App.push(ContentBannerDetailVC(items: bannerItems, index: index))
    }

    func scrollViewWillEndDragging(view: BannerView, index: Int) {
        print("scrollViewWillEndDragging index : ", index)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        let contentSizeHeight = scrollView.contentSize.height
        let currentViewHeight = scrollView.frame.height
        
        if position > (contentSizeHeight - currentViewHeight - 100) {
            guard !isPaging else { return }
            self.isPaging = true
            print("paging")
            
            self.getComment()
        }
    }
}

extension ContentDetailVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let size:CGSize = CGSize(width: textView.frame.width, height: 400)
        let estimatedSize:CGSize = textView.sizeThatFits(size)
        
        //1줄 높이 구하기
        let oneView = UITextView()
        oneView.frame.size = CGSize(width: textView.frame.width, height: 400)
        oneView.font = getFont(size: 12)
        oneView.text = """
        1
        """
        oneView.sizeToFit()
        
        //4줄 높이 구하기
        let fourView = UITextView()
        fourView.frame.size = CGSize(width: textView.frame.width, height: 400)
        fourView.font = getFont(size: 12)
        fourView.text = """
        1
        2
        3
        4
        """
        fourView.sizeToFit()
        
        let minimumHeight:CGFloat = oneView.frame.height
        let maximumHeight:CGFloat = fourView.frame.height
        let currentHeight:CGFloat = estimatedSize.height
        let otherHeight:CGFloat = originalInputViewHeight - minimumHeight

        if currentHeight > maximumHeight {
            //4줄 넘겼을 때
            inputCommentView.snp.updateConstraints{
                $0.height.equalTo(otherHeight + maximumHeight)
            }
        } else if currentHeight == minimumHeight {
            //1줄
            inputCommentView.snp.updateConstraints{
                $0.height.equalTo(originalInputViewHeight)
            }
        } else {
            //2~4줄
            inputCommentView.snp.updateConstraints{
                $0.height.equalTo(otherHeight + currentHeight)
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.placeholderColor {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = inputCommentView.placeholderString
            textView.textColor = UIColor.placeholderColor
        }
    }
}
