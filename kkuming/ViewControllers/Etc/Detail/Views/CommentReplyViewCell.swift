//
//  CommentReplyViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/11.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class CommentReplyViewCell: UITableViewCell {
    static let identifier = String(describing: CommentReplyViewCell.self)
    var disposeBag = DisposeBag()
    
    var isOnCommentWriteVC:Bool = false
    var commentIndex: Int?
    var item:Comment? = nil
    
    var topInset: CGFloat = 0
    var leftInset: CGFloat = 25
    var bottomInset: CGFloat = 0
    var rightInset: CGFloat = 25
    
    var grayBackgroundView = BackgroundView(RGB(red: 250, green: 250, blue: 250)).then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 15
    }
    
    let replyImageView = UIImageView(image: UIImage(named: "ic_reply"))
    
    let bottomSpacingView = BackgroundView(UIColor.clear)
    
    let profileImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 30 / 2
        $0.contentMode = .scaleToFill
        $0.image = UIImage(named: "coinExample")
    }
    
    let profileImageViewButton = UIButton()
    
    let nameLabel = UILabel().then{
        $0.font = getFont(size: 12, type: .EBold)
        $0.text = "초보다꾸러람찌"
    }
    
    let timeLabel = UILabel().then{
        $0.font = getFont(size: 10)
        $0.textColor = RGB(red: 188, green: 188, blue: 188)
        $0.text = "03.18 AM 11:23"
    }
    
    let moreImageView = UIImageView(image: UIImage(named: "ic_comment_more"))
    
    lazy var moreButton = UIButton()
    
    let contentLabel = UILabel().then{
        $0.font = getFont(size: 11)
        $0.textColor = RGB(red: 114, green: 114, blue: 114)
        $0.text = "취향 저걱이에요.취향 저걱이에요.취향 저걱이에요.취향 저걱이에요.취향 저걱이에요."
        $0.numberOfLines = 0
    }
    
    let replyView = UIView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 10
        $0.backgroundColor = RGB(red: 116, green: 116, blue: 116)
        
        let label = UILabel()
        label.text = "답글"
        label.font = getFont(size: 10)
        label.textColor = .white
        
        $0.addSubview(label)
        label.snp.makeConstraints{
            $0.centerY.centerX.equalToSuperview()
        }
    }
    
    lazy var replyButton = UIButton()

    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.disposeBag = DisposeBag()
    }
    
    override func layoutMarginsDidChange() {
        super.layoutMarginsDidChange()
        self.layoutMargins = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
        selectionStyle = .none
    }
    
    private func setupUI() {
        contentView.addSubViews([
            grayBackgroundView,
            replyImageView,
            profileImageView,
            profileImageViewButton,
            nameLabel,
            timeLabel,
            moreImageView,
            moreButton,
            contentLabel,
            replyView,
            replyButton,
            bottomSpacingView
        ])
        
        contentView.layoutMargins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        grayBackgroundView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.leading.equalTo(replyImageView.snp.trailing).offset(10)
            $0.trailing.equalToSuperview()
            $0.bottom.equalTo(bottomSpacingView.snp.top)
        }
        
        replyImageView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.leading.equalToSuperview().offset(17)
            $0.width.height.equalTo(10)
        }
        
        profileImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(10)
            $0.leading.equalTo(grayBackgroundView.snp.leading).offset(16)
            $0.width.height.equalTo(30)
        }
        
        nameLabel.snp.makeConstraints{
            $0.leading.equalTo(profileImageView.snp.trailing).offset(9)
            $0.centerY.equalTo(profileImageView.snp.centerY)
        }
        
        profileImageViewButton.snp.makeConstraints{
            $0.top.leading.bottom.equalTo(profileImageView)
            $0.trailing.equalTo(nameLabel)
        }
        
        timeLabel.snp.makeConstraints{
            $0.leading.equalTo(nameLabel.snp.trailing).offset(8)
            $0.centerY.equalTo(profileImageView.snp.centerY)
        }
        
        moreImageView.snp.makeConstraints{
            $0.width.height.equalTo(24)
            $0.top.equalTo(profileImageView.snp.top)
            $0.trailing.equalToSuperview().offset(-6)
        }
        
        moreButton.snp.makeConstraints{
            $0.width.height.equalTo(40)
            $0.top.equalTo(profileImageView.snp.top)
            $0.trailing.equalToSuperview()
        }
        
        contentLabel.snp.makeConstraints{
            $0.top.equalTo(profileImageView.snp.bottom).offset(5)
            $0.leading.equalTo(nameLabel.snp.leading)
            $0.trailing.equalToSuperview().offset(-18)
        }
        
        replyView.snp.makeConstraints{
            $0.top.equalTo(contentLabel.snp.bottom).offset(7)
            $0.trailing.equalToSuperview().offset(-16)
            $0.bottom.equalTo(bottomSpacingView.snp.top).offset(-15)
            $0.width.equalTo(42)
            $0.height.equalTo(18)
        }
        
        replyButton.snp.makeConstraints{
            $0.trailing.equalToSuperview()
            $0.bottom.equalTo(bottomSpacingView.snp.top)
            $0.width.equalTo(60)
            $0.height.equalTo(40)
        }
        
        bottomSpacingView.snp.makeConstraints{
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(20)
        }
    }
    
    public func setupRx(parentItem:Comment?, currentItem:Comment) {
        profileImageViewButton.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else {return}
            print("profileImageViewButton")
            if let userId = strongSelf.item?.commentOwner?.id {
                if UserManager.shared.getUserId() == userId {
                    //작성자 본인
                    AppManager.shared.popToRoot()
                    AppManager.shared.moveToTab(index: 4)
                    return
                } else {
                    //타인
                    App.push(ProfileVC(userId: userId))
                }
            }
        }.disposed(by: disposeBag)
        
        replyButton.rx.tap.bind{
            print("답글")
            if let rootViewController = App.getWindow().rootViewController {
                let naviController = rootViewController as! UINavigationController
                let viewController = naviController.viewControllers.last
    //                    commentVC.targetUserName = self.nameLabel.text
    //                    commentVC.commentIndex = self.commentIndex
                if (viewController is CommentWriteVC) {
                    let commentVC = viewController as! CommentWriteVC
                    commentVC.configuration(parentItem: parentItem, currentItem: currentItem)
                } else {
                    let commentVC = CommentWriteVC()
                    commentVC.configuration(parentItem: parentItem, currentItem: currentItem)
                    App.push(commentVC)
                }
            }
        }.disposed(by: disposeBag)
        
        moreButton.rx.tap.bind{
            print("더보기")
        }.disposed(by: disposeBag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
