//
//  InputCommentView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/06.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class InputCommentView: UIView {
    let disposeBag = DisposeBag()
    let placeholderString = "댓글을 입력해 주세요 :)"
    
    let commentFieldBackgroundView = UIView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 25
        $0.backgroundColor = .white
    }
    
    //만약 Inset 추가하면 textView.textContainer.size.width 로 바꿔줘야함
    lazy var commentTextView = UITextView().then{
        $0.font = getFont(size: 12)
        $0.text = placeholderString
        $0.textColor = UIColor.placeholderColor
    }
    
    let commentInputImageView = UIImageView(image: UIImage(named: "ic_comment_input")!)
    var commentInputButton = UIButton()
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public func getText() -> String{
        //TODO: UITextView로 변경할 때 같이 변경해야됨, API 연동을 위해 일단 임시로라도 진행
        return commentTextView.text ?? ""
    }
    
    public func clearText() {
        commentTextView.text = ""
    }
    
    private func setupUI() {
        setupProperties()
        
        addSubViews([
            commentFieldBackgroundView,
            commentTextView,
            commentInputImageView,
            commentInputButton
        ])
        
        setupConstraints()
    }
    
    private func setupProperties() {
        backgroundColor = RGB(red: 234, green: 234, blue: 234)
    }
    
    private func setupConstraints() {
        commentFieldBackgroundView.snp.makeConstraints{
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-25)
            $0.centerY.equalToSuperview()
            $0.top.equalToSuperview().offset(17)
            $0.bottom.equalToSuperview().offset(-17)
        }
        
        commentTextView.snp.makeConstraints{
            $0.top.equalTo(commentFieldBackgroundView.snp.top).offset(10)
            $0.leading.equalTo(commentFieldBackgroundView.snp.leading).offset(30)
            $0.trailing.equalTo(commentInputImageView.snp.leading).offset(-10)
            $0.bottom.equalTo(commentFieldBackgroundView.snp.bottom).offset(-10)
        }
        
        commentInputImageView.snp.makeConstraints{
            $0.width.height.equalTo(26)
            $0.trailing.equalTo(commentFieldBackgroundView.snp.trailing).offset(-14)
            $0.centerY.equalToSuperview()
        }
        
        commentInputButton.snp.makeConstraints{
            $0.top.equalTo(commentInputImageView.snp.top)
            $0.leading.equalTo(commentInputImageView.snp.leading)
            $0.trailing.equalTo(commentInputImageView.snp.trailing)
            $0.bottom.equalTo(commentInputImageView.snp.bottom)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
