//
//  CommentListView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/11.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher
import Alamofire

class CommentListView: UIView {
    var originItems:[Comment] = []
    var items:[Comment] = [] //가공된 아이템
    
    var refreshHandler:(()->Void)? = nil
    
    var isOnCommentWriteVC:Bool = false
    
    lazy var tableView = UITableView().then{
        $0.register(CommentListViewCell.self, forCellReuseIdentifier: CommentListViewCell.identifier)
        $0.register(CommentReplyViewCell.self, forCellReuseIdentifier: CommentReplyViewCell.identifier)
        $0.showsVerticalScrollIndicator = false
        $0.isScrollEnabled = false
        $0.delegate = self
        $0.dataSource = self
        $0.separatorStyle = .none
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public func configuration(items:[Comment]) {
        var result:[Comment] = []
        
        for item in items {
            result.append(item)
            if let replyComments = item.replyComments {
                for replyComment in replyComments {
                    var temp = replyComment
                    temp.isReply = true
                    result.append(temp)
                }
            }
        }
        
        self.originItems = items
        self.items = result
        
        tableView.reloadData()
    }
    
    public func add(items:[Comment]) {
        var result:[Comment] = []
        
        for item in items {
            if let replyComments = item.replyComments {
                for replyComment in replyComments {
                    var temp = replyComment
                    temp.isReply = true
                    result.append(temp)
                }
            }
        }
        
        self.originItems += items
        self.items += result
        
        tableView.reloadData()
    }
    
    public func getListViewHeight() -> CGFloat {
        return tableView.contentSize.height
    }
    
    private func setupUI() {
        addSubViews([
            tableView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        tableView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-25)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension CommentListView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        let item = items[index]
        var parentItem:Comment? = nil
        
        if item.isReply {
            //답변
            let cell = tableView.dequeueReusableCell(withIdentifier: CommentReplyViewCell.identifier, for: indexPath) as! CommentReplyViewCell
            
            cell.item = item
            
            //댓글쓰기를 위한 인덱스 설정
            cell.isOnCommentWriteVC = isOnCommentWriteVC
            cell.commentIndex = indexPath.row
            
            //이벤트 등록
            for origin in originItems {
                if origin.id == item.parentId {
                    parentItem = origin
                }
            }
            
            cell.setupRx(parentItem: parentItem, currentItem: item)
            cell.moreButton.rx.tap.bind{[weak self] in
                guard let strongSelf = self else { return }
                guard let owner = item.commentOwner, let ownerId = owner.id else { return }
                let myId = UserManager.shared.getUserId()
                
                if myId == ownerId {
                    //작성자
                    strongSelf.showAlertForOwner(parentItem: parentItem, currentItem: item)
                } else {
                    //일반
                    strongSelf.showAlertForNormal(item: item)
                }
            }.disposed(by: cell.disposeBag)
            
            if let owner = item.commentOwner {
                
                //프로필 영역
                if let urlString = owner.avatar, let url = URL(string: urlString) {
                    cell.profileImageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
                }
                
                //닉네임
                cell.nameLabel.text = owner.nickname
                
                //날짜
                cell.timeLabel.text = dateToFormat(string: item.createdAt, dateFormat: .comment)
                
                //내용
                switch item.status {
                case StatusType.USER_REMOVED.rawValue:
                    cell.contentLabel.text = StatusString.USER_REMOVED.rawValue
                    break
                case StatusType.ADMIN_WARNING.rawValue:
                    cell.contentLabel.text = StatusString.ADMIN_WARNING.rawValue
                    break
                case StatusType.BLOCKED.rawValue:
                    cell.contentLabel.text = StatusString.BLOCKED.rawValue
                    break
                default:
                    if let nickName = item.tagCommentOwner?.nickname {
                        let targetString = "@" + nickName
                        cell.contentLabel.text = targetString + " " + item.content
                        cell.contentLabel.bold(targetString: targetString)
                    }
//                    cell.contentLabel.text = item.content
                    break
                }
            }
            
            return cell
        } else {
            //일반
            let cell = tableView.dequeueReusableCell(withIdentifier: CommentListViewCell.identifier, for: indexPath) as! CommentListViewCell
            
            cell.item = item
            
            //댓글쓰기를 위한 인덱스 설정
            cell.isOnCommentWriteVC = isOnCommentWriteVC
            cell.commentIndex = indexPath.row
            
            //이벤트 등록
            cell.setupRx(parentItem: parentItem, currentItem: item)
            cell.moreButton.rx.tap.bind{[weak self] in
                guard let strongSelf = self else { return }
                guard let owner = item.commentOwner, let ownerId = owner.id else { return }
                let myId = UserManager.shared.getUserId()

                if myId == ownerId {
                    //작성자
                    strongSelf.showAlertForOwner(parentItem: parentItem, currentItem: item)
                } else {
                    //일반
                    strongSelf.showAlertForNormal(item: item)
                }
            }.disposed(by: cell.disposeBag)
            
            //첫번째 선 지우기
            if index != 0 {
                cell.isUseTopBorder = true
            } else {
                cell.isUseTopBorder = false
            }
            
            if let owner = item.commentOwner, let urlString = owner.avatar, let url = URL(string: urlString) {
                //프로필 영역
                cell.profileImageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
                
                //닉네임
                cell.nameLabel.text = owner.nickname
                
                //날짜
                cell.timeLabel.text = dateToFormat(string: item.createdAt, dateFormat: .comment)
                
                //내용
                switch item.status {
                case StatusType.USER_REMOVED.rawValue:
                    cell.contentLabel.text = StatusString.USER_REMOVED.rawValue
                    break
                case StatusType.ADMIN_WARNING.rawValue:
                    cell.contentLabel.text = StatusString.ADMIN_WARNING.rawValue
                    break
                case StatusType.BLOCKED.rawValue:
                    cell.contentLabel.text = StatusString.BLOCKED.rawValue
                    break
                default:
//                    if let nickName = item.tagCommentOwner?.nickname {
//                        cell.contentLabel.text = nickName + " " + item.content
//                    } else {
                        cell.contentLabel.text = item.content
//                    }
                    break
                }
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension CommentListView {
    func showAlertForOwner(parentItem:Comment?, currentItem:Comment) {
        let selectReviseView = AlertButtonView(title: "수정하기", buttonType: .mainColor){
            print("showAlertForOwner 수정하기")
            
            if let rootViewController = App.getWindow().rootViewController {
                let naviController = rootViewController as! UINavigationController
                let viewController = naviController.viewControllers.last
                
                if (viewController is CommentWriteVC) {
                    let commentVC = viewController as! CommentWriteVC
                    commentVC.isEdit = true
                    commentVC.configuration(parentItem: parentItem, currentItem: currentItem)
                } else {
                    let commentVC = CommentWriteVC()
                    commentVC.isEdit = true
                    commentVC.configuration(parentItem: parentItem, currentItem: currentItem)
                    App.push(commentVC)
                }
            }
        }
        
        let selectDeleteView = AlertButtonView(title: "삭제하기", buttonType: .orange){
            AlertManager.shared.show(text: "삭제 하시겠습니까?", type: .two) {
                print("삭제하기")
                let param:Parameters = [
                    "id" : currentItem.id
                ]
                
                API.deleteComment(param: param) { model in
                    guard let model = model else {
                        return
                    }

                    if model.success {
                        print("success : ", model.data.messages)

                        if self.refreshHandler != nil {
                            self.refreshHandler!()
                        }
                    }
                }
            }
        }
        
        AlertManager.shared.showAlertView(buttonList: [
            selectReviseView,
            selectDeleteView
        ])
    }
    
    func showAlertForNormal(item:Comment) {
        let selectProfileView = AlertButtonView(title: "프로필 보기", buttonType: .mainColor){
            print("프로필 보기")
            
            App.push(ProfileVC(userId: item.commentOwner?.id ?? 0))
        }
        
        let selectReportView = AlertButtonView(title: "신고하기", buttonType: .orange){
            print("신고하기")
            let view = ReportView(isPost: false)
            view.completion = { [weak self] in
                guard let strongSelf = self else { return }
                view.reportComment(commentId: item.id)
                strongSelf.refreshHandler?()
            }
            AlertManager.shared.showWindows(view: view)
        }
        
        AlertManager.shared.showAlertView(buttonList: [
            selectProfileView,
            selectReportView
        ])
    }
    
}
