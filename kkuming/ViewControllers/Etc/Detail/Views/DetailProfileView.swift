//
//  DetailProfileView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/06.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class DetailProfileView: UIView {
    let disposeBag = DisposeBag()
    var item: User?
    var isFollowed = false {
        didSet {
            self.updateButtonStatus()
        }
    }
    var shareHandler:(()->Void)? = nil
    
    let profileImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 41/2
        $0.image = UIImage(named: "coinExample")
    }
    
    lazy var profileImageViewButton = UIButton()
    
    let nameLabel = UILabel().then{
        $0.font = getFont(size: 14, type: .EBold)
        $0.textColor = .black
        $0.text = "초보다꾸러람찌"
    }
    
    let shareImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 25/2
        $0.image = UIImage(named: "ic_share")
    }
    
    lazy var shareButton = UIButton().then{
        $0.rx.tap.bind{ [weak self] in
            print("shareButton")
            
            self?.shareHandler?()
        }.disposed(by: disposeBag)
    }
    
    let followView = UIView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 13
        $0.backgroundColor = .mainColor
    }
    let followLabel = UILabel().then{
        $0.font = getFont(size: 12, type: .bold)
        $0.textColor = .white
        $0.text = "팔로우"
    }
    lazy var followButton = UIButton().then{
        $0.rx.tap.bind{ [weak self] in
            guard let strongSelf = self, let followingId = strongSelf.item?.id else {return}
            print("followButton")
            strongSelf.followReact(followingId: followingId)
        }.disposed(by: disposeBag)
    }
    
    let bottomBorderView = BorderView()
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public func updateButtonStatus(){
        if self.isFollowed {
            followLabel.text = "팔로잉"
            followLabel.textColor = .mainColor
            followView.backgroundColor = UIColor.white
            followView.layer.borderWidth = 1
            followView.layer.borderColor = UIColor.mainColor.cgColor
        } else {
            if UserManager.shared.getUserId() == self.item?.id {
                //본인
                followLabel.text = "팔로우"
                followLabel.textColor = RGB(red: 51, green: 51, blue: 51)
                followView.backgroundColor = RGB(red: 237, green: 237, blue: 237)
            } else {
                followLabel.text = "팔로우"
                followLabel.textColor = UIColor.white
                followView.backgroundColor = .mainColor
                followView.layer.borderWidth = 0
            }
        }
    }
    
    public func configuration(item:DetailData) {
        if let owner = item.post.owner, let urlString = owner.avatar, let url = URL(string: urlString) {
            self.item = owner
            
            //프로필 사진
            profileImageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
            
            //프로필 명
            nameLabel.text = owner.nickname
            
            //팔로우 상태 업데이트
            if let isFollowed = owner.isFollowed {
                self.isFollowed = isFollowed
            }
        }
    }
    
    private func setupUI() {
        setupFollowView()
        
        addSubViews([
            profileImageView,
            nameLabel,
            profileImageViewButton,
            shareImageView,
            shareButton,
            followView,
            bottomBorderView
        ])
        
        setupConstraints()
        setupRx()
    }
    
    private func setupConstraints() {
        profileImageView.snp.makeConstraints{
            $0.width.height.equalTo(41)
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(25)
        }
        
        profileImageViewButton.snp.makeConstraints{
            $0.top.leading.bottom.equalTo(profileImageView)
            $0.trailing.equalTo(nameLabel.snp.trailing)
        }
        
        nameLabel.snp.makeConstraints{
            $0.leading.equalTo(profileImageView.snp.trailing).offset(10)
            $0.trailing.equalTo(shareImageView.snp.leading).offset(-10)
            $0.centerY.equalToSuperview()
        }
        
        shareImageView.snp.makeConstraints{
            $0.width.height.equalTo(25)
            $0.centerY.equalToSuperview()
        }
        
        shareButton.snp.makeConstraints{
            $0.top.equalTo(shareImageView.snp.top)
            $0.leading.equalTo(shareImageView.snp.leading)
            $0.trailing.equalTo(shareImageView.snp.trailing)
            $0.bottom.equalTo(shareImageView.snp.bottom)
        }
        
        followView.snp.makeConstraints{
            $0.width.equalTo(72)
            $0.height.equalTo(25)
            $0.trailing.equalToSuperview().offset(-25)
            $0.centerY.equalToSuperview()
            $0.leading.equalTo(shareButton.snp.trailing).offset(7)
        }
        
        bottomBorderView.snp.makeConstraints{
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(1)
        }
    }
    
    private func setupFollowView() {
        followView.addSubViews([
            followLabel,
            followButton
        ])
        
        setupFollowViewConstraints()
    }
    
    private func setupFollowViewConstraints(){
        followLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        followButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupRx() {
        profileImageViewButton.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else { return }
            print("profileImageViewButton")
            if let userId = strongSelf.item?.id {
                if UserManager.shared.getUserId() == userId {
                    //본인
                    AppManager.shared.popToRoot()
                    AppManager.shared.moveToTab(index: 4)
                    return
                } else {
                    //타인
                    App.push(ProfileVC(userId: userId))
                }
            }
        }.disposed(by: disposeBag)
    }
    
    private func getParentViewController(view: UIView) -> UIViewController? {
        var parentResponder: UIResponder? = view.next
        while parentResponder != nil {
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
            parentResponder = parentResponder?.next
        }
        return nil
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DetailProfileView {
    func followReact(followingId:Int) {
        guard let item = item, let followingId = item.id else {return}
        API.followReact(followingId: followingId) { [weak self] model, data in
            guard let strongSelf = self, let model = model else {
                if let data = data {
                    JSON.decode(data: data) { errorData in
                        guard let errorData = errorData else {
                            return
                        }
                        
                        if errorData.data.code == "7003" {
                            AlertManager.shared.show(text: "본인은 팔로우 할 수 없습니다.", type: .one)
                        }
                    }
                }
                return
            }
            
            if model.success {
                strongSelf.isFollowed = model.data.isFollowed
            } else {
                AlertManager.shared.show(text: "에러 발생", type: .one)
            }
        }
    }
}
