//
//  CommentWriteVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/11.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Alamofire

class CommentWriteVC: BaseVC {
    let disposeBag = DisposeBag()
    var item:Comment? = nil
    var currentItem:Comment? = nil
    
    var HEIGHT:CGFloat = 0 {
        didSet {
            commentListView.snp.updateConstraints{
                $0.height.equalTo(self.HEIGHT)
            }
        }
    }
    var isEdit:Bool = false
    
    //입력 창 원래 높이
    var originalInputViewHeight:CGFloat = 81

    //페이징
    var page = 1
    var isPaging = false

    var targetTagItem:Comment? = nil
    var targetUserName: String?
    var commentIndex: Int?

    let topView = TopView(title: "댓글쓰기")

    lazy var scrollView = UIScrollView().then{
        $0.delegate = self
    }

    lazy var commentListView = CommentListView().then{
        $0.isOnCommentWriteVC = true
        $0.refreshHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.getReply(isFirst: true)
        }
    }

    lazy var inputCommentView = InputCommentView().then{
        $0.commentTextView.becomeFirstResponder()
        $0.commentTextView.delegate = self
        $0.commentTextView.textColor = .black
    }

    let bottomSafeAreaView = BackgroundView(RGB(red: 234, green: 234, blue: 234))

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        KkumingKeyboardManager.shared.setKeyboardNotification(
            parentView: view,
            keyboardView: inputCommentView,
            scrollView: scrollView,
            registKey: "commentWrite"
        )

        commentListView.tableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        //TODO: Fatal error: Array index is out of range 버그 있어서 일단 제외함.
//        scrollToTargetComment()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        KkumingKeyboardManager.shared.removeKeyboardNotification(registKey: "commentWrite")
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        //답변 화면 때문에 여기에 설정
        commentListView.tableView.removeObserver(self, forKeyPath: "contentSize")
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){
            if let newvalue = change?[.newKey] {
                let contentHeight: CGFloat = commentListView.tableView.contentSize.height
                print(contentHeight)
                self.HEIGHT = contentHeight
            }
        }
    }

    private func setupUI() {
        scrollView.addSubview(commentListView)

        addSubViews([
            topView,
            scrollView,
            inputCommentView,
            bottomSafeAreaView
        ])

        setupConstraints()
        setupRx()
    }

    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }

        scrollView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
        }

        commentListView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
            $0.height.equalTo(commentListView.getListViewHeight())
        }

        inputCommentView.snp.makeConstraints{
            $0.top.equalTo(scrollView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(self.view.safeAreaLayoutGuide)
            $0.height.equalTo(81)
        }

        bottomSafeAreaView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    private func setupRx() {
        inputCommentView.commentInputButton.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else { return }
            guard let item = strongSelf.currentItem else {
                print("getDetailPost item 설정 안됨")
                return
            }

            let content = strongSelf.inputCommentView.getText()
            var text = ""

            if let targetUserName = strongSelf.targetUserName, !strongSelf.isEdit {
                let startIndex = content.index(content.startIndex, offsetBy: targetUserName.count)
                let endIndex = content.endIndex
                text = String(content[startIndex..<endIndex])
            } else {
                text = strongSelf.inputCommentView.getText()
            }

            print("댓글 입력한 텍스트 : ", text)

            guard text != "" else {
                AlertManager.shared.show(text: "댓글을 입력해 주세요", type: .one)
                return
            }

            var parentId = 0
            if item.parentId != 0 {
                parentId = item.parentId
            } else {
                parentId = item.id
            }

            print("strongSelf.isEdit : ", strongSelf.isEdit)

            if strongSelf.isEdit {
                //수정 모드
                let param:Parameters = [
                    "id" : item.id,
                    "content" : text,
                ]

                API.updateComment(param: param) { model in
                    guard let strongSelf = self, let model = model else {
                        return
                    }

                    if model.success {
                        print("success")
                        strongSelf.clearText()
                        
                        //MARK: 1번 수정 했으니 수정 잠금
                        strongSelf.isEdit = false
                        strongSelf.getReply(isFirst: true)
                        strongSelf.view.endEditing(true)
                    }
                }
            } else {
                //댓글 쓰기
                var param:Parameters = [
                    "postId" : item.postId,
                    "parentId" : parentId,
                    "content" : text,
                ]

                if strongSelf.targetUserName != "" {
                    //태그로 답글을 선택 했을 때에만 해당
                    param.updateValue(item.commentOwner?.id ?? 0, forKey: "parentCommentTagId")
                }

                API.postReply(param: param) { model in
                    guard let model = model else {
                        return
                    }

                    if model.success {
                        print("success : ", model.data?.content)
                        strongSelf.clearText()
                        strongSelf.getReply(isFirst: true)
                        strongSelf.updateInputComment()
                    }
                }
            }
        }.disposed(by: disposeBag)
    }

    private func scrollToTargetComment() {
        DispatchQueue.main.async {
            let visibleCells = self.commentListView.tableView.visibleCells[0...self.commentIndex!]
            var height: CGFloat = 0
            var diff: CGFloat = 0

            for cell in visibleCells {
                diff = self.inputCommentView.frame.minY - self.topView.frame.maxY
                height += cell.bounds.height
            }
            height = height - diff < 0 ? 0 : height - diff

            var maxIndex:Int = 0
            for index in 0...self.commentListView.tableView.numberOfSections - 1 {
                maxIndex += self.commentListView.tableView.numberOfRows(inSection: index)
            }
            maxIndex = maxIndex - 1

            if (self.commentIndex! <= maxIndex) {
                self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentOffset.x, y: height), animated: false)
            } else {
                self.scrollView.setContentOffset(CGPoint(x: self.scrollView.contentOffset.x, y: 0), animated: false)
            }
        }
    }

    private func updateInputComment() {
        if let targetUserName = currentItem?.tagCommentOwner?.nickname {
            self.targetUserName = "@" + targetUserName + " "
            inputCommentView.commentTextView.text = self.targetUserName
        } else {
            if let nickname = currentItem?.commentOwner?.nickname {
                self.targetUserName = "@" + nickname + " "
                inputCommentView.commentTextView.text = self.targetUserName
            }
        }
        
        if isEdit {
            inputCommentView.commentTextView.text = currentItem?.content
            inputCommentView.commentTextView.becomeFirstResponder()
        }
    }
    
    private func clearText() {
        self.inputCommentView.clearText()
        
        inputCommentView.snp.updateConstraints{
            $0.height.equalTo(originalInputViewHeight)
        }
    }
}


//MARK: - Public
extension CommentWriteVC {
    //일반
    public func configuration(parentItem:Comment?, currentItem:Comment) {
        self.item = parentItem
        self.currentItem = currentItem
        
        updateInputComment()
        getReply(isFirst:true)
    }
    
}

//MARK: - Delegate
extension CommentWriteVC: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let textViewText = (textView.text! as NSString).replacingCharacters(in: range, with: text)
//        return textViewText.starts(with: targetUserName!)
        let text = (textView.text! as NSString).replacingCharacters(in: range, with: text)
//        let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)

        //MARK: 밑에는 @가 들어간 상태임
        if let targetUserName = targetUserName, !isEdit {
            return text.starts(with: targetUserName)
        } else {
            return true
        }
    }

    func textViewDidChange(_ textView: UITextView) {
        let size:CGSize = CGSize(width: textView.frame.width, height: 400)
        let estimatedSize:CGSize = textView.sizeThatFits(size)
        
        //1줄 높이 구하기
        let oneView = UITextView()
        oneView.frame.size = CGSize(width: textView.frame.width, height: 400)
        oneView.font = getFont(size: 12)
        oneView.text = """
        1
        """
        oneView.sizeToFit()
        
        //4줄 높이 구하기
        let fourView = UITextView()
        fourView.frame.size = CGSize(width: textView.frame.width, height: 400)
        fourView.font = getFont(size: 12)
        fourView.text = """
        1
        2
        3
        4
        """
        fourView.sizeToFit()
        
        let minimumHeight:CGFloat = oneView.frame.height
        let maximumHeight:CGFloat = fourView.frame.height
        let currentHeight:CGFloat = estimatedSize.height
        let otherHeight:CGFloat = originalInputViewHeight - minimumHeight

        if currentHeight > maximumHeight {
            //4줄 넘겼을 때
            inputCommentView.snp.updateConstraints{
                $0.height.equalTo(otherHeight + maximumHeight)
            }
        } else if currentHeight == minimumHeight {
            //1줄
            inputCommentView.snp.updateConstraints{
                $0.height.equalTo(originalInputViewHeight)
            }
        } else {
            //2~4줄
            inputCommentView.snp.updateConstraints{
                $0.height.equalTo(otherHeight + currentHeight)
            }
        }
    }

    func textViewDidChangeSelection(_ textView: UITextView) {
        if let selectedRange = textView.selectedTextRange {
            let cursorStartPosition = textView.offset(
                from: textView.beginningOfDocument,
                to: selectedRange.start
            )

            let cursorEndPosition = textView.offset(
                from: textView.beginningOfDocument,
                to: selectedRange.end
            )

            if !isEdit {
                if let selectedRangeLength:Int = textView.text(in: selectedRange)?.utf8.count {
                    if (cursorStartPosition < targetUserName!.count) {
                        if (selectedRangeLength == 0) {
                            if let newCursorPosition = textView.position(from: textView.beginningOfDocument, offset: targetUserName!.count) {
                                textView.selectedTextRange = textView.textRange(
                                    from: newCursorPosition,
                                    to: newCursorPosition
                                )
                            }
                        } else {
                            if let newCursorStartPosition = textView.position(from: textView.beginningOfDocument, offset: targetUserName!.count),
                               let newCursorEndPosition = textView.position(from: textView.beginningOfDocument, offset: cursorEndPosition) {
                                textView.selectedTextRange = textView.textRange(
                                    from: newCursorStartPosition,
                                    to: newCursorEndPosition
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

//MARK: - API
extension CommentWriteVC {
    public func getReply(isFirst:Bool = false) {
        if isFirst {
            scrollView.isScrollEnabled = false
            page = 1
            isPaging = false
        }
        guard let item = self.currentItem else {
            print("getReply 아이템 비었음")
            return
        }
        var commentId = 0
        if item.parentId != 0 {
            commentId = item.parentId
        } else {
            commentId = item.id
        }

        let param:Parameters = [
            "commentId" : commentId,
            "offset" : self.page
        ]

        API.replyGets(param: param) { [weak self] model in
            guard let strongSelf = self,
                  let model = model,
                  let comment = model.data?.comments?.first,
                  let meta = comment.metaData
            else {
                print("replyGets item 정보")
                return
            }

            if model.success {
                if isFirst {
                    strongSelf.commentListView.configuration(items: [comment])
                    strongSelf.scrollView.isScrollEnabled = true
                } else {
                    //더 불러오기
                    strongSelf.commentListView.add(items: [comment])
                }

                if strongSelf.page >= meta.totalPages {
                    strongSelf.isPaging = true
                } else {
                    strongSelf.isPaging = false
                }

                strongSelf.page += 1
                
                //댓글이 지워진 상태라면..?
                if comment.status == "REMOVED" {
                    App.pop()
                }
            }
        }
    }
}

//MARK: - More
extension CommentWriteVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        let contentSizeHeight = scrollView.contentSize.height
        let currentViewHeight = scrollView.frame.height

        if position > (contentSizeHeight - currentViewHeight - 100) {
            guard !isPaging else { return }
            self.isPaging = true
            print("paging")

            if let comments = item?.replyComments, comments.count >= 10 {
                self.getReply()
            }
        }
    }
}
