//
//  ContentBannerDetailVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/30.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class ContentBannerDetailVC: BaseVC {
    let disposeBag = DisposeBag()
    var currentIndex:Int = 0
    lazy var items:[UIImage] = []
    
    lazy var imageListView = DirectionBannerView(isBackgroundBlack: true, isDetail: true).then{
        $0.delegate = self
    }
    
    lazy var closeView = UIView().then{
        let closeImageView = UIImageView()
        closeImageView.image = UIImage(named: "ic_close_photo")!
        
        $0.addSubview(closeImageView)
        $0.addSubview(closeButton)
        
        closeImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        closeButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    lazy var closeButton = UIButton().then{
        $0.rx.tap.bind{
            print("closeButton")
            App.pop()
        }.disposed(by: disposeBag)
    }
    
    init(items:[UIImage], index:Int) {
        super.init(nibName: nil, bundle: nil)
        
        self.items = items
        self.currentIndex = index
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        configuration()
    }
    
    private func configuration() {
        imageListView.configuration(items: items)
        imageListView.moveToItem(index: currentIndex)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - UI
extension ContentBannerDetailVC {
    private func setupUI() {
        addSubViews([
            imageListView,
            closeView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        imageListView.snp.makeConstraints{
            $0.top.leading.bottom.trailing.equalToSuperview()
        }
        
        closeView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.top.equalTo(self.view.safeAreaLayoutGuide).offset(17)
            $0.trailing.equalToSuperview().offset(-17)
        }
        
        closeButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension ContentBannerDetailVC: BannerViewDelegate {
    func didSelectItemAt(index: Int) {
        print("didSelectItemAt index : ", index)
    }
    
    func scrollViewWillEndDragging(view: BannerView, index: Int) {
        print("scrollViewWillEndDragging index : ", index)
    }
}
