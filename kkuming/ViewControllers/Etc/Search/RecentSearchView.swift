//
//  RecentSearchView.swift
//  kkuming
//
//  Created by pjy on 2022/06/13.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class RecentSearchView: UIView, UITableViewDelegate {
    
    var isFocusOnTextField: Bool = false {
        didSet {
            if isFocusOnTextField {
                doShowRecentSearchKeyword()
            } else {
                doHideRecentSearchKeyword()
            }
        }
    }
    
    var recentSearchKeyword: [Any] = UserDefaultsUtil.readUserRecentSearchKeyword() {
        didSet {
            checkRecentSearchKeywordIsEmpty()
        }
    }
    
    deinit {
        removeKeyboardNotification()
    }
    
    let borderView = BorderView()
    
    let contentView = UIView().then{
        $0.backgroundColor = RGB(red: 255, green: 255, blue: 255)
    }
    
    let contentIfRecentSearchKeywordIsEmptyView = UIView().then{
        $0.backgroundColor = RGB(red: 94, green: 94, blue: 94, alpha: 0.6)
    }
    
    let contentTitle = TitleLabel(text: "최근 검색어")
    
    lazy var deleteAllRecentSearchKeywordButton = UIButton().then{
        $0.backgroundColor = RGB(red: 116, green: 116, blue: 116)
        $0.setTitle("전체삭제", for: .normal)
        $0.setTitleColor(RGB(red: 255, green: 255, blue: 255), for: .normal)
        $0.titleLabel?.font = getFont(size: 12, type: .bold)
        $0.titleLabel?.textAlignment = .center
        $0.layer.cornerRadius = 12
        $0.addTarget(
            self,
            action: #selector(deleteAllRecentSearchKeyword),
            for: .touchUpInside
        )
    }
    
    lazy var recentSearchKeywordTableView = UITableView().then{
        $0.backgroundColor = .white
        $0.separatorStyle = .singleLine
        $0.delegate = self
        $0.dataSource = self
        $0.bounces = false
        $0.showsVerticalScrollIndicator = false
        $0.separatorInset = .zero
        $0.layoutMargins = .zero
        $0.register(
            RecentSearchKeywordCell.self,
            forCellReuseIdentifier: RecentSearchKeywordCell.identifier
        )
    }
    
    init() {
        super.init(frame: CGRect.zero)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        addSubViews([
            contentView,
            borderView
        ])
        
        contentView.addSubViews([
            contentTitle,
            deleteAllRecentSearchKeywordButton,
            recentSearchKeywordTableView
        ])
    
        addSubViews([
            contentIfRecentSearchKeywordIsEmptyView,
            borderView
        ])
        
        setupConfiguration()
        checkRecentSearchKeywordIsEmpty()
        addKeyboardNotification()
    }
    
    func addKeyboardNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleKeyboardShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(handleKeyboardHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    func removeKeyboardNotification() {
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.removeObserver(
            self,
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
    private func setupConfiguration() {
        contentView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        
        contentTitle.snp.makeConstraints{
            $0.top.equalToSuperview().offset(33)
            $0.leading.equalToSuperview().offset(25)
        }

        deleteAllRecentSearchKeywordButton.snp.makeConstraints{
            $0.top.equalToSuperview().offset(33)
            $0.trailing.equalToSuperview().offset(-25)
            $0.height.equalTo(24)
            $0.width.equalTo(64)
        }
        
        borderView.snp.makeConstraints{
            $0.bottom.equalTo(contentView.snp.top).offset(1)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(1)
        }
        
        recentSearchKeywordTableView.snp.makeConstraints{
            $0.top.equalTo(contentTitle.snp.bottom).offset(23)
            $0.leading.equalTo(contentTitle.snp.leading)
            $0.trailing.equalTo(deleteAllRecentSearchKeywordButton.snp.trailing)
            $0.bottom.equalTo(contentView)
        }
        
        contentIfRecentSearchKeywordIsEmptyView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
    
    private func checkRecentSearchKeywordIsEmpty() {
        if (recentSearchKeyword.count == 0) {
            contentIfRecentSearchKeywordIsEmptyView.isHidden = false
            contentView.isHidden = true
        } else {
            contentIfRecentSearchKeywordIsEmptyView.isHidden = true
            contentView.isHidden = false
        }
        
        reloadTableCell(tableView: recentSearchKeywordTableView)
    }
    
    private func doShowRecentSearchKeyword() {
        self.isHidden = false
        checkRecentSearchKeywordIsEmpty()
    }
    
    private func doHideRecentSearchKeyword() {
        self.isHidden = true
    }
    
    func reloadTableCell(tableView: UITableView) {
        tableView.reloadData()
    }
    
    func setUserRecentSearchKeyword(data: Array<Any>, tableView: UITableView) {
        recentSearchKeyword = data
        UserDefaultsUtil.save(value: data, key: UserDefaultsKey.userRecentSearchKeyword)
        reloadTableCell(tableView: tableView)
    }
}

extension RecentSearchView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentSearchKeyword.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: RecentSearchKeywordCell.identifier, for: indexPath) as! RecentSearchKeywordCell)
        
        cell.cellTextLabel.text = recentSearchKeyword[indexPath.row] as? String
        cell.cellTextLabel.tag = indexPath.row
        cell.deleteRecentSearchKeywordButton.tag = indexPath.row
        cell.deleteRecentSearchKeywordButton.addTarget(
            self,
            action: #selector(RecentSearchView.deleteRecentSearchKeyword),
            for: .touchUpInside
        )
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? RecentSearchKeywordCell {
            cellDidTap(cell.cellTextLabel.tag)
        }
    }
}

//objc
extension RecentSearchView {
    @objc
    func handleKeyboardShow(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardHeight = keyboardFrame.cgRectValue.height
            recentSearchKeywordTableView.snp.updateConstraints{
                $0.bottom.equalTo(contentView).offset(-1 * keyboardHeight)
            }
        }
    }
    
    @objc
    func handleKeyboardHide(notification: Notification) {
        recentSearchKeywordTableView.snp.updateConstraints{
            $0.bottom.equalTo(contentView)
        }
        isFocusOnTextField = false
    }
    
    @objc
    func deleteRecentSearchKeyword(sender: UIButton) {
        recentSearchKeyword.remove(at: sender.tag)
        setUserRecentSearchKeyword(
            data: recentSearchKeyword,
            tableView: recentSearchKeywordTableView
        )
    }
    
    @objc
    private func deleteAllRecentSearchKeyword(sender: UIButton) {
        recentSearchKeyword.removeAll()
        setUserRecentSearchKeyword(
            data: recentSearchKeyword,
            tableView: recentSearchKeywordTableView
        )
    }
    
    @objc
    func cellDidTap(_ key: Int) {
        let value = recentSearchKeyword[key] as? String
        recentSearchKeyword.remove(at: key)
        
        recentSearchKeyword.reverse()
        recentSearchKeyword.append(value!)
        recentSearchKeyword.reverse()
        
        setUserRecentSearchKeyword(
            data: recentSearchKeyword,
            tableView: recentSearchKeywordTableView
        )
        
        let searchResultViewController = SearchResultVC()
        searchResultViewController.searchText = value!
        
        if (App.getWindowVCLast() is SearchResultVC) {
            App.pop(animated: false)
        }
        
        App.push(searchResultViewController)
    }
}

class RecentSearchKeywordCell: UITableViewCell {
    static let identifier = "recentSearchKeyword"
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var deleteRecentSearchKeywordButton = UIButton().then{
        $0.setImage(UIImage(named: "ic_alert_delete"), for: .normal)
    }
    
    let cellTextLabel = UILabel().then{
        $0.textColor = RGB(red: 157, green: 157, blue: 157)
        $0.font = getFont(size: 14, type: .bold)
        $0.layoutMargins.left = 0
    }
    
    private func setupUI() {
        contentView.addSubViews([
            cellTextLabel,
            deleteRecentSearchKeywordButton
        ])
        
        setupConstraint()
    }
    
    private func setupConstraint() {
        cellTextLabel.snp.makeConstraints{
            $0.centerY.leading.equalToSuperview()
            $0.trailing.equalTo(deleteRecentSearchKeywordButton.snp.leading)
        }
        
        deleteRecentSearchKeywordButton.snp.makeConstraints{
            $0.width.height.equalTo(28)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
    }
}
