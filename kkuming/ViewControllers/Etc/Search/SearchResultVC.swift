//
//  SearchResultVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/09.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class SearchResultVC: BaseVC {
    let disposeBag = DisposeBag()
    var page: Int = 1
    var searchText: String?
    
    lazy var topView = TopView(type: .search).then{
        $0.configurationSearch(image: UIImage(named: "ic_top_search")!){
            print("검색누르기")
        }
        $0.searchField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        $0.searchField.addTarget(self, action: #selector(textFieldEditingDidBegin(_:)), for: .editingDidBegin)
        $0.searchField.addTarget(self, action: #selector(textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        $0.searchField.delegate = self
        $0.searchField.text = searchText
    }
    
    let contentView = SearchResultPagerVC()
    
    let recentSearchKeywordView = RecentSearchView().then{
        $0.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupSearchConfiguration()
        setupSearchIcon()
        configuration(keyword: searchText ?? "")
    }
    
    private func setupUI() {
        addSubViews([
            topView,
            contentView.view,
            recentSearchKeywordView
        ])
        
        addChild(contentView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        contentView.view.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        recentSearchKeywordView.snp.makeConstraints{
            $0.bottom.leading.trailing.equalToSuperview()
            $0.top.equalTo(topView.snp.bottom)
        }
    }
    
    func setupSearchConfiguration() {
        topView.searchField.returnKeyType = .search
        topView.configurationSearch(image: UIImage(named: "ic_top_search")!, searchHandler: { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.topView.isSearchClose {
                strongSelf.topView.searchField.text = ""
                strongSelf.topView.isSearchClose = false
                strongSelf.topView.searchField.becomeFirstResponder()
            } else {
                strongSelf.topView.searchField.becomeFirstResponder()
            }
        })
    }
    
    private func setupSearchIcon() {
        if searchText != nil && searchText?.count != 0 {
            topView.isSearchClose = true
        }
    }
}

extension SearchResultVC: UITextFieldDelegate {
    @objc
    func textFieldDidChange(_ textField:UITextField) {
        guard let text = textField.text else {
            topView.isSearchClose = false
            return
        }
        
        if text.count == 0 {
            topView.isSearchClose = false
        } else {
            topView.isSearchClose = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        if text.count == 0 {
            if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
                let popupView = PopupView(title: "검색어를 입력해 주세요.") {
                    
                }.then{
                    $0.previousButtonView.isHidden = true
                }
                window.addSubview(popupView)
                
                popupView.snp.makeConstraints{
                    $0.top.leading.trailing.bottom.equalToSuperview()
                }
            }
        } else {
            var recentSearchKeyword = recentSearchKeywordView.recentSearchKeyword as? [String] ?? []
            let trimText = text.trimmingCharacters(in: .whitespaces)
            
            if !recentSearchKeyword.contains(
                where: {
                    $0 == trimText
                }
            ) {
                recentSearchKeyword.reverse()
                recentSearchKeyword.append(trimText)
                recentSearchKeyword.reverse()
            } else {
                if let duplicatedIndex = recentSearchKeyword.firstIndex(of: trimText) {
                    recentSearchKeyword.remove(at: duplicatedIndex)
                    recentSearchKeyword.reverse()
                    recentSearchKeyword.append(trimText)
                    recentSearchKeyword.reverse()
                }
            }
            
            recentSearchKeywordView.setUserRecentSearchKeyword(
                data: recentSearchKeyword,
                tableView: recentSearchKeywordView.recentSearchKeywordTableView
            )
            
            configuration(keyword: trimText)
        }
        return true
    }
    
    @objc
    func textFieldEditingDidBegin(_ textField:UITextField) {
        recentSearchKeywordView.isFocusOnTextField = true
    }
    
    @objc
    func textFieldEditingDidEnd(_ textField:UITextField) {
        recentSearchKeywordView.isFocusOnTextField = false
    }
    
}

extension SearchResultVC {
    func configuration(keyword:String) {
        self.view.endEditing(true)
        recentSearchKeywordView.isFocusOnTextField = false
        
        self.contentView.feedVC.search(keyword: keyword, type: .FEED, isFirst: true)
        self.contentView.profileVC.search(keyword: keyword, type: .USER, isFirst: true)
    }
}
