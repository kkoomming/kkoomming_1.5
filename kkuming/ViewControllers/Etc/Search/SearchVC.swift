//
//  SearchVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/03.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class SearchVC: BaseVC {
    var items:[String] = ["#오메베어", "#한톨상점", "#후카후카", "#푸르름디자인", "#모나미", "#로이텀", "#스크랩"]
    var page:Int = 1
    
    lazy var topView = TopView(type: .search).then{
        $0.searchField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        $0.searchField.addTarget(self, action: #selector(textFieldEditingDidBegin(_:)), for: .editingDidBegin)
        $0.searchField.addTarget(self, action: #selector(textFieldEditingDidEnd(_:)), for: .editingDidEnd)
        $0.searchField.delegate = self
    }
    
    let scrollView = UIScrollView().then{
        $0.showsVerticalScrollIndicator = false
    }
    
    let contentView = UIView().then{
        $0.backgroundColor = .white
    }
    
    let popularTagLabel = TitleLabel(text: "인기 태그")
    
    let tagView = TagView().then{
        $0.borderColor = RGB(red: 252, green: 107, blue: 88)
        $0.textColor = RGB(red: 252, green: 107, blue: 88)
    }
    
    let recommendLabel = TitleLabel(text: "추천 팔로우")
    
    let recommendListView = SearchRecommendListView()
    
    lazy var bannerView = GeneralBannerView().then{
        $0.delegate = self
    }
    
    lazy var pageControlView = BannerPageControlView().then{
        $0.type = .EVENT
    }
    
    let recentSearchKeywordView = RecentSearchView().then{
        $0.isHidden = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tagView.updateTagViewLayout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        getSearchALL()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        recentSearchKeywordView.recentSearchKeyword = UserDefaultsUtil.readUserRecentSearchKeyword()
    }
    
    private func setupUI() {
        contentView.addSubViews([
            popularTagLabel,
            tagView,
            recommendLabel,
            recommendListView,
            bannerView,
            pageControlView
        ])
        
        scrollView.addSubview(contentView)
        addSubViews([
            topView,
            scrollView,
            recentSearchKeywordView
        ])
        
        setupConstraints()
        setupSearchConfiguration()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        scrollView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        contentView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
            $0.height.equalTo(747)
        }
        
        recentSearchKeywordView.snp.makeConstraints{
            $0.bottom.leading.trailing.equalToSuperview()
            $0.top.equalTo(topView.snp.bottom)
        }
        
        setupContentViewConstraints()
    }
    
    private func setupContentViewConstraints() {
        popularTagLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(33)
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-25)
        }
        
        tagView.snp.makeConstraints{
            $0.top.equalTo(popularTagLabel.snp.bottom).offset(23)
            $0.leading.equalTo(popularTagLabel.snp.leading)
            $0.trailing.equalTo(popularTagLabel.snp.trailing)
            $0.height.equalTo(100)
        }
        
        recommendLabel.snp.makeConstraints{
            $0.top.equalTo(tagView.snp.bottom).offset(32)
            $0.leading.equalTo(popularTagLabel.snp.leading)
            $0.trailing.equalTo(popularTagLabel.snp.trailing)
        }
        
        recommendListView.snp.makeConstraints{
            $0.top.equalTo(recommendLabel.snp.bottom).offset(16)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(264)
        }
        
        //비율은 327 * 90px
        let width = UIScreen.main.bounds.width - 50 //-50은 left와 right마진 합친 값
        let height = width * 90 / 327
        bannerView.snp.makeConstraints{
            $0.top.equalTo(recommendListView.snp.bottom).offset(31)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(height)
        }
        
        pageControlView.snp.makeConstraints{
            $0.width.equalTo(60)
            $0.height.equalTo(19)
            $0.bottom.equalTo(bannerView.snp.bottom).offset(-8)
            $0.trailing.equalTo(bannerView.snp.trailing).offset(-16 - 25)
        }
    }
    
    func setupSearchConfiguration() {
        topView.searchField.returnKeyType = .search
        topView.configurationSearch(image: UIImage(named: "ic_top_search")!, searchHandler: { [weak self] in
            guard let strongSelf = self else { return }
            if strongSelf.topView.isSearchClose {
                strongSelf.topView.searchField.text = ""
                strongSelf.topView.isSearchClose = false
                strongSelf.topView.searchField.becomeFirstResponder()
            } else {
                strongSelf.topView.searchField.becomeFirstResponder()
            }
        })
    }
}

extension SearchVC: BannerViewDelegate {
    func didSelectItemAt(index: Int) {
        print("didSelectItemAt : \(index)")
        let item = bannerView.items[index]
        selectBannerHandler(data: item)
    }
    
    func scrollViewWillEndDragging(view: BannerView, index: Int) {
        if bannerView == view {
            pageControlView.updateCurrentNumber(index: index)
        }
    }
    
    func selectBannerHandler(data:BannerData) {
        if let dataType = data.type, let type = BannerType(rawValue: dataType) {
            switch type {
            case .BANNER_WEB_VIEW, .WEB_VIEW:
                if let linkUrl = data.linkUrl {
                    let vc = WebVC(title: data.webViewTitle ?? "", url: linkUrl)
                    AppManager.shared.push(vc)
                }
                break
            case .CHALLENGE:
                if let id = data.eventsId {
                    let vc = TagDetailVC(topTitleString: data.webViewTitle ?? "",
                                         id: id,
                                         type:.CHALLENGE,
                                         isUseBanner: true)
                    AppManager.shared.push(vc)
                }
                
                break
            case .URL:
                if let linkUrl = data.linkUrl {
                    openSafari(url: linkUrl)
                }
                break
            case .EVENT:
                if let eventsId = data.eventsId {
                    API.eventsDetail(id: eventsId, type: .EVENT) { model in
                        guard let model = model, let events = model.data?.events  else { return }
                        
                        if model.success {
                            if let linkUrl = events.linkUrl {
                                let vc = WebVC(title: data.webViewTitle ?? "", url: linkUrl)
                                AppManager.shared.push(vc)
                            }
                        }
                    }
                }
                break
            }
        }
    }
}

extension SearchVC: UITextFieldDelegate {
    @objc
    func textFieldDidChange(_ textField:UITextField) {
        guard let text = textField.text else {
            topView.isSearchClose = false
            return
        }
        
        if text.count == 0 {
            topView.isSearchClose = false
        } else {
            topView.isSearchClose = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text else {
            return true
        }
        
        if text.count == 0 {
            if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
                let popupView = PopupView(title: "검색어를 입력해 주세요.").then{
                    $0.previousButtonView.isHidden = true
                }
                window.addSubview(popupView)
                
                popupView.snp.makeConstraints{
                    $0.top.leading.trailing.bottom.equalToSuperview()
                }
            }
        } else {
            var recentSearchKeyword = recentSearchKeywordView.recentSearchKeyword as? [String] ?? []
            let trimText = text.trimmingCharacters(in: .whitespaces)
            
            if !recentSearchKeyword.contains(
                where: {
                    $0 == trimText
                }
            ) {
                recentSearchKeyword.reverse()
                recentSearchKeyword.append(trimText)
                recentSearchKeyword.reverse()
            } else {
                if let duplicatedIndex = recentSearchKeyword.firstIndex(of: trimText) {
                    recentSearchKeyword.remove(at: duplicatedIndex)
                    recentSearchKeyword.reverse()
                    recentSearchKeyword.append(trimText)
                    recentSearchKeyword.reverse()
                }
            }
            
            recentSearchKeywordView.setUserRecentSearchKeyword(
                data: recentSearchKeyword,
                tableView: recentSearchKeywordView.recentSearchKeywordTableView
            )
            
            search(keyword: trimText)
        }
        return true
    }
    
    @objc
    func textFieldEditingDidBegin(_ textField:UITextField) {
        recentSearchKeywordView.isFocusOnTextField = true
        scrollView.isScrollEnabled = false
    }
    
    @objc
    func textFieldEditingDidEnd(_ textField:UITextField) {
        recentSearchKeywordView.isFocusOnTextField = false
        scrollView.isScrollEnabled = true
    }
    
}

extension SearchVC {
    func getSearchALL() {
        API.getSearchAll() { [weak self] data in
            guard let strongSelf = self else { return }
            strongSelf.configuration(data: data)
        }
    }
    
    func search(keyword:String) {
        let vc = SearchResultVC()
        vc.searchText = keyword
        
        App.push(vc)
    }
    
    public func configuration(data model:SearchMainData?) {
        guard let model = model,
              let slides = model.data?.banner?.slides,
              let recomUser = model.data?.recomUser,
              let searchTag = model.data?.searchTag
        else {
            print("getSearchAll 데이터 못가져옴")
            return
        }
        
        //인기 태그
        var tagList:[String] = []
        for item in searchTag {
            tagList.append(item.tagName)
        }
        self.tagView.configuration(items: tagList)
        
        //추천 팔로우
        self.recommendListView.configuration(items:recomUser)
        
        //추천 배너
        bannerView.configuration(items: slides)
        pageControlView.configuration(items: slides)
    }
}
