//
//  SearchFeedVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/09.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip

class SearchFeedVC: BaseVC, IndicatorInfoProvider {
    var itemInfo = IndicatorInfo(title: "피드")
    var items:[Post] = []
    
    var titleRefreshHandler:(()->Void)? = nil
    
    private var keyword = ""
    private var type:SearchType = .FEED
    
    //MARK: 페이징
    var page:Int = 1
    
    let emptyView = UIView().then{
        $0.isHidden = true
        let imageView = UIImageView(image: UIImage(named: "emptyKM"))
        
        $0.addSubview(imageView)
        imageView.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
            $0.width.height.equalTo(164)
        }
    }
    
    let twoListView = VariousListView(type: .two).then{
        $0.collectionView.isScrollEnabled = true
        $0.collectionView.contentInset = UIEdgeInsets(top: 10, left: $0.leftMargin, bottom: 10, right: $0.rightMargin)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        configuration(items: items)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("SearchFeedVC")
    }
    
    private func setupUI() {
        addSubViews([
            twoListView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    public func configuration(items:[Post]) {
        self.items = items
        if self.items.count == 0 {
            self.isEmpty(true)
        } else {
            self.isEmpty(false)
            
            twoListView.configuration(items: self.items)
            twoListView.moreHandler = {
                self.search(keyword: self.keyword, isFirst: false)
            }
        }
    }
    
    public func add(items:[Post]) {
        self.items += items
        twoListView.configuration(items: self.items)
    }
    
    private func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        twoListView.isHidden = bool
    }
    
    private func setupConstraints() {
        twoListView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension SearchFeedVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension SearchFeedVC {
    func search(keyword:String, type:SearchType = .FEED, isFirst:Bool) {
        self.keyword = keyword
        self.type = type
        
        self.twoListView.isPaging = true
        API.search(keyword: keyword, offset: page, type: type) {[weak self] model in
            guard let strongSelf = self, let model = model, let data = model.data else { return }
            guard let items = data.feedData?.posts,
                  let meta = data.feedData?.metaData else { return }
            
            DispatchQueue.main.async {
                if model.success {
                    if isFirst {
                        //MARK: 테스트용
//                        let list:[Post] = Array(repeating: strongSelf.createExample(), count: 20)
//                        strongSelf.configuration(items: list)
                        
                        //MARK: 실제
                        strongSelf.configuration(items: items)
                    } else {
                        strongSelf.add(items: items)
                    }
                    
                    if strongSelf.page >= meta.totalPages {
                        strongSelf.twoListView.isPaging = true
                    } else {
                        strongSelf.twoListView.isPaging = false
                    }
                    
                    strongSelf.page += 1
                    
                    //개수 표기
                    let titleString = "피드 " + meta.totalRecords.toKilo
                    print("titleString : ", titleString)
                    strongSelf.itemInfo = IndicatorInfo(title: titleString)
                    strongSelf.titleRefreshHandler?()
                } else {
                    AlertManager.shared.show(text: "검색 피드 에러", type: .one)
                }
                
            }
        }
    }
}
