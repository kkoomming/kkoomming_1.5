//
//  SearchProfileVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/09.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import XLPagerTabStrip

class SearchProfileVC: BaseVC, IndicatorInfoProvider {
    var itemInfo = IndicatorInfo(title: "프로필(99)")
    var items:[User] = []
    
    var titleRefreshHandler:(()->Void)? = nil
    
    private var keyword = ""
    private var type:SearchType = .FEED
    
    //MARK: 페이징
    var page:Int = 1
    
    let emptyView = UIView().then{
        $0.isHidden = true
        let imageView = UIImageView(image: UIImage(named: "emptyKM"))
        
        $0.addSubview(imageView)
        imageView.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
            $0.width.height.equalTo(164)
        }
    }
    
    let profileAndFollowListView = ProfileAndFollowListView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("SearchProfileVC")
    }
    
    public func configuration(items:[User]) {
        self.items = items
        if self.items.count == 0 {
            self.isEmpty(true)
        } else {
            self.isEmpty(false)
            
            self.profileAndFollowListView.configuration(items: self.items)
            self.profileAndFollowListView.moreHandler = {
                self.search(keyword: self.keyword, isFirst: false)
            }
        }
    }
    
    public func add(items:[User]) {
        self.items += items
        self.profileAndFollowListView.configuration(items: self.items)
    }
    
    private func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        profileAndFollowListView.isHidden = bool
    }
    
    private func setupUI() {
        addSubViews([
            profileAndFollowListView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileAndFollowListView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}

extension SearchProfileVC {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension SearchProfileVC {
    func search(keyword:String, type:SearchType = .USER, isFirst:Bool) {
        self.keyword = keyword
        self.type = type
        
        self.profileAndFollowListView.isPaging = true
        API.search(keyword: keyword, offset: 1, type: type) {[weak self] model in
            guard let strongSelf = self, let model = model, let data = model.data else { return }
            guard let items = data.userData?.users,
                  let meta = data.userData?.metaData else { return }
            
            DispatchQueue.main.async {
                if model.success {
                    if isFirst {
                        //MARK: 테스트용
//                        let list:[User] = Array(repeating: strongSelf.createExample()!, count: 20)
//                        strongSelf.configuration(items: list)
                        
                        //MARK: 실제
                        strongSelf.configuration(items: items)
                    } else {
                        strongSelf.add(items: items)
                    }
                    
                    if strongSelf.page >= meta.totalPages {
                        strongSelf.profileAndFollowListView.isPaging = true
                    } else {
                        strongSelf.profileAndFollowListView.isPaging = false
                    }
                    
                    strongSelf.page += 1
                    
                    //개수 표기
                    let titleString = "프로필 " + meta.totalRecords.toKilo
                    strongSelf.itemInfo = IndicatorInfo(title: titleString)
                    strongSelf.titleRefreshHandler?()
                } else {
                    AlertManager.shared.show(text: "검색 유저 에러", type: .one)
                }
            }
        }
    }
}
