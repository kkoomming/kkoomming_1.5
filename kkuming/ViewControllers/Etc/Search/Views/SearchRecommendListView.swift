//
//  SearchRecommendListView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/03.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class SearchRecommendListView: UIView {
    var items:[User] = []
    
    let leftMargin:CGFloat = 25
    let rightMargin:CGFloat = 25
    lazy var width:CGFloat = 100
    lazy var height:CGFloat = 124
    let lineSpacing:CGFloat = 16
    
    lazy var layout = UICollectionViewFlowLayout().then{
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .vertical
    }
    
    lazy var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(SearchRecommendListViewCell.self, forCellWithReuseIdentifier: SearchRecommendListViewCell.identifier)
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
        $0.isScrollEnabled = false
        $0.isPagingEnabled = false
    }
    
    public func configuration(items:[User]) {
        self.items = items
        collectionView.reloadData()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    private func setupUI() {
        addSubViews([
            collectionView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension SearchRecommendListView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchRecommendListViewCell.identifier, for: indexPath) as! SearchRecommendListViewCell
        let index = indexPath.item
        let item = items[index]
        
        cell.profileImageView.kf.setImage(with: item.avatar?.toURL, options: [.loadDiskFileSynchronously])
        cell.profileLabel.text = item.nickname
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        let item = items[index]
        
        App.push(ProfileVC(userId: item.id ?? 0))
    }
}


class SearchRecommendListViewCell: UICollectionViewCell {
    static let identifier = String(describing: SearchRecommendListViewCell.self)
    
    let profileImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 50
        
        $0.contentMode = .scaleToFill
    }
    
    let profileLabel = UILabel().then{
        $0.font = getFont(size: 12, type: .bold)
        $0.textAlignment = .center
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        contentView.addSubViews([
            profileImageView,
            profileLabel
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileImageView.snp.makeConstraints{
            $0.height.equalTo(100)
            $0.leading.trailing.top.equalToSuperview()
        }
        
        profileLabel.snp.makeConstraints{
            $0.top.equalTo(profileImageView.snp.bottom).offset(11)
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
