//
//  ReportGuideVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/13.
//

import UIKit
import SnapKit
import Then

class ReportGuideVC: BaseVC {
    let topView = TopView(type: .back, title: "신고 안내")
    
    let guideStackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 15
        $0.backgroundColor = RGB(red: 250, green: 250, blue: 250)
        $0.layoutMargins = UIEdgeInsets(top: 40, left: 10, bottom: 40, right: 10)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    let titleLabel = UILabel().then{
        $0.text = "신고가 접수되어 주의를 부탁드립니다."
        $0.font = getFont(size: 16, type: .bold)
        $0.textAlignment = .center
    }
    
    let contentLabel = UILabel().then{
        $0.text = """
        게시물 및 댓글에 아래와 같은 내용이 있는지 확인해주세요.
        
        1. 욕설 및 성희롱
        2. 음란성 또는 청소년에게 부적합한 내용
        3. 명예훼손 및 사생활 침해
        4. 저작권 침해
        
        신고가 반복적으로 발생할 경우,
        서비스 이용이 정지될 수 있습니다.
        다시 한번 주의를 부탁드립니다.
        """
        $0.font = getFont(size: 12)
        $0.textColor = RGB(red: 154, green: 154, blue: 154)
        $0.numberOfLines = 0
        $0.textAlignment = .center
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    private func setupUI() {
        [
            titleLabel,
            contentLabel
        ].forEach{
            guideStackView.addArrangedSubview($0)
        }
        
        guideStackView.setCustomSpacing(20, after: titleLabel)
        
        addSubViews([
            topView,
            guideStackView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        guideStackView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom).offset(5)
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-25)
        }
    }
    
    private func setupRx() {
        
    }
}
