//
//  SwitchView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/13.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class SwitchView: UIView {
    let disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
