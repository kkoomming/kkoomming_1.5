//
//  SettingToggleView.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/28.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import Then

class SettingToggleView: UIView {
    let disposeBag = DisposeBag()
    
    let toggleImage = UIImage(named: "toggle")
    let toggleOverImage = UIImage(named: "toggle_over")
    
    var isToggle:Bool = false {
        didSet {
            self.toggleImageView.image = isToggle ? toggleOverImage : toggleImage
        }
    }
    
    var completion:(()->Void)? = nil
    
    let titleLabel = UILabel().then{
        $0.font = getFont(size: 14)
        $0.textColor = RGB(red: 93, green: 93, blue: 93)
    }
    
    lazy var toggleImageView = UIImageView()
    
    lazy var toggleButton = UIButton().then{
        $0.rx.tap.bind{[weak self] in
            guard let strongSelf = self else {
                return
            }
            strongSelf.completion?()
        }.disposed(by: disposeBag)
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public func configuration(title:String = "", status isToggle:String, completion:(()->Void)? = nil) {
        if title != "" {
            self.titleLabel.text = title
        }
        self.isToggle = isToggle == SwitchType.ON.rawValue ? true : false
        
        if completion != nil {
            self.completion = completion
        }
    }
    
    private func setupUI() {
        addSubViews([
            titleLabel,
            toggleImageView,
            toggleButton
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints{
            $0.leading.equalToSuperview()
            $0.centerY.equalToSuperview()
        }
        
        toggleImageView.snp.makeConstraints{
            $0.width.equalTo(39)
            $0.height.equalTo(20)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        
        toggleButton.snp.makeConstraints{
            $0.width.equalTo(39)
            $0.height.equalTo(20)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
