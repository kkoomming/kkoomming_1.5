//
//  SettingVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/04.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import AuthenticationServices
import NaverThirdPartyLogin
import Kingfisher

class SettingVC: BaseVC {
    var model: SettingData? = nil
    
    let disposeBag = DisposeBag()
    
    let topView = TopView(type: .back, title: "설정")
    
    let scrollView = UIScrollView()
    
    let stackView = UIStackView().then {
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.spacing = 15
        $0.layoutMargins = UIEdgeInsets(top: 28, left: 25, bottom: 0, right: 25)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    let pushAlarmView = UIView().then{
        let pushAlarmLabel = PaddingLabel(top: 0, left: 0, right: 0, bottom: 0).then{
            $0.font = getFont(size: 16, type: .bold)
            $0.text = "푸시 알림 설정"
        }
        
        $0.addSubview(pushAlarmLabel)
        
        pushAlarmLabel.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
        }
    }
    
    lazy var likeView = SettingToggleView() //좋아요
    lazy var followView = SettingToggleView() //팔로우
    lazy var commentView = SettingToggleView() //댓글
    lazy var replyView = SettingToggleView() //답글
    lazy var eventView = SettingToggleView() //이벤트
    
    lazy var cashDataView = createMenuButtonView(title: "캐시 데이터", buttonName: "삭제") {
        print("캐시 데이터")
        AlertManager.shared.show(text: "삭제 하시겠습니까?", type: .two) {
            self.cleanCashData() {
                AlertManager.shared.show(text: "삭제가 완료되었습니다.", type: .one)
            }
        }
    }
    lazy var versionInfoView = createMenuButtonView(title: "버전 정보 Ver.\(AppVersion.getVersion())", buttonName: "업데이트 확인") {
        print("버전 정보")
        self.checkVersion()
    }
    lazy var nextBlockManageView = createMenuButtonView(title: "차단 관리",isNext: true) {
        print("차단 관리")
        App.push(SettingBlockVC())
    }
    lazy var nextNoticeView = createMenuButtonView(title: "공지사항",isNext: true) {
        print("공지사항")
        App.push(SettingNoticeVC())
    }
    lazy var nextEventView = createMenuButtonView(title: "이벤트",isNext: true) {
        print("이벤트")
        App.push(EventVC())
    }
    lazy var nextUseTermsView = createMenuButtonView(title: "이용약관",isNext: true) {
        print("이용약관")
        if let url = self.model?.appTermsServiceUrl {
            App.showWebView(title: "이용약관", url: url)
        }
    }
    lazy var nextPrivacyTermsView = createMenuButtonView(title: "개인정보처리방침",isNext: true) {
        print("개인정보처리방침")
        if let url = self.model?.appPrivacyPolicyUrl {
            App.showWebView(title: "개인정보처리방침", url: url)
        }
    }
    lazy var qnaView = createMenuButtonView(title: "문의 및 제안하기") {
        print("문의 및 제안하기")
        AppManager.shared.push(SettingInquireVC())
    }
    lazy var logoutView = createMenuButtonView(title: "로그아웃") {
        AlertManager.shared.show(text: "로그아웃 하시겠습니까?", type: .two) {
            AppManager.shared.logout()
        }
    }
    lazy var withdrawalView = createMenuButtonView(title: "회원탈퇴") {
        print("회원탈퇴")
        AlertManager.shared.show(
            title: "회원탈퇴 하시겠습니까?",
            subTitle: "(교보북클럽 회원탈퇴는 교보문고 사이트 및 앱에서 가능)",
            isWithdrawal: true) { [weak self] in
                guard let strongSelf = self else{
                    return
                }
                
                strongSelf.quitUser()
            }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        getPushStatus()
        getAppSettings()
    }
    
    private func setupUI() {
        setupPushStatus()
        setupStackView()
        
        addSubViews([
            topView,
            scrollView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        scrollView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
    }
    
    private func setupStackView() {
        scrollView.addSubview(stackView)
        
        [
            pushAlarmView,
            likeView,
            followView,
            commentView,
            replyView,
            eventView,
            BorderView(isHeight: true),
            cashDataView,
            BorderView(isHeight: true),
            versionInfoView,
            BorderView(isHeight: true),
            nextBlockManageView,
            nextNoticeView,
            nextEventView,
            BorderView(isHeight: true),
            nextUseTermsView,
            nextPrivacyTermsView,
            qnaView,
            logoutView,
            withdrawalView
        ].forEach{
            stackView.addArrangedSubview($0)
        }
        
        setupStackViewConstraints()
    }
    
    private func setupStackViewConstraints() {
        pushAlarmView.snp.makeConstraints{
            $0.height.equalTo(28)
        }
        
        likeView.snp.makeConstraints{
            $0.height.equalTo(20)
        }
        followView.snp.makeConstraints{
            $0.height.equalTo(20)
        }
        commentView.snp.makeConstraints{
            $0.height.equalTo(20)
        }
        replyView.snp.makeConstraints{
            $0.height.equalTo(20)
        }
        eventView.snp.makeConstraints{
            $0.height.equalTo(20)
        }
        cashDataView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        versionInfoView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        nextBlockManageView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        nextNoticeView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        nextEventView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        nextUseTermsView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        nextPrivacyTermsView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        qnaView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        logoutView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
        withdrawalView.snp.makeConstraints{
            $0.height.equalTo(24)
        }
    }
    
    private func setupPushStatus() {
        likeView.configuration(title: "좋아요", status: SwitchType.ON.rawValue) { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.updateAppPush(view: strongSelf.likeView, status: .NOTI_NEW_LIKE_POST)
        }
        followView.configuration(title: "팔로우", status: SwitchType.ON.rawValue) { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.updateAppPush(view: strongSelf.followView, status: .NOTI_NEW_FOLLOWER)
        }
        commentView.configuration(title: "댓글", status: SwitchType.ON.rawValue) { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.updateAppPush(view: strongSelf.commentView, status: .NOTI_NEW_COMMENT_POST)
        }
        replyView.configuration(title: "답글", status: SwitchType.ON.rawValue) { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.updateAppPush(view: strongSelf.replyView, status: .NOTI_NEW_REPLY_COMMENT)
        }
        eventView.configuration(title: "이벤트", status: SwitchType.ON.rawValue) { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.updateAppPush(view: strongSelf.eventView, status: .NOTI_NEW_NOTICE_EVENT_FROM_SYSYTEM)
        }
    }
}

extension SettingVC {
    func createMenuButtonView(title:String, buttonName:String = "", isRoundButtonStatus:Bool = true, isNext:Bool = false, completion:(()->Void)? = nil) -> UIView{
        let view = UIView().then{
            let isRoundButton = (buttonName != "")
            var isRoundButtonOver:Bool = isRoundButtonStatus
            
            let titleLabel = PaddingLabel(top: 0, left: 0, right: 0, bottom: 0).then{
                $0.font = getFont(size: 16, type: .bold)
                $0.text = title
            }
            
            let buttonView = UIView()
            buttonView.backgroundColor = isRoundButtonOver ? .mainColor : RGB(red: 116, green: 116, blue: 116)
            buttonView.clipsToBounds = true
            buttonView.layer.masksToBounds = true
            buttonView.layer.cornerRadius = 13
            
            let buttonLabel = UILabel()
            buttonLabel.text = buttonName
            buttonLabel.font = getFont(size: 12, type: .bold)
            buttonLabel.textColor = .white
            
            let buttonImageView = UIImageView()
            buttonImageView.image = UIImage(named: "next_btn")
            
            let button = UIButton()
            button.rx.tap.bind{
                
                /*색상 변경 처리
                isRoundButtonOver = !isRoundButtonOver
                
                if isRoundButton {
                    if isRoundButtonOver  {
                        buttonView.backgroundColor = .mainColor
                    } else {
                        buttonView.backgroundColor = RGB(red: 116, green: 116, blue: 116)
                    }
                }
                */
                
                if let action = completion {
                    action()
                }
            }.disposed(by: disposeBag)
            
            if isRoundButton {
                buttonView.addSubview(buttonLabel)
            }
            if isNext {
                buttonView.addSubview(buttonImageView)
            }
            
            buttonView.addSubview(button)
            
            if isRoundButton {
                buttonLabel.snp.makeConstraints{
                    $0.centerY.centerX.equalToSuperview()
                }
            }
            if isNext {
                buttonImageView.snp.makeConstraints{
                    $0.width.height.equalTo(24)
                    $0.trailing.equalToSuperview()
                    $0.centerY.equalToSuperview()
                }
            }
            
            button.snp.makeConstraints{
                $0.top.leading.trailing.bottom.equalToSuperview()
            }
            
            $0.addSubViews([
                titleLabel,
            ])
            
            $0.addSubview(buttonView)
            
            if isRoundButton {
                buttonView.snp.makeConstraints{
                    $0.width.equalTo(titleLabel.intrinsicContentSize.width - 20)
                    $0.height.equalTo(24)
                    $0.centerY.equalToSuperview()
                    $0.trailing.equalToSuperview()
                }
            }
            
            if isNext {
                buttonView.backgroundColor = .clear
                buttonView.snp.makeConstraints{
                    $0.top.leading.trailing.bottom.equalToSuperview()
                }
            }
            
            //라운드 버튼이랑, next버튼 없는 경우
            if !isRoundButton && !isNext {
                buttonView.backgroundColor = .clear
                buttonView.snp.makeConstraints{
                    $0.top.leading.trailing.bottom.equalToSuperview()
                }
            }
            
            titleLabel.snp.makeConstraints{
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        return view
    }

}

//MARK: - 기능
extension SettingVC {
    private func configuration(model:SettingData){
        self.model = model
    }
    
    private func configuration(data:PushStatusData) {
        likeView.configuration(status: data.notiNewLikePost)
        followView.configuration(status: data.notiNewFollower)
        commentView.configuration(status: data.notiNewCommentPost)
        replyView.configuration(status: data.notiNewReplyComment)
        eventView.configuration(status: data.notiNewNoticeEventFromSystem)
    }
    
    private func cleanCashData(completion:(()->Void)? = nil) {
        UserDefaultsUtil.save(value: [], key: .userRecentSearchKeyword)
        UserDefaultsUtil.save(value: "", key: .mainPopupDate)
        
        let cache = ImageCache.default
        cache.clearCache()
        cache.clearDiskCache()
        cache.clearMemoryCache()
        
        let caches = (NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0])
        let appId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        //삭제할 내용 있으면 변경(Cache.db-wal -> somethings)
        let path = String(format:"%@/%@/Cache.db-wal",caches, appId)
        
        do {
            try FileManager.default.removeItem(atPath: path)
            completion?()
        } catch {
            print("ERROR DESCRIPTION: \(error)")
        }
    }
    
    private func checkVersion() {
        let currentVersion = AppVersion.getVersion()
        var apiVersion = ""
        
        if let version = model?.iOSVersion {
            apiVersion = version
        }
        
        if currentVersion != apiVersion {
            let appId = "id1574307909"
            AlertManager.shared.show(text: "업데이트가 필요합니다.\n앱스토어로 이동하시겠습니까?", type: .two) {
                openSafari(url: "itms-apps://itunes.apple.com/app/\(appId)")
            }
        } else {
            AlertManager.shared.show(text: "현재 최신 버전입니다.", type: .one)
        }
    }
}

//MARK: - API
extension SettingVC {
    //푸시 알림 설정 가져오기
    func getPushStatus() {
        API.getAppPushStatus(){ [weak self] model in
            guard let strongSelf = self, let model = model, let data = model.data else {
                return
            }
            
            strongSelf.configuration(data: data)
        }
    }
    
    //설정 가져오기
    func getAppSettings() {
        API.getAppSettings() {[weak self] model in
            guard let strongSelf = self, let model = model, let data = model.data else {
                return
            }
            
            strongSelf.configuration(model: data)
        }
    }
    
    func updateAppPush(view:SettingToggleView, status:PushType) {
        API.updatePushStatus(bool: !view.isToggle, notiType: status) { model in
            guard let model = model else {
                return
            }

            if model.success {
                view.isToggle = !view.isToggle
            }
        }
    }
    
    //회원탈퇴
    func quitUser() {
        //Apple 회원 탈퇴
//        let loginVC = LoginVC()
//        loginVC.doWithdrawalApple()
        
        API.quitUser() { model in
            guard let model = model else {
                return
            }
            
            if model.success {
                UserDefaultsUtil.clear()
                App.setNavi(LoginVC())
            } else {
                AlertManager.shared.show(text: "잠시 후 다시 시도해 주세요.", type: .one)
            }
        }
    }
}
