//
//  SettingInquireVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/20.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class SettingInquireVC: BaseVC {
    //유효성 검사
    var isCheckEmail:Bool = false //이메일
    var isCheckType:Bool = false //문의 내용 선택
    var isCheckContent:Bool = false //문의 내용
    var isWrite = false //작성 완료 버튼 상태
    
    var type:InquiryType = .INQUIRY
    var selectedIndex:Int = 999999
    
    let inquireContentPlaceholder = "문의 내용을 적어주세요"
    
    let topView = TopView(type: .back, title: "문의 및 제안하기")
    
    let scrollView = UIScrollView()
    
    let stackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.layoutMargins = UIEdgeInsets(top: 20, left: 25, bottom: 0, right: 25)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    lazy var emailFieldView = TextFieldView(cornerRadius: 10).then{
        $0.setTextFieldProperties(font: getFont(size: 12), placeholder: "이메일 입력")
        $0.textField.delegate = self
    }
    
    let inquireLabel = PaddingLabel().then{
        $0.textColor = .black
        $0.font = getFont(size: 16, type: .bold)
        let textString = "문의 내용"
        $0.text = textString
    }
    
    lazy var tagView = TagView().then{
        $0.configuration(items: ["이용문의", "이벤트관련", "서비스제안", "기타"])
        $0.collectionView.delegate = self
        $0.collectionView.dataSource = self
    }
    
    lazy var inquireContentTextView = CustomTextView(cornerRadius:10).then{
        $0.setTextViewProperties(font: getFont(size: 12))
        $0.textView.text = inquireContentPlaceholder
        $0.textView.delegate = self
    }
    
    let guideLabel = UILabel().then{
        $0.font = getFont(size: 10)
        $0.textColor = RGB(red: 112, green: 112, blue: 112)
        $0.numberOfLines = 0
        $0.text = "입력하신 이메일로 답변드립니다. 메일 주소를 확인해주세요. 욕설 및 폭언 등의 내용이 포함된 경우 상담이 제한될 수 있습니다."
    }
    
    lazy var submitButtonView = ButtonView(type: .gray, completion: { [weak self] in
        guard let strongSelf = self else {return}
        
        if !(strongSelf.isCheckEmail && strongSelf.isCheckType && strongSelf.isCheckContent) {
            //작성완료 비활성화 상태
            AlertManager.shared.show(text: "이메일 및 문의 내용을 입력해주세요.\n혹은 문의 내용을 선택해 주세요.", type: .one)
            strongSelf.view.endEditing(true)
            return
        }

        strongSelf.submit()
    }).then{
        $0.titleLabel.font = getFont(size: 13, type: .EBold)
        $0.titleLabel.text = "작성완료"
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tagView.updateTagViewLayout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupUI()
        setupStackView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        KkumingKeyboardManager.shared.isUseKeyboardLibrary(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        KkumingKeyboardManager.shared.isUseKeyboardLibrary(false)
    }
    
    private func setupUI() {
        addSubViews([
            topView,
            scrollView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        scrollView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupStackView() {
        scrollView.addSubview(stackView)
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
        
        [
            emailFieldView,
            inquireLabel,
            tagView,
            inquireContentTextView,
            guideLabel,
            submitButtonView
        ].forEach{
            stackView.addArrangedSubview($0)
        }
        
        emailFieldView.snp.makeConstraints{
            $0.height.equalTo(40)
        }
        
        stackView.setCustomSpacing(28, after: emailFieldView)
        stackView.setCustomSpacing(20, after: inquireLabel)
        
        tagView.snp.makeConstraints{
            $0.height.equalTo(1)
        }
        
        stackView.setCustomSpacing(20, after: tagView)
        
        inquireContentTextView.snp.makeConstraints{
            $0.height.equalTo(217)
        }
        
        stackView.setCustomSpacing(20, after: inquireContentTextView)
        stackView.setCustomSpacing(20, after: guideLabel)
        
        submitButtonView.snp.makeConstraints{
            $0.height.equalTo(43)
        }
    }
}

//MARK: - 기능
extension SettingInquireVC {
    func updateWriteStatus() {
        isCheckEmail = false
        isCheckType = false
        isCheckContent = false
        isWrite = false
        
        //이메일
        if let email = emailFieldView.textField.text {
            if email.count != 0 {
                isCheckEmail = true
            }
        }
        
        //문의 내용 선택
        if selectedIndex != 999999 {
            isCheckType = true
        }
        
        //문의 내용
        if let text = inquireContentTextView.textView.text {
            if text.count != 0 && text != inquireContentPlaceholder {
                isCheckContent = true
            }
        }
        
        if self.isCheckEmail && self.isCheckType && self.isCheckContent {
            isWrite = true
        }
        
        updateWriteButtonUI()
    }
    
    func updateWriteButtonUI() {
        if isWrite {
            updateButtonStatus(true)
        } else {
            updateButtonStatus(false)
        }
    }
    
    public func updateButtonStatus(_ bool:Bool){
        if bool {
            submitButtonView.layer.borderColor = UIColor.mainColor.cgColor
            submitButtonView.titleLabel.textColor = UIColor.mainColor
        } else {
            submitButtonView.layer.borderColor = RGB(red: 210, green: 210, blue: 210).cgColor
            submitButtonView.titleLabel.textColor = RGB(red: 210, green: 210, blue: 210)
        }
    }
}

//MARK: - 이메일, 문의 내용
extension SettingInquireVC: UITextFieldDelegate, UITextViewDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("textFieldDidEndEditing")
        self.updateWriteStatus()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if let text = textView.text, text == inquireContentPlaceholder {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if let text = textView.text, text == "" {
            textView.text = inquireContentPlaceholder
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("textViewDidChange")
        self.updateWriteStatus()
    }
}

//MARK: - Tag 관련
extension SettingInquireVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tagView.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagViewCell.identifier, for: indexPath) as! TagViewCell
        let index = indexPath.item
        
        if selectedIndex == index {
            cell.backgroundColor = .mainColor
            cell.layer.cornerRadius = tagView.cornerRadius
            cell.layer.borderWidth = 0
            cell.tagLabel.textColor = UIColor.white
        } else {
            cell.backgroundColor = .white
            cell.layer.cornerRadius = tagView.cornerRadius
            cell.layer.borderWidth = tagView.borderWidth
            cell.layer.borderColor = UIColor.mainColor.cgColor
            cell.tagLabel.textColor = UIColor.mainColor
        }
        
        cell.tagLabel.font = tagView.textFont
        cell.tagLabel.text = tagView.items[index]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pageIndex = indexPath.item
        print("pageIndex : \(pageIndex)")
        self.selectedIndex = pageIndex
        self.updateWriteStatus()
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let leftMargin:CGFloat = 15
        let rightMargin:CGFloat = 15
        let index = indexPath.item
        let size = getLabelSize(text: tagView.items[index], font: getFont(size: 11, type: .bold))
        let width = leftMargin + size.width + rightMargin
        
        return CGSize(width: width, height: tagView.cellHeight)
    }
}

//MARK: - API
extension SettingInquireVC {
    func submit() {
        guard let email = self.emailFieldView.textField.text, let questionContent = self.inquireContentTextView.textView.text else {
            return
        }
        API.inquiry(email: email, type: self.type, questionContent: questionContent) { [weak self] model in
            guard let model = model else {
                return
            }
            
            if model.success {
                AlertManager.shared.show(text: "문의가 접수되었습니다.", type: .one) {
                    App.pop()
                }
            } else {
                AlertManager.shared.show(text: "잠시 후 다시 시도해 주세요.", type: .one)
            }
        }
    }
}
