//
//  SettingNoticeVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/16.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class SettingNoticeVC: BaseVC {
    var items:[Notice] = []
    
    var page = 1
    var isPaging = false
    var isMain = true
    
    let disposeBag = DisposeBag()
    
    let topView = TopView(type: .back, title: "공지사항")
    
    lazy var tableView = UITableView().then{
        $0.register(SettingNoticeViewCell.self, forCellReuseIdentifier: SettingNoticeViewCell.identifier)
        $0.separatorStyle = .none
        $0.delegate = self
        $0.dataSource = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        getListNotice()
    }
    
    private func setupUI() {
        addSubViews([
            topView,
            tableView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        tableView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func configuration(items:[Notice]) {
        self.items = items
        self.tableView.reloadData()
    }
    
    private func add(items:[Notice]) {
        self.items += items
        self.tableView.reloadData()
    }
}

extension SettingNoticeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingNoticeViewCell.identifier, for: indexPath) as! SettingNoticeViewCell
        let item = self.items[indexPath.row]
        
        cell.titleLabel.text = item.title
        cell.timeLabel.text = dateToFormat(string: item.createdAt, dateFormat: .comment)
        
        cell.setNewView(item.isNew)
        
        if item.isOpen {
            cell.contentLabel.text = item.content
        } else {
            cell.contentLabel.text = ""
        }
        
        cell.isOpen(item.isOpen)
        
        cell.completion = {
            self.items[indexPath.row].isOpen = !self.items[indexPath.row].isOpen
            cell.isOpen(self.items[indexPath.row].isOpen)
            tableView.reloadData()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class SettingNoticeViewCell: UITableViewCell{
    static let identifier = String(describing: SettingNoticeViewCell.self)
    var disposeBag = DisposeBag()
    var isOpen = false
    var previousContentHeight:CGFloat = 0.0
    var completion:(()->Void)? = nil
    
    let newView = UIView().then{
        $0.backgroundColor = .mainColor
        let label = UILabel()
        label.text = "New"
        label.textColor = .white
        label.font = getFont(size: 12)
        $0.addSubview(label)
        label.snp.makeConstraints{
            $0.centerY.centerX.equalToSuperview()
        }
    }
    let arrowImageView = UIImageView(image: UIImage(named: "ic_community_arrow")!)
    
    lazy var arrowView = UIView().then{
        let arrowButton = UIButton()
        arrowButton.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            if strongSelf.completion != nil {
                strongSelf.completion!()
            }
        }.disposed(by: disposeBag)
        
        $0.addSubViews([
            arrowImageView,
            arrowButton
        ])
        
        arrowImageView.snp.makeConstraints{
            $0.width.height.equalTo(20)
            $0.centerX.centerY.equalToSuperview()
        }
        
        arrowButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    let titleLabel = UILabel().then{
        $0.font = getFont(size: 14, type: .bold)
        $0.text = "꾸밍 리뉴얼 오픈 기념 이벤트"
        $0.numberOfLines = 2
    }
    
    let timeLabel = UILabel().then{
        $0.font = getFont(size: 10, type: .bold)
        $0.textColor = RGB(red: 154, green: 154, blue: 154)
        $0.text = "2023.04.26"
    }
    
    let contentStackView = UIStackView().then{
        $0.alignment = .fill
        $0.axis = .vertical
        $0.distribution = .fill
        $0.layoutMargins = UIEdgeInsets(top: 6, left: 0, bottom: 0, right: 0)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    let contentLabel = UILabel().then{
        $0.numberOfLines = 0
        $0.font = getFont(size: 11)
        $0.textColor = RGB(red: 93, green: 93, blue: 93)
        $0.text = ""
    }
    
    let bottomBorderView = BorderView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        setupUI()
    }
    
    public func isOpen(_ bool:Bool) {
        if bool {
            arrowImageView.transform = CGAffineTransform(rotationAngle: .pi)
            contentStackView.setCustomSpacing(27, after: timeLabel)
        } else {
            arrowImageView.transform = .identity
            contentStackView.setCustomSpacing(0, after: timeLabel)
        }
    }
    
    private func setupUI() {
        contentStackView.addArrangedSubview(timeLabel)
        contentStackView.addArrangedSubview(contentLabel)
        
        contentView.addSubViews([
            newView,
            titleLabel,
            arrowView,
            contentStackView,
            bottomBorderView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        newView.snp.makeConstraints{
            $0.width.equalTo(37)
            $0.height.equalTo(22)
            $0.top.equalToSuperview().offset(30)
            $0.leading.equalToSuperview().offset(25)
        }
        
        titleLabel.snp.makeConstraints{
            $0.top.equalTo(newView.snp.top).offset(4)
            $0.leading.equalTo(newView.snp.trailing).offset(10)
            $0.trailing.equalTo(arrowView.snp.leading).offset(-8)
        }
        
        arrowView.snp.makeConstraints{
            $0.width.height.equalTo(20)
            $0.top.equalTo(titleLabel.snp.top)
            $0.trailing.equalToSuperview().offset(-25)
        }
        
        contentStackView.snp.makeConstraints{
            $0.top.equalTo(titleLabel.snp.bottom)
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-50)
        }
        
        bottomBorderView.snp.makeConstraints{
            $0.top.equalTo(contentStackView.snp.bottom).offset(30)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.height.equalTo(1)
        }
    }
    
    public func setNewView(_ bool:Bool) {
        self.newView.isHidden = !bool
        
        if bool {
            newView.snp.updateConstraints{
                $0.width.equalTo(37)
            }
            titleLabel.snp.updateConstraints{
                $0.leading.equalTo(newView.snp.trailing).offset(10)
            }
        } else {
            newView.snp.updateConstraints{
                $0.width.equalTo(0)
            }
            titleLabel.snp.updateConstraints{
                $0.leading.equalTo(newView.snp.trailing).offset(0)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SettingNoticeVC: UIScrollViewDelegate {
    func getListNotice() {
        API.getListNotice(offset: page) { model in
            guard let items = model?.data?.notices, let meta = model?.data?.metaData else { return }
            
            if self.isMain {
                self.configuration(items: items)
                self.isMain = false
            } else {
                self.add(items: items)
            }
            
            if self.page >= meta.totalPages {
                self.isPaging = true
            } else {
                self.isPaging = false
            }
            
            self.page += 1
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        let contentSizeHeight = scrollView.contentSize.height
        let currentViewHeight = scrollView.frame.height
        
        if position > (contentSizeHeight - currentViewHeight - 100) {
            guard !isPaging else { return }
            self.isPaging = true
            print("paging")
            self.getListNotice()
        }
    }
}
