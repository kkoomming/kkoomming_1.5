//
//  SettingBlockVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/16.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class SettingBlockVC: BaseVC {
    let disposeBag = DisposeBag()
    var items:[User] = []
    
    //MARK: 페이징
    var page = 1
    var isPaging = false
    
    let topView = TopView(type: .back, title: "차단 관리")
    
    lazy var tableView = UITableView().then{
        $0.register(SettingBlockViewCell.self, forCellReuseIdentifier: SettingBlockViewCell.identifier)
        $0.separatorStyle = .none
        $0.delegate = self
        $0.dataSource = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        getListUserBlocked(isFirst: true)
    }
    
    private func setupUI() {
        addSubViews([
            topView,
            tableView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        tableView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
    }
}

extension SettingBlockVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingBlockViewCell.identifier, for: indexPath) as! SettingBlockViewCell
        let index = indexPath.row
        let item = self.items[index]
        
        cell.configuration(user: item)
        cell.buttonView.completion = {
            print("차단중")
            if let isBlocked = self.items[index].isBlocked {
                self.items[index].isBlocked = !isBlocked
                if isBlocked {
                    //차단 해제
                    AlertManager.shared.show(text: "차단 해제 하시겠습니까?", type: .two) {
                        self.unblockUser(userId: item.id ?? 0)
                    }
                } else {
                    //차단
                    AlertManager.shared.show(text: "차단하시겠습니까?", type: .two) {
                        self.blockUser(userId: item.id ?? 0)
                    }
                }
                
                self.tableView.reloadData()
            }
        }
        
        if let isBlocked = item.isBlocked {
            cell.isButtonStatus = isBlocked
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.items.count - 1 {
            guard !isPaging else { return }
            self.isPaging = true
            self.getListUserBlocked(isFirst: false)
        }
    }
}

//MARK: - 기능
extension SettingBlockVC {
    func configuration(items:[User]) {
        var users:[User] = []
        
        for item in items {
            var temp = item
            temp.isBlocked = true
            users.append(temp)
        }
        
        self.items = users
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    public func add(items:[User]) {
        var users:[User] = []
        
        for item in items {
            var temp = item
            temp.isBlocked = true
            users.append(temp)
        }
        
        self.items += users
        tableView.reloadData()
    }
}

//MARK: - API
extension SettingBlockVC {
    //리스트 가져오기
    func getListUserBlocked(isFirst:Bool) {
        API.getListUserBlocked(page:page) { [weak self] model in
            guard let strongSelf = self,
                  let model = model,
                  let data = model.data,
                  let users = data.users,
                  let meta = data.metaData
            else {
                return
            }
            
            if isFirst {
                strongSelf.configuration(items: users)
            } else {
                strongSelf.add(items: users)
            }
            
            if strongSelf.page >= meta.totalPages {
                strongSelf.isPaging = true
            } else {
                strongSelf.isPaging = false
            }
            
            strongSelf.page += 1
        }
    }
    
    func blockUser(userId:Int) {
        API.blockUser(userId: userId) { [weak self] model, data in
            guard let strongSelf = self, let model = model else {
                AlertManager.shared.show(text: "프로필 차단 에러\n잠시 후 다시 시도해 주세요.", type: .one)
                return
            }
            
            if model.success {
                strongSelf.tableView.reloadData()
            } else {
                AlertManager.shared.show(text: "프로필 차단 에러", type: .one)
            }
                
        }
    }
    
    func unblockUser(userId:Int) {
        API.unblockUser(userId: userId) { [weak self] model in
            guard let strongSelf = self, let model = model
            else {
                return
            }
            
            if model.success {
                strongSelf.tableView.reloadData()
            }
        }
    }
}

//MARK: - CELL
class SettingBlockViewCell: UITableViewCell {
    static let identifier = String(describing: SettingBlockViewCell.self)
    
    let profileImageView = UIImageView().then {
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 36 / 2
        $0.image = UIImage(named: "coinExample")
    }
    
    let profileNameLabel = UILabel().then{
        $0.font = getFont(size: 14, type: .bold)
        $0.text = "Jang"
    }
    
    var isButtonStatus = true {
        didSet {
            buttonView.updateButtonStatus(isButtonStatus)
        }
    }
    
    lazy var buttonView = ButtonView(type: .follow, isButtonStatus: isButtonStatus).then{
        $0.setButtonTitle(onString: "차단중", offString: "차단하기")
        $0.layer.cornerRadius = 12
        $0.titleLabel.font = getFont(size: 9, type: .bold)
    }
    
    let bottomBorder = BorderView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        
        setupUI()
    }
    
    private func setupUI() {
        contentView.addSubViews([
            profileImageView,
            profileNameLabel,
            buttonView,
            bottomBorder
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileImageView.snp.makeConstraints{
            $0.leading.equalToSuperview()
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(36)
        }
        
        profileNameLabel.snp.makeConstraints{
            $0.leading.equalTo(profileImageView.snp.trailing).offset(10)
            $0.centerY.equalToSuperview()
        }
        
        buttonView.snp.makeConstraints{
            $0.width.equalTo(56)
            $0.height.equalTo(23)
            $0.centerY.equalTo(profileImageView.snp.centerY)
            $0.trailing.equalToSuperview()
        }
        
        bottomBorder.snp.makeConstraints{
            $0.top.equalTo(profileImageView.snp.bottom).offset(12)
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(1)
        }
    }
    
    public func configuration(user:User) {
        profileImageView.kf.setImage(with: user.avatar?.toURL, options: [.loadDiskFileSynchronously])
        profileNameLabel.text = user.nickname
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
