//
//  GalleryTestVC.swift
//  Ggooming
//
//  Created by 장효원 on 2022/04/12.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import Then

class GalleryTestVC: BaseVC {
    let disposeBag = DisposeBag()
    
    lazy var button = UIButton().then{
        $0.setTitle("Click", for: .normal)
        $0.setTitleColor(UIColor.black, for: .normal)
        $0.rx.tap.bind{
            print("Photo Picker Open")
            self.navigationController?.pushViewController(GalleryPickerVC(), animated: true)
        }.disposed(by: disposeBag)
    }
    
    let previewImageView = UIImageView().then{
        $0.contentMode = .scaleAspectFill
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        addSubViews([
            button,
            previewImageView
        ])
        
        setupConstraints()
    }
    
    func setupConstraints() {
        button.snp.makeConstraints{
            $0.top.equalTo(view.safeAreaLayoutGuide)
            $0.centerX.equalToSuperview()
        }
        
        previewImageView.snp.makeConstraints{
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(0)
        }
        
    }
}
