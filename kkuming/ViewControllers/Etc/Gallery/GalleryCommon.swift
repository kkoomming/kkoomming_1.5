//
//  GalleryCommon.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/23.
//

import Foundation
import UIKit

class GalleryCommon {
    static let shared = GalleryCommon()
    
    private var itemSize:CGSize = .zero
    
    func setItemSize(_ itemSize:CGSize) {
        self.itemSize = itemSize
    }
    
    func getItemSize() -> CGSize {
        return self.itemSize
    }
}
