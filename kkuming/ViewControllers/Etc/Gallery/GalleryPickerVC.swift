//
//  GalleryPickerVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/14.
//

import UIKit
import SnapKit
import Then
import Photos
import RxSwift
import RxCocoa

class GalleryPickerVC: BaseVC {
    let disposeBag = DisposeBag()
    //MARK: - 앨범 관련
    var isConfirm:Bool = false
    var allMedia: PHFetchResult<PHAsset>?
    var selectedData:[GalleryData?] = [] {
        didSet {
            bottomView.disposeBag = DisposeBag()
            bottomView.setupEditButton(images: self.selectedData)
        }
    }
    
    //MARK: 앨범 동기화
    private var queue = DispatchQueue(label: "kr.kkuming.album")
    private var requestIDs = SynchronizedDictionary<IndexPath,PHImageRequestID>()
    
    //MARK: 멀티 선택 가능한지 아닌지 확인(멀티 true : 글쓰기, false : 프로필수정)
    var isMulti:Bool = true
    
    var profileUpdateHandler:((GalleryData?)->Void)? = nil
    
    lazy var topView:TopView = TopView(type:.back, title: "앨범")
    
    var numberLabel = UILabel().then{
        $0.text = ""
        $0.font = getFont(size: 16, type: .EBold)
    }
    
    lazy var confirmButton = UIButton().then{
        $0.setTitle("완료", for: .normal)
        $0.titleLabel?.font = getFont(size: 16)
        $0.setTitleColor(.mainColor, for: .normal)
        $0.rx.tap.bind{
            print("confirmButton")
            GalleryManager.shared.setSelectedImages(self.selectedData)
            NotificationCenter.default.post(name: .updateSelectedData, object: nil)
            App.pop()
        }.disposed(by: disposeBag)
    }
    
    let gridLayout:GalleryGridLayout = GalleryGridLayout()
    
    lazy var collectionView:UICollectionView = UICollectionView(
        frame: CGRect.zero,
        collectionViewLayout: gridLayout
    ).then{
        $0.register(GalleryGridDetailCell.self, forCellWithReuseIdentifier: "GalleryGridDetailCell")
        $0.register(GalleryEmptyCell.self, forCellWithReuseIdentifier: "GalleryEmptyCell")
        $0.allowsMultipleSelection = true
        $0.delegate = self
        $0.dataSource = self
    }
    
    lazy var bottomView = GalleryBottomView().then {
        $0.setupEditButton(images: self.selectedData)
    }
    
    init(isMulti:Bool = true) {
        super.init(nibName: nil, bundle: nil)
        
        self.isMulti = isMulti
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchPhotos()
        setupUI()
        
        if !self.isMulti {
            confirmButton.isHidden = true
            bottomView.isHidden = true
        }
    }
    
    func setupUI() {
        topView.addSubViews([
            confirmButton,
            numberLabel
        ])
        
        addSubViews([
            topView,
            collectionView,
            bottomView
        ])
        
        setupConstraints()
    }
    
    func setupConstraints() {
        confirmButton.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        numberLabel.snp.makeConstraints{
            $0.trailing.equalTo(confirmButton.snp.leading).offset(-5)
            $0.centerY.equalToSuperview()
        }
        
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        collectionView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(bottomView.snp.top)
        }
        
        bottomView.snp.makeConstraints{
            $0.bottom.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(50)
        }
    }
}

extension GalleryPickerVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let allMedia = allMedia {
            return allMedia.count + 1
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var index = indexPath.item
        //카메라
        if index == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryEmptyCell", for: indexPath) as! GalleryEmptyCell

            return cell
        }
        
        //필수
        index -= 1
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryGridDetailCell", for: indexPath) as! GalleryGridDetailCell
        let asset = self.allMedia?[index]
        
        if self.isMulti {
            cell.circleView.isHidden = false
        } else {
            cell.circleView.isHidden = true
        }
        
        if let index = self.selectedData.firstIndex(where: {
            $0?.identifier == asset?.localIdentifier
        }) {
            //selected
            let number = (index + 1)
            cell.setCircleNumber(number)
        } else {
            //deselected
            cell.isSelected(false)
        }
        
        queue.async { [weak self, weak cell] in
            guard let strongSelf = self else { return }
            let requestIDs = LocalImageManager.shared.imageAsset(asset: asset!, completionBlock: { (data,image,complete) in
                
                DispatchQueue.main.async {
                    if strongSelf.requestIDs[indexPath] != nil {
                        cell?.setImage(image: image)
                        
                        if complete {
                            strongSelf.requestIDs.removeValue(forKey: indexPath)
                        }
                    }
                }
            })
            if requestIDs > 0 {
                strongSelf.requestIDs[indexPath] = requestIDs
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        
        var index = indexPath.item
        //카메라 클릭
        if index == 0 {
            print("카메라 촬영")
            if GalleryManager.shared.isRequestCamera() {
                GalleryManager.shared.startCamera(self) { [weak self] in
                    guard let strongSelf = self else {
                        return
                    }
                    
                    if !strongSelf.isMulti {
                        let vc = GallerySelectedVC()
                        let images = strongSelf.createHighQualityImages(images: GalleryManager.shared.getSelectedImages())

                        vc.profileUpdateHandler = strongSelf.profileUpdateHandler
                        vc.images = images
                        vc.galleryUpdateHandler = { [weak self] in
                            guard let strongSelf = self else {
                                return
                            }
                            strongSelf.fetchPhotos()
                        }
                        
                        GalleryManager.shared.clear()
                        
                        App.push(vc)
                    } else {
                        strongSelf.fetchPhotos()
                    }
                }
            }
            
            return
        }
        
        //필수
        index -= 1
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? GalleryGridDetailCell else {
            return
        }
        
        let asset = self.allMedia?[index]
        let identifier = asset?.localIdentifier
        
        if self.isMulti {
            if let index = self.selectedData.firstIndex(where: {$0?.identifier == identifier}) {
                self.selectedData.remove(at: index)
                cell.isSelected(false)
            } else {
                if !self.isCheckMoreThanCount() {
                    LocalImageManager.shared.requestImage(with: asset, completion: { [weak self] data in
                        guard let storngSelf = self else {
                            return
                        }
                        storngSelf.selectedData.append(data)
                        let number = storngSelf.selectedData.count
                        cell.setCircleNumber(number)
                    })
                } else {
                    AlertManager.shared.show(text: "사진은 최대 10장까지만 등록가능합니다.", type: .one)
                }
            }
            
            self.updateGalleryData()
            self.orderUpdateCells()
        } else {
            //바로 편집창 이동
            let vc = GallerySelectedVC()
            vc.profileUpdateHandler = self.profileUpdateHandler
            LocalImageManager.shared.requestImage(with: asset, completion: { data in
                vc.images = self.createHighQualityImages(images: [data])
            })
            App.push(vc)
        }
    }
    
    private func orderUpdateCells() {
        let visibleIndexPaths = self.collectionView.indexPathsForVisibleItems.sorted(by: { $0.row < $1.row })
        for indexPath in visibleIndexPaths {
            guard let cell = self.collectionView.cellForItem(at: indexPath) as? GalleryGridDetailCell else { continue }
            
            let index = indexPath.item - 1
            let asset = self.allMedia?[index]
            
            if let index = self.selectedData.firstIndex(where: {
                $0?.identifier == asset?.localIdentifier
            }) {
                //selected
                let number = (index + 1)
                cell.setCircleNumber(number)
            } else {
                //deselected
                cell.isSelected(false)
            }
        }
    }
}

/*
 https://stackoverflow.com/questions/28259961/swift-how-to-get-last-taken-3-photos-from-photo-library
 https://zeddios.tistory.com/620
 */
extension GalleryPickerVC {
    func fetchPhotos() {
        selectedData = GalleryManager.shared.getSelectedImages()
        updateGalleryData()
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
        self.allMedia = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        self.collectionView.reloadData()
    }
    
    func isCheckMoreThanCount() -> Bool {
        return self.selectedData.count >= 10 ? true : false
    }
    
    func updateGalleryData() {
        let count = selectedData.count
        if count == 0 {
            numberLabel.text = ""
        } else {
            let dataCount = selectedData.count
            numberLabel.text = String(dataCount)
        }
    }
    
    private func createHighQualityImages(images:[GalleryData?]) -> [GalleryData?] {
        var temp:[GalleryData?] = []
        for item in images {
            if let data = item {
                LocalImageManager.shared.requestImage(with: data.asset) { result in
                    temp.append(result)
                }
            }
        }
        return temp
    }
}
