//
//  GalleryCropVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/19.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class GalleryCropVC: BaseVC {
    let disposeBag:DisposeBag = DisposeBag()
    var cropHandler:((UIImage)->Void)? = nil
    var image:UIImage? = nil
    
    private lazy var cropPickerView: CropPickerView = {
        let cropPickerView = CropPickerView()
        cropPickerView.backgroundColor = .black
        cropPickerView.delegate = self
        cropPickerView.image(self.image)
        cropPickerView.aspectRatio = 1
        return cropPickerView
    }()
    
    lazy var bottomView:TopView = TopView(type: .back, title: "자르기").then{
        for view in $0.leftView.subviews {
            if view is UIImageView {
                let imageView = view as! UIImageView
                imageView.image = UIImage(named: "ic_back_white")
            }
        }
        $0.titleLabel.textColor = .white
    }
    
    var numberLabel = UILabel().then{
        $0.text = ""
        $0.font = getFont(size: 16, type: .EBold)
    }
    
    lazy var confirmButton = UIButton().then{
        $0.setTitle("완료", for: .normal)
        $0.titleLabel?.font = getFont(size: 16)
        $0.setTitleColor(.white, for: .normal)
        $0.rx.tap.bind{
            self.crop()
        }.disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        
        setupUI()
    }
    
    func setupUI() {
        bottomView.addSubViews([
            confirmButton,
            numberLabel
        ])
        
        addSubViews([
            cropPickerView,
            bottomView
        ])
        
        setupConstraints()
    }
    
    func setupConstraints() {
        cropPickerView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide).offset(20)
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
            $0.bottom.equalTo(bottomView.snp.top).offset(-20)
        }
        
        confirmButton.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        numberLabel.snp.makeConstraints{
            $0.trailing.equalTo(confirmButton.snp.leading).offset(-5)
            $0.centerY.equalToSuperview()
        }
        
        bottomView.snp.makeConstraints{
            $0.height.equalTo(45)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(self.view.safeAreaLayoutGuide)
        }
    }
    
    func crop() {
        self.cropPickerView.crop { [weak self](crop) in
            guard let strongSelf = self else { return }
            
            if strongSelf.cropHandler != nil {
                if let image = crop.image {
                    strongSelf.cropHandler!(image)
                } else {
                    print("이미지 없음")
                }
            }
        }
    }
}

extension GalleryCropVC: CropPickerViewDelegate {
    func cropPickerView(_ cropPickerView: CropPickerView, result: CropResult) {

    }

    func cropPickerView(_ cropPickerView: CropPickerView, didChange frame: CGRect) {

    }
}
