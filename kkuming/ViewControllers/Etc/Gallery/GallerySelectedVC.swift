//
//  GallerySelectedVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/15.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class GallerySelectedVC: BaseVC {
    let disposeBag = DisposeBag()
    var selectedIndex:Int = 0

    var galleryUpdateHandler:(()->Void)? = nil
    var profileUpdateHandler:((GalleryData?)->Void)? = nil
    
    lazy var currentIndex: CGFloat = 0
    
    var images:[GalleryData?] = [] {
        didSet{
            collectionView.reloadData()
        }
    }
    
    var isProfileUpdateVC:Bool = false

    lazy var topView = TopView(type: .back){
    }.then{
        for view in $0.leftView.subviews {
            if view is UIImageView {
                let imageView = view as! UIImageView
                imageView.image = UIImage(named: "ic_back_white")
            }
        }
        $0.backgroundColor = RGB(red: 20, green: 20, blue: 20)
        $0.galleryUpdateHandler = galleryUpdateHandler
    }
    
    lazy var confirmButton = UIButton().then{
        $0.setTitle("완료", for: .normal)
        $0.titleLabel?.font = getFont(size: 16)
        $0.setTitleColor(.white, for: .normal)
        $0.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            if strongSelf.profileUpdateHandler == nil {
                GalleryManager.shared.setSelectedImages(strongSelf.images)
                NotificationCenter.default.post(name: .updateSelectedData, object: nil)
                App.pop()
                App.pop()
            } else {
                strongSelf.profileUpdateHandler?(strongSelf.images.first as? GalleryData)
                App.pop()
                if !strongSelf.isProfileUpdateVC {
                    App.pop()
                }
            }
        }.disposed(by: disposeBag)
    }
    
    lazy var gridLayout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()

    lazy var collectionView:UICollectionView = UICollectionView(
        frame: CGRect.zero,
        collectionViewLayout: gridLayout
    ).then{
        $0.register(GallerySelectedCell.self, forCellWithReuseIdentifier: "GallerySelectedCell")
        $0.backgroundColor = .black
        $0.delegate = self
        $0.dataSource = self
    }
    
    let bottomView = UIView().then{
        $0.backgroundColor = RGB(red: 20, green: 20, blue: 20)
    }
    
    let cropView = UIView()
    let cropImageView = UIImageView().then{
        $0.image = UIImage(named: "ic_crop")
    }
    lazy var cropButton = UIButton().then{
        $0.rx.tap.bind{
            print("cropButton")
            let vc = GalleryCropVC()
            if let data = self.images[self.selectedIndex] {
                vc.image = data.image
            }
            vc.cropHandler = { (data) in
                if let item = self.images[self.selectedIndex] {
                    item.image = data
                    self.images[self.selectedIndex] = item
                }
                
                self.collectionView.reloadData()
                App.pop()
            }
            App.push(vc)
        }.disposed(by: disposeBag)
    }
    
    let rotateView = UIView()
    let rotateImageView = UIImageView().then{
        $0.image = UIImage(named: "ic_rotate")
    }
    lazy var rotateButton = UIButton().then{
        $0.rx.tap.bind{
            print("rotateButton selectedIndex : ", self.selectedIndex)
            if let item = self.images[self.selectedIndex] {
                let image = GalleryManager.shared.rotate(image: item.image)
                self.images[self.selectedIndex]!.image = image
                
                self.collectionView.reloadData()
            }
        }.disposed(by: disposeBag)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        gridLayout.itemSize = CGSize(width: self.collectionView.frame.width, height: collectionView.frame.height)
        gridLayout.scrollDirection = .horizontal
        
        let interval:CGFloat = 10
        gridLayout.minimumInteritemSpacing = interval
        gridLayout.minimumLineSpacing = interval
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black
        setupUI()
    }
    
    func setupUI() {
        topView.addSubViews([
            confirmButton
        ])
        
        //자르기 버튼
        cropView.addSubViews([
            cropImageView,
            cropButton
        ])
        
        //회전 버튼
        rotateView.addSubViews([
            rotateImageView,
            rotateButton
        ])
        
        bottomView.addSubViews([
            cropView,
            rotateView
        ])
        
        addSubViews([
            topView,
            collectionView,
            bottomView
        ])
        
        setupConstraints()
    }
    
    func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        confirmButton.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        collectionView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(bottomView.snp.top)
        }
        
        cropView.snp.makeConstraints{
            $0.width.height.equalTo(30)
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview().offset(-40)
        }
        
        cropImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        cropButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        rotateView.snp.makeConstraints{
            $0.width.height.equalTo(30)
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview().offset(40)
        }
        
        rotateImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        rotateButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        bottomView.snp.makeConstraints{
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(self.view.safeAreaLayoutGuide)
            $0.height.equalTo(45)
        }
    }
}

extension GallerySelectedVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:GallerySelectedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GallerySelectedCell", for: indexPath) as! GallerySelectedCell

        if let item = images[indexPath.row] {
            cell.configuration(image: item.image)
        }
        
        return cell
    }
    
}

extension GallerySelectedVC : UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // item의 사이즈와 item 간의 간격 사이즈를 구해서 하나의 item 크기로 설정.
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        var roundedIndex = round(index)
    
        if scrollView.contentOffset.x > targetContentOffset.pointee.x {
            roundedIndex = floor(index)
        } else if scrollView.contentOffset.x < targetContentOffset.pointee.x {
            roundedIndex = ceil(index)
        } else {
            roundedIndex = round(index)
        }
        
        if currentIndex > roundedIndex {
            currentIndex -= 1
            roundedIndex = currentIndex
        } else if currentIndex < roundedIndex {
            currentIndex += 1
            roundedIndex = currentIndex
        }
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
        
        let pageIndex:Int = Int(roundedIndex)
        self.selectedIndex = pageIndex
        print("pageIndex : ", pageIndex)
    }
}


