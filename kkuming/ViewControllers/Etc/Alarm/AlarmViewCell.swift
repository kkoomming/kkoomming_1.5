//
//  AlertViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/12.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class AlarmViewCell: UITableViewCell {
    static let identifier = String(describing: AlarmViewCell.self)
    var disposeBag = DisposeBag()
    
    let profileImageView = UIImageView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 10
        $0.image = UIImage(named: "coinExample")!
    }
    
    let titleLabel = UILabel().then{
        $0.font = getFont(size: 12, type: .bold)
        $0.textColor = .black
        $0.text = "해람님이 팔로우 했어요 :)"
        $0.numberOfLines = 2
    }
    
    let timeLabel = UILabel().then{
        $0.font = getFont(size: 10)
        $0.textColor = RGB(red: 154, green: 154, blue: 154)
        $0.text = "2023.09.10"
    }
    
    let deleteButtonView = UIImageView(image: UIImage(named: "ic_alert_delete")!)
    lazy var deleteButton = UIButton()
    
    let bottomBorder = BorderView()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.disposeBag = DisposeBag()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        setupUI()
    }
    
    public func isHiddenProfileImageView(_ bool:Bool) {
        profileImageView.isHidden = bool
        if bool {
            profileImageView.snp.updateConstraints{
                $0.top.equalToSuperview().offset(15)
                $0.width.equalTo(0)
            }
            timeLabel.snp.makeConstraints{
                $0.bottom.equalTo(bottomBorder.snp.bottom).offset(-20)
            }
        } else {
            profileImageView.snp.updateConstraints{
                $0.top.equalToSuperview().offset(20)
                $0.width.equalTo(42)
            }
            profileImageView.snp.makeConstraints{
                $0.bottom.equalTo(bottomBorder.snp.bottom).offset(-20)
            }
        }
    }
    
    private func setupUI() {
        contentView.addSubViews([
            profileImageView,
            titleLabel,
            timeLabel,
            deleteButtonView,
            deleteButton,
            bottomBorder
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        profileImageView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(20)
            $0.leading.equalToSuperview().offset(25)
            $0.width.height.equalTo(42)
        }
        
        titleLabel.snp.makeConstraints{
            $0.top.equalTo(profileImageView.snp.top).offset(5)
            $0.leading.equalTo(profileImageView.snp.trailing).offset(10)
            $0.trailing.equalTo(deleteButtonView.snp.leading).offset(-10)
        }
        
        timeLabel.snp.makeConstraints{
            $0.top.equalTo(titleLabel.snp.bottom).offset(8)
            $0.leading.equalTo(titleLabel.snp.leading)
        }
        
        deleteButtonView.snp.makeConstraints{
            $0.trailing.equalToSuperview().offset(-20)
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(24)
        }
        
        deleteButton.snp.makeConstraints{
            $0.centerY.centerX.equalTo(deleteButtonView)
            $0.width.height.equalTo(50)
        }
        
        bottomBorder.snp.makeConstraints{
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(1)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
