//
//  AlarmVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/04.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

//알림 분기 처리
enum AlarmType: String{
    case REPORT_COMMENT_POST = "REPORT_COMMENT_POST"
    case NEW_COMMENT_POST = "NEW_COMMENT_POST"
    case NEW_FOLLOWER = "NEW_FOLLOWER"
    case NEW_LIKE_POST = "NEW_LIKE_POST"
    case NEW_NOTICE_FROM_SYSTEM_TYPE_EVENT = "NEW_NOTICE_FROM_SYSTEM_TYPE_EVENT"
    case NEW_REPLY_COMMENT = "NEW_REPLY_COMMENT"
    case NEW_EVENT = "NEW_EVENT"
    case NEW_CHALLENGE = "NEW_CHALLENGE"
    case NEW_WEB_VIEW = "NEW_WEB_VIEW"
    case NEW_URL = "NEW_URL"
}

enum AlarmStatus: String {
    case SEEN = "SEEN"
    case UNSEEN = "UNSEEN"
}

class AlarmVC: BaseVC {
    var items:[AlarmData] = []
    
    //페이징
    var page = 1
    var isPaging = false
    
    lazy var topView = TopView(type: .alert).then{
        $0.isBorder = true
        $0.addRightButtonEvent(){ [weak self] in
            AlertManager.shared.show(text: "전체 삭제 하시겠습니까?", type: .two) { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.deleteAll()
            }
        }
    }
    
    let emptyView = ListEmptyView().then{
        $0.isHidden = true
    }
    
    lazy var tableView = UITableView().then{
        $0.separatorStyle = .none
        $0.register(AlarmViewCell.self, forCellReuseIdentifier: AlarmViewCell.identifier)
        $0.delegate = self
        $0.dataSource = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getList()
        setupUI()
    }
    
    public func configuration(items:[AlarmData]) {
        self.items = items
        
        tableView.reloadData()
    }
    
    public func add(items:[AlarmData]) {
        self.items += items
        
        tableView.reloadData()
    }
    
    private func setupUI() {
        addSubViews([
            topView,
            tableView,
            emptyView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        tableView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
        
        emptyView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }
}

//MARK: - 기능
extension AlarmVC {
    private func isEmpty(_ bool:Bool) {
        emptyView.isHidden = !bool
        tableView.isHidden = bool
    }
    
    private func updateStatus(index:Int, status: String) {
        var item = items[index]
        item.status = status

        self.items[index] = item
        
        updateAlertManagerStatus()
    }
    
    private func updateAlertManagerStatus() {
        var status = false
        
        for item in items {
            if item.status == AlarmStatus.UNSEEN.rawValue {
                status = true
            }
        }
        
        AlertManager.shared.status = status
    }
}

//MARK: - Delegate
extension AlarmVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.count == 0 {
            isEmpty(true)
        } else {
            isEmpty(false)
        }
        
        updateAlertManagerStatus()
        
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AlarmViewCell.identifier, for: indexPath) as! AlarmViewCell
        let index = indexPath.row
        let item = self.items[index]
        
        //status 분기 처리
        if item.status == AlarmStatus.SEEN.rawValue {
            cell.backgroundColor = .white
        } else {
            cell.backgroundColor = RGB(red: 240, green: 254, blue: 255)
        }
        
        //프로필 이미지
        cell.profileImageView.kf.setImage(with: item.imageUrl.toURL, options: [.loadDiskFileSynchronously])
        if item.imageUrl != "" {
            cell.isHiddenProfileImageView(false)
        }else {
            cell.isHiddenProfileImageView(true)
        }
        
        //알림 제목
        cell.titleLabel.text = item.title
        
        //알림 시간
        cell.timeLabel.text = dateToFormat(string: item.createdAt, dateFormat: .post)
        
        //삭제 이벤트 등록
        cell.deleteButton.rx.tap.bind{ [weak self] in
            AlertManager.shared.show(text: "삭제 하시겠습니까?", type: .two) { [weak self] in
                self?.delete(index: index)
            }
        }.disposed(by: cell.disposeBag)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        
        self.read(index: index)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.items.count - 1 {
            guard !isPaging else { return }
            self.isPaging = true
            
            self.getList()
        }
    }
}

//MARK: - API
extension AlarmVC {
    func getList(isFirst:Bool = false) {
        if isFirst {
            page = 1
        }
        
        API.getAlarmList(page: page){[weak self] model in
            guard let strongSelf = self,
                  let model = model,
                  let list = model.data?.notifications,
                  let meta = model.data?.metaData else {
                print("getAlarmList 데이터 확인 필요")
                return
            }
            
            if strongSelf.page == 1 {
                strongSelf.configuration(items: list)
            } else {
                strongSelf.add(items: list)
            }
            
            if strongSelf.page >= meta.totalPages {
                strongSelf.isPaging = true
            } else {
                strongSelf.isPaging = false
            }
            
            strongSelf.page += 1
        }
    }
    
    func read(index:Int){
        let item = items[index]
        
        API.getAlarmDetail(id: item.id ?? 0) { [weak self] model in
            guard let strongSelf = self, let model = model, let data = model.data else {
                return
            }
            
            UIView.animate(withDuration: 0, animations: {
                strongSelf.updateStatus(index: index, status: "SEEN")
                strongSelf.tableView.reloadData()
                strongSelf.tableView.layoutIfNeeded()
            }, completion: { _ in
                strongSelf.readHandler(item: item)
            })
        }
    }
    
    func delete(index:Int) {
        let id = items[index].id
        
        API.deleteAlarm(id: id ?? 0) { [weak self] model in
            guard let strongSelf = self, let model = model else {
                return
            }
            
            if model.success {
                strongSelf.items.remove(at: index)
                strongSelf.tableView.reloadData()
            }
        }
    }
    
    func deleteAll() {
        print("전체 삭제")
        
        API.deleteAllAlarm() { [weak self] model in
            guard let strongSelf = self, let model = model else {
                return
            }
            
            if model.success {
                strongSelf.getList(isFirst: true)
            }
        }
    }
    
    func readHandler(item:AlarmData) {
        guard let type = AlarmType(rawValue: item.type) else {
            return
        }
        
        switch type {
        case .REPORT_COMMENT_POST:
            print("REPORT_COMMENT_POST")
            App.push(ReportGuideVC())
            break
        case .NEW_COMMENT_POST, .NEW_LIKE_POST, .NEW_REPLY_COMMENT:
            //NEW_COMMENT_POST :댓글이 달린 내글 상세페이지로 이동
            //NEW_LIKE_POST : 좋아요가 달린 내글 상세페이지로 이동
            //NEW_REPLY_COMMENT : 답글이 달린 (내)글 상세페이지로 이동
            print(".NEW_COMMENT_POST, .NEW_LIKE_POST, .NEW_REPLY_COMMENT")
            if let postId = item.data?.postId {
                let vc = ContentDetailVC()
                let item = Post(id: postId)
                vc.configuration(postData: item, postType: .NONE)
                vc.getDetailPost() {
                    App.push(vc)
                    DeepLinkManager.shared.openHandler = nil
                }
            }
            break
        case .NEW_FOLLOWER:
            //팔로워 프로필로 이동
            print("NEW_FOLLOWER")
            if let follwerId = item.data?.followerId {
                App.push(ProfileVC(userId: follwerId))
            }
            break
        case .NEW_NOTICE_FROM_SYSTEM_TYPE_EVENT:
            //공지사항 리스트
            print("NEW_NOTICE_FROM_SYSTEM_TYPE_EVENT")
            App.push(SettingNoticeVC())
            break
        case .NEW_EVENT, .NEW_WEB_VIEW:
            //NEW_EVENT : 이벤트 상세로 이동
            //NEW_WEB_VIEW : 내부 webView
            print("NEW_EVENT, NEW_WEB_VIEW")
            if let linkUrl = item.data?.linkUrl {
                let vc = WebVC(title: item.data?.title ?? "", url: linkUrl)
                AppManager.shared.push(vc)
            }
            break
        case .NEW_CHALLENGE:
            //챌린지 상세로 이동
            print("NEW_CHALLENGE")
            if let id = item.data?.eventsId {
                let vc = TagDetailVC(topTitleString: "",
                                     id: id,
                                     type:.CHALLENGE,
                                     isUseBanner: true)
                AppManager.shared.push(vc)
            }
            break
        case .NEW_URL:
            //외부 브라우저 (크롬, 사파리 등)
            print("NEW_URL")
            if let linkUrl = item.data?.linkUrl {
                openSafari(url: linkUrl)
            }
            
            break
        }
        
        updateAlertManagerStatus()
    }
}
