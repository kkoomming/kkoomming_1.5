//
//  ProfileBlockVC.swift
//  kkuming
//
//  Created by pjy on 2022/06/13.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class ProfileBlockVC: BaseVC {
    
    let disposeBag = DisposeBag()
    var items:[Any] = []
    
    let topView = TopView(type: .profile)
    
    lazy var scrollView = UIScrollView().then{
        $0.contentInsetAdjustmentBehavior = .never
        $0.delegate = self
    }
    
    let stackView = UIStackView().then{
        $0.alignment = .fill
        $0.axis = .vertical
        $0.distribution = .fill
        $0.spacing = 30
    }
    
    let topProfileView = UIView()
    
    let profileBackgroundView = UIView().then{
        $0.backgroundColor = RGB(red: 215, green: 243, blue: 246)
    }
    
    let profileView = ProfileView(isMyProfile: false).then{
        $0.profileNameLabel.text = "서비스 이용 정지"
        $0.removeProfileViewForBlock()
        $0.profileImageView.image = UIImage(named: "emptyKM")
    }
    
    lazy var blockCollectionView = ContentSelectView(type: .three).then{
        $0.collectionView.dataSource = self
        $0.collectionView.delegate = self
        $0.collectionView.isScrollEnabled = false
        $0.collectionView.contentInset = UIEdgeInsets(
            top: 10,
            left: $0.leftMargin,
            bottom: 10,
            right: $0.rightMargin
        )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        configuration(items: [
//            TestData(idx: 1),
//            TestData(idx: 2),
//            TestData(idx: 3),
//            TestData(idx: 4),
//            TestData(idx: 5),
//            TestData(idx: 6),
//            TestData(idx: 7),
//            TestData(idx: 8)
//        ])
        
        setupUI()
        updateCollectionView()
    }
    
    public func configuration(items:[Post]) {
        self.items = items
        
        //TODO: MORE 처리 필요(마이꾸밍의 "스크랩" More 하는데 여기서도 쓰네..?)
//        blockCollectionView.configuration(items: items, metaData: <#MetaData#>)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        profileView.layoutIfNeeded()
        
        let profileImageViewHeight = profileView.profileImageView.frame.size.height
        let profileContentViewHeight = profileView.profileContentView.frame.size.height
        
        profileView.snp.updateConstraints{
            $0.height.equalTo(profileImageViewHeight + profileContentViewHeight)
        }
        
        topProfileView.layoutIfNeeded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AlertManager.shared.updateStatus(view: topView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        profileBackgroundView.snp.makeConstraints{
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(255 + self.view.safeAreaInsets.top)
        }
    }
    
    private func setupUI() {
        scrollView.addSubViews([
            stackView,
        ])
        
        topProfileView.addSubViews([
            profileBackgroundView,
            topView,
            profileView
        ])
        
        stackView.addArrangedSubview(topProfileView)
        stackView.addArrangedSubview(blockCollectionView)
        
        self.view.addSubview(scrollView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        scrollView.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
        
        profileBackgroundView.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(profileView.getBackgroundHeight() + AppManager.shared.getSafeAreaInsets().top)
        }
        
        topView.snp.makeConstraints{
            $0.top.equalTo(self.stackView.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        profileView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(297)
        }
        
        profileView.layoutIfNeeded()
        let profileViewHeight = profileView.getCotentHeight() + AppManager.shared.getSafeAreaInsets().top
        
        topProfileView.snp.makeConstraints{
            $0.height.equalTo(profileViewHeight)
        }
        
        blockCollectionView.snp.makeConstraints{
            $0.top.equalTo(profileView.snp.bottom).offset(31)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalTo(scrollView)
            $0.height.equalTo(0)
        }
        
    }
    
    private func updateCollectionView() {
        blockCollectionView.collectionView.performBatchUpdates(nil, completion: {result in
            self.blockCollectionView.snp.updateConstraints {
                $0.height.equalTo(self.blockCollectionView.collectionView.contentSize.height)
            }
        })
    }
    
}

extension ProfileBlockVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.frame.origin = CGPoint(x: 0, y: 0)
        }
    }
}

extension ProfileBlockVC: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VariousListViewCell.identifier, for: indexPath) as! VariousListViewCell
        let item = items[indexPath.item] as! Post
        
        cell.configuration(item: item)
        cell.isVisibleCheckBox(false)
        if let urlString = item.thumbnailImage, let url = URL(string: urlString) {
            cell.imageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
        }
        
        return cell
    }
    
}
