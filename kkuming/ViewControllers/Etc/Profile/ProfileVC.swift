//
//  ProfileVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/02.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Promises

class ProfileVC: BaseVC {
    let disposeBag = DisposeBag()
    var userId:Int = 0
    var items:[Post] = []
    
    //페이징
    var page = 1
    var isPaging = false
    var isMain = true
    
    lazy var topView = TopView(type: .profile).then{
        $0.delegate = self
    }
    
    lazy var scrollView = UIScrollView().then{
        $0.contentInsetAdjustmentBehavior = .never
        $0.delegate = self
    }
    let stackView = UIStackView().then{
        $0.alignment = .fill
        $0.axis = .vertical
        $0.distribution = .fill
        $0.spacing = 30
    }
    
    let topProfileView = UIView()
    
    let profileBackgroundView = UIView().then{
        $0.backgroundColor = RGB(red: 215, green: 243, blue: 246)
    }
    
    let profileView = ProfileView(isMyProfile: false)
    
    lazy var threeListView = VariousListView(type: .three).then{
        $0.collectionView.isScrollEnabled = false
        $0.collectionView.isPagingEnabled = false
        $0.collectionView.contentInset = UIEdgeInsets(top: 10, left: $0.leftMargin, bottom: 10, right: $0.rightMargin)
        $0.updateHeightHandler = { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.updateViewHeight()
        }
    }
    
    init(userId:Int) {
        super.init(nibName: nil, bundle: nil)
        self.userId = userId
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        initialize()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateViewHeight()
    }
    
    private func initialize() {
        self.getDetailUser(userId: self.userId)
        self.getSomeonePost(userId: self.userId)
    }
    
    //프로필 설정
    public func configuration(user:User) {
        profileView.configuration(isMyProfile: false, item: user, refreshHandler: { [weak self] in
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.getDetailUser(userId: strongSelf.userId)
        })
    }
    
    //프로필 > 하단 포스트
    public func configuration(items:[Post]) {
        self.items = items
        threeListView.configuration(items: items)
        
        self.updateViewHeight()
    }
    
    private func add(items:[Post]) {
        threeListView.add(items: items)
        
        self.updateViewHeight()
    }
    
    private func setupUI() {
        scrollView.addSubViews([
            stackView,
        ])
        
        topProfileView.addSubViews([
            profileBackgroundView,
            topView,
            profileView,
        ])
        
        stackView.addArrangedSubview(topProfileView)
        stackView.addArrangedSubview(threeListView)
        
        self.view.addSubview(scrollView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        scrollView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
        
        profileBackgroundView.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(profileView.getBackgroundHeight() + AppManager.shared.getSafeAreaInsets().top)
        }
        
        topView.snp.makeConstraints{
            $0.top.equalTo(self.stackView.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        profileView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(297)
        }
        
        profileView.layoutIfNeeded()
        profileView.snp.updateConstraints{
            $0.height.equalTo(profileView.getProfileViewHeight())
        }
        
        let profileCotentHeight = profileView.getCotentHeight()
        topProfileView.snp.makeConstraints{
            $0.height.equalTo(profileCotentHeight)
        }
        
        threeListView.snp.makeConstraints{
            $0.height.equalTo(1)
        }
        
    }
    
    private func updateViewHeight() {
        profileView.layoutIfNeeded()
        profileView.profileImageView.layoutIfNeeded()
        profileView.profileContentView.layoutIfNeeded()
        
        let profileViewHeight = profileView.getCotentHeight()
        
        topProfileView.snp.updateConstraints{
            $0.height.equalTo(profileViewHeight)
        }
        
        let safeTop = AppManager.shared.getSafeAreaInsets().top
        let profileImageTop = profileView.profileImageView.frame.origin.y
        let profileDividedHeight = (profileView.profileImageView.frame.size.height / 2)
        let contentHeight = profileView.profileContentView.frame.height
        let backgroundHeight = (contentHeight / 2) + 65 + safeTop + profileImageTop + profileDividedHeight
        
        //파란색 뒷 배경 영역
        profileBackgroundView.snp.updateConstraints{
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(backgroundHeight)
        }
        
        topProfileView.layoutIfNeeded()
        
        threeListView.layoutIfNeeded()
        threeListView.collectionView.layoutIfNeeded()
        threeListView.snp.updateConstraints{
            $0.height.equalTo(threeListView.collectionView.contentSize.height)
        }
    }
}

extension ProfileVC: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 0 {
            scrollView.frame.origin = CGPoint(x: 0, y: 0)
        }
        
        if scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.height) {
            guard !isPaging else { return }
            self.isPaging = true
            
            print("페이징 처리")
            
            self.getSomeonePost(userId: self.userId)
        }
    }
}

//MARK: - API
extension ProfileVC {
    
    //프로필 조회
    func getDetailUser(userId:Int) {
        API.getDetailUser(userId: userId) { [weak self] model in
            guard let strongSelf = self, let model = model, let user = model.data else {
                return
            }
            
            if model.success {
                strongSelf.configuration(user: user)
            } else {
                AlertManager.shared.show(text: "사용자가 탈퇴하거나 활동중지된 회원입니다.", type: .one)
            }
        }
    }
    
    //프로필 > 하단 포스트
    func getSomeonePost(userId:Int) {
        API.getSomeonePost(userId: userId, offset: page) {[weak self] model in
            guard let strongSelf = self, let model = model, let posts = model.data?.posts, let meta = model.data?.metaData else {
                return
            }
            
            if model.success {
                if strongSelf.isMain {
                    strongSelf.configuration(items: posts)
                    strongSelf.isMain = false
                } else {
                    strongSelf.add(items: posts)
                }
                
                if strongSelf.page >= meta.totalPages {
                    strongSelf.isPaging = true
                } else {
                    strongSelf.isPaging = false
                }
                
                strongSelf.page += 1
            } else {
                AlertManager.shared.show(text: "사용자가 탈퇴하거나 활동중지된 회원입니다.", type: .one)
            }
        }
    }
}

extension ProfileVC: TopViewDelegate {
    func selectBlock() {
        print("selectBlock")
        AlertManager.shared.show(text: "차단하시겠습니까?", type: .two) {
            API.blockUser(userId: self.userId) { model, data in
                guard let model = model, let isBlocked = model.data?.isBlocked else {
                    AlertManager.shared.show(text: "프로필 차단 에러\n잠시 후 다시 시도해 주세요.", type: .one)
                    return
                }
                
                if model.success {
                    if isBlocked {
                        AppManager.shared.pop()
                    }
                } else {
                    AlertManager.shared.show(text: "프로필 차단 에러", type: .one)
                }
                    
            }
        }
    }
    
//    func selectReport() {
//        print("selectReport")
//        let view = ReportView(isPost: false)
//        view.completion = { [weak self] in
//            guard let strongSelf = self else { return }
//            view.reportPost()
//        }
//        AlertManager.shared.showWindows(view: view)
//    }
}
