//
//  WriteVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/21.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Alamofire

class WriteVC: BaseVC {
    let disposeBag = DisposeBag()
    
    let tagPlaceholderString = "태그 입력 (최대 10개)"
    let contentPlaceholderString = "내용을 입력해주세요 :)"
    
    //글 수정 조회 여부
    var isEdit = false
    var id:Int = 0
    
    var items:[GalleryData?] = []
    
    //추천 태그 선택
    var selectedRecomTagList:[String] = []
    
    //추천 태그
    var recomTagList:[String] = []
    
    //태그
    var tagInputs:[String] = []
    
    //(글 수정 전용) 업로드된 파일 id 리스트
    var fileIds:[Int] = []
    
    //수정 완료 됐을 때 로직
    var editHandler:(()->Void)? = nil
    
    lazy var topView = TopView(type: .write, completion: { [weak self] in
        guard let strongSelf = self else { return }
        
        if strongSelf.isEdit {
            strongSelf.updatePost()
        } else {
            strongSelf.createNewPost()
        }
    })
    
    let scrollView = UIScrollView()
    
    let stackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.layoutMargins = UIEdgeInsets(top: 10, left: 25, bottom: 0, right: 25)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    let layout = UICollectionViewFlowLayout().then{
        $0.scrollDirection = .horizontal
        $0.minimumInteritemSpacing = 10
        $0.itemSize = CGSize(width: 101, height: 101)
    }
    
    lazy var galleryCollectionView = UICollectionView(frame: CGRect.zero,
                                                      collectionViewLayout: layout).then{
        $0.contentInset = UIEdgeInsets(top: 10, left: 25, bottom: 30, right: 25)
        $0.showsHorizontalScrollIndicator = false
        $0.register(WriteViewCell.self, forCellWithReuseIdentifier: WriteViewCell.identifier)
        $0.dataSource = self
        $0.delegate = self
    }
    
    lazy var uploadView:UIView = {
        let view = UIView()
        view.backgroundColor = .mainColor
        view.clipsToBounds = true
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 10
        
        let uploadCameraImageView = UIImageView(image: UIImage(named: "camera")!)
        let uploadLabel = UILabel().then{
            $0.font = getFont(size: 13, type: .EBold)
            $0.textColor = .white
            $0.text = "사진 올리기"
        }
        lazy var uploadButton = UIButton().then{
            //MARK: 메모리 관리
            $0.rx.tap.bind{ [weak self] in
                guard let strongSelf = self else {
                    return
                }
                var selectAlbumView = AlertButtonView(title: "앨범에서 선택", buttonType: .mainColor){
                    print("앨범에서 선택")
                    GalleryManager.shared.requestAlbum()
                }
                
                var selectCameraView = AlertButtonView(title: "카메라 촬영", buttonType: .mainColor){ [weak self] in
                    guard let strongSelf = self else {
                        return
                    }
                    print("카메라 촬영")
                    if GalleryManager.shared.isRequestCamera() {
                        GalleryManager.shared.startCamera(strongSelf)
                    }
                }
                
                AlertManager.shared.showAlertView(buttonList: [
                    selectAlbumView,
                    selectCameraView
                ])
            }.disposed(by: self.disposeBag)
        }
        
        [
            uploadCameraImageView,
            uploadLabel,
            uploadButton
        ].forEach{
            view.addSubview($0)
        }
        
        uploadLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.centerX.equalToSuperview().offset(15)
        }
        
        uploadCameraImageView.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.width.height.equalTo(20)
            $0.trailing.equalTo(uploadLabel.snp.leading).offset(-10)
        }
        
        uploadButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        return view
    }()
    
    let recommendTagSelectLabel = UILabel().then{
        $0.textColor = .black
        $0.font = getFont(size: 16, type: .bold)
        let textString = "추천 태그 선택"
        $0.text = textString
    }
    
    lazy var recomTagView = TagView()
    
    let tagLabel = UILabel().then{
        $0.textColor = .black
        $0.font = getFont(size: 16, type: .bold)
        let textString = "태그"
        $0.text = textString
    }
    
    lazy var tagTextView = UITextView().then{
        $0.returnKeyType = .done
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 10
        $0.layer.borderColor = UIColor.borderColor.cgColor
        $0.layer.borderWidth = 1
        $0.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        $0.delegate = self
        
        $0.text = tagPlaceholderString
        $0.textColor = UIColor.placeholderColor
    }
    
    lazy var tagView = TagView().then{
        $0.isClose = true
        $0.delegate = self
    }
    
    let contentLabel = UILabel().then{
        $0.textColor = .black
        $0.font = getFont(size: 16, type: .bold)
        let textString = "내용"
        $0.text = textString
    }
    
    lazy var contentTextView = UITextView().then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 10
        $0.layer.borderColor = UIColor.borderColor.cgColor
        $0.layer.borderWidth = 1
        $0.textContainerInset = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        $0.delegate = self
        
        $0.text = contentPlaceholderString
        $0.textColor = UIColor.placeholderColor
    }
    
    deinit {
        print("WriteVC deinit")
        registerNotification(false)
        GalleryManager.shared.clear()
    }
    
    init(id:Int = 0) {
        super.init(nibName: nil, bundle: nil)
        self.id = id
        //아이디가 있다면 글 수정 모드로 판단
        self.isEdit = (id != 0) ? true : false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        getListTag()
        self.registerNotification(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        KkumingKeyboardManager.shared.isUseKeyboardLibrary(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        KkumingKeyboardManager.shared.isUseKeyboardLibrary(false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        recomTagView.updateTagViewLayout()
        tagView.updateTagViewLayout()
    }

    private func setupUI() {
        [
            uploadView,
            createSpacingView(height: 32),
            recommendTagSelectLabel,
            createSpacingView(height: 23),
            recomTagView,
            createSpacingView(height: 32),
            tagLabel,
            createSpacingView(height: 6),
            tagTextView,
            createSpacingView(height: 10),
            tagView,
            createSpacingView(height: 32),
            contentLabel,
            createSpacingView(height: 6),
            contentTextView
        ].forEach {
            stackView.addArrangedSubview($0)
        }
        
        scrollView.addSubview(stackView)
        
        addSubViews([
            topView,
            galleryCollectionView,
            scrollView,
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        topView.snp.makeConstraints{
            $0.top.equalTo(self.view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(65)
        }
        
        galleryCollectionView.snp.makeConstraints{
            $0.top.equalTo(topView.snp.bottom)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(0)
        }
        
        scrollView.snp.makeConstraints{
            $0.top.equalTo(galleryCollectionView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
            $0.width.equalTo(scrollView.snp.width)
        }
        
        setupStackViewConstraints()
    }
    
    private func setupStackViewConstraints(){
        uploadView.snp.makeConstraints{
            $0.height.equalTo(43)
        }

        recomTagView.snp.makeConstraints{
            $0.height.equalTo(146)
        }
        
        tagTextView.snp.makeConstraints{
            $0.height.equalTo(50)
        }
        
        tagView.snp.makeConstraints{
            $0.height.equalTo(10)
        }
        
        contentTextView.snp.makeConstraints{
            $0.height.equalTo(115)
        }
    }
    
    private func registerNotification(_ bool:Bool){
        if bool {
            NotificationCenter.default.addObserver(self, selector: #selector(self.moveToAlbum), name: .requestAlbum, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.updateSelectedData), name: .updateSelectedData, object: nil)
        } else {
            NotificationCenter.default.removeObserver(self, name: .requestAlbum, object: nil)
            NotificationCenter.default.removeObserver(self, name: .updateSelectedData, object: nil)
        }
    }

    @objc
    private func updateSelectedData() {
        items = GalleryManager.shared.getSelectedImages()
        
        if items.count == 0 {
            galleryCollectionView.snp.updateConstraints{
                $0.height.equalTo(0)
            }
        } else {
            galleryCollectionView.snp.updateConstraints{
                $0.height.equalTo(141)
            }
        }
        
        galleryCollectionView.reloadData()
    }
    
    //앨범 선택으로 이동
    @objc
    private func moveToAlbum() {
        DispatchQueue.main.async {
            App.push(GalleryPickerVC())
        }
    }
}

extension WriteVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WriteViewCell.identifier, for: indexPath) as! WriteViewCell
        let index = indexPath.item
        let item = self.items[index]
        
        cell.preImageView.image = item?.image
        //MARK: 메모리 관리
        cell.closeButton.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else {
                return
            }
            print("closeButton")
            strongSelf.items.removeAll(where: { $0?.identifier == item?.identifier })
            GalleryManager.shared.setSelectedImages(strongSelf.items)
            strongSelf.updateSelectedData()
        }.disposed(by: cell.disposeBag)
        
        return cell
    }
    
}

extension WriteVC:UITextViewDelegate, TagDelegate {
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == tagTextView && textView.text == tagPlaceholderString {
            textView.text = ""
        } else if textView == contentTextView && textView.text == contentPlaceholderString {
            textView.text = ""
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == tagTextView && textView.text == "" {
            textView.text = tagPlaceholderString
        } else if textView == contentTextView && textView.text == "" {
            textView.text = contentPlaceholderString
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if tagTextView == textView {
            //# 입력 안되게 처리
            if text == "#" {
                return false
            }
            
            if text == " " {
                guard let string = textView.text else { return true }
                //맨 처음 글자 스페이스 허락하지 않음
                if string.count == 0 {
                    return false
                }
                
                if isValidation(string: string) {
                    return false
                }
                
                filterAndAdd(target:string, text: " ")
            }
            
            if text == "\n" {
                guard let string = textView.text else { return true }
                //맨 처음 글자 스페이스 허락하지 않음
                if string.count == 0 {
                    return false
                }
                
                if isValidation(string: string) {
                    return false
                }
                
                filterAndAdd(target:string, text: "\n")
            }
        }
        
        return true
    }
    
    private func isValidation(string:String) -> Bool {
        if tagInputs.contains("#\(string)") {
            view.endEditing(true)
            AlertManager.shared.show(text: "이미 동일한 태그를 입력하였어요.", type: .one)
            return true
        } else if tagInputs.count == 10 {
            AlertManager.shared.show(text: "태그는 최대 10개까지만 입력 가능합니다.", type: .one)
            return true
        }
        
        return false
    }
    
    private func filterAndAdd(target string:String ,text:String) {
        let arr = string.components(separatedBy: text)
        
        for item in arr {
            if !item.contains("#") {
                let tagText = "#\(item)"
                tagInputs.append(tagText)
            } else {
                tagInputs.append(item)
            }
        }
        
        configurationTagList(items: tagInputs)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == tagTextView {
            if let text = textView.text {
                if text.contains(" ") || text.contains("\n") {
                    textView.text = ""
                }
            }
        }
    }
    
    func didSelectItemAt(items: [String]) {
        self.tagInputs = items
        configurationTagList(items: items)
    }
    
    //추천 태그
    func configurationRecomTagList(items:[String]) {
        self.recomTagView.selectedItems = selectedRecomTagList
        self.recomTagView.configuration(items: items)
        self.recomTagView.updateTagViewLayout()
    }
    
    //일반 태그
    func configurationTagList(items: [String]) {
        tagView.configuration(items: items)
        tagView.updateTagViewLayout()
    }
}

//MARK: - API
extension WriteVC {
    //추천 태그 불러오기
    func getListTag() {
        API.getRecomTagList() {[weak self] model in
            guard let strongSelf = self,
                  let tags = model?.data?.manageTag else { return }
            var list:[String] = []
            for tag in tags{
                list.append(tag.tagName)
            }
            
            if strongSelf.isEdit {
                strongSelf.recomTagList = list
                strongSelf.getDetailMyPost()
            } else {
                strongSelf.configurationRecomTagList(items: list)
            }
            
            //MARK: 메모리 관리
            strongSelf.recomTagView.selectHandler = {[weak self] selectedList in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.selectedRecomTagList = selectedList
                print("selectedList count : ", selectedList.count)
            }
        }
    }
    
    //글 작성
    func createNewPost() {
        guard isValidation() else {
            return
        }
        
        var param:[String:Any] = [:]
        
        if let description = self.contentTextView.text {
            param.updateValue(description, forKey: "description")
        }
        
        //tagInputs
        if !self.tagInputs.isEmpty {
            param.updateValue(self.tagInputs, forKey: "tagInputs")
        }
        
        //tagPopularIds
        var tagPopularIds:[String] = []
        for string in self.selectedRecomTagList {
            tagPopularIds.append(string)
        }
        if !tagPopularIds.isEmpty {
            param.updateValue(tagPopularIds, forKey: "tagPopularIds")
        }
        
        //file image
        var dataList:[Data] = []
        for item in self.items {
            if let data = item?.image?.jpegData(compressionQuality: 0.5) {
                print("글쓰기 데이터 추가")
                dataList.append(data)
            }
        }
        
        API.createNewPost(param: param, files: dataList){ model in
            App.pop() {
                guard let model = model, let data = model.data else { return }
                let vc = ContentDetailVC()
                vc.configuration(postData: data, postType: .NONE)
                vc.getDetailPost() {
                    App.push(vc)
                    RefreshManager.shared.refreshALL()
                }
            }
        }
    }
    
    /*
     [사진 없을 때]
     최소 1개의 이미지를 업로드 해야 합니다.

     [사진 최대 10장]
     사진은 최대 10장까지만 등록가능합니다.

     [내용 없을 때]
     내용을 입력해주세요
     */
    private func isValidation() -> Bool{
        let check = false
        
        if !isEdit {
            //글쓰기
            if items.count == 0 {
                AlertManager.shared.show(text: "최소 1개의 이미지를 업로드 해야 합니다.", type: .one)
                return check
            }
        } else {
            //수정
            //이미지 예외 처리
            if fileIds.isEmpty {
                AlertManager.shared.show(text: "최소 1개의 이미지를 업로드 해야 합니다.", type: .one)
                return check
            }
        }
        
        
//        if selectedRecomTagList.count == 0 && tagInputs.count == 0 {
//            AlertManager.shared.show(text: "하나 이상의 태그를 선택해주세요.", type: .one)
//            return check
//        }
        
        if let text = contentTextView.text, text.count == 0 || text == contentPlaceholderString {
            AlertManager.shared.show(text: "내용을 입력해주세요.", type: .one)
            return check
        }
        
        return !check
    }
    
    //글 수정
    func updatePost() {
        guard isValidation() else {
            return
        }
            
        var param:[String:Any] = [:]
        
        param.updateValue(self.id, forKey: "postId")
        
        if let description = self.contentTextView.text {
            param.updateValue(description, forKey: "description")
        }
        
        //tagInputs
        if !self.tagInputs.isEmpty {
            param.updateValue(self.tagInputs, forKey: "tagInputs")
        }
        
        //tagPopularIds
        var tagPopularIds:[String] = []
        for string in self.selectedRecomTagList {
            tagPopularIds.append(string)
        }
        if !tagPopularIds.isEmpty {
            param.updateValue(tagPopularIds, forKey: "tagPopularIds")
        }
        
        //file image
        var dataList:[Data] = []
        self.fileIds.removeAll()
        for item in self.items {
            if item?.asset != nil {
                //사진 올리기
                if let data = item?.image?.jpegData(compressionQuality: 1.0) {
                    print("글쓰기 데이터 추가")
                    dataList.append(data)
                }
            } else {
                //이미 있는 파일 id 담기
                if let identifier = item?.identifier, let id = Int(identifier) {
                    self.fileIds.append(id)
                }
            }
        }
        
        //이미 있는 파일 fileIds 올리기
        param.updateValue(fileIds, forKey: "fileIds")
        
        API.updatePost(param: param, files: dataList){ [weak self] model in
            App.pop() {
                if let lastVC = App.getWindowVCLast() {
                    if lastVC is ContentDetailVC {
                        App.pop() {
                            guard let strongSelf = self else { return }
                            let vc = ContentDetailVC()
                            let postData = Post(id: strongSelf.id)
                            vc.configuration(postData: postData, postType: .NONE)
                            vc.getDetailPost() {
                                App.push(vc)
                            }
                        }
                    }
                } else {
                    guard let strongSelf = self else { return }
                    let vc = ContentDetailVC()
                    let postData = Post(id: strongSelf.id)
                    vc.configuration(postData: postData, postType: .NONE)
                    vc.getDetailPost() {
                        App.push(vc)
                    }
                }
            }
        }
    }
    
    //내 글 조회
    func getDetailMyPost() {
        let param:Parameters = [
            "id" : self.id
        ]
        
        API.getDetailMyPost(param: param) {[weak self] model in
            guard let strongSelf = self, let model = model, let data = model.data else {
                return
            }
            
            strongSelf.configuration(data: data)
            print("getDetailMyPost description : ", data.description)
        }
    }
    
    func configuration(data:MyPost) {
        //GalleryManager에 이미지 담기
        if let album = data.album {
            var albumList:[GalleryData] = []
            
            for item in album {
                if let urlString = item.url, let url = urlString.toURL {
                    do {
                        if UIApplication.shared.canOpenURL(url) {
                            let fileData = try Data(contentsOf: url)
                            let image = UIImage(data: fileData)
                            let data = GalleryData(identifier: String(item.id), image: image, asset: nil)
                            albumList.append(data)
                        } else {
                            print("WriteVC url 연결 실패")
                        }
                    } catch {
                        print("configuration fileData 에러")
                    }
                }
            }
            
            GalleryManager.shared.setSelectedImages(albumList)
        }
        
        //추천 태그 선택
        var recomItems:[String] = []
        if let tagPopulars = data.hashTags?.tagPopulars {
            //API에서 가져온 추천 태그 저장(recomItems)
            for item in tagPopulars {
                recomItems.append(item.tagName)
            }
            
            //중복된거 삭제
            for item in recomItems {
                recomTagList.removeAll(where: { $0 == item })
            }
            
            //추천태그 맨 앞으로 가기 위해 임시 저장
            let tempArray:[String] = recomTagList
            //추천태그 맨 앞 설정
            self.recomTagList = recomItems
            //현재 선택되어 있는 태그 리스트
            self.selectedRecomTagList = recomItems
            //API 가져온거 맨 뒤에 추가
            self.recomTagList += tempArray
            
            configurationRecomTagList(items: self.recomTagList)
        }
        
        //태그
        var tagItems:[String] = []
        if let tagInputs = data.hashTags?.tagInputs {
            self.tagInputs.removeAll()
            
            for item in tagInputs {
                let tagName = item.tagName
                
                self.tagInputs.append(tagName)
                tagItems.append(tagName)
            }
            
            configurationTagList(items: tagItems)
        }
        
        //내용
        self.contentTextView.text = data.description
        
        //이미지 파일 업데이트
        updateSelectedData()
        self.fileIds.removeAll()
        for item in self.items {
            if item?.asset == nil, let identifier = item?.identifier, let id = Int(identifier) {
                //이미 있는 파일 id 담기
                self.fileIds.append(id)
            }
        }
    }
}
