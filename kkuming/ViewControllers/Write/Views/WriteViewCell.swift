//
//  WriteViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/10.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class WriteViewCell: UICollectionViewCell {
    static let identifier = String(describing: WriteViewCell.self)
    
    var disposeBag = DisposeBag()
    
    let preImageView = UIImageView().then{
        $0.contentMode = .scaleAspectFill
    }
    
    let closeButton = UIButton().then{
        $0.imageView?.contentMode = .scaleAspectFit
        $0.setImage(UIImage(named: "close_og"), for: .normal)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = 15
        layer.borderColor = RGB(red: 234, green: 234, blue: 234).cgColor
        layer.borderWidth = 2
        
        [
            preImageView,
            closeButton
        ].forEach{
            self.contentView.addSubview($0)
        }
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        preImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        closeButton.snp.makeConstraints{
            $0.width.height.equalTo(20)
            $0.top.equalToSuperview().offset(10)
            $0.trailing.equalToSuperview().offset(-5)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
