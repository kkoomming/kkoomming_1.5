//
//  SplashVC.swift
//  Ggooming
//
//  Created by 장효원 on 2022/04/12.
//

import UIKit
import SnapKit
import Then

class SplashVC: BaseVC {
    //MARK: - Properties
    let viewModel = SplashViewModel()
    
    let kkumingLetterImageView = UIImageView().then{
        $0.image = UIImage(named: "kkuming_letter")
        $0.contentMode = .scaleToFill
    }
    
    let pencilImageView = UIImageView().then{
        $0.image = UIImage(named: "pencil")
        $0.contentMode = .scaleToFill
    }
    
    let rabbitImageView = UIImageView().then{
        $0.image = UIImage(named: "main_character")
        $0.contentMode = .scaleToFill
    }
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commonInit()
        setupUI()
        userStatusCheck()
    }
}
//MARK: - User
extension SplashVC {
    /**
     함수 정의 : user의 최신 앱 버전, 자동로그인 체크 후 화면 전환
     */
    private func userStatusCheck() {
        // Api 호출
        viewModel.getAppVersions()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            // 최신 앱 버전 확인
            self.viewModel.userCurrentAppVersionCheck { isForceAppUpdate in
                switch isForceAppUpdate {
                case .yes:
                    let appId = "id1574307909"
                    AlertManager.shared.show(text: "최신 버전 업데이트 후 실행해주세요.", type: .one, rightTitle: "업데이트 하기") {
                        openSafari(url: "itms-apps://itunes.apple.com/app/\(appId)")
                    }
                case .no:
                    // user 기존 로그인 정보 확인
                    self.viewModel.userAutoLoginCheck { userInfo in
                        guard let userInfo = userInfo else {
                            Log.error(message: "userStatusCheck() | userInfo Nil MoveTo LoginVC")
                            App.setNavi(LoginVC())
                            return
                        }
                        // user 기존 로그인 정보 Json Data Parsing
                        self.viewModel.userInfoDataParsing(userInfo: userInfo) { model in
                            if let model = model {
                                AppManager.shared.login(clientId: model.clientId,
                                                        loginMethod: LoginType(rawValue: model.loginMethod)!)
                            } else {
                                AlertManager.shared.show(text: "로그인 중에 오류가 발생했습니다.", type: .one)
                            }
                        }
                    }
                }
            }
        }
    }
}
//MARK: - UI
extension SplashVC {
    private func commonInit() {
        view.backgroundColor = .mainColor
    }
    
    private func setupUI() {
        addSubViews([
            kkumingLetterImageView,
            pencilImageView,
            rabbitImageView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        kkumingLetterImageView.snp.makeConstraints{
            $0.width.equalTo(105)
            $0.height.equalTo(54)
            $0.centerX.centerY.equalToSuperview()
        }
        
        pencilImageView.snp.makeConstraints{
            $0.width.equalTo(35)
            $0.height.equalTo(51)
            $0.leading.equalTo(kkumingLetterImageView.snp.trailing).offset(15)
            $0.bottom.equalTo(kkumingLetterImageView.snp.bottom)
        }
        
        rabbitImageView.snp.makeConstraints{
            $0.width.equalTo(279)
            $0.height.equalTo(233)
            $0.centerX.equalToSuperview().offset(-20)
            $0.bottom.equalToSuperview()
        }
    }
}
