//
//  SplashModel.swift
//  kkuming
//
//  Created by 이진욱 on 2022/10/20.
//

import Foundation

// user 최신 버전 업데이트 유무
enum UserIsUpdate: String {
    case yes = "Y"
    case no = "N"
}

// admin 강제 업데이트 사용 유무
enum UserIsForceUpdate: String {
    case yes = "Y"
    case no = "N"
}
