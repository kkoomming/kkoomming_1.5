//
//  SplashViewModel.swift
//  kkuming
//
//  Created by 이진욱 on 2022/10/19.
//

import Foundation

class SplashViewModel {
    //MARK: - Properties
    private var bundleCurrentAppVersion: String = {    // App Version
        let version: String = AppVersion.getVersion()
        return version
    }()
    
    private var userIsUpdate: UserIsUpdate = .no       // 강제 업데이트 유무
}
//MARK: - User
extension SplashViewModel {
    
    /**
     함수 정의 : user의 bundle, server App Version Check
     - Parameter completionHandler : Current App Check 결과를 Bool 값으로 전달
     */
    func userCurrentAppVersionCheck(completionHandler: @escaping(UserIsUpdate) -> Void) {
        userIsUpdate == .yes ? completionHandler(.yes) : completionHandler(.no)
    }
    
    /**
     함수 정의 : user의 저장된 로그인 정보가 있는지 확인
     - Parameter completionHandler : user의 저장된 로그인 정보가 있는지 여부 결과를 Bool 값으로 전달
     */
    func userAutoLoginCheck(completionHandler: @escaping(String?) -> Void) {
        let info = UserDefaultsUtil.readUserInfo()
        !info.isEmpty ? completionHandler(info) : completionHandler(nil)
    }
    
    /**
     함수 정의 : userInfo 정보를 Json 파싱 후 UserInfo 값으로 전달
     - Parameter userInfo : user의 저장된 로그인 정보
     - Parameter completionHandler : UserInfo Data Parsing 후 결과를 Userinfo Or Nil 값으로 전달
     */
    func userInfoDataParsing(userInfo: String, completionHandler: @escaping(UserInfo?) -> Void) {
        do {
            let decoder = JSONDecoder()
            if let data = userInfo.data(using: .utf8) {
                let model = try decoder.decode(UserInfo.self, from: data)
                completionHandler(model)
            }
        } catch {
            completionHandler(nil)
        }
    }
    
}
//MARK: - Method
extension SplashViewModel {
    /**
     함수 정의 : server로 부터 받은 forceUpdate -> IsUpdate 체크 후 userIsUpdate Enum 값 변경
     - Parameter isUpdate : user 강제 업데이트 유무
     */
    private func setUserIsUpdate(isUpdate: String, forceUpdate: String) {
        guard forceUpdate == UserIsForceUpdate.yes.rawValue else { return }
        switch isUpdate {
        case UserIsUpdate.yes.rawValue:
            userIsUpdate = .yes
        case UserIsUpdate.no.rawValue:
            userIsUpdate = .no
        default:
            Log.error(message: "setUserIsUpdate(isUpdate: String) | isUpdate 값 오류")
            break
        }
    }
}
//MARK: - APi
extension SplashViewModel {
    /**
     함수 정의 : App Version API 호출
     */
    func getAppVersions() {
        API.getAppVersions(version: bundleCurrentAppVersion) { [weak self] model in
            guard let strongSelf = self, let model = model, let data = model.data else {
                return
            }
            
            Log.network(message: "getAppVersions() | 성공")
            Log.info(message: "data.isUpdate : \(data.isUpdate)")
            Log.info(message: "data.forceUpdate : \(data.forceUpdate)")
            
            strongSelf.setUserIsUpdate(isUpdate: data.isUpdate, forceUpdate: data.forceUpdate)
        }
    }
}

