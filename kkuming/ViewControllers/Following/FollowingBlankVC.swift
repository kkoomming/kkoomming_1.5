//
//  FollowingBlankVC.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/21.
//

import UIKit
import SnapKit

class FollowingBlankVC: BaseVC {
    var feedVC:UIView = App.getFeedVC().view!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        feedVC = App.getFeedVC().view!
        
        self.view.addSubview(feedVC)
        addChild(App.getFeedVC())
        
        feedVC.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}
