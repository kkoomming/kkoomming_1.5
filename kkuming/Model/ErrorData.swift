//
//  ErrorData.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/15.
//

import Foundation

//게시물 상세 조회
struct ErrorData: Codable {
    let status: Int
    let success: Bool
    let data:ErrorDetailData
}
struct ErrorDetailData: Codable {
    let error: String?
    let code: String
    let isShow: Bool
    let request: String
    let method: String
}

/*
 {
     "status":403,
     "success":false,
     "data":{
        "code":"2003",
        "isShow":true,
        "request":"/api/CLIENT/v1.5/user/getAccessToken",
        "method":"GET"
     }
 }
 
 */
