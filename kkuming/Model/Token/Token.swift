//
//  Token.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/23.
//

import Foundation

struct Token: Codable {
    let accessToken: String?
}
