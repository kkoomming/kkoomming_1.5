import UIKit

struct UserData: Codable {
    let error:String?
    var user: User?
    var accessToken: String?
    let refreshToken: String?
}

struct User: Codable {
    let id: Int?
    let position: String?
    let accountType, nickname, memberID: String?
    let email: String?
    let role: String?
    let totalFollower, totalFollowing, totalReported, totalMyPost: Int?
    let linkShare: String?
    let bio: String?
    let isOnline: Bool?
    let status: String?
    let avatar: String?
    let userType: String?
    let totalAccessApp: Int?
    let lastDateAccessApp: String?
    let lastDateSignin, createdAt, deviceToken: String?
    let deviceID: Int?
    var isFollowed: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, position, accountType, nickname, memberID, email, role, totalFollower, totalFollowing, totalReported, totalMyPost, linkShare, bio, isOnline, status, avatar, userType, totalAccessApp, lastDateAccessApp, lastDateSignin, createdAt, deviceToken, deviceID, isFollowed
    }
    
    var isBlocked: Bool?
}

//로컬 저장용
struct UserInfo: Codable {
    let clientId:String
    let loginMethod:String
}
