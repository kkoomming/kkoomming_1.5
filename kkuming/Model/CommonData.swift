//
//  CommonData.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/21.
//

import UIKit

//공통 REST API
struct CommonData: Codable {
    let status: Int
    let success: Bool
    let data: Message
}

struct Message: Codable {
    let messages: String
}


//토큰 갱신
struct TokenData: Codable {
    let status: Int
    let success: Bool
    let data: Token?
}

//로그인
struct LoginData: Codable {
    let status: Int
    let success: Bool
    let data: UserData?
}

//내정보
struct MyProfileData: Codable {
    let status: Int
    let success: Bool
    let data: User?
}

//팔로잉, 팔로워
struct MyFollowData: Codable {
    let status: Int
    let success: Bool
    let data: UserAndMetaData?
}

//타인) 팔로워
struct ProfileFollowerData: Codable {
    let status: Int
    let success: Bool
    let data: FollowerAndMetaData?
}

struct ProfileFollowingData: Codable {
    let status: Int
    let success: Bool
    let data: FollowingAndMetaData?
}

//좋아요
struct LikeData: Codable {
    let status: Int
    let success: Bool
    let data: LikeStatusData
}
struct LikeStatusData: Codable {
    let isLiked:Bool
    let totalLikes:Int?
}

struct UnLikeData: Codable {
    let status: Int
    let success: Bool
    let data: UnLikeStatusData
}
struct UnLikeStatusData: Codable {
    let unLikeAll:Bool
}

//팔로잉, 언팔로잉
struct FollowingData: Codable {
    let status: Int
    let success: Bool
    let data: FollowedData
}
struct FollowedData: Codable {
    let isFollowed:Bool
}

//스크랩
struct ScrapData: Codable {
    let status: Int
    let success: Bool
    let data: ScrapStatusData
}
struct ScrapStatusData: Codable {
    let isScrap:Bool
}

//스크랩 해제
struct UnScrapData: Codable {
    let status: Int
    let success: Bool
    let data: UnScrapStatusData
}
struct UnScrapStatusData: Codable {
    let unScrapAll:Bool
}

//메인
struct MainData: Codable {
    let status: Int
    let success: Bool
    let data: MainListData?
}

//게시판 상세
struct PostData: Codable {
    let status: Int
    let success: Bool
    var data: DetailData?
}

//게시판 글쓰기 완료후
struct CreatePostData: Codable {
    let status: Int
    let success: Bool
    var data: Post?
}

//내 글 조회
struct MyPostData: Codable {
    let status: Int
    let success: Bool
    let data: MyPost?
}

//게시판 댓글 조회
struct CommentsData: Codable {
    let status: Int
    let success: Bool
    let data: CommentList?
}

//게시판 답글 조회
struct ReplyData: Codable {
    let status: Int
    let success: Bool
    let data: ReplyList?
}

//게시판 댓글 등록
struct CommentData: Codable {
    let status: Int
    let success: Bool
    let data: Comment?
}

struct FeedData: Codable {
    let status: Int
    let success: Bool
    let data: Feed?
}

//태그
struct PostsData: Codable {
    let status: Int
    let success: Bool
    let data: ContentData?
}

//추천 태그
struct RecomTagData: Codable {
    let status: Int
    let success: Bool
    let data: ContentTagData?
}

struct BannerAndPopupData: Codable {
    let status: Int
    let success: Bool
    let data: SlideData
}

struct NoticeData: Codable {
    let status: Int
    let success: Bool
    let data: NoticeList?
}

//검색 메인
struct SearchMainData: Codable {
    let status: Int
    let success: Bool
    let data: SearchData?
}

struct SearchResultData: Codable {
    let status: Int
    let success: Bool
    let data: SearchResultListData?
}

struct EventsData: Codable {
    let status: Int
    let success: Bool
    let data: EventsList?
}

struct EventDeatilData: Codable {
    let status: Int
    let success: Bool
    let data: EventsListDetail?
}

//댓글 조회
struct ReportListData: Codable {
    let status: Int
    let success: Bool
    let data: [ReportData]?
}

//댓글 신고
struct ReportCommentData: Codable {
    let status: Int
    let success: Bool
    let data: ReportData?
}

struct BlockUserListData: Codable {
    let status: Int
    let success: Bool
    let data:UserAndMetaData?
}

//차단 데이터
struct BlockUserData: Codable {
    let status: Int
    let success: Bool
    let data: BlockUserStatusData?
}

struct BlockUserStatusData: Codable {
    let isBlocked:Bool?
}

//피드 - 팔로잉
struct FeedFollowingData: Codable {
    let status: Int
    let success: Bool
    let data: FeedFollowingListData?
}
struct FeedFollowingListData: Codable {
    let posts: ContentData?
    let following: UserAndMetaData?
}

// 설정 - 앱 버전 가져오기
struct AppVersionDatas: Codable {
    let status: Int
    let success: Bool
    let data: AppVersionData?
}

//설정-앱설정 가져오기
struct SettingDatas: Codable {
    let status: Int
    let success: Bool
    let data:SettingData?
}

struct AppSettingPushStatusData: Codable {
    let status: Int
    let success: Bool
    let data:PushStatusData?
}

//문의 작성
struct InquiryDatas: Codable {
    let status: Int
    let success: Bool
    let data:InquiryData?
}

struct InquiryData: Codable {
    let id, createBy: Int?
    let email, type, questionContent, status, updatedAt, createdAt: String?
}

//알림
struct AlarmDatas: Codable {
    let status: Int
    let success: Bool
    var data:NotificationData?
}
struct NotificationData: Codable {
    var notifications:[AlarmData]?
    let metaData:MetaData
}
//알림 읽을 때
struct AlarmReadData: Codable {
    let status: Int
    let success: Bool
    var data:AlarmData?
}
//알림 삭제
struct AlarmDeleteData: Codable {
    let status: Int
    let success: Bool
    let data: [Int]?
}
