import UIKit
import Photos

class GalleryData {
    var identifier:String?
    var image:UIImage?
    var asset:PHAsset?
    
    init(identifier:String?, image:UIImage?, asset:PHAsset? = nil) {
        self.identifier = identifier
        self.image = image
        self.asset = asset
    }
}
