//
//  SettingData.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/14.
//

import Foundation

struct SettingData: Codable {
    let id: Int?
    let appVersion: String?
    let androidVersion, iOSVersion: String
    let appPrivacyPolicyUrl, appTermsServiceUrl: String
    let sysEmail, status, createdAt, updatedAt: String
}

struct PushStatusData: Codable {
    let notiNewFollower, notiNewLikePost,
        notiNewCommentPost, notiNewReplyComment,
        notiNewNoticeEventFromSystem: String
}

struct AppVersionData: Codable {
    let os: String
    let newVersion: String
    let nowVersion: String
    let isUpdate: String
    let forceUpdate: String
}
