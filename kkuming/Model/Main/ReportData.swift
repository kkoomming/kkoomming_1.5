//
//  ReportData.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/14.
//

import Foundation

struct ReportData: Codable {
    let reportState: String?
    let isSeen: Bool?
    let id, reporterID, reportedPersonID, reportPostID: Int?
    let reportCommentID, reportCategoryID: Int?
    let reportType, status, updatedAt, createdAt: String?
    let description, type: String?
}
