//
//  SearchData.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/05.
//

import Foundation

struct SearchData: Codable {
    var banner: SlideData?
    var searchTag: [SearchRecommendTag]?
    var recomUser: [User]?
}

struct SearchRecommendTag: Codable {
    var tagName: String
    var totalPopular: String
}

struct SearchResultListData: Codable {
    var userData:UserAndMetaData?
    var feedData:SearchFeedData?
}

struct UserAndMetaData: Codable {
    var users:[User]?
    var metaData:MetaData?
}

struct FollowerAndMetaData: Codable {
    var follower:UserAndMetaData?
    var metaData:MetaData?
}

struct FollowingAndMetaData: Codable {
    var following:UserAndMetaData?
    var metaData:MetaData?
}

struct SearchFeedData: Codable {
    var posts:[Post]?
    var metaData:MetaData?
}
