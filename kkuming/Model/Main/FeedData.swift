//
//  FeedData.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/29.
//

import Foundation

struct Feed: Codable {
    let posts: [Post]
    let metaData: MetaData
}
