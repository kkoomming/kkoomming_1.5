//
//  Comment.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/04.
//

import Foundation

struct CommentList: Codable {
    let comments: [Comment]?
    let metaData:MetaData?
}

struct ReplyList: Codable {
    let comments: [Comment]?
}

struct CommentOwner: Codable {
    let id: Int?
    let nickname: String?
    let memberID: String?
    let avatar: String?
}
