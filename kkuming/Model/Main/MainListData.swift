//
//  MainListData.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/23.
//

import Foundation

struct MainListData: Codable {
    let banner, popup: SlideData?
    let today, best: ContentData?
    let challenge: [EventData]?
    let recommendTag: RecommendTag?
    let userList: [User]?
    
    enum CodingKeys:String, CodingKey {
        case banner = "banner"
        case popup = "popup"
        case today = "today"
        case best = "best"
        case challenge = "challenge"
        case recommendTag = "recommendTag"
        case userList = "bestUser"
    }
}

// MARK: - Slide
struct SlideData: Codable {
    let slides:[BannerData]?
    let metaData:MetaData?
}

// MARK: - Banner
struct BannerData: Codable {
    let id: Int
    let type, webViewTitle, linkUrl: String?
    let mainImage, subImage: ImageData?
    let popupImage:ImageData?
    let numOrder, createdBy: Int
    let startAt, endAt, position, exposure: String
    let eventsId: Int?
    let status: String?
    let createdAt, updatedAt: String
    let decoChallengeTagName, decoChallengeImage, decoChallengeDetailImage, mainImg: String?
}

struct ContentData: Codable {
    let posts: [Post]?
    let metaData: MetaData?
}

struct ContentTagData: Codable {
    let manageTag: [Tag]?
    let metaData: MetaData?
}

// MARK: - MetaData
struct MetaData: Codable {
    let previousPage, currentPage, nextPage, firstPage: Int
    let lastPage, pageSize, totalPages: Int
    var totalRecords: Int
}

// MARK: - Post
struct Post: Codable {
    let id: Int
    var description: String?
    var createdBy, thumbnail, totalLikes, totalComments: Int?
    var totalViews, totalScraps, totalPopularTags, numOrder: Int?
    var postType: String?
    var createdAt: String?
    var thumbnailImage: String?
    var isComment, isLike, isScrap, isReported: Bool?
    var hashTags: [String]?
    var owner: User?
    var album: [String]?
    
    init(id:Int) {
        self.id = id
    }
}

struct MyPost: Codable {
    let id: Int
    let description: String?
    let createdBy, thumbnail, totalLikes, totalComments: Int?
    let totalViews, totalScraps, totalPopularTags, numOrder: Int?
    let postType: String?
    let createdAt: String?
    let thumbnailImage: String?
    let isComment, isLike, isScrap, isReported: Bool?
    let hashTags: MyPostTag?
    let owner: User?
    let album: [ImageData]?
    var fileIdFirst: Int?
    
}

struct MyPostTag: Codable {
    var tagPopulars: [Tag]?
    var tagInputs: [Tag]?
}

struct ImageData: Codable {
    let id: Int
    let url: String?
    let filename: String?
}

// MARK: - Challenge
struct EventsList: Codable {
    let events: [EventData]
    let metaData: MetaData?
}

struct EventsListDetail: Codable {
    let events: EventData
    let postsList: EventListDeatilPost?
}

struct EventListDeatilPost: Codable {
    let posts: [Post]?
    let metaData: MetaData?
}

// MARK: - EventsList
struct EventData: Codable {
    let id: Int
    let title, type: String?
    let createdBy: Int
    let status: String?
    let startAt, endAt: String
    let linkUrl: String?
    let contentLinkType: String?
    let imageURLID, imageURLId2, numOrder: Int?
    let primaryTag, createdAt, updatedAt: String?
    let files2, files: Files?
    let manage_tags: [Tag]?
    let posts: [Post]?
}

// MARK: - Files
struct Files: Codable {
    let id, createdBy: Int
    let postID: String?
    let type, bucket: String?
    let url: String
    let key, filename: String?
    let size: Int
    let status, createdAt, updatedAt: String
}

// MARK: - Tag
struct Tag: Codable {
    let id: Int
    let createdBy: Int?
    let roleCreator: String?
    let tagName: String
    let totalSelects, totalSearchs, numOrder: Int?
    let status: String?
    let type: String?
    let startAt, endAt: String?
    let createdAt, updatedAt: String?
}

// MARK: - RecommendTag
struct RecommendTag: Codable {
    let manageTag: [Tag]?
    let metaData: MetaData?
}
