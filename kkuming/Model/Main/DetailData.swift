import Foundation

struct DetailData: Codable {
    var post: Post
    var commentsList: CommentsList
    
    enum CodingKeys: String, CodingKey {
        case post = "posts"
        case commentsList
    }
}

// MARK: - CommentsList
struct CommentsList: Codable {
    var comments: [Comment]
    let metaData: MetaData
    let replysCommentsMetaData: MetaData
}

// MARK: - Comment
struct Comment: Codable {
    let id, postId, postOwnerId, parentId: Int
    let content: String
    let isEdit: Bool
    let totalReply, createdBy: Int
    let status: String
    let tagCommentOwnerId: Int?
    let createdAt, updatedAt: String
    let commentOwner: User?
    let tagCommentOwner: User?
    let isReported: Bool?
    var replyComments: [Comment]?
    let metaData:MetaData?
    
    enum CodingKeys: String, CodingKey {
        case id, postId, postOwnerId, parentId, content, isEdit,
             totalReply, createdBy, status, tagCommentOwnerId,
             createdAt, updatedAt, commentOwner, tagCommentOwner,
             isReported, replyComments, metaData
    }
    
    //일반 댓글과 답변을 구분하기 위해 사용됨
    var isReply:Bool = false
}

