import Foundation

struct NoticeList: Codable {
    let notices:[Notice]?
    let metaData:MetaData?
}

struct Notice: Codable {
    let id: Int
    let title, content, type: String
    let createdBy: Int
    let isShow: Bool
    let isNew: Bool
    let status, createdAt, updatedAt: String
    
    enum CodingKeys:String, CodingKey {
        case id = "id"
        case title = "title"
        case content = "content"
        case type = "type"
        case createdBy = "createdBy"
        case isShow = "isShow"
        case isNew = "isNew"
        case status = "status"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
    }
    
    var isOpen:Bool = false
}
