//
//  AlarmData.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/27.
//

import Foundation

struct AlarmData: Codable {
    let id, ownerId:Int?
    var title, body:String?
    var type, imageUrl, status, collapseKey, createdAt, updatedAt: String
    var data: AlarmTypeData?
}

struct AlarmTypeData: Codable {
    let type:String?
    let followerId, postId, noticeId, eventsId:Int?
    let linkUrl, title:String? //MainListData에서는 title이 webViewTitle로 되어있음(서로 다름)
    
    let commentId, commentOwner, totalComments, likeOwnerm, totalLikes:Int?
    let followerNickname, commentNickname, likeNickname:String?
}
