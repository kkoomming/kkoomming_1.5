//
//  PagerViewController.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/27.
//

import UIKit
import XLPagerTabStrip

class PagerVC: ButtonBarPagerTabStripViewController {
    let unSelectedColor = RGB(red: 142, green: 142, blue: 142)
    let selectedColor = UIColor.mainColor
    
    func updateTitleLabel(oldCell:ButtonBarViewCell?, newCell: ButtonBarViewCell?) {
        oldCell?.label.textColor = .black
        newCell?.label.textColor = .black
        
        if let startIndex = oldCell?.label.text?.firstIndex(of: "("),
           let endIndex = oldCell?.label.text?.firstIndex(of: ")"),
           let originalText = oldCell?.label.text {
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: getFont(size: 16, type: .bold)
            ]
            let attributedText = NSMutableAttributedString(string: originalText, attributes: attributes)
            
            let substringRange = originalText.range(of: String(originalText[startIndex...endIndex]))!
            let range = NSRange(substringRange, in: originalText)
            
            attributedText.addAttribute(.foregroundColor, value: self.unSelectedColor, range: range)
            
            oldCell?.label.attributedText = attributedText
        }
        
        if let startIndex = newCell?.label.text?.firstIndex(of: "("),
           let endIndex = newCell?.label.text?.firstIndex(of: ")"),
           let originalText = newCell?.label.text {
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: getFont(size: 16, type: .bold)
            ]
            let attributedText = NSMutableAttributedString(string: originalText, attributes: attributes)
            
            let substringRange = originalText.range(of: String(originalText[startIndex...endIndex]))!
            let range = NSRange(substringRange, in: originalText)
            
            attributedText.addAttribute(.foregroundColor, value: self.unSelectedColor, range: range)
            
            newCell?.label.attributedText = attributedText
        }
    }
}
