public enum FontType {
    case light
    case regular
    case bold
    case EBold
}

public enum SNSType {
    case naver
    case kakao
    case google
    case apple
    case `default`
}

public enum TopViewType {
    case main
    case feed
    case write
    case mypage
    case search
    case alert
    case detail
    case myDetail
    case galleryEdit
    case profile
    case back
    case profileUpdate
    case `default`
}

public enum VariousListType {
    case two
    case three
}

public enum ButtonViewType {
    case gray
    case mainColor
    case follow
}

public enum FollowingContentType {
    case like
    case comment
    case bookmark
}

public enum ViewControllerType {
    case search
    case alert
    case setting
}

public enum TabBarType {
    case home
    case feed
    case feedPopular
    case feedFollowing
    case write
    case mypage
}

public enum AlertButtonType {
    case gray //회색
    case mainColor //메인컬러
    case orange
    case blank
}

public enum PostDetailType: String {
    case POST_TODAY_DECORATING = "POST_TODAY_DECORATING" // 홈>메인>오늘의 꾸미기에서 접근시
    case POST_BEST_DECORATING = "POST_BEST_DECORATING" // 홈>메인>베스트 꾸미기에서 접근
    case POST_CHALLENGE = "POST_CHALLENGE" // 홈>메인>챌린지, 챌린지>상세 에서 접근시
    case POST_BY_TAG = "POST_BY_TAG" // #태그 상세
    case POST_FEED = "POST_FEED" // 피드>상세에서 접근시
    case POST_FOLLOWING = "POST_FOLLOWING" // 피드>팔로잉, 하단>팔로잉 에서 접근시
    case NONE = "NONE" //마이꾸밍, 글쓰기
}

public enum DateFormatType:String {
    case post = "yyyy.MM.dd"
    case comment = "MM.dd a hh:mm"
}

public enum StatusType:String {
    case ACTIVE = "ACTIVE" //일반
    case USER_REMOVED = "USER_REMOVED" //삭제
    case ADMIN_WARNING = "ADMIN_WARNING" //신고
    case BLOCKED = "BLOCKED" //차단
}

public enum StatusString:String {
    case USER_REMOVED = "삭제된 글입니다"
    case ADMIN_WARNING = "신고에 의해 보이지 않습니다"
    case BLOCKED = "차단된 사용자입니다"
}

public enum FeedType:String {
    case POST_LATEST = "POST_LATEST" //최신순
    case LIKE_MOST_7DAY = "LIKE_MOST_7DAY" //인기순
}

public enum ExposureType:String {
    case POPUP = "POPUP" //팝업
    case BANNER = "BANNER" //배너
}

public enum PeriodType:String {
    case ING = "ING" //진행중
    case CLOSE = "CLOSE" //마감
}

public enum SearchType:String {
    case FEED = "FEED" //피드
    case USER = "USER" //유저
}

public enum MyFeedType:String {
    case post = "post"
    case like = "like"
    case scrap = "scrap"
}

public enum InquiryType: String {
    case INQUIRY = "INQUIRY" //이용문의
    case EVENT_RELATED = "EVENT_RELATED" //이벤트관련
    case SERVICE_PROPOSAL = "SERVICE_PROPOSAL" //서비스제안
    case ETC = "ETC" //기타
}

public enum SwitchType: String {
    case ON = "ON"
    case OFF = "OFF"
}

public enum PushType: String {
    case NOTI_NEW_FOLLOWER = "NOTI_NEW_FOLLOWER" //팔로워 푸시 설정
    case NOTI_NEW_LIKE_POST = "NOTI_NEW_LIKE_POST" //좋아요 푸시 설정
    case NOTI_NEW_COMMENT_POST = "NOTI_NEW_COMMENT_POST" //댓글 푸시 설정
    case NOTI_NEW_REPLY_COMMENT = "NOTI_NEW_REPLY_COMMENT" //답글 푸시 설정
    case NOTI_NEW_NOTICE_EVENT_FROM_SYSYTEM = "NOTI_NEW_NOTICE_EVENT_FROM_SYSYTEM" //이벤트 푸시 설정
}
