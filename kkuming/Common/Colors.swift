import UIKit

extension UIColor {
    //#38C4CF
    static let mainColor = UIColor(red: 56.0/255.0, green: 196.0/255.0, blue: 207.0/255.0, alpha: 1.0)
    
    //#8C8B90
    static let textGrayColor = UIColor(red: 140.0/255.0, green: 139.0/255.0, blue: 144.0/255.0, alpha: 1.0)
    static let borderColor = UIColor(red: 239.0/255.0, green: 239.0/255.0, blue: 239.0/255.0, alpha: 1.0)
    static let placeholderColor = UIColor(red: 121.0/255.0, green: 121.0/255.0, blue: 121.0/255.0, alpha: 1.0)
}
