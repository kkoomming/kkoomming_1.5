//
//  BannerEnum.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/19.
//

import UIKit

enum BannerType:String {
    case BANNER_WEB_VIEW = "BANNER_WEB_VIEW"
    case WEB_VIEW = "WEB_VIEW"
    case EVENT = "EVENT"
    case CHALLENGE = "CHALLENGE"
    case URL = "URL"
}
