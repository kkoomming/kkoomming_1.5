//
//  MainTabBarController.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/21.
//

import UIKit
import SnapKit

public class MainTabBarController: UITabBarController {
    var mainViewControllers:[UIViewController]!
    var previousIndex:Int = 0
    var realFeedVC:UIViewController!
    
    public init(){
        super.init(nibName: nil, bundle: nil)
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tabBar.frame.size.height = tabBar.frame.size.height + 14
        tabBar.frame.origin.y = tabBar.frame.origin.y - 14
    }
    
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        setupUI()
        
        //MARK: 한번 초기화
        _ = App.getFeedVC()
    }

    private func setupUI() {
        setupTabBar()
        setupTabBarProperties()
        setupTabBarAppearance()
        setupBottomShadow()
    }
    
    func setupTabBar() {
        
        mainViewControllers = [
            mainVC,
            feedVC,
            writeVC,
            followingVC,
            myPageVC
        ]
        
        App.setTwoDepthNavi(mainViewControllers as! [UINavigationController])
        
        self.viewControllers = mainViewControllers
    }
    
    var mainVC: UINavigationController {
        let vc = MainVC()
        let image = UIImage(named: "ic_home")?.withRenderingMode(.alwaysOriginal)
        let selected_image = UIImage(named: "ic_home_over")?.withRenderingMode(.alwaysOriginal)
        let title = "홈"
        
        vc.tabBarItem.image = image
        vc.tabBarItem.selectedImage = selected_image
        vc.tabBarItem.title = title
        
        let navigationController = UINavigationController(rootViewController: vc)
        
        return navigationController
    }
    
    var feedVC: UINavigationController {
        let vc = FeedVC()
        let image = UIImage(named: "ic_feed")?.withRenderingMode(.alwaysOriginal)
        let selected_image = UIImage(named: "ic_feed_over")?.withRenderingMode(.alwaysOriginal)
        let title = "피드"
        vc.tabBarItem.image = image
        vc.tabBarItem.selectedImage = selected_image
        vc.tabBarItem.title = title
        vc.loadViewIfNeeded()
        
        let navi = UINavigationController(rootViewController: vc)

        return navi
    }
    
    var writeVC: UINavigationController {
        let vc = WriteBlankVC()
        let image = UIImage(named: "ic_write")?.withRenderingMode(.alwaysOriginal)
        let title = "글쓰기"
        vc.tabBarItem.image = image
        vc.tabBarItem.title = title
        
        let navigationController = UINavigationController(rootViewController: vc)

        return navigationController
    }
    
    var followingVC: UINavigationController {
        let vc = FollowingBlankVC()
        let image = UIImage(named: "ic_following")?.withRenderingMode(.alwaysOriginal)
        let selected_image = UIImage(named: "ic_following_over")?.withRenderingMode(.alwaysOriginal)
        let title = "팔로잉"
        vc.tabBarItem.image = image
        vc.tabBarItem.selectedImage = selected_image
        vc.tabBarItem.title = title
        
        let navigationController = UINavigationController(rootViewController: vc)

        return navigationController
    }
    
    var myPageVC: UINavigationController {
        let vc = MyPageVC()
        let image = UIImage(named: "ic_mypage")?.withRenderingMode(.alwaysOriginal)
        let selected_image = UIImage(named: "ic_mypage_over")?.withRenderingMode(.alwaysOriginal)
        let title = "마이꾸밍"
        vc.tabBarItem.image = image
        vc.tabBarItem.selectedImage = selected_image
        vc.tabBarItem.title = title
        
        let navigationController = UINavigationController(rootViewController: vc)

        return navigationController
    }
    
}

//MARK: 속성
extension MainTabBarController {
    func setupTabBarProperties() {
        self.view.backgroundColor = .white
        tabBar.clipsToBounds = true
        tabBar.layer.masksToBounds = true
        tabBar.layer.cornerRadius = 14
        tabBar.backgroundColor = .white
        tabBar.barTintColor = .white
        tabBar.tintColor = .black
        tabBar.isTranslucent = false
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = .white
        
        tabBar.insertSubview(backgroundView, at: 0)
        backgroundView.snp.makeConstraints{
            $0.leading.trailing.bottom.equalToSuperview()
            $0.height.equalTo(30)
        }
    }
    
    func setupTabBarAppearance() {
        let appearance = UITabBarItem.appearance()
        
        //선택 전
        let attributes:[NSAttributedString.Key: AnyObject] = [
            NSAttributedString.Key.font:getFont(size: 12, type: .regular),
            NSAttributedString.Key.foregroundColor:UIColor.black
        ]
        appearance.setTitleTextAttributes(attributes, for: .normal)
        
        //선택 후
        let attributes_selected:[NSAttributedString.Key: AnyObject] = [
            NSAttributedString.Key.font:getFont(size: 12, type: .regular)
        ]
        appearance.setTitleTextAttributes(attributes_selected, for: .selected)
        
        //이미지와 제목 사이 간격
        appearance.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -5)
    }
    
    func setupBottomShadow() {
        tabBar.layer.shadowColor = UIColor.black.cgColor
        tabBar.layer.shadowRadius = 4
        tabBar.layer.shadowOpacity = 0.5
        tabBar.layer.shadowOffset = CGSize(width: 0, height: 2)
        tabBar.layer.masksToBounds = false
    }
}

extension MainTabBarController: UITabBarControllerDelegate {
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let index = tabBarController.selectedIndex
        
        switch index {
        case 0:
            previousIndex = index
            App.setIndex(index: 0)
            break
        case 1:
            //피드
            App.setIndex(index: 1)
            App.push(tabType: .feed)
            break
        case 2:
            //글쓰기
            //마이꾸밍 -> 설정으로 안가지는 버그 때문에 삭제
//            App.setIndex(index: 2)
            App.push(WriteVC())
            tabBarController.selectedIndex = previousIndex
            break;
        case 3:
            //팔로잉
            App.setIndex(index: 3)
            App.push(tabType: .feedFollowing)
        case 4:
            previousIndex = index
            App.setIndex(index: 4)
        default:
            //기타(추가되면)
            previousIndex = index
        }
    }
}
