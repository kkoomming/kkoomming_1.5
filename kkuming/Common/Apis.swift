import UIKit
import Alamofire

class API {
    //토큰 갱신
    static func refreshToken(completion:(()->Void)? = nil) {
        ApiManager.request("/user/getAccessToken", isRefresh: true, method: .get, model: TokenData.self){ response, _  in
            guard let model = response,
                  let data = model.data,
                  let accessToken = data.accessToken else { return }
            
            UserManager.shared.updateAccessToken(accessToken: accessToken)
            completion?()
        }
    }

    //로그인
    static func login(param:Parameters, completion:@escaping (LoginData?)->Void) {
        ApiManager.request("/user/snsLogin", method: .post, param: param, model: LoginData.self){ model, data in
            do{
                if let data = data {
                    let errorData = try? JSONDecoder().decode(ErrorData.self, from: data)
                    if errorData?.data.code == Errors.e2003.rawValue || errorData?.data.code == Errors.e0001.rawValue {
                        AlertManager.shared.show(text: "로그인 정보가 만료되어 로그아웃 처리합니다.", type: .one) {
                            AppManager.shared.logout()
                        }
                        return
                    }
                    
                    if errorData == nil {
                        print("💙 login OK errorData == nil")
                        completion(model)
                    }
                }
            } catch {
                print("💙 login OK")
                completion(model)
            }
        }
    }
    
    //메인 조회
    static func getMainALL(completion:@escaping (MainData?)->Void) {
        ApiManager.request("/main/all", method: .get, model: MainData.self){ model, _ in
            completion(model)
        }
    }
    
    //게시물 상세 조회
    static func getDetailPost(param:Parameters, completion:@escaping (PostData?, Data?)->Void) {
        ApiManager.request("/post/getDetailPost", method: .get, param: param, model: PostData.self){ model, data in
            completion(model, data)
        }
    }
    
//MARK: - 글쓰기
    //글쓰기
    static func createNewPost(param:Parameters, files:[Data], completion:@escaping (CreatePostData?)->Void) {
        ApiManager.upload("/post/createNewpost", method: .post, param: param, files: files, model: CreatePostData.self){ model, _ in
            completion(model)
        }
    }
    
    //글 수정
    static func updatePost(param:Parameters, files:[Data], completion:@escaping (CommonData?)->Void) {
        ApiManager.upload("/post/updatePost", method: .post, param: param, files: files, model: CommonData.self){ model, _ in
            completion(model)
        }
    }
    
    //내 글 조회
    static func getDetailMyPost(param:Parameters, completion:@escaping (MyPostData?)->Void) {
        ApiManager.request("/post/getDetailMyPost", method: .get, param: param, model: MyPostData.self){ model, _ in
            completion(model)
        }
    }
    
    //내 글 삭제
    static func deletePost(param:Parameters, completion:@escaping (CommonData?)->Void) {
        ApiManager.request("/post/deletePost", method: .delete, param: param, model: CommonData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 댓글
    //댓글 조회
    static func getComment(param:Parameters, completion:@escaping (CommentsData?)->Void) {
        ApiManager.request("/comment/gets", method: .get, param: param, model: CommentsData.self){ model, _ in
            completion(model)
        }
    }
    
    //댓글 작성
    static func postComment(param:Parameters, completion:@escaping (CommentData?)->Void) {
        ApiManager.request("/comment/create", method: .post, param: param, model: CommentData.self){ model, _ in
            completion(model)
        }
    }
    
    //댓글 수정
    static func updateComment(param:Parameters, completion:@escaping (CommonData?)->Void) {
        ApiManager.request("/comment/edit", method: .put, param: param, model: CommonData.self){ model, _ in
            completion(model)
        }
    }
    
    //댓글 삭제
    static func deleteComment(param:Parameters, completion:@escaping (CommonData?)->Void) {
        ApiManager.request("/comment/delete", method: .delete, param: param, model: CommonData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 답글
    //답글 작성
    static func postReply(param:Parameters, completion:@escaping (CommentData?)->Void) {
        ApiManager.request("/comment/reply", method: .post, param: param, model: CommentData.self){ model, _ in
            completion(model)
        }
    }
    
    //답글 조회
    static func replyGets(param:Parameters, completion:@escaping (ReplyData?)->Void) {
        ApiManager.request("/comment/replyGets", method: .get, param: param, model: ReplyData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 피드
    //최신, 인기 피드
    static func getPostInFeed(param:Parameters, completion:@escaping (FeedData?)->Void) {
        ApiManager.request("/post/getPostInFeed", method: .get, param: param, model: FeedData.self){ model, _ in
            completion(model)
        }
    }
    
    // 팔로잉 피드 : /post/getSomeoneFollowingPost
    static func getSomeoneFollowingPost(offset:Int = 1, completion:@escaping (FeedFollowingData?)->Void) {
        let param:Parameters = [
            "offset" : offset
        ]
        
        ApiManager.request("/post/getSomeoneFollowingPost", method: .get, param: param, model: FeedFollowingData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 마이꾸밍
    //프로필 조회
    static func getMyProfile(param:Parameters = [:], completion:@escaping (MyProfileData?)->Void) {
        ApiManager.request("/user/getMyProfile", method: .get, param: param, model: MyProfileData.self){ model, _ in
            completion(model)
        }
    }
    
    //마이꾸밍 피드조회(내 글/좋아요/스크랩)
    static func getMyFeed(type:MyFeedType, offset:Int = 1, completion:@escaping (PostsData?)->Void) {
        let param:Parameters = [
            "type" : type.rawValue,
            "offset" : offset
        ]
        
        ApiManager.request("/post/getMyFeed", method: .get, param: param, model: PostsData.self){ model, _ in
            completion(model)
        }
    }
    
    //프로필 수정 : /updateMyProfile/all
    static func updateMyProfile(param:Parameters = [:], files:[Data] = [], completion:@escaping (MyProfileData?, Data?)->Void) {
        ApiManager.upload("/user/updateMyProfile/all", method: .put, param: param, files: files, model: MyProfileData.self){ model, data in
            completion(model, data)
        }
    }
    
//MARK: - 태그
    //검색 태그
    static func getTagList(param:Parameters, completion:@escaping (PostsData?)->Void) {
        ApiManager.request("/post/search", method: .get, param: param, model: PostsData.self){ model, _ in
            completion(model)
        }
    }
    
    //글쓰기 추천 태그
    static func getRecomTagList(completion:@escaping (RecomTagData?)->Void) {
        ApiManager.request("/manageTag/getListTag", method: .get, model: RecomTagData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 배너/팝업
    //이벤트에서 사용
    static func getBannerList(exposureType:ExposureType, periodType:PeriodType, offset:Int = 1, completion:@escaping (BannerAndPopupData?)->Void) {
        let parameters:Parameters = [
            "exposure" : exposureType.rawValue,
            "periodType" : periodType.rawValue,
            "offset" : offset
        ]
        
        ApiManager.request("/slide/getBannerList", method: .get, param: parameters, model: BannerAndPopupData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 검색
    static func getSearchAll(param:Parameters = [:], completion:@escaping (SearchMainData?)->Void) {
        ApiManager.request("/main/searchAll", method: .get, param: param, model: SearchMainData.self){ model, _ in
            completion(model)
        }
    }

    static func search(keyword:String, offset:Int, type:SearchType, completion:@escaping (SearchResultData?)->Void) {
        let param:Parameters = [
            "limit" : 10,
            "offset" : offset,
            "textSearch" : keyword,
            "searchType" : type.rawValue
        ]
        
        ApiManager.request("/main/search", method: .get, param: param, model: SearchResultData.self){ model, _ in
            completion(model)
        }
    }
//MARK: - 프로필
    //내 프로필 팔로워
    static func getMyFollower(offset:Int = 1, completion:@escaping (ProfileFollowerData?)->Void) {
        let param:Parameters = ["offset":offset]
        ApiManager.request("/user/getMyFollower", method: .get, param: param, model: ProfileFollowerData.self){ model, _ in
            completion(model)
        }
    }
    
    //내 프로필 팔로잉
    static func getMyFollowing(offset:Int = 1, completion:@escaping (ProfileFollowingData?)->Void) {
        let param:Parameters = ["offset":offset]
        ApiManager.request("/user/getMyFollowing", method: .get, param: param, model: ProfileFollowingData.self){ model, _ in
            completion(model)
        }
    }
    
    //타인 프로필 팔로워
    static func getSomeoneFollower(param:Parameters, completion:@escaping (ProfileFollowerData?)->Void) {
        ApiManager.request("/user/getSomeoneFollower", method: .get, param: param, model: ProfileFollowerData.self){ model, _ in
            completion(model)
        }
    }
    
    //타인 프로필 팔로잉
    static func getSomeoneFollowing(param:Parameters, completion:@escaping (ProfileFollowingData?)->Void) {
        ApiManager.request("/user/getSomeoneFollowing", method: .get, param: param, model: ProfileFollowingData.self){ model, _ in
            completion(model)
        }
    }
    
    //프로필(타인)
    static func getDetailUser(userId:Int, completion:@escaping (MyProfileData?)->Void) {
        let param:Parameters = [
            "userId" : userId
        ]
        
        ApiManager.request("/user/getDetailUser", method: .get, param: param, model: MyProfileData.self){ model, _ in
            completion(model)
        }
    }
    
    //프로필 > 하단 포스터 조회
    static func getSomeonePost(userId:Int, offset:Int, completion:@escaping (PostsData?)->Void) {
        let param:Parameters = [
            "userId" : userId,
            "offset" : offset
        ]
        
        ApiManager.request("/post/getSomeonePost", method: .get, param: param, model: PostsData.self){ model, _ in
            completion(model)
        }
    }
//MARK: - 꾸밍 챌린지
    //리스트
    static func getEventsList(type:BannerPageType, periodType:PeriodType, offset:Int = 1, completion:@escaping (EventsData?)->Void) {
        let parameters:Parameters = [
            "offset" : offset,
            "periodType" : periodType.rawValue,
            "type" : type.rawValue,
            "limit" : 15
        ]
        
        ApiManager.request("/events/getEventsList", method: .get, param: parameters, model: EventsData.self){ model, _ in
            completion(model)
        }
    }
    
    //꾸밍 상세보기
    static func eventsDetail(id:Int, type:BannerPageType, offset:Int = 1, completion:@escaping (EventDeatilData?)->Void) {
        let parameters:Parameters = [
            "id" : id,
            "offset" : offset,
            "type" : type.rawValue,
            "limit" : 15
        ]
        
        ApiManager.request("/events/eventsDetail", method: .get, param: parameters, model: EventDeatilData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 좋아요/해제, 팔로잉/언팔로잉, 스크랩/해제
    //좋아요
    static func likeReact(postId:Int, completion:@escaping (LikeData?)->Void) {
        let parameters:Parameters = [
            "postId" : postId
        ]
        
        ApiManager.request("/like/react", method: .post, param: parameters, model: LikeData.self){ model, _ in
            completion(model)
        }
    }
    
    //좋아요 해제
    static func unLikeSelect(postIdList:[Int], completion:@escaping (UnLikeData?)->Void) {
        let parameters:Parameters = [
            "postIdList" : postIdList
        ]
        
        ApiManager.request("/like/react/unLikeSelect", method: .put, param: parameters, encoding: JSONEncoding.default, model: UnLikeData.self){ model, _ in
            completion(model)
        }
    }
    
    static func followReact(followingId:Int, completion:@escaping (FollowingData?, Data?)->Void) {
        let parameters:Parameters = [
            "followingId" : followingId
        ]
        
        ApiManager.request("/follow/followReact", method: .post, param: parameters, model: FollowingData.self){ model, data in
            completion(model, data)
        }
    }
    
    //스크랩 추가
    static func scrapReact(postId:Int, completion:@escaping (ScrapData?)->Void) {
        let parameters:Parameters = [
            "postId" : postId
        ]
        
        ApiManager.request("/scrap/react", method: .post, param: parameters, model: ScrapData.self){ model, _ in
            completion(model)
        }
    }
    
    //스크랩 해제
    static func unScrapSelect(postIdList:[Int], completion:@escaping (UnScrapData?)->Void) {
        let parameters:Parameters = [
            "postIdList" : postIdList
        ]
        
        ApiManager.request("/scrap/react/unScrapSelect", method: .put, param: parameters, encoding: JSONEncoding.default, model: UnScrapData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 신고
    //신고 리스트 조회 : /reportCategory/getListReportCategory
    static func getListReportCategory(completion:@escaping (ReportListData?)->Void) {
        ApiManager.request("/reportCategory/getListReportCategory", method: .get, encoding: URLEncoding.default, model: ReportListData.self){ model, _ in
            completion(model)
        }
    }
    
    //댓글 신고하기 : /user/reportComment
    static func reportComment(commentId:Int, reportCategoryId:Int, completion:@escaping (ReportCommentData?)->Void) {
        let parameters:Parameters = [
            "commentId" : commentId,
            "reportCategoryId" : reportCategoryId
        ]
        
        ApiManager.request("/user/reportComment", method: .post, param: parameters, encoding: URLEncoding.default, model: ReportCommentData.self){ model, _ in
            completion(model)
        }
    }
    
    //게시글 신고하기 : /user/reportPost
    static func reportPost(postId:Int, reportCategoryId:Int, completion:@escaping (ReportCommentData?)->Void) {
        let parameters:Parameters = [
            "postId" : postId,
            "reportCategoryId" : reportCategoryId
        ]
        
        ApiManager.request("/user/reportPost", method: .post, param: parameters, encoding: URLEncoding.default, model: ReportCommentData.self){ model, _ in
            completion(model)
        }
    }
    
//MARK: - 알림
    static func getAlarmList(page:Int, completion:@escaping (AlarmDatas?)->Void) {
        let param:Parameters = [
            "offset" : page
        ]
        ApiManager.request("/notification/getList", method: .get, param: param, encoding: URLEncoding.default, model: AlarmDatas.self){ model, _ in
            completion(model)
        }
    }
    
    //읽기
    static func getAlarmDetail(id:Int, completion:@escaping (AlarmReadData?)->Void) {
        let param:Parameters = [
            "id" : id
        ]
        
        ApiManager.request("/notification/getDetail", method: .get, param: param, encoding: URLEncoding.default, model: AlarmReadData.self){ model, _ in
            completion(model)
        }
    }
    
    //삭제
    static func deleteAlarm(id:Int, completion:@escaping (AlarmDeleteData?)->Void) {
        let param:Parameters = [
            "id" : id
        ]
        
        ApiManager.request("/notification/deleteNoti", method: .delete, param: param, encoding: URLEncoding.default, model: AlarmDeleteData.self){ model, _ in
            completion(model)
        }
    }
    
    //전체 삭제
    static func deleteAllAlarm(completion:@escaping (AlarmDeleteData?)->Void) {
        ApiManager.request("/notification/deleteAllNoti", method: .delete, encoding: URLEncoding.default, model: AlarmDeleteData.self){ model, _ in
            completion(model)
        }
    }
//MARK: - 설정
    //앱 버전 조회:
    static func getAppVersions(version: String, completion:@escaping (AppVersionDatas?)->Void) {
        let parameters: Parameters = [
            "os" : "I",
            "version": version
        ]
           ApiManager.request("/main/appVersionCheck", method: .get, param: parameters, encoding: URLEncoding.default, model: AppVersionDatas.self){ model, _ in
            completion(model)
        }
    }
    //앱 설정 조회 :
    static func getAppSettings(completion:@escaping (SettingDatas?)->Void) {
           ApiManager.request("/appSettings/getAppSettings", method: .get, encoding: URLEncoding.default, model: SettingDatas.self){ model, _ in
            completion(model)
        }
    }
    
    //푸시 설정 조회 : /user/settings/getStatus
    static func getAppPushStatus(completion:@escaping (AppSettingPushStatusData?)->Void) {
        ApiManager.request("/user/setting/getStatus", method: .get, model: AppSettingPushStatusData.self){ model, _ in
            completion(model)
        }
    }
    
    //푸시 설정 상태 변경
    static func updatePushStatus(bool:Bool, notiType:PushType, completion:@escaping (CommonData?)->Void) {
        let parameters:Parameters = [
            "action" : bool ? SwitchType.ON.rawValue : SwitchType.OFF.rawValue,
            "notiType" : notiType.rawValue
        ]
        
        ApiManager.request("/user/setting/pushNoti", method: .put, param: parameters, model: CommonData.self){ model, _ in
            completion(model)
        }
    }
    
    //공지사항 조회
    static func getListNotice(offset:Int = 1, completion:@escaping (NoticeData?)->Void) {
        let parameters:Parameters = [
            "limit" : 10,
            "offset" : offset,
            "order" : "DESC"
        ]
        
        ApiManager.request("/notice/getListNotice", method: .get, param: parameters, model: NoticeData.self){ model, _ in
            completion(model)
        }
    }
    
    //차단 조회 : /block/getListUserBlocked
    static func getListUserBlocked(page:Int = 1, completion:@escaping (BlockUserListData?)->Void) {
        let param:Parameters = [
            "offset" : page
        ]
        
        ApiManager.request("/user/block/getListUserBlocked", method: .get, param: param, model: BlockUserListData.self){ model, _ in
            completion(model)
        }
    }
    
    //차단 : /user/block/blockUser
    static func blockUser(userId:Int, completion:@escaping (BlockUserData?, Data?)->Void) {
        let parameters:Parameters = [
            "userId" : userId
        ]
        
        ApiManager.request("/user/block/blockUser", method: .post, param: parameters, model: BlockUserData.self){ model, data in
            completion(model, data)
        }
    }
    
    //차단해제 : user/block/unblockUser
    static func unblockUser(userId:Int, completion:@escaping (BlockUserData?)->Void) {
        let parameters:Parameters = [
            "userId" : userId
        ]
        
        ApiManager.request("/user/block/unblockUser", method: .put, param: parameters, model: BlockUserData.self){ model, _ in
            completion(model)
        }
    }
    
    //문의하기
    static func inquiry(email:String, type:InquiryType,questionContent:String, completion:@escaping (InquiryDatas?)->Void) {
        let param:Parameters = [
            "email" : email,
            "type" : type.rawValue,
            "questionContent" : questionContent
        ]
        
        ApiManager.request("/user/inquiry", method: .post, param: param, encoding: URLEncoding.default, model: InquiryDatas.self){ model, _ in
            completion(model)
        }
    }
    
    //회원탈퇴
    static func quitUser(completion:@escaping (CommonData?)->Void) {
        ApiManager.request("/user/quit", method: .put, encoding: URLEncoding.default, model: CommonData.self){ model, _ in
            completion(model)
        }
    }
}
