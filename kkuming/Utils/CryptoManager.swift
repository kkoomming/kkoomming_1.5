import UIKit
import Foundation

public class CryptoManager: NSObject{
    static let shared = CryptoManager()
    
    public func encrypt(text:String) -> String{
        //1. 선언문
        let password = text
        let passphrase = "A5qPuUgojz9Q/0GnOfpECLlwtgySPjIJmUyXRzMrRXdlsSGa478FL0zvw7U0Pdz9yAWch38JuyriBQMJ2RKxDw=="
        let saltBytes:[Int8] = [1,2,3,4,5,6,7,8]
        let passwordBytes = [UInt8](password.utf8)
        
        //EvpKDF 계산
        let items = EvpKDF(passphrase: passphrase, saltBytes: saltBytes)
        let aes = try! AES(key: items.0, blockMode: CBC(iv: items.1), padding: .pkcs5)
        let encrypted = try! aes.encrypt(passwordBytes)
        
        return calcResult(encrypted: encrypted, saltBytes: saltBytes)
    }
    
    private func EvpKDF(passphrase:String, saltBytes:[Int8]) -> ([UInt8], [UInt8]) {
        var keySize = 256
        var ivSize = 128
        var key:[Int8] = []
        var iv:[Int8] = []
        
        keySize = keySize / 32
        ivSize = ivSize / 32
        
        let targetKeySize = keySize + ivSize;
        var numberOfDerivedWords = 0
        var derivedBytes:[Int8] = []
        var block:[Int8] = []
        
        while (numberOfDerivedWords < targetKeySize) {
            let temp:[Int8] = passphrase.bytes.map { Int8(bitPattern: $0)}

            block += temp + saltBytes
            
            let test = block.map { UInt8(bitPattern: $0)}.md5().map { Int8(bitPattern: $0)}
            
            block = test
            derivedBytes += block
            
            numberOfDerivedWords = numberOfDerivedWords + test.count / 4
        }
        
        for index in 0..<keySize * 4 {
            key.append(derivedBytes[index])
        }
        
        for index in keySize * 4..<keySize * 4 + ivSize * 4 {
            iv.append(derivedBytes[index])
        }
        
        return (key.map { UInt8(bitPattern: $0)}, iv.map { UInt8(bitPattern: $0)})
    }
    
    private func calcResult(encrypted:[UInt8], saltBytes:[Int8]) -> String {
        let firstSalt:[UInt8] = [UInt8]("Salted__".utf8)
        let secondSalt = saltBytes.map{ UInt8(bitPattern: $0)}
        var resultBytes:[UInt8] = []
        
        resultBytes += firstSalt
        resultBytes += secondSalt
        resultBytes += encrypted
        
        return resultBytes.toBase64()
    }
}
