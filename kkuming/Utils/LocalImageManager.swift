//
//  LocalImageManager.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/14.
//

import UIKit
import Photos

final class LocalImageManager {
    
    static var shared = LocalImageManager()
    
    fileprivate let imageManager = PHImageManager.default()
    
    var representedAssetIdentifier: String?
    
    //완료
    func requestImage(with asset: PHAsset?, thumbnailSize: CGSize, completion: @escaping (GalleryData?) -> Void) {
        guard let asset = asset else {
            completion(nil)
            return
        }

        self.representedAssetIdentifier = asset.localIdentifier

        let requestImageOption = PHImageRequestOptions()
        requestImageOption.deliveryMode = .highQualityFormat
        requestImageOption.isSynchronous = true

        //iCloud 사진 사용할거면 주석 해제
        requestImageOption.isNetworkAccessAllowed = true

        let targetSize = CGSize(width: asset.pixelWidth, height: asset.pixelHeight)
        self.imageManager.requestImage(for: asset,
                                       targetSize: targetSize,
                                       contentMode: .aspectFit,
                                       options: requestImageOption,
                                       resultHandler: { image, info in
            if self.representedAssetIdentifier == asset.localIdentifier {
                let data = GalleryData(identifier: self.representedAssetIdentifier, image: image, asset: asset)

                completion(data)
            }
        })
    }
    
    @discardableResult
    func imageAsset(asset: PHAsset, size: CGSize = CGSize(width: 160, height: 160), options: PHImageRequestOptions? = nil, completionBlock:@escaping (GalleryData?, UIImage,Bool)-> Void ) -> (PHImageRequestID) {
        var options = options
        if options == nil {
            options = PHImageRequestOptions()
            options?.isSynchronous = false
            options?.resizeMode = .exact
            options?.deliveryMode = .opportunistic
            options?.isNetworkAccessAllowed = true
        }
        let scale = min(UIScreen.main.scale,2)
        let targetSize = CGSize(width: size.width*scale, height: size.height*scale)
        
        var data:GalleryData? = nil
        self.representedAssetIdentifier = asset.localIdentifier
        
        let requestID = self.imageManager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFill, options: options) { image, info in
            let complete = (info?["PHImageResultIsDegradedKey"] as? Bool) == false
            data = GalleryData(identifier: self.representedAssetIdentifier, image: image, asset: asset)
            
            if let image = image {
                completionBlock(data, image,complete)
            }
        }
        
        return requestID
    }
    
    //편집 전용
    func requestImage(with asset: PHAsset?, completion: @escaping (GalleryData?) -> Void) {
        guard let asset = asset else {
            completion(nil)
            return
        }
        
        self.representedAssetIdentifier = asset.localIdentifier
        
        let requestImageOption = PHImageRequestOptions()
        requestImageOption.deliveryMode =  .highQualityFormat
        requestImageOption.version = .current
        requestImageOption.resizeMode = .exact
        requestImageOption.isSynchronous = true
        
        print("asset.pixelWidth : ", asset.pixelWidth)
        print("asset.pixelHeight : ", asset.pixelHeight)

        self.imageManager.requestImage(for: asset,
                                       targetSize: CGSize(width: asset.pixelWidth, height: asset.pixelHeight),
                                       contentMode: .aspectFill,
                                       options: requestImageOption,
                                       resultHandler: { image, info in
            if self.representedAssetIdentifier == asset.localIdentifier {
                let data = GalleryData(identifier: self.representedAssetIdentifier, image: image, asset: asset)
                
                completion(data)
            }
        })
    }
}
