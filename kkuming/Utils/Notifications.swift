//
//  Notifications.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/27.
//

import UIKit

extension Notification.Name {
    static let showHome = Notification.Name("showHome")
    static let showFeed = Notification.Name("showFeed")
    static let showFeedPopular = Notification.Name("showFeedPopular")
    static let showFeedFollowing = Notification.Name("showFeedFollowing")
    static let showMypage = Notification.Name("showMypage")
    static let requestAlbum = Notification.Name("requestAlbum")
    static let updateSelectedData = Notification.Name("updateSelectedData")
    
    //게시글 상세 새로고침
    static let refreshPostDetail = Notification.Name("refreshPostDetail")
    
    //피드(최신) 새로고침
    static let refreshFeedRecent = Notification.Name("refreshFeedRecent")
}
