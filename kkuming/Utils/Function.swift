import UIKit
import SnapKit
import Then

//폰트
func getFont(size:CGFloat, type:FontType = .regular) -> UIFont{
    let font:UIFont!
    
    switch type {
    case .light:
        font = UIFont(name: "NanumSquareRoundOTFL", size: size)
    case .regular:
        font = UIFont(name: "NanumSquareRoundOTFR", size: size)
    case .bold:
        font = UIFont(name: "NanumSquareRoundOTFB", size: size)
    case .EBold:
        font = UIFont(name: "NanumSquareRoundOTFEB", size: size)
    }
    
    return font
}

//Label 사이즈
func getLabelSize(text:String, font:UIFont) -> CGSize {
    let label = UILabel().then{
        $0.text = text
        $0.font = font
    }
    label.sizeToFit()
    
    return label.frame.size
}

//컬러 색상
func RGB(red:CGFloat, green:CGFloat, blue:CGFloat, alpha:CGFloat = 1.0) -> UIColor {
    return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
}

func createSpacingView(height:CGFloat = 10) -> UIView {
    let view = UIView().then{
        $0.snp.makeConstraints{
            $0.height.equalTo(height)
        }
    }
    
    return view
}

func dateToFormat(string:String, dateFormat:DateFormatType) -> String {
    let dateFormatter = DateFormatter()
    //2022-06-29T08:51:44.000Z
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.000Z"
    let originalDate = dateFormatter.date(from: string)
    
    let convertDateFormatter = DateFormatter()
    convertDateFormatter.dateFormat = dateFormat.rawValue
    
    if let date = originalDate {
        return convertDateFormatter.string(from: date)
    } else {
        return ""
    }
}

func openSafari(url urlString:String) {
    if let url = URL(string: urlString) {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}
