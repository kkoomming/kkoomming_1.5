//
//  TestFactory.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/20.
//

import Foundation

enum TestType {
    case User
    case Post
}

protocol TestCreator {
    static func createModel<T:Codable>(type:T.Type) -> T?
    static func createModelList<T:Codable>(type:T.Type) -> [T]
}

class TestFactory: TestCreator {
    static func createModel<T:Codable>(type:T.Type) -> T? {
        switch type {
        case is User.Type:
            return self.createUser() as! T
        case is Post.Type:
            return self.createPostList() as! T
        case is EventData.Type:
            return self.createEventData() as! T
        default:
            break
        }
        
        return nil
    }
    
    static func createModelList<T:Codable>(type:T.Type) -> [T] {        
        switch type {
        case is User.Type:
            return createUserList() as! [T]
        case is Post.Type:
            return createPostList() as! [T]
        default:
            break
        }
        
        return []
    }
    
//MARK: - User
    private static func createUser() -> Codable? {
        let testString = """
{
    "id": 19,
    "position": null,
    "accountType": "NORMAL",
    "nickname": "꾸밍",
    "email": null,
    "role": "USER",
    "totalFollower": 15,
    "totalFollowing": 1,
    "totalReported": 0,
    "totalMyPost": 1,
    "linkShare": "https://www.instagram.com/kkoomming_official",
    "bio": "꾸밍 운영자",
    "isOnline": true,
    "status": "ACTIVE",
    "avatar": "https://dftgic7kmxqhe.cloudfront.net/PROFILE/41ee90c6-50aa-41e8-aec0-ec440def9716.jpg",
    "userType": "NORMAL",
    "totalAccessApp": 159,
    "lastDateAccessApp": "2021-12-23T00:47:56.000Z",
    "lastDateSignin": "2022-01-19T07:52:17.000Z",
    "createdAt": "2021-10-15T07:02:56.000Z",
    "isFollowed": false
}
"""
        do{
            let decoder = JSONDecoder()
            let data = testString.data(using: .utf8)!
            let model = try decoder.decode(User.self, from: data)
            
            return model
        }catch{
            print("createExample error")
        }
        
        return nil
    }
    
    private static func createUserList(count:Int = 20) -> [Codable] {
        return Array(repeating: createUser()!, count: count)
    }

//MARK: - Post
    private static func createPost() -> Post{
        let testString = """
{
    "id": 38891,
    "description": "Test",
    "createdBy": 4246,
    "thumbnail": 223936,
    "totalLikes": 2,
    "totalComments": 14,
    "totalViews": 3,
    "totalScraps": 2,
    "totalPopularTags": 6,
    "numOrder": 0,
    "postType": "NORMAL",
    "postInfoId": 1,
    "createdAt": "2022-07-19T05:29:46.000Z",
    "owner": {
        "id": 4246,
        "accountType": "GOOGLE",
        "nickname": "km15496621",
        "memberID": "km15496621",
        "role": "USER",
        "totalFollower": 1,
        "totalFollowing": 0,
        "linkShare": null,
        "bio": null,
        "isOnline": true,
        "avatar": "https://d16sraug4hsad1.cloudfront.net/PROFILE/avatarDefault.png"
    },
    "thumbnailImage": "https://d16sraug4hsad1.cloudfront.net/POST/c766e610-cfb0-48e6-895a-95976dd309ff.png",
    "isComment": false,
    "isLike": false
}
"""
        do{
            let decoder = JSONDecoder()
            let data = testString.data(using: .utf8)!
            let model = try decoder.decode(Post.self, from: data)
            
            return model
        }catch{
            print("createExample error")
        }
        
        return Post(id: 0)
    }
    
    private static func createPostList(count:Int = 20) -> [Codable] {
        return Array(repeating: createPost(), count: count)
    }
    
    private static func createEventData() -> EventData?{
        let testString = """
{
"id":4,
"title":"두근두근 새학기 챌린지",
"type":"CHALLENGE",
"createdBy":1,
"status":"ACTIVE",
"startAt":"2021-06-24T23:00:00.000Z",
"endAt":"2023-04-03T23:00:00.000Z",
"linkUrl":"https://www.naver.com",
"contentLinkType":"URL",
"imageUrlId":224292,
"imageUrlId2":224291,
"numOrder":0,
"primaryTag":"#꾸밍챌린지",
"createdAt":"2022-06-21T06:58:50.000Z",
"updatedAt":"2022-07-29T06:36:25.000Z",
"manage_tags":[],
"files":{
"id":224292,
"createdBy":1,
"postId":null,
"type":"image/png",
"bucket":"kkooming-dev",
"url":"https://d16sraug4hsad1.cloudfront.net/SLIDE/773e8dbb-1709-4a63-89a7-f77ad628c583.png",
"key":"SLIDE/773e8dbb-1709-4a63-89a7-f77ad628c583.png",
"filename":"홈_챌린지배너_2번.png",
"size":44149,
"status":"ACTIVE",
"createdAt":"2022-07-29T06:36:24.000Z",
"updatedAt":"2022-07-29T06:36:25.000Z"
},
"files2":{
"id":224292,
"createdBy":1,
"postId":null,
"type":"image/png",
"bucket":"kkooming-dev",
"url":"https://d16sraug4hsad1.cloudfront.net/SLIDE/773e8dbb-1709-4a63-89a7-f77ad628c583.png",
"key":"SLIDE/773e8dbb-1709-4a63-89a7-f77ad628c583.png",
"filename":"홈_챌린지배너_2번.png",
"size":44149,
"status":"ACTIVE",
"createdAt":"2022-07-29T06:36:24.000Z",
"updatedAt":"2022-07-29T06:36:25.000Z"
}
}
"""
        do{
            let decoder = JSONDecoder()
            let data = testString.data(using: .utf8)!
            let model = try decoder.decode(EventData.self, from: data)
            
            return model
        }catch{
            print("createExample error")
        }
        
        return nil
    }
}
