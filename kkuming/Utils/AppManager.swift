//
//  AppManager.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/15.
//

import UIKit
import WebKit
import FirebaseMessaging
import Promises
import NaverThirdPartyLogin

public class AppManager: NSObject, UIGestureRecognizerDelegate {
    static let shared = AppManager()
    let processPool: WKProcessPool = WKProcessPool()
    
    private var navigation:UINavigationController!
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    //메인 진입했다면 true
    var isRunMain:Bool = false
    
    //버튼 이벤트 중복 방지
    var isPopRunning:Bool = false
    
    //2뎁스 전용
    private var index:Int = 0
    private var twoDepthNaviList:[UINavigationController]!
    private var twoDepthNavi:UINavigationController!
    private var isUseTwoDepth:Bool = false
    
    private var feedVC:FeedCommonVC!
    private var tabBarType:TabBarType = .home
    
    private var depthList:[Bool] = []
    
    public func setRoot(_ vc:UIViewController) {
        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            window.rootViewController = vc
        }
    }
    
    public func configNavigation(_ navigation:UINavigationController) {
        self.navigation = navigation
    }
    
    public func setNavi(_ vc:UIViewController, animated:Bool = true){
        self.navigation.setViewControllers([vc], animated: animated)
    }
    
    public func getNavi() -> UINavigationController {
        return navigation
    }
    
    public func push(_ vc:UIViewController, animated:Bool = false){
        depthList.append(false)
        
        DispatchQueue.main.async {
            self.navigation.pushViewController(vc, animated: animated)
        }
    }
    
    func push(type:ViewControllerType, animated:Bool = false) {
        depthList.append(false)
        
        var vc:UIViewController!
        
        switch type {
        case .search:
            vc = SearchVC()
        case .alert:
            vc = AlarmVC()
        case .setting:
            vc = SettingVC()
        }
        
        self.navigation.pushViewController(vc, animated: animated)
    }
    
    public func popToRoot() {
        self.navigation.popToRootViewController(animated: false)
    }
    
    public func pop(animated:Bool = false, completion:(()->Void)? = nil){
        guard !isPopRunning else {
            print("버튼 중복 클릭")
            return
        }
        isPopRunning = true
        
        var isDepth = false
        if !depthList.isEmpty {
            isDepth = depthList.last!
        }
        
        if twoDepthNavi != nil {
            if self.twoDepthNavi == twoDepthNaviList[index] {
                if twoDepthNavi.viewControllers.count >= 2 && isDepth {
                    self.twoDepthNavi.popViewController(animated: animated)
                } else {
                    self.navigation.popViewController(animated: animated)
                }
            }
        } else {
            self.navigation.popViewController(animated: animated)
        }
        
        if depthList.count != 0 {
            self.depthList.removeLast()
        }
        
        isPopRunning = false
        completion?()
    }
    
    public func pushToDepth(_ vc:UIViewController,
                            type tabBarType:TabBarType,
                            animated:Bool = false){
        depthList.append(true)
        
        print("index : ", index)
        self.setIndex(index: index)
        self.twoDepthNavi.pushViewController(vc, animated: animated)
    }
    
    public func getWindow() -> UIWindow {
        if let window = delegate.window {
            return window
        }
        
        return UIWindow()
    }
    
    public func getWindowVCLast() -> UIViewController? {
        if let rootViewController = App.getWindow().rootViewController {
            let naviController = rootViewController as! UINavigationController
            let viewController = naviController.viewControllers.last
            
            return viewController
        }
        
        return nil
    }
    
    public func getSafeAreaInsets() -> UIEdgeInsets {
        if let safeAreaInsets = delegate.window?.safeAreaInsets {
            return safeAreaInsets
        }
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    public func getTabController() -> MainTabBarController {
        for item in self.navigation.viewControllers {
            if item is UITabBarController {
                let vc = item as! MainTabBarController
                return vc
            }
        }
        
        return MainTabBarController()
    }
    
    public func tabReset() {
        for item in self.navigation.viewControllers {
            if item is UITabBarController {
                let vc = item as! UITabBarController
                vc.selectedIndex = 0
            }
        }
    }
    
    public func moveToTab(index:Int) {
        DispatchQueue.main.async {
            for item in self.navigation.viewControllers {
                if item is UITabBarController {
                    let vc = item as! UITabBarController
                    vc.selectedIndex = index
                }
            }
        }
    }
    
    func setTwoDepthNavi(_ vcList:[UINavigationController]) {
        self.twoDepthNaviList = vcList
    }
    
    func setFeedVC(_ vc:FeedCommonVC) {
        self.feedVC = vc
        self.feedVC.loadViewIfNeeded()
        self.feedVC.view.layoutIfNeeded()
    }
    
    func getFeedVC() -> FeedCommonVC {
        if feedVC == nil {
            feedVC = FeedCommonVC()
            feedVC.loadViewIfNeeded()
            feedVC.view.layoutIfNeeded()
        }
        return self.feedVC
    }
    
    func showWebView(title:String = "", url:String) {
        let vc = WebVC(title: title, url: url)
        App.push(vc)
    }
    
    //앱 설정 이동
    public func pushSetting() {
        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }

        if UIApplication.shared.canOpenURL(url) {
            DispatchQueue.main.async {
                UIApplication.shared.open(url)
            }
        }
    }
}

//특정 ViewController 이동
extension AppManager {
    func setIndex(index:Int) {
        self.twoDepthNavi = twoDepthNaviList[index]
        self.index = index
    }
    
    func push(tabType:TabBarType) {
        switch tabType {
        case .home:
            NotificationCenter.default.post(name: .showHome, object: nil)
            let tab = getTabController()
            tab.selectedIndex = 0
            break
        case .feed:
            getFeedVC().type = .feed
            let tab = getTabController()
            tab.selectedIndex = 1
            break
        case .feedPopular:
            getFeedVC().type = .feedPopular
            let tab = getTabController()
            tab.selectedIndex = 1
            break
        case .feedFollowing:
            getFeedVC().type = .feedFollowing
            let tab = getTabController()
            tab.selectedIndex = 3
            break
        case .write:
            print("write 구현 안됨")
            break
        case .mypage:
            NotificationCenter.default.post(name: .showMypage, object: nil)
            let tab = getTabController()
            tab.selectedIndex = 4
            break
        }
    }
}

extension AppManager {
    func login(clientId:String, loginMethod:LoginType) {
        //FCM 토큰
        var deviceToken = ""

        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
              deviceToken = token
              
              let timestamp = Int64(NSDate().timeIntervalSince1970)
              let tokenParam: [String : Any] = [
                  "timestamp" : timestamp,
                  "clientId" : clientId,
                  "loginMethod" : loginMethod.rawValue
              ]
              let json = tokenParam.toJsonString().unsafelyUnwrapped
              let dataHash = CryptoManager.shared.encrypt(text: json)
              print("dataHash : ", dataHash)
              
              let param = [
                  "deviceToken" : deviceToken,
                  "dataHash" : dataHash,
                  "language" : "KR"
              ]
              
              print("param : ", param)
              
              API.login(param: param) { response in
                  guard let model = response, let data = model.data else {return}
                  print("로그인 완료")
                  self.saveAutoLogin(clientId: clientId, loginMethod: loginMethod)
                  UserManager.shared.set(userData: data)
                  
                  API.getAlarmList(page: 1) { model in
                      if let model = model, let notifications = model.data?.notifications {
                          var checkRead = false
                          
                          for item in notifications {
                              if item.status == AlarmStatus.UNSEEN.rawValue {
                                  checkRead = true
                                  break
                              }
                          }
                          
                          AlertManager.shared.status = checkRead
                      }
                      
                      App.setNavi(MainTabBarController())
                  }
              }
          }
        }
    }
    
    func logout() {
        UserDefaultsUtil.clear()
        NaverThirdPartyLoginConnection.getSharedInstance().requestDeleteToken()
        App.setNavi(LoginVC())
    }
    
    //자동 로그인 저장
    private func saveAutoLogin(clientId:String, loginMethod:LoginType) {
        //clientId, loginMethod
        let userInfo = UserInfo(clientId: clientId, loginMethod: loginMethod.rawValue)
        do {
            let encoder = JSONEncoder()
            let data = try encoder.encode(userInfo)
            if let json = String(data: data, encoding: .utf8) {
                print("json : ", json)
                UserDefaultsUtil.save(value: json, key: .userInfo)
            }
        } catch {
            print("error saveAutoLogin")
        }
    }
}
