//
//  AppVersion.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/21.
//

import Foundation

class AppVersion {
    static func getVersion() -> String {
        if let info = Bundle.main.infoDictionary, let currentVersion = info["CFBundleShortVersionString"] as? String {
            return currentVersion
        } else {
            return "버전 정보 조회 실패"
        }
    }
}
