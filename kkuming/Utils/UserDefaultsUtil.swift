import UIKit

enum UserDefaultsType:String {
    case array = "array"
}

enum UserDefaultsKey:String {
    case userInfo = "userInfo"
    case userRecentSearchKeyword = "userRecentSearchKeyword" //최근 검색어
    case mainPopupDate = "mainPopupDate" //공지사항 팝업
}

class UserDefaultsUtil {
    //UserDefaultsKey가 추가될 때마다 추가 필요
    static func clear() {
        UserDefaultsUtil.save(value: "", key: .userInfo)
        UserDefaultsUtil.save(value: [], key: .userRecentSearchKeyword)
        UserDefaultsUtil.save(value: "", key: .mainPopupDate)
    }
    
    static func save(value:Any, key: UserDefaultsKey) {
        UserDefaults.standard.set(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func readUserInfo() -> String {
        if let info = UserDefaults.standard.string(forKey: UserDefaultsKey.userInfo.rawValue) {
            return info
        } else {
            return ""
        }
    }
    
    static func readUserRecentSearchKeyword() -> [String]{
        let list = UserDefaults.standard.array(forKey: UserDefaultsKey.userRecentSearchKeyword.rawValue)
        if list == nil {
            return []
        }
        
        return UserDefaults.standard.array(forKey: UserDefaultsKey.userRecentSearchKeyword.rawValue) as! [String]
    }
    
    static func readMainPopupDate() -> String {
        if let info = UserDefaults.standard.string(forKey: UserDefaultsKey.mainPopupDate.rawValue) {
            return info
        } else {
            return ""
        }
    }
}
