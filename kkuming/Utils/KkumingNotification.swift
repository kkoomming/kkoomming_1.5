//
//  KkumingKeyboardManager.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/09.
//

import UIKit
import IQKeyboardManagerSwift

class KkumingKeyboardManager: NSObject {
    static let shared = KkumingKeyboardManager()
    //전체 뷰(endEditing, safeArea bottom 값 구하기에 사용)
    private var parentView:UIView? = nil
    //키보드 뷰
    private var keyboardView:UIView? = nil
    //스크롤 뷰
    private var scrollView:UIScrollView? = nil
    
    private var isObserverRegisted:Dictionary<String, Bool> = [:]
    
    //MARK: 사용 후 반드시 false로 선택 필수
    //상세화면과 댓글쓰기 화면에서 문제있어서 사용 할 때에만 true로 사용
    public func isUseKeyboardLibrary(_ bool:Bool) {
        IQKeyboardManager.shared.enable = bool
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    public func setKeyboardNotification(parentView:UIView, keyboardView:UIView, scrollView:UIScrollView) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.parentView = parentView
        self.keyboardView = keyboardView
        self.scrollView = scrollView
        
        addGestureEndEditingTo(scrollView: scrollView)
    }
    
    public func removeKeyboardNotification() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        clearView()
    }
    
    public func setKeyboardNotification(parentView:UIView, keyboardView:UIView, scrollView:UIScrollView, registKey:String) {
        if(!self.isObserverRegisted(registKey: registKey)) {
            self.setKeyboardNotification(parentView:parentView, keyboardView:keyboardView, scrollView:scrollView)
            
            self.isObserverRegisted(registKey: registKey, toStat: true)
        }
    }
    
    public func removeKeyboardNotification(registKey:String) {
        if(self.isObserverRegisted(registKey: registKey)) {
            self.removeKeyboardNotification()
            
            self.isObserverRegisted(registKey: registKey, toStat: false)
        }
    }
    
    @objc
    private func keyboardWillShowNotification(_ notification:NSNotification) {
        print("keyboardWillShowNotification")
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
        let parentView = parentView, let keyboardView = keyboardView, let scrollView = scrollView
        {
            let height = keyboardSize.height - parentView.safeAreaInsets.bottom
            UIView.animate(withDuration: 0.3, animations: {
                keyboardView.transform = CGAffineTransform(translationX: 0, y: -height)
                scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: height, right: 0)
            })
        }
    }
    
    @objc
    private func keyboardWillHideNotification(_ notification:NSNotification) {
        print("keyboardWillHideNotification")
        if let keyboardView = keyboardView, let scrollView = scrollView {
            keyboardView.transform = .identity
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    private func addGestureEndEditingTo(scrollView:UIScrollView) {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        gesture.numberOfTapsRequired = 1
        gesture.isEnabled = true
        gesture.cancelsTouchesInView = false
        scrollView.addGestureRecognizer(gesture)
    }
    
    private func clearView() {
        self.parentView = nil
        self.keyboardView = nil
        self.scrollView = nil
    }
    
    private func isObserverRegisted(registKey:String, toStat:Bool) {
        isObserverRegisted[registKey] = toStat
    }
    
    private func isObserverRegisted(registKey:String) -> Bool {
        return isObserverRegisted[registKey] ?? false
    }

    @objc
    func endEditing() {
        if let parentView = parentView {
            parentView.endEditing(true)
        }
    }
}
