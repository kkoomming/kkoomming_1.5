import UIKit

extension UIViewController {
    func addSubViews(_ views:[UIView]) {
        views.forEach{ self.view.addSubview($0) }
    }
    
    func pushViewController(_ vc:UIViewController, animated:Bool = false, completion:(()->Void)? = nil) {
        if let navi = self.navigationController {
            navi.pushViewController(vc, animated: animated)
        } else {
            print("네비게이션 없음")
        }
    }
}
