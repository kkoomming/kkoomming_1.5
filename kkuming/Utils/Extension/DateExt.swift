//
//  DateExt.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/22.
//

import Foundation

extension Date {
    func dateFormat(to dateFormat:String = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        
        return formatter.string(from: self)
    }
}
