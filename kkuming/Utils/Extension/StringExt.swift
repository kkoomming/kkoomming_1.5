//
//  String+Extension.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/04.
//

import UIKit

extension String {
    var encodingQuery: String {
        get {
            if let string = self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                return string
            } else {
                print("encoding 안됨")
                return self
            }
        }
    }
    
    var toURL: URL? {
        get{
            if let url = URL(string: self.encodingQuery) {
                return url
            } else {
                return nil
            }
        }
    }
    
    //이모지 제외
    func isValidation() -> Bool{
        do {
            //@# 제외
            let regex = try NSRegularExpression(pattern: "^[0-9a-zA-Z가-힣ㄱ-ㅎㅏ-ㅣ`~!$%^&*()\\-_=+\\[{\\]}\\\\|;:'\",<.>/?\\s]$", options: .caseInsensitive)
            if let _ = regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, self.count)) {
                return true
            }
        }catch {
            return false
        }
        return false
    }
}
