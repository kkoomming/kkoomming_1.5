import UIKit

extension Int {
    var toKilo: String {
        let totalFollower = self ?? 0
        let dividedNumber = totalFollower / 1000
        var string = "(\(totalFollower))"
        
        if dividedNumber != 0 {
            string = "(\(dividedNumber)k)"
        }
        
        return string
    }
}
