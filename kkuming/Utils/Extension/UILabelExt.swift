//
//  UILabelExt.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/19.
//

import UIKit

extension UILabel {
    func bold(targetString: String) {
        let fontSize = self.font.pointSize
        let font = getFont(size: fontSize, type: .bold)
        let fullText = self.text ?? ""
        let range = (fullText as NSString).range(of: targetString)
        let attributedString = NSMutableAttributedString(string: fullText)
        attributedString.addAttribute(.font, value: font, range: range)
        attributedString.addAttribute(.foregroundColor, value: UIColor.black, range: range)
        self.attributedText = attributedString
    }
}
