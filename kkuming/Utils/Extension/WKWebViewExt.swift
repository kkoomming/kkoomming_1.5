import WebKit

extension WKWebView {
    func scheme() -> String? {
        return self.url?.scheme
    }

    func host() -> String? {
        return self.url?.host
    }

    func path() -> String? {
        return self.url?.path
    }
    
    func setCookie(key: String, value: String, expires: NSDate) -> Void {
        if let domain = self.host(), let path = self.path() {
            self.configuration.websiteDataStore.httpCookieStore.setCookie(
                HTTPCookie(properties: [
                    .domain: domain,
                    .path: path,
                    .name: key,
                    .value: value,
                    .secure: "TRUE",
                    .expires: expires
                ])!
            )
        }
    }
    
    func setCookie(key: String, value: String) -> Void {
        if let domain = self.host(), let path = self.path() {
            self.configuration.websiteDataStore.httpCookieStore.setCookie(
                HTTPCookie(properties: [
                    .domain: domain,
                    .path: path,
                    .name: key,
                    .value: value,
                    .secure: "TRUE"
                ])!
            )
        }
    }
}
