//
//  GalleryManager.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/15.
//

import UIKit
import AVFoundation
import Photos

class GalleryManager: NSObject {
    static let shared = GalleryManager()
    
    private var selectedDataList:[GalleryData?] = []
    private var finishCompletion:(()->Void)? = nil
    
    public func append(_ data:GalleryData?) {
        self.selectedDataList.append(data)
    }
    
    public func remove(_ index:Int){
        self.selectedDataList.remove(at: index)
    }
    
    public func setSelectedImages(_ selectedDataList:[GalleryData?]) {
        self.selectedDataList = selectedDataList
    }
    
    public func getSelectedImages() -> [GalleryData?] {
        return selectedDataList
    }
    
    public func clear() {
        self.selectedDataList.removeAll()
    }
    
    //https://stackoverflow.com/questions/67536507/swift-rotating-uiimage-and-filling-it
//    public func rotate(image:UIImage?, degree:CGFloat) -> UIImage? {
//        guard let image = image else {
//            return nil
//        }
//
//        guard let cgImage = image.cgImage else { return nil }
//        UIGraphicsBeginImageContextWithOptions(CGSize(width: 1000, height: image.size.height), false, UIScreen.main.scale)
//        guard let context = UIGraphicsGetCurrentContext() else { return nil }
//        defer { UIGraphicsEndImageContext() }
//        UIColor.white.setFill()
//        context.fill(.init(origin: .zero, size: image.size))
//        context.translateBy(x: image.size.width/2, y: image.size.height/2)
//        context.scaleBy(x: 1, y: -1)
//        context.rotate(by: -degree * .pi / 180)
//        context.draw(cgImage, in: CGRect(origin: .init(x: -image.size.width/2, y: -image.size.height/2), size: image.size))
//        return UIGraphicsGetImageFromCurrentImageContext()
//    }
    
    public func rotate(image:UIImage?) -> UIImage? {
        guard let image = image else { return nil }
        guard let cgImage = image.cgImage else { return nil }
        switch image.imageOrientation{
            case .right:
            return UIImage(cgImage: cgImage, scale: 1.0, orientation: .down)
            case .down:
            return UIImage(cgImage: cgImage, scale: 1.0, orientation: .left)
            case .left:
            return UIImage(cgImage: cgImage, scale: 1.0, orientation: .up)
            default:
            return UIImage(cgImage: cgImage, scale: 1.0, orientation: .right)
        }
    }
}

extension GalleryManager {
    //카메라 권한 얻기
    public func isRequestCamera() -> Bool{
        var bool = false
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .authorized:
                print("authorized")
                bool = true
                break
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { granted in
                    if granted {
                        print("notDetermined true")
                        bool = true
                    } else {
                        print("notDetermined false")
                        bool = false
                    }
                }
                break
            case .denied:
                print("denied")
                bool = false
                break
            case .restricted:
                print("restricted")
                bool = false
                break
        }
        
        if !bool {
            App.pushSetting()
        }
        
        return bool
    }
    
    //앨범 권한 얻기
    public func requestAlbum(){
        PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
            switch status {
                case .authorized:
                print("authorized")
                NotificationCenter.default.post(name: .requestAlbum, object: nil)
                break
                case .limited:
                    // The user authorized this app for limited Photos access.
                print("limited")
                break
                @unknown default:
                print("default")
                App.pushSetting()
                break
            }
        }
    }
    
    //카메라 실행
    public func startCamera(_ vc:UIViewController, finishCompletion:(()->Void)? = nil) {
        self.finishCompletion = finishCompletion
        
        let camera = UIImagePickerController()
        camera.sourceType = .camera
        camera.allowsEditing = true
        camera.cameraDevice = .rear
        camera.cameraCaptureMode = .photo
        camera.delegate = self
        vc.present(camera, animated: true)
    }
    
    public func saveLocal(image:UIImage) {
        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAsset(from: image)
        },completionHandler: {_,_ in
            let fetchOptions = PHFetchOptions()
            fetchOptions.sortDescriptors = [NSSortDescriptor(key:"creationDate", ascending: false)]
            let allMedia = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            let asset = allMedia.firstObject
            
            let data = GalleryData(identifier: asset?.localIdentifier,
                                   image: image, asset: asset)
            GalleryManager.shared.append(data)
            
            DispatchQueue.main.async { [weak self] in
                if self?.finishCompletion == nil {
                    NotificationCenter.default.post(name: .updateSelectedData, object: nil)
                } else {
                    self?.finishCompletion?()
                }
            }
        })
    }
}

extension GalleryManager: UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {

            self.saveLocal(image: image)            
        }

        picker.dismiss(animated: true, completion: nil)
    }
}
