import Alamofire
import UIKit
import Promises

class ApiManager {
    static func request<T:Codable>(_ url:String,
                        isRefresh:Bool = false,
                        method: HTTPMethod,
                        param parameters:Parameters = [:],
                        encoding:ParameterEncoding = URLEncoding.default,
                        model: T.Type,
                        completion:@escaping (T?, Data?) -> Void) {
        
        var headers:HTTPHeaders = [:]

        print("isRefresh : ", isRefresh)
        if isRefresh {
            headers.update(name: "Authorization", value: UserManager.shared.getRefreshToken())
        } else {
            headers.update(name: "Authorization", value: UserManager.shared.getAccessToken())
        }
        
        print("api url : ", "\(baseUrl)\(url)")
        print("headers : \(headers)")
        print("param : ", "\(parameters)")
        
        AlertManager.shared.show(isLoading: true)
        AF.request("\(baseUrl)\(url)", method: method, parameters: parameters, encoding: encoding, headers: headers).response { response in
            guard let data = response.data else {
                completion(nil, nil)
                AlertManager.shared.show(isLoading: false)
                return
            }
            
            if let data = String(data:data, encoding: .utf8) {
                print("\(url) data : ", data)
            }
            
            //정상
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(T.self, from: data)
                
                completion(model, data)
                AlertManager.shared.show(isLoading: false)
                return
            } catch(let error) {
                print("\(url) error : \(error)")
                if isRefresh {
                    AlertManager.shared.show(text: "로그인 정보가 만료되어 로그아웃 처리합니다.", type: .one) {
                        AppManager.shared.logout()
                    }
                    AlertManager.shared.show(isLoading: false)
                }
            }
            
            //토큰 갱신 처리
            do {
                let decoder = JSONDecoder()
                let error = try decoder.decode(ErrorData.self, from: data)
                
                //세션 만료
                //다른 디바이스로 로그인 시 세션 만료
                if error.data.code == Errors.e2003.rawValue || error.data.code == Errors.e0001.rawValue {
                    AlertManager.shared.show(isLoading: false)
                    AlertManager.shared.show(text: "로그인 정보가 만료되어 로그아웃 처리합니다.", type: .one) {
                        AppManager.shared.logout()
                    }
                    return
                }
                
                //토큰 재 발급
                if error.data.code == Errors.e2002.rawValue {
                    API.refreshToken() {
                        let headers:HTTPHeaders = ["Authorization" : UserManager.shared.getAccessToken()]
                        AF.request("\(baseUrl)\(url)", method: method, parameters: parameters, encoding: encoding, headers: headers).response { response in
                            guard let data = response.data else {
                                completion(nil, nil)
                                AlertManager.shared.show(isLoading: false)
                                return
                            }
                            
                            //정상
                            do {
                                let decoder = JSONDecoder()
                                let model = try decoder.decode(T.self, from: data)
                                
                                completion(model, nil)
                                AlertManager.shared.show(isLoading: false)
                            } catch(let error) {
                                print("\(url) refreshToken error : \(error)")
                                AlertManager.shared.show(isLoading: false)
                                AlertManager.shared.show(text: "서비스가 원할하지 않습니다.", type: .one)
                            }
                        }
                    }
                } else {
                    completion(nil, data)
                    AlertManager.shared.show(isLoading: false)
                }
                
            } catch(let error) {
                print("\(url) API error : \(error)")
                AlertManager.shared.show(text: "API 확인 요청", type: .one)
                AlertManager.shared.show(isLoading: false)
            }
        }
    }
    
    static func upload<T:Codable>(_ url:String,
                        method: HTTPMethod,
                        param parameters: [String: Any],
                        files:[Data] = [],
                        model: T.Type,
                        completion:@escaping (T?, Data?) -> Void) {
        let headers:HTTPHeaders = ["Authorization" : UserManager.shared.getAccessToken()]
        print("api url : ", "\(baseUrl)\(url)")
        print("Authorization : ", UserManager.shared.getAccessToken())
        print("param : ", "\(parameters)")
        print("files count : ", files.count)
        
        AlertManager.shared.show(isLoading: true)
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in parameters {
                if let temp = value as? String {
                    multiPart.append(temp.data(using: .utf8)!, withName: key)
                }
                if let temp = value as? Int {
                    multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
                }
                if let temp = value as? NSArray {
                    temp.forEach({ element in
                        let keyObj = key + "[]"
                        if let string = element as? String {
                            multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                        } else
                            if let num = element as? Int {
                                let value = "\(num)"
                                multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                        }
                    })
                }
            }
            
            for file in files {
                multiPart.append(file, withName: "files", fileName: "file.png", mimeType: "image/png")
            }
            
        }, to: "\(baseUrl)\(url)", method: method, headers: headers).response { response in
            guard let data = response.data else {
                AlertManager.shared.show(isLoading: false)
                completion(nil, nil)
                return
            }
            
            print("data : ", String(data:data, encoding: .utf8) ?? "")
            
            do {
                let decoder = JSONDecoder()
                let model = try decoder.decode(T.self, from: data)
                
                AlertManager.shared.show(isLoading: false)
                completion(model, data)
            } catch(let error) {
                print(error)
                AlertManager.shared.show(isLoading: false)
                completion(nil, data)
            }
        }
    }
}
