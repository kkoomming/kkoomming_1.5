//
//  RefreshManager.swift
//  kkuming
//
//  Created by 장효원 on 2022/08/10.
//

import Foundation

class RefreshData {
    var isMain = false
    //MARK: 만약 추가적인 화면에 대한 새로고침이 필요하면 여기에 추가.
    // var isFeed = false 가 예시
}

class RefreshManager: NSObject{
    static let shared = RefreshManager()
    public var data = RefreshData()
    
    //여기에 전부 true를 해주고, 해당 화면에서 체크하면 됨
    public func refreshALL() {
        data.isMain = true
    }
}
