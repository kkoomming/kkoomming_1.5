//
//  AlertManager.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/04.
//

import UIKit
import Lottie
import Then
import SnapKit

class AlertManager: NSObject{
    static let shared = AlertManager()

    //현재 알림 상태
    var status:Bool = false
    
    var onImage:UIImage = UIImage(named: "ic_alert_over")!
    var offImage:UIImage = UIImage(named: "ic_alert")!
    
    private lazy var lottieView = UIView().then{
        let dimView = UIView()
        dimView.backgroundColor = UIColor.black.withAlphaComponent(0)
        
        $0.addSubview(dimView)
        $0.addSubview(animationView)
        
        dimView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        animationView.snp.makeConstraints{
            $0.centerX.centerY.equalTo(dimView)
            $0.width.height.equalTo(100)
        }
    }
    private let animationView = AnimationView()
    
    public func show(isLoading:Bool = true) {
        let animation = Animation.named("loading_lottie")
        
        if isLoading {
            animationView.animation = animation
            animationView.contentMode = .scaleAspectFit
            animationView.loopMode = .loop
            animationView.play()
            
            showWindows(view: lottieView)
        } else {
            animationView.pause()
            lottieView.removeFromSuperview()
        }
    }
    
    public func updateStatus(view:TopView) {
        let iv = view.getAlertImageView()
        
        if self.status {
            iv.image = self.onImage
        } else {
            iv.image = self.offImage
        }
    }
    
    public func showWindows(view:UIView) {
        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            window.addSubview(view)
            
            view.snp.makeConstraints{
                $0.top.leading.trailing.bottom.equalToSuperview()
            }
        }
    }
    
    public func showWindows(view:UIView, size:CGSize) {
        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            window.addSubview(view)
            
            view.snp.makeConstraints{
                $0.centerY.centerX.equalToSuperview()
                $0.width.equalTo(size.width)
                $0.height.equalTo(size.height)
            }
        }
    }
    
    public func showAlertView(buttonList: [AlertButtonView] = []){
        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            
            window.rootViewController?.view.endEditing(true)
            
            DispatchQueue.main.async {
                let alertView = AlertView(buttonList: buttonList)
                
                window.addSubview(alertView)
                
                alertView.snp.makeConstraints{
                    $0.top.leading.trailing.bottom.equalToSuperview()
                }
            }
            
        }
    }
    
    /*
                    title
                   subTitle
            leftButton  rightButton
     */
    public func show(title:String, subTitle:String, isWithdrawal:Bool=false, completion:(()->Void)? = nil) {
        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            var popupView:PopupView!
            
            popupView = PopupView(title:title, subTitle: subTitle, type: .two, rightCompletion: {
                completion?()
            })
            
            if isWithdrawal {
                popupView.subTitleLabel.font = getFont(size: 11)
            }
            
            window.addSubview(popupView)
            popupView.snp.makeConstraints{
                $0.top.leading.trailing.bottom.equalToSuperview()
            }
        }
    }
    
    /*
                   subTitle(text)
            leftButton  rightButton
     */
    public func show(text:String, type:PopupType, completion:(()->Void)? = nil){
        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            var popupView:PopupView!
            
            switch type {
            case .one:
                popupView = PopupView(subTitle: text, type: .one, rightCompletion: {
                    completion?()
                })
            case .two:
                popupView = PopupView(subTitle: text, type: .two, rightCompletion: {
                    completion?()
                })
            }
            
            window.addSubview(popupView)
            popupView.snp.makeConstraints{
                $0.top.leading.trailing.bottom.equalToSuperview()
            }
        }
    }
    /*
                   subTitle(text)
            leftButton(text)  rightButton(text)
     */
    public func show(text:String, type:PopupType, leftTitle: String = "이전", rightTitle: String = "확인", completion:(()->Void)? = nil){
        if let window = UIApplication.shared.windows.first(where: { $0.isKeyWindow }) {
            var popupView:PopupView!
            
            switch type {
            case .one:
                popupView = PopupView(subTitle: text, type: .one, rightTitle: rightTitle, rightCompletion: {
                    completion?()
                })
            case .two:
                popupView = PopupView(subTitle: text, type: .two, leftTitle: leftTitle, rightTitle: rightTitle, rightCompletion: {
                    completion?()
                })
            }
            
            window.addSubview(popupView)
            popupView.snp.makeConstraints{
                $0.top.leading.trailing.bottom.equalToSuperview()
            }
        }
    }
}
