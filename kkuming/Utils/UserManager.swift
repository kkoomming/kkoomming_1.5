import UIKit

class UserManager: NSObject{
    static let shared = UserManager()
    
    private var userData:UserData!
    
    public func set(userData:UserData) {
        self.userData = userData
    }
    
    public func update(user:User) {
        self.userData.user = user
    }
    
    public func getUserData() -> UserData {
        if userData == nil {
            print("userData == nil")
        }
        
        return userData
    }
    
    public func getUserId() -> Int {
        if let userData = userData, let user = userData.user, let id = user.id {
            return id
        }
        
        return 0
    }
    
    public func getAccessToken() -> String {
        if userData == nil {
            print("userData == nil")
            return ""
        }
        
        return userData.accessToken ?? "fail"
    }
    
    public func getRefreshToken() -> String {
        if userData == nil {
            print("userData == nil")
            return ""
        }
        
        return userData.refreshToken ?? "fail"
    }
    
    public func updateAccessToken(accessToken:String) {
        self.userData.accessToken = accessToken
    }
}
