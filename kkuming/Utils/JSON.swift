//
//  JSON.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/16.
//

import UIKit

class JSON: NSObject {
    
    //에러 예외처리
    static func decode(data:Data, completion:((ErrorData?) -> Void)){
        do {
            let decoder = JSONDecoder()
            let model = try decoder.decode(ErrorData.self, from: data)
            
            completion(model)
        } catch(let error) {
            print("JSON error : \(error)")
            completion(nil)
        }
    }
}
