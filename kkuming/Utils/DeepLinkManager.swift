//
//  DeepLinkManager.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/26.
//

import UIKit

struct DeepLinkData {
    var postId:Int
}

class DeepLinkManager: NSObject {
    static let shared = DeepLinkManager()
    
    private var data:DeepLinkData? = nil
    
    public var openHandler:(()->Void)? = nil
}
