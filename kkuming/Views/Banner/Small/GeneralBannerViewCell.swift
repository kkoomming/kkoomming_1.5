//
//  GeneralBannerViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/03.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class GeneralBannerViewCell: UICollectionViewCell {
    static let identifier = String(describing: GeneralBannerView.self)
    
    let thumbImageView = UIImageView().then{
        $0.contentMode = .scaleAspectFit
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        setupProperties()
        
        addSubViews([
            thumbImageView
        ])
        
        setupConstraints()
    }
    
    private func setupProperties() {
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = 15
    }
    
    private func setupConstraints() {
        thumbImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
