//
//  GeneralBannerView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/03.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class GeneralBannerView: BannerView {
    let leftMargin:CGFloat = 25
    let rightMargin:CGFloat = 25
    lazy var width = UIScreen.main.bounds.width - leftMargin - rightMargin
    lazy var height:CGFloat = 90
    let lineSpacing:CGFloat = 10
    var items:[BannerData] = []
    lazy var currentIndex: CGFloat = 0
    
    weak var delegate: BannerViewDelegate? = nil
    
    lazy var layout = UICollectionViewFlowLayout().then{
        //비율은 327 * 90px
        let height = width * 90 / 327
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .horizontal
    }
    
    lazy var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(GeneralBannerViewCell.self, forCellWithReuseIdentifier: GeneralBannerViewCell.identifier)
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
        $0.isPrefetchingEnabled = false
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public func configuration(items:[BannerData]) {
        self.items = items
        self.collectionView.reloadData()
    }
    
    private func setupUI() {
        addSubViews([
            collectionView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension GeneralBannerView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GeneralBannerViewCell.identifier, for: indexPath) as! GeneralBannerViewCell
        let index = indexPath.item
        let item = items[index]
        
        cell.thumbImageView.kf.setImage(with: item.subImage?.url?.toURL, options: [.loadDiskFileSynchronously])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pageIndex = indexPath.item
        
        delegate?.didSelectItemAt(index: pageIndex)
    }
    
}

extension GeneralBannerView : UIScrollViewDelegate {
    
    //https://jintaewoo.tistory.com/33
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // item의 사이즈와 item 간의 간격 사이즈를 구해서 하나의 item 크기로 설정.
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        // targetContentOff을 이용하여 x좌표가 얼마나 이동했는지 확인
        // 이동한 x좌표 값과 item의 크기를 비교하여 몇 페이징이 될 것인지 값 설정
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        var roundedIndex = round(index)
        
        // scrollView, targetContentOffset의 좌표 값으로 스크롤 방향을 알 수 있다.
        // index를 반올림하여 사용하면 item의 절반 사이즈만큼 스크롤을 해야 페이징이 된다.
        // 스크로로 방향을 체크하여 올림,내림을 사용하면 좀 더 자연스러운 페이징 효과를 낼 수 있다.
        if scrollView.contentOffset.x > targetContentOffset.pointee.x {
            roundedIndex = floor(index)
        } else if scrollView.contentOffset.x < targetContentOffset.pointee.x {
            roundedIndex = ceil(index)
        } else {
            roundedIndex = round(index)
        }
        
        if currentIndex > roundedIndex {
            currentIndex -= 1
            roundedIndex = currentIndex
        } else if currentIndex < roundedIndex {
            currentIndex += 1
            roundedIndex = currentIndex
        }
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
        
        let pageIndex:Int = Int(roundedIndex)
        delegate?.scrollViewWillEndDragging?(view: self, index: pageIndex)
    }
}


