//
//  DirectionBannerView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/12.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class DirectionBannerView: BannerView {
    var leftMargin:CGFloat = 0
    var rightMargin:CGFloat = 0
    lazy var width:CGFloat = 0
    lazy var height:CGFloat = 0
    let lineSpacing:CGFloat = 0
    lazy var currentIndex: CGFloat = 0
    var isBackgroundBlack:Bool = false
    var isDetail:Bool = false
    
    lazy var items:[UIImage] = []
    
    weak var delegate: BannerViewDelegate? = nil
    
    lazy var layout = UICollectionViewFlowLayout().then{
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .horizontal
    }
    
    lazy var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(DirectionBannerViewCell.self, forCellWithReuseIdentifier: DirectionBannerViewCell.identifier)
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.allowsMultipleSelection = false
        $0.isPagingEnabled = true
        $0.isPrefetchingEnabled = false
        $0.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
    }
    
    lazy var imageListPageControlView = AdvancedPageControlView().then{
        $0.drawer = pageControlView
    }
    var pageControlView:ContentBannerPageControlView = ContentBannerPageControlView()
    
    init(isBackgroundBlack:Bool = false, isDetail:Bool = false) {
        super.init(frame: CGRect.zero)
        self.isBackgroundBlack = isBackgroundBlack
        self.isDetail = isDetail
        
        let window = App.getWindow()
        self.width = window.bounds.width
        self.height = window.bounds.height
        
        setupUI()
    }
    
    init(width:CGFloat, height:CGFloat) {
        super.init(frame: CGRect.zero)
        
        self.width = width
        self.height = height
        setupUI()
    }
    
    public func configuration(items:[UIImage]) {
        self.items = items
        
        //페이지 번호
        pageControlView.numberOfPages = items.count
        self.imageListPageControlView.numberOfPages = items.count
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    public func moveToItem(index:Int) {
        DispatchQueue.main.async {
            self.currentIndex = CGFloat(index)
            if let rect = self.collectionView.layoutAttributesForItem(at: IndexPath(item: index, section: 0))?.frame {
                self.collectionView.scrollRectToVisible(rect, animated: false)
            }
            
            self.imageListPageControlView.setPage(index)
        }
    }
    
    private func setupUI() {
        self.layoutIfNeeded()
        
        addSubViews([
            collectionView,
            imageListPageControlView
        ])
        
        collectionView.collectionViewLayout = layout
        if isBackgroundBlack {
            collectionView.backgroundColor = .black
        }
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        if isDetail {
            imageListPageControlView.snp.makeConstraints{
                $0.leading.trailing.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-60)
                $0.height.equalTo(16)
            }
        } else {
            imageListPageControlView.snp.makeConstraints{
                $0.leading.trailing.equalToSuperview()
                $0.bottom.equalToSuperview().offset(-20)
                $0.height.equalTo(16)
            }
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension DirectionBannerView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DirectionBannerViewCell.identifier, for: indexPath) as! DirectionBannerViewCell
        let index = indexPath.row

        cell.thumbImageView.image = items[index]
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didSelectItemAt(_:)))
        cell.scrollView.tag = 1000 + index
        cell.scrollView.addGestureRecognizer(gesture)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pageIndex = indexPath.item
        
        delegate?.didSelectItemAt(index: pageIndex)
    }
    
    @objc
    private func didSelectItemAt(_ sender:UITapGestureRecognizer) {
        print("sender.view?.tag : ", sender.view?.tag)
        if let tag = sender.view?.tag {
            delegate?.didSelectItemAt(index: tag - 1000)
        }
    }
}

extension DirectionBannerView : UIScrollViewDelegate {
    
    //https://jintaewoo.tistory.com/33
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // item의 사이즈와 item 간의 간격 사이즈를 구해서 하나의 item 크기로 설정.
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        // targetContentOff을 이용하여 x좌표가 얼마나 이동했는지 확인
        // 이동한 x좌표 값과 item의 크기를 비교하여 몇 페이징이 될 것인지 값 설정
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        var roundedIndex = round(index)
        
        // scrollView, targetContentOffset의 좌표 값으로 스크롤 방향을 알 수 있다.
        // index를 반올림하여 사용하면 item의 절반 사이즈만큼 스크롤을 해야 페이징이 된다.
        // 스크로로 방향을 체크하여 올림,내림을 사용하면 좀 더 자연스러운 페이징 효과를 낼 수 있다.
        if scrollView.contentOffset.x > targetContentOffset.pointee.x {
            roundedIndex = floor(index)
        } else if scrollView.contentOffset.x < targetContentOffset.pointee.x {
            roundedIndex = ceil(index)
        } else {
            roundedIndex = round(index)
        }
        
        if currentIndex > roundedIndex {
            currentIndex -= 1
            roundedIndex = currentIndex
        } else if currentIndex < roundedIndex {
            currentIndex += 1
            roundedIndex = currentIndex
        }
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
        
        let pageIndex:Int = Int(roundedIndex)
        
        delegate?.scrollViewWillEndDragging?(view: self, index: pageIndex)
        imageListPageControlView.setPage(pageIndex)
    }
}


