//
//  DirectionBannerViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/12.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class DirectionBannerViewCell: UICollectionViewCell {
    static let identifier = String(describing: DirectionBannerViewCell.self)
    
    lazy var scrollView = UIScrollView().then{
        $0.showsVerticalScrollIndicator = false
        $0.showsHorizontalScrollIndicator = false
        $0.minimumZoomScale = 1.0
        $0.maximumZoomScale = 2.0
        $0.delegate = self
    }
    
    let thumbImageView = UIImageView().then{
        $0.contentMode = .scaleAspectFit
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        setupProperties()
        
        scrollView.addSubview(thumbImageView)
        self.contentView.addSubview(scrollView)
        
        setupConstraints()
    }
    
    private func setupProperties() {
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = 0
    }
    
    private func setupConstraints() {
        scrollView.snp.makeConstraints({
            $0.size.equalTo(self.contentView)
            $0.top.right.left.bottom.equalTo(self.contentView)
        })
        
        thumbImageView.snp.makeConstraints({
            $0.size.equalTo(self.scrollView)
            $0.top.right.left.bottom.equalTo(self.scrollView.contentLayoutGuide)
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DirectionBannerViewCell: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.thumbImageView
    }
    
    //https://velog.io/@jxxnnee/Swift-CollectionView%EB%A1%9C-ImagePager%EB%A7%8C%EB%93%A4%EA%B8%B0-2
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        guard let colletionView = self.superview as? UICollectionView else { return }
        guard let image = self.thumbImageView.image else { return }
        let enlarged = scrollView.zoomScale > 1
        
        colletionView.isScrollEnabled = !enlarged
        
        if enlarged {
            let ratioW = self.thumbImageView.frame.width / image.size.width
            let ratioH = self.thumbImageView.frame.height / image.size.height
            let ratio = ratioW < ratioH ? ratioW : ratioH
            let imageWidth = image.size.width * ratio
            let imageHeight = image.size.height * ratio
            let leftCondition = imageWidth * scrollView.zoomScale > self.thumbImageView.frame.width
            var leftInset =
            leftCondition ?
            imageWidth - self.thumbImageView.frame.width :
            scrollView.frame.width - scrollView.contentSize.width
            
            leftInset = leftInset * 0.5
            
            let topCondition = imageHeight * scrollView.zoomScale > self.thumbImageView.frame.height
            var topInset = topCondition ?
            imageHeight - self.thumbImageView.frame.height :
            scrollView.frame.height - scrollView.contentSize.height
            
            topInset = topInset * 0.5

            scrollView.contentInset = UIEdgeInsets(top: topInset, left: leftInset, bottom: topInset, right: leftInset)
        } else {
            scrollView.contentInset = .zero
        }
    }
}
