//
//  BigBannerView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/21.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class BigBannerView: BannerView {
    let leftMargin:CGFloat = 25
    let rightMargin:CGFloat = 25
    lazy var width = UIScreen.main.bounds.width - leftMargin - rightMargin
    let lineSpacing:CGFloat = 10
    var items:[BannerData] = []
    lazy var currentIndex: CGFloat = 0
    
    weak var delegate: BannerViewDelegate? = nil
    
    var numberOfItems = 1000
    var calculatedIndex:CGFloat = 0.0
    
    lazy var layout = UICollectionViewFlowLayout().then{
        let width = UIScreen.main.bounds.width - 50 //-50은 left와 right마진 합친 값
        let height = width * 588 / 654
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .horizontal
    }
    
    lazy var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(BigBannerViewCell.self, forCellWithReuseIdentifier: "BigBannerViewCell")
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
//        $0.isPrefetchingEnabled = false
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public func configuration(items:[BannerData]) {
        self.items = items
        self.numberOfItems = items.count * 50
        
        collectionView.reloadData()
        //MARK: layoutSubviews에도 같이 설정 필요
        self.calculatedIndex = CGFloat(self.numberOfItems % items.count) + CGFloat(self.numberOfItems / 2)
        self.currentIndex = self.calculatedIndex
        collectionView.performBatchUpdates{ [weak self] in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                if items.count > 0 {
                    let indexPath = IndexPath(row: Int(strongSelf.currentIndex) , section: 0)
                    strongSelf.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
                }
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if items.count > 0 {
            //MARK: 무한 스크롤이라 무조건 설정 필요
            currentIndex = self.calculatedIndex
            let indexPath = IndexPath(row: Int(currentIndex) , section: 0)
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }
    }
    
    private func setupUI() {
        addSubViews([
            collectionView
        ])
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BigBannerView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if self.items.count > 0{
            return 1
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.items.count > 0{
            return numberOfItems
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BigBannerViewCell", for: indexPath) as! BigBannerViewCell
        let item = items[indexPath.row % self.items.count]
        
        if let urlString = item.mainImage?.url, let url = URL(string: urlString) {
            cell.bannerImageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
//            cell.configSubBannerView(image: UIImage(data: Data())!)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pageIndex = indexPath.item % items.count
        
        print("pageIndex : \(pageIndex)")
        
        delegate?.didSelectBigBanner?(index: pageIndex)
    }
    
}

extension BigBannerView : UIScrollViewDelegate {
    
    //https://jintaewoo.tistory.com/33
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // item의 사이즈와 item 간의 간격 사이즈를 구해서 하나의 item 크기로 설정.
        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        
        // targetContentOff을 이용하여 x좌표가 얼마나 이동했는지 확인
        // 이동한 x좌표 값과 item의 크기를 비교하여 몇 페이징이 될 것인지 값 설정
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        var roundedIndex = round(index)
        
        // scrollView, targetContentOffset의 좌표 값으로 스크롤 방향을 알 수 있다.
        // index를 반올림하여 사용하면 item의 절반 사이즈만큼 스크롤을 해야 페이징이 된다.
        // 스크로로 방향을 체크하여 올림,내림을 사용하면 좀 더 자연스러운 페이징 효과를 낼 수 있다.
        if scrollView.contentOffset.x > targetContentOffset.pointee.x {
            roundedIndex = floor(index)
        } else if scrollView.contentOffset.x < targetContentOffset.pointee.x {
            roundedIndex = ceil(index)
        } else {
            roundedIndex = round(index)
        }
        
        if currentIndex > roundedIndex {
            currentIndex -= 1
            roundedIndex = currentIndex
        } else if currentIndex < roundedIndex {
            currentIndex += 1
            roundedIndex = currentIndex
        }
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
        targetContentOffset.pointee = offset
        
        let number = Int(roundedIndex) - Int(self.calculatedIndex)
        var pageIndex = 0
        
        if number < 0 {
            var dividendNumber = (number % items.count)
            if dividendNumber == 0 {
                dividendNumber = -items.count
            }
            
            pageIndex = items.count + dividendNumber
        } else {
            pageIndex = number % items.count
        }
        
        delegate?.scrollViewWillEndDragging?(view: self, index: pageIndex)
    }
}

