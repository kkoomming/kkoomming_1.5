//
//  BigBannerViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/21.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class BigBannerViewCell: UICollectionViewCell {
    let bannerView = UIView()
//    .then{
//        $0.clipsToBounds = true
//        $0.layer.masksToBounds = true
//        $0.layer.cornerRadius = 15
//    }
    
    let bannerImageView = UIImageView().then{
        $0.contentMode = .scaleAspectFit
    }
    
    let subBannerView:UIView = {
        let view = UIView()
        
        //MARK: 이미지 사용만해서 해당 뷰는 사용 안함
        view.isHidden = true
        view.clipsToBounds = true
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 13
        view.backgroundColor = .white
        
        return view
    }()
    
    let topTitleLabel = UILabel().then{
        $0.numberOfLines = 2
        $0.textAlignment = .center
        $0.font = getFont(size: 20, type: .bold)
        $0.textColor = .black
    }
    let subTitleLabel = UILabel().then{
        $0.numberOfLines = 1
        $0.textAlignment = .center
        $0.font = getFont(size: 12, type: .regular)
        $0.textColor = RGB(red: 142, green: 142, blue: 142)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        setupUI()
    }
    
    public func configSubBannerView(topString:String = "", subString:String = "", image:UIImage) {
        bannerImageView.image = image
        
        //밑에 2개는 사용 안함
        topTitleLabel.text = topString
        subTitleLabel.text = subString
    }
    
    private func setupUI() {
        bannerView.addSubview(bannerImageView)
        setupSubBannerView()
        
        addSubViews([
            bannerView,
            subBannerView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        bannerView.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
//            $0.height.equalTo(250)
        }
        
        bannerImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
//        setupSubBannerViewConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - 서브 뷰
extension BigBannerViewCell {
    
    private func setupSubBannerView() {
        subBannerView.addSubViews([
            topTitleLabel,
            subTitleLabel
        ])
    }
    
    private func setupSubBannerViewConstraints() {
        subBannerView.snp.makeConstraints{
            $0.leading.equalToSuperview().offset(15)
            $0.trailing.equalToSuperview().offset(-15)
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(bannerView.snp.bottom).offset(44)
            $0.height.equalTo(103)
        }
        
        topTitleLabel.snp.makeConstraints{
            $0.top.leading.equalToSuperview().offset(20)
            $0.centerX.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-20)
        }
        
        subTitleLabel.snp.makeConstraints{
            $0.top.equalTo(topTitleLabel.snp.bottom).offset(5)
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
            $0.centerX.equalToSuperview()
        }
    }
}
