//
//  BannerPageControlView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/22.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

enum BannerPageType:String {
    case EVENT = "EVENT"
    case CHALLENGE = "CHALLENGE"
}

class BannerPageControlView: UIView {
    var items:[Any] = []
    var type:BannerPageType!
    
    let disposeBag = DisposeBag()
    
    weak var delegate:BannerViewDelegate? = nil
    
    let currentNumberLabel = UILabel().then{
        $0.text = "01"
        $0.font = getFont(size: 10, type: .bold)
        $0.textColor = .white
    }
    
    let slashLabel = UILabel().then{
        $0.text = "/"
        $0.font = getFont(size: 10, type: .regular)
        $0.textColor = .white
    }
    
    let totalNumberLabel = UILabel().then{
        $0.text = "10"
        $0.font = getFont(size: 10, type: .regular)
        $0.textColor = .white
    }
    
    let plusLabel = UILabel().then{
        $0.text = "+"
        $0.font = getFont(size: 13, type: .regular)
        $0.textColor = .white
    }
    
    let selectButton = UIButton()
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
        setupRx()
    }
    
    public func configuration(items:[Any]) {
        self.items = items
        updateCurrentNumber(index: 0)
        updateTotalNumber(total: items.count)
    }
    
    public func updateCurrentNumber(index:Int) {
        var currentNumberString = ""
        let number = index + 1
        
        if number < 10 {
            currentNumberString = "0" + String(number)
        } else {
            currentNumberString = String(number)
        }
        
        currentNumberLabel.text = currentNumberString
    }
    
    public func updateTotalNumber(total:Int) {
        var totalNumberString = ""
        let number = total
        
        if number < 10 {
            totalNumberString = "0" + String(number)
        } else {
            totalNumberString = String(number)
        }
        
        totalNumberLabel.text = totalNumberString
    }
    
    private func setupUI() {
        self.backgroundColor = RGB(red: 146, green: 146, blue: 146)
        self.clipsToBounds = true
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 10
        
        addSubViews([
            currentNumberLabel,
            slashLabel,
            totalNumberLabel,
            plusLabel,
            selectButton
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        currentNumberLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(8)
        }
        
        slashLabel.snp.makeConstraints{
            $0.centerY.equalTo(currentNumberLabel.snp.centerY)
            $0.leading.equalTo(currentNumberLabel.snp.trailing).offset(3)
        }
        
        totalNumberLabel.snp.makeConstraints{
            $0.centerY.equalTo(slashLabel.snp.centerY)
            $0.leading.equalTo(slashLabel.snp.trailing).offset(3)
        }
        
        plusLabel.snp.makeConstraints{
            $0.centerY.equalToSuperview().offset(1)
            $0.trailing.equalToSuperview().offset(-6)
        }
        
        selectButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    public func setupRx() {
        selectButton.rx.tap.bind{
            switch self.type {
            case .EVENT:
                App.push(EventVC())
            case .CHALLENGE:
                App.push(KkumingChallengeVC())
            case .none:
                print("지정되지 않음")
            }
        }.disposed(by: disposeBag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BannerPageControlView: BannerViewDelegate {
    func didSelectItemAt(index: Int) {
        print("")
    }
    
    func scrollViewWillEndDragging(view: BannerView, index: Int) {
        print("BannerPageControlView index : ", index)
        updateCurrentNumber(index: (index+1))
    }
    
    func scrollViewWillEndDragging(view: BannerPageControlView, index: Int) {
        updateCurrentNumber(index: (index+1))
    }

}
