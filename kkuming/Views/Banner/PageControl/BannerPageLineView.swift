//
//  BannerPageLineView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/22.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift

class BannerPageLineView: UIView {
    var items:[Any]!
    
    let currentLineView = UIView().then{
        $0.backgroundColor = .black
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public func configuration(items:[Any]) {
        self.items = items
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3){
            self.updateCurrentLine(index: 0)
        }
    }
    
    public func updateCurrentLine(index:Int) {
        UIView.animate(withDuration: 0.318, animations: {
            self.currentLineView.snp.updateConstraints{
                $0.width.equalTo( self.frame.width / CGFloat(self.items.count))
                
                let leading = self.frame.width / CGFloat(self.items.count) * CGFloat(index)
                $0.leading.equalToSuperview().offset(leading)
            }
            self.currentLineView.superview?.layoutIfNeeded()
        })
    }
    
    private func setupUI() {
        self.backgroundColor = RGB(red: 227, green: 227, blue: 227)
        
        addSubViews([
            currentLineView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        currentLineView.snp.makeConstraints{
            $0.top.leading.bottom.equalToSuperview()
            $0.width.equalTo(0)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
