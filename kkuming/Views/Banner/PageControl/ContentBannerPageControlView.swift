//
//  ContentBannerPageControlView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/30.
//

import UIKit

class ContentBannerPageControlView: ExtendedDotDrawer {
    init() {
        super.init(height: 10,
                   width: 10,
                   space: 10,
                   raduis: 5,
                   indicatorColor: .white,
                   dotsColor: .white,
                   isBordered: true,
                   borderColor: .gray,
                   borderWidth: 1.0,
                   indicatorBorderColor: .clear,
                   indicatorBorderWidth: 0.0)
    }
}
