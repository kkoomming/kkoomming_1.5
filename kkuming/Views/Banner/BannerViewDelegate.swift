//
//  BannerViewDelegate.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/26.
//

import UIKit

class BannerView: UIView {
    
}

@objc
protocol BannerViewDelegate: NSObjectProtocol {
    func didSelectItemAt(index:Int)
    @objc optional func didSelectBigBanner(index:Int)
    @objc optional func scrollViewWillEndDragging(view:BannerView, index:Int)
    @objc optional func scrollViewWillEndDragging(depthView:DepthBannerView, index:Int)
}

