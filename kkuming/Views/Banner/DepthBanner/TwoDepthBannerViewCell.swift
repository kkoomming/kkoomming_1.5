//
//  TwoDepthBannerViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/02.
//

import UIKit
import SnapKit
import Then

class TwoDepthBannerViewCell: UICollectionViewCell {
    static let identifier = String(describing: TwoDepthBannerViewCell.self)
    
    let thumbImageView = UIImageView().then{
        $0.contentMode = .scaleAspectFill
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = frame.height / 2
        
        setupUI()
    }
    
    private func setupUI() {
        self.contentView.addSubview(thumbImageView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        thumbImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
}
