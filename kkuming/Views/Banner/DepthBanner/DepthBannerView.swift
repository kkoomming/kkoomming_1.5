//
//  DepthBannerView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/26.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class DepthBannerView: BannerView {
    lazy var width = UIScreen.main.bounds.width
    lazy var height:CGFloat = (UIScreen.main.bounds.width * 230 / 375) + 90
    let lineSpacing:CGFloat = 0
    var items:[Any] = []
    lazy var currentIndex: CGFloat = 0
    
    weak var delegate: BannerViewDelegate? = nil
    
    let numberOfItems = 1000
    
    lazy var oneDepthLayout = UICollectionViewFlowLayout().then{
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .horizontal
    }
    
    lazy var oneDepthCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: oneDepthLayout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(DepthBannerViewCell.self, forCellWithReuseIdentifier: DepthBannerViewCell.identifier)
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.bounces = false
        $0.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        $0.isPrefetchingEnabled = false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        addSubViews([
            oneDepthCollectionView
        ])
        setupConstraints()
    }
    
    private func setupConstraints() {
        oneDepthCollectionView.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(height)
        }
    }
    
    public func configuration(items:[Any]) {
        self.items = items
        
        oneDepthCollectionView.reloadData()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DepthBannerView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DepthBannerViewCell.identifier, for: indexPath) as! DepthBannerViewCell
        let item = items[indexPath.item] as! EventData
        
        if let file = item.files,
            let url = URL(string: file.url) {
            cell.bannerImageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
        }
        
        if let posts = item.posts {
            cell.configuration(items: posts)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pageIndex = indexPath.item
        
        print("pageIndex : \(pageIndex)")
        delegate?.didSelectItemAt(index: pageIndex)
    }
    
}

extension DepthBannerView : UIScrollViewDelegate {
    //https://jintaewoo.tistory.com/33
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if oneDepthCollectionView == scrollView {
            let layout = self.oneDepthCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
            let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
            var offset = targetContentOffset.pointee
            let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
            var roundedIndex = round(index)
            
            if scrollView.contentOffset.x > targetContentOffset.pointee.x {
                roundedIndex = floor(index)
            } else if scrollView.contentOffset.x < targetContentOffset.pointee.x {
                roundedIndex = ceil(index)
            } else {
                roundedIndex = round(index)
            }
            
            if currentIndex > roundedIndex {
                currentIndex -= 1
                roundedIndex = currentIndex
            } else if currentIndex < roundedIndex {
                currentIndex += 1
                roundedIndex = currentIndex
            }
            
            offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: -scrollView.contentInset.top)
            targetContentOffset.pointee = offset
            
            let number = Int(roundedIndex) //- (numberOfItems / items.count)
            var pageIndex = 0
            
            if number < 0 {
                var dividendNumber = (number % items.count)
                if dividendNumber == 0 {
                    dividendNumber = -items.count
                }
                
                pageIndex = items.count + dividendNumber
            } else {
                pageIndex = number % items.count
            }
            
            print("pageIndex : ", pageIndex)

            delegate?.scrollViewWillEndDragging?(view: self, index: pageIndex)
            delegate?.scrollViewWillEndDragging?(depthView: self, index: pageIndex)
        }
    }
}

