//
//  DepthBannerViewCell.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/26.
//

import UIKit
import SnapKit
import Then
import RxCocoa
import RxSwift
import Kingfisher

class DepthBannerViewCell: UICollectionViewCell {
    static let identifier = String(describing: DepthBannerViewCell.self)
    var items:[Post] = []
    
    let leftMargin:CGFloat = 25
    let rightMargin:CGFloat = 25
    lazy var width:CGFloat = 70
    lazy var height:CGFloat = 70
    let lineSpacing:CGFloat = 10
    
    let bannerImageView = UIImageView()
    
    lazy var twoDepthLayout = UICollectionViewFlowLayout().then{
        $0.itemSize = CGSize(width: width, height: height)
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .horizontal
    }
    
    lazy var twoDepthCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: twoDepthLayout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(TwoDepthBannerViewCell.self, forCellWithReuseIdentifier: TwoDepthBannerViewCell.identifier)
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
        $0.isPrefetchingEnabled = false
        $0.bounces = false
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    public func configuration(items:[Post]) {
        self.items = items
        
        twoDepthCollectionView.reloadData()
    }
    
    private func setupUI() {
        [
            bannerImageView,
            twoDepthCollectionView
        ].forEach{
            contentView.addSubview($0)
        }
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        let challengeHeight = (UIScreen.main.bounds.width * 230 / 375)
        bannerImageView.snp.makeConstraints{
            $0.top.leading.trailing.equalToSuperview()
            $0.height.equalTo(challengeHeight)
        }
        
        twoDepthCollectionView.snp.makeConstraints{
            $0.top.equalTo(bannerImageView.snp.bottom).offset(15)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
            $0.height.equalTo(70)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DepthBannerViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TwoDepthBannerViewCell.identifier, for: indexPath) as! TwoDepthBannerViewCell
        let item = items[indexPath.item]
        
        if let urlString = item.thumbnailImage, let url = URL(string: urlString) {
            cell.thumbImageView.kf.setImage(with: url, options: [.loadDiskFileSynchronously])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pageIndex = indexPath.item
        let item = items[pageIndex]
        let vc = ContentDetailVC()
        
        print("pageIndex : \(pageIndex)")
        
        vc.configuration(postData: item, postType: .NONE)
        vc.getDetailPost() {
            App.push(vc)
        }
    }
    
}
