import UIKit
import SnapKit
import Then

class GallerySelectedCell: UICollectionViewCell {
    let imageView = UIImageView().then{
        $0.contentMode = .scaleAspectFit
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    func setupUI() {
        [
            imageView
        ].forEach{ self.contentView.addSubview($0) }
        
        setupConstraints()
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configuration(image:UIImage?) {
        self.imageView.image = image
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
