import UIKit
import Then
import SnapKit
import Photos

class GalleryGridDetailCell: UICollectionViewCell {
    
    let checkView = UIView().then{
        $0.isHidden = true
        $0.layer.borderColor = UIColor.mainColor.cgColor
        $0.layer.borderWidth = 2
    }
    
    let circleView = UIView().then{
        $0.layer.borderWidth = 1
        $0.layer.borderColor = UIColor.white.cgColor
        $0.layer.cornerRadius = 10
        $0.backgroundColor = .clear
        $0.clipsToBounds = true
    }

    let circleNumberLabel = UILabel().then{
        $0.font = getFont(size: 11, type: .regular)
        $0.textColor = .white
    }
    
    let imageView = UIImageView().then{
        $0.contentMode = .scaleAspectFill
        $0.clipsToBounds = true
    }
    
    var data:GalleryData? = nil
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    func setupUI() {
        circleView.addSubview(circleNumberLabel)
        
        [
            imageView,
            circleView,
            checkView
        ].forEach{ self.contentView.addSubview($0) }
        
        setupConstraints()
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        checkView.snp.makeConstraints{
            $0.top.leading.equalToSuperview()
            $0.trailing.bottom.equalToSuperview()
        }
        
        circleView.snp.makeConstraints{
            $0.top.equalToSuperview().offset(5)
            $0.trailing.equalToSuperview().offset(-5)
            $0.width.height.equalTo(20)
        }
        
        circleNumberLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
    }
    
    func setImage(image:UIImage?) {
        imageView.image = image
    }
    
    func setCircleNumber(_ number:Int) {
        isSelected(true)
        circleNumberLabel.text = String(number)
    }

    func isSelected(_ bool:Bool) {
        //숫자 초기화
        circleNumberLabel.text = nil
        
        self.checkView.isHidden = !bool
        
        if bool {
            //선택
            self.circleView.backgroundColor = .mainColor
        } else {
            //해제
            self.circleView.backgroundColor = .clear
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
