//
//  GalleryBottomView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/14.
//

import UIKit
import SnapKit
import Then
import Photos
import RxSwift
import RxCocoa

class GalleryBottomView: UIView {
    var disposeBag = DisposeBag()
    
    lazy var leftView:UIView = {
        let view = UIView()
        
        let label = UILabel()
        label.font = getFont(size: 16)
        label.text = "편집"
        
        view.addSubview(label)
        
        label.snp.makeConstraints{
            $0.centerX.centerY.equalTo(view)
        }
        
        return view
    }()
    
    let button = UIButton()
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    func setupUI() {
        addSubViews([
            leftView,
            button
        ])
        
        setupConstraints()
    }
    
    func setupConstraints() {
        leftView.snp.makeConstraints{
            $0.leading.top.bottom.equalToSuperview()
            $0.width.equalTo(50)
        }
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func setupEditButton(images:[GalleryData?]) {
        button.rx.tap.bind{
            print("편집")
            let vc = GallerySelectedVC()
            vc.images = self.createHighQualityImages(images: images)
            App.push(vc)
        }.disposed(by: disposeBag)
    }
    
    private func createHighQualityImages(images:[GalleryData?]) -> [GalleryData?] {
        var temp:[GalleryData?] = []
        for item in images {
            if let data = item {
                LocalImageManager.shared.requestImage(with: data.asset) { result in
                    temp.append(result)
                }
            }
        }
        return temp
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
