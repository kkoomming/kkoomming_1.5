//
//  GalleryGridLayout.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/14.
//

import UIKit

class GalleryGridLayout: UICollectionViewFlowLayout {
    
    open override func prepare() {
        super.prepare()
        
        var minItemWidth: CGFloat = 100
        if UIDevice.current.userInterfaceIdiom == .pad {
            minItemWidth = 120
        }
    
        let interval: CGFloat = 2
        self.minimumInteritemSpacing = interval
        self.minimumLineSpacing = interval
        
        let contentWidth = self.collectionView!.bounds.width
        
        let itemCount = Int(floor(contentWidth / minItemWidth))
        var itemWidth = (contentWidth - interval * (CGFloat(itemCount) - 1)) / CGFloat(itemCount)
        let actualInterval = (contentWidth - CGFloat(itemCount) * itemWidth) / (CGFloat(itemCount) - 1)
        itemWidth += actualInterval - interval
        
        let itemSize = CGSize(width: itemWidth, height: itemWidth)
        
        GalleryCommon.shared.setItemSize(itemSize)
        self.itemSize = GalleryCommon.shared.getItemSize()
    }
    
}

