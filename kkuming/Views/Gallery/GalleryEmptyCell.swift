import UIKit
import Then
import SnapKit

class GalleryEmptyCell: UICollectionViewCell {
    let imageView = UIImageView().then{
        $0.contentMode = .scaleAspectFill
        $0.image = UIImage(named: "camera2")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.mainColor
        
        setupUI()
    }
    
    func setupUI() {
        self.contentView.addSubview(imageView)
        
        setupConstraints()
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints{
            $0.centerY.centerX.equalToSuperview()
            $0.width.height.equalTo(100)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
