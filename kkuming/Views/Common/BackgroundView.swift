//
//  BackgroundView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/06.
//

import UIKit
import SnapKit

class BackgroundView: UIView {
    init(_ color:UIColor) {
        super.init(frame: CGRect.zero)
        
        self.backgroundColor = color
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class BackgroundBorderView: UIView {
    init(_ color:UIColor, width:CGFloat = 1, height:CGFloat = 8) {
        super.init(frame: CGRect.zero)
        
        let view = UIView()
        view.backgroundColor = color
        
        self.addSubview(view)
        
        view.snp.makeConstraints{
            $0.centerY.centerX.equalTo(self)
            $0.width.equalTo(width)
            $0.height.equalTo(height)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
