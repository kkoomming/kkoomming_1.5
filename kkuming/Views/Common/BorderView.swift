//
//  BorderView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/28.
//

import UIKit
import Then
import SnapKit

class BorderView: UIView {
    init(isHeight:Bool) {
        super.init(frame: CGRect.zero)
        commonInit()
        
        if isHeight {
            self.snp.makeConstraints{
                $0.height.equalTo(1)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        self.backgroundColor = .borderColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
