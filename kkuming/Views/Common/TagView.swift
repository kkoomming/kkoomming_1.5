//
//  TagView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/02.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

protocol TagDelegate:NSObject {
    func didSelectItemAt(items:[String])
}

class TagView: UIView {
    weak var delegate:TagDelegate? = nil
    
    //태그CELL Properties
    var cornerRadius:CGFloat = 15
    var borderWidth:CGFloat = 1
    var borderColor:UIColor = RGB(red: 227, green: 227, blue: 227)
    var textColor:UIColor = RGB(red: 142, green: 142, blue: 142)
    var textFont:UIFont = getFont(size: 11, type: .bold)
    var tagBackgroundColor:UIColor = .clear
    var tagSelectedColor:UIColor = RGB(red: 252, green: 107, blue: 88)
    var cellHeight:CGFloat = 29
    
    var leftMargin:CGFloat = 0
    var rightMargin:CGFloat = 0
    var lineSpacing:CGFloat = 10
    var items:[String] = []
    var selectedItems:[String] = []
    
    var selectHandler:(([String])->Void)? = nil
    
    //글쓰기에서 사용
    var isClose:Bool = false
    
    //게시글 상세보기에서 사용
    var isDetail:Bool = false
    
    lazy var layout = LeftAlignedCollectionViewFlowLayout().then{
        $0.minimumLineSpacing = lineSpacing
        $0.scrollDirection = .vertical
    }
    
    lazy var collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout).then{
        $0.delegate = self
        $0.dataSource = self
        $0.register(TagViewCell.self, forCellWithReuseIdentifier: TagViewCell.identifier)
        $0.decelerationRate = UIScrollView.DecelerationRate.fast
        $0.showsHorizontalScrollIndicator = false
        $0.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
        $0.isPrefetchingEnabled = false
    }
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    //MARK: viewDidLayoutSubviews에서 실행해야됨
    public func updateTagViewLayout() {
        self.collectionView.contentInset = UIEdgeInsets(top: 0, left: leftMargin, bottom: 0, right: rightMargin)
        self.layoutIfNeeded()
        self.collectionView.layoutIfNeeded()
        let height = self.collectionView.contentSize.height
        
        self.snp.updateConstraints{
            $0.height.equalTo(height)
        }
    }
    
    public func configuration(items:[String]) {
        self.items = items
        collectionView.reloadData()
    }
    
    private func setupUI() {
        addSubViews([
            collectionView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        collectionView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension TagView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagViewCell.identifier, for: indexPath) as! TagViewCell
        let index = indexPath.item
        
        //닫기 버튼
        cell.isClose = isClose
        
        cell.backgroundColor = self.tagBackgroundColor
        cell.layer.cornerRadius = self.cornerRadius
        cell.layer.borderWidth = self.borderWidth
        cell.tagLabel.font = self.textFont
        
        cell.tagLabel.text = items[index]
        
        if selectedItems.contains(where: { $0 == items[index] }) {
            cell.layer.borderColor = self.tagSelectedColor.cgColor
            cell.tagLabel.textColor = self.tagSelectedColor
        } else {
            cell.layer.borderColor = self.borderColor.cgColor
            cell.tagLabel.textColor = self.textColor
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = indexPath.item
        let item = items[index]
        print("TagView 단어 : \(item)")
        
        //MARK: isClose를 통해 글쓰기에서 삭제 기능
        if isClose {
            items.remove(at: index)
            delegate?.didSelectItemAt(items: items)
            collectionView.reloadData()
            return
        }
        
        if !isDetail {
            if let index = selectedItems.firstIndex(where: { $0 == item }) {
                selectedItems.remove(at: index)
            } else {
                selectedItems.append(item)
            }
        }
        
        if selectHandler == nil {
            App.push(TagDetailVC(topTitleString: item))
        } else {
            selectHandler!(selectedItems)
        }
        
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let leftMargin:CGFloat = 15
        let rightMargin:CGFloat = 15
        let index = indexPath.item
        let size = getLabelSize(text: self.items[index], font: getFont(size: 11, type: .bold))
        let width = leftMargin + size.width + rightMargin
        
        return CGSize(width: width, height: cellHeight)
    }
}

//MARK: - TagViewCell
class TagViewCell: UICollectionViewCell {
    static let identifier = String(describing: TagViewCell.self)
    
    var isClose:Bool = false {
        didSet{
            if isClose {
                self.addSubview(closeImageView)
                self.setupConstraints()
            } else {
                closeImageView.removeFromSuperview()
            }
        }
    }
    
    lazy var tagLabel = UILabel().then{
        $0.text = "예제"
        $0.font = getFont(size: 11, type: .bold)
    }
    
    let closeImageView = UIImageView(image: UIImage(named: "ic_top_search_close"))
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    private func setupUI() {
        addSubViews([
            tagLabel
        ])
        
        commonInit()
        setupConstraints()
    }
    
    private func commonInit() {
        clipsToBounds = true
        layer.masksToBounds = true
    }
    
    private func setupConstraints() {
        if isClose {
            tagLabel.snp.remakeConstraints{
                $0.centerX.equalToSuperview().offset(-5)
                $0.centerY.equalToSuperview()
            }
            
            closeImageView.snp.remakeConstraints{
                $0.width.height.equalTo(10)
                $0.trailing.equalToSuperview().offset(-5)
                $0.leading.equalTo(tagLabel.snp.trailing).offset(5)
                $0.centerY.equalToSuperview()
            }
        } else {
            tagLabel.snp.remakeConstraints{
                $0.centerX.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
