//
//  ButtonView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/25.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class ButtonView: UIView {
    private var type:ButtonViewType = .mainColor
    private var isButtonStatus = false {
        didSet {
            updateButtonStatus(isButtonStatus)
        }
    }
    private var onString:String = ""
    private var offString:String = ""
    var item:User? = nil
    
    let disposeBag = DisposeBag()
    
    var titleLabel = UILabel().then{
        $0.font = getFont(size: 13)
    }
    
    lazy var button = UIButton().then{
        $0.rx.tap.bind{
            if self.completion != nil {
                self.completion!()
            }
        }.disposed(by: disposeBag)
    }
    
    var completion:(()->Void)? = nil
    
    init(type:ButtonViewType, isButtonStatus:Bool = false, completion:(()->Void)? = nil) {
        super.init(frame: CGRect.zero)
        self.type = type
        self.completion = completion
        self.isButtonStatus = isButtonStatus
        
        commonInit()
        setupUI()
        setupTypeView()
    }
    
    //ON: OFF:
    public func setButtonTitle(onString:String, offString:String) {
        self.onString = onString
        self.offString = offString
    }
    
    public func setTitle(_ text:String) {
        self.titleLabel.text = text
    }
    
    public func setAttributedText(_ attributedText:NSMutableAttributedString) {
        self.titleLabel.attributedText = attributedText
    }
    
    public func updateButtonStatus(_ bool:Bool){
        if bool {
            titleLabel.text = onString
            titleLabel.textColor = RGB(red: 56, green: 196, blue: 207)
            backgroundColor = UIColor.white
            layer.borderWidth = 1
        } else {
            titleLabel.text = offString
            layer.borderWidth = 0
            
            if UserManager.shared.getUserId() == self.item?.id ?? 0 {
                //본인
                titleLabel.textColor = RGB(red: 51, green: 51, blue: 51)
                backgroundColor = RGB(red: 237, green: 237, blue: 237)
            } else {
                titleLabel.textColor = UIColor.white
                backgroundColor = RGB(red: 56, green: 196, blue: 207)
            }
        }
    }
    
    private func commonInit() {
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = 10
        layer.borderWidth = 1
    }
    
    private func setupUI() {
        addSubViews([
            titleLabel,
            button
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints{
            $0.centerY.centerX.equalToSuperview()
        }
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupTypeView() {
        var color:UIColor!
        
        switch type {
        case .gray:
            color = RGB(red: 142, green: 142, blue: 142)
            titleLabel.textColor = color
            layer.borderColor = RGB(red: 210, green: 210, blue: 210).cgColor
            break
        case .mainColor:
            color = UIColor.mainColor
            titleLabel.textColor = color
            layer.borderColor = color.cgColor
            break
        case .follow:
            layer.cornerRadius = 13
            layer.borderWidth = 0
            layer.borderColor = UIColor.mainColor.cgColor
            backgroundColor = .mainColor
            titleLabel.font = getFont(size: 12, type: .bold)
            titleLabel.textColor = .white
            updateButtonStatus(self.isButtonStatus)
            break
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
