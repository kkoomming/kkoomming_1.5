//
//  TopView.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/14.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

@objc protocol TopViewDelegate {
    @objc optional func selectEdit()
    @objc optional func selectReport()
    @objc optional func selectBlock()
    @objc optional func selectProfile()
    @objc optional func selectDelete()
}

class TopView: UIView {
    let disposeBag = DisposeBag()
    weak var delegate:TopViewDelegate? = nil
    
    var type:TopViewType = .default
    
    var galleryUpdateHandler:(()->Void)? = nil
    
    var leftView = UIView()
    
    let titleLabel = UILabel().then{
        $0.textColor = .black
        $0.font = getFont(size: 18, type: .EBold)
    }
    
    lazy var rightView = UIView()
    
    var logoView:UIView!
    var searchView:UIView!
    var alertView:UIView!
    var settingView:UIView!
    var moreView:UIView!
    
    //검색
    var searchBarView:UIView!
    var searchField:UITextField = UITextField()
    var searchImageView:UIImageView!
    var searchHandler:(()->Void)? = nil
    
    //하단 border 추가
    var isBorder:Bool = false {
        didSet {
            if self.isBorder {
                let view = BorderView()
                self.addSubview(view)
                view.snp.makeConstraints{
                    $0.leading.trailing.bottom.equalToSuperview()
                    $0.height.equalTo(1)
                }
            }
        }
    }
    
    //검색 아이콘 변경
    var isSearchClose:Bool = false {
        didSet {
            if isSearchClose {
                self.searchImageView.image = UIImage(named: "ic_top_search_close")
            } else {
                self.searchImageView.image = UIImage(named: "ic_top_search")
            }
        }
    }
    
    init(type:TopViewType, title:String? = nil, completion:(()->Void)? = nil) {
        super.init(frame: CGRect.zero)
        self.type = type
        
        switch type {
        case .main:
            setupMainViewUI()
        case .feed:
            setupFeedViewUI(title: title)
        case .write:
            setupCompleteViewUI(title: "글쓰기", completion: completion)
        case .mypage:
            setupMyPage()
        case .search:
            setupSearchUI()
        case .alert:
            setupAlertViewUI()
        case .detail:
            setupContentDetailView()
        case .myDetail:
            setupMyContentDetailView()
        case .galleryEdit:
            setupGalleryEditUI(completion: completion)
        case .profile:
            setupProfileViewUI(title: title)
        case .back:
            setupBackViewUI(title: title)
        case .profileUpdate:
            setupCompleteViewUI(title: "프로필 수정", completion: completion)
        case .default:
            setupUI()
        }
    }
    
    init(title:String) {
        super.init(frame: CGRect.zero)
        titleLabel.text = title
        
        setupUI()
    }
    
    public func getAlertImageView() -> UIImageView {
        if alertView == nil {
            print("알림 뷰 없음")
            return UIImageView()
        }
        
        for view in alertView.subviews {
            if view is UIImageView {
                let imageView = view as! UIImageView
                return imageView
            }
        }
        
        return UIImageView()
    }
    
    public func addRightButtonEvent(completion:(()->Void)? = nil) {
        let button = UIButton()
        button.rx.tap.bind{
            completion?()
        }.disposed(by: disposeBag)
        rightView.addSubview(button)
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
//MARK: - 메인
    func setupMainViewUI() {
        logoView = UIView().then{
            let logoImageView = UIImageView()
            logoImageView.image = UIImage(named: "logo")
            
            $0.addSubview(logoImageView)
            
            logoImageView.snp.makeConstraints{
                $0.width.equalTo(121)
                $0.height.equalTo(14)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        searchView = createIconView(imageName: "search"){
            print("검색")
            App.push(type: .search)
        }
        alertView = createIconView(){
            print("알림")
            App.push(type: .alert)
        }
        
        addSubViews([
            logoView,
            searchView,
            alertView
        ])
        
        setupMainViewConstraints()
    }
    
    func setupMainViewConstraints() {
        logoView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalToSuperview().offset(23)
            $0.width.equalTo(120)
        }
        
        searchView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(alertView.snp.leading)
        }
        
        alertView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-16)
        }
    }

//MARK: - 피드
    func setupFeedViewUI(title:String? = nil) {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        searchView = createIconView(imageName: "search"){
            print("검색")
            App.push(type: .search)
        }
        alertView = createIconView(){
            print("알림")
            App.push(type: .alert)
        }
        
        addSubViews([
            leftView,
            titleLabel,
            searchView,
            alertView
        ])
        
        setupFeedViewConstraints()
        
        var titleString = title
        if title == nil {
            titleString = "피드"
            setupLeftBackButton(isTabReset: true)
        } else {
            setupLeftBackButton(isTabReset: false)
        }
        
        titleLabel.text = titleString
    }

    func setupFeedViewConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        searchView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(alertView.snp.leading)
        }
        
        alertView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-16)
        }
    }
    
//MARK: - 글쓰기, 프로필 수정
    func setupCompleteViewUI(title:String, completion:(()->Void)? = nil) {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        titleLabel.text = title
        
        rightView = UIView().then{
            let label = UILabel().then{
                $0.text = "완료"
                $0.font = getFont(size: 18)
                $0.textColor = RGB(red: 56, green: 196, blue: 207)
            }
            
            lazy var button = UIButton().then{
                $0.rx.tap.bind{
                    print("완료 버튼")
                    if completion != nil {
                        completion!()
                    }
                }.disposed(by: self.disposeBag)
            }
            
            $0.addSubview(label)
            $0.addSubview(button)
            
            label.snp.makeConstraints{
                $0.centerY.centerX.equalToSuperview()
            }
            
            button.snp.makeConstraints{
                $0.top.leading.trailing.bottom.equalToSuperview()
            }
        }
        
        addSubViews([
            leftView,
            titleLabel,
            rightView
        ])
        
        setupWriteViewConstraints()
        setupLeftBackButton()
    }
    
    func setupWriteViewConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        rightView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-24)
            $0.width.equalTo(30)
        }
    }
    
//MARK: - 마이꾸밍
    func setupMyPage() {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        titleLabel.text = "마이꾸밍"
        
        searchView = createIconView(imageName: "search"){
            print("검색")
            App.push(type: .search)
        }
        alertView = createIconView(){
            print("알림")
            App.push(type: .alert)
        }
        settingView = createIconView(imageName: "ic_setting"){
            print("설정")
            App.push(SettingVC())
        }
        
        addSubViews([
            leftView,
            titleLabel,
            searchView,
            alertView,
            settingView
        ])
        
        setupMyPageConstraints()
        setupLeftBackButton(isTabReset: true)
    }
    
    func setupMyPageConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        searchView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(alertView.snp.leading)
        }
        
        alertView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalTo(settingView.snp.leading)
        }
        
        settingView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-16)
        }
    }
//MARK: - 검색
    public func configurationSearch(image:UIImage, searchHandler:(()->Void)? = nil){
        searchImageView.image = image
        self.searchHandler = searchHandler
    }
    
    func setupSearchUI() {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        searchBarView = UIView().then{
            $0.clipsToBounds = true
            $0.layer.masksToBounds = true
            $0.layer.cornerRadius = 15
            $0.backgroundColor = RGB(red: 245, green: 245, blue: 245)
            
            self.searchField.textColor = UIColor.black
            self.searchField.font = getFont(size: 12)
            let attributes = [
                NSAttributedString.Key.foregroundColor: UIColor.placeholderColor,
                NSAttributedString.Key.font : getFont(size: 12)
            ]
            self.searchField.attributedPlaceholder = NSAttributedString(string: "검색어를 입력해주세요 :)", attributes: attributes)
            
            self.searchImageView = UIImageView()
            
            lazy var button = UIButton().then{
                $0.rx.tap.bind{
                    if self.searchHandler != nil {
                        self.searchHandler!()
                    }
                }.disposed(by: self.disposeBag)
            }
            
            $0.addSubview(searchField)
            $0.addSubview(searchImageView)
            $0.addSubview(button)
            
            searchField.snp.makeConstraints{
                $0.leading.equalToSuperview().offset(20)
                $0.trailing.equalTo(searchImageView.snp.leading).offset(-20)
                $0.centerY.equalToSuperview()
            }
            
            searchImageView.snp.makeConstraints{
                $0.width.height.equalTo(30)
                $0.centerY.equalToSuperview()
                $0.trailing.equalToSuperview().offset(-12)
            }
            
            button.snp.makeConstraints{
                $0.top.trailing.bottom.equalToSuperview()
                $0.width.equalTo(50)
            }
        }
        
        
        addSubViews([
            leftView,
            searchBarView
        ])
        
        setupSearchConstraints()
        setupLeftBackButton()
    }

    func setupSearchConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        searchBarView.snp.makeConstraints{
            $0.leading.equalTo(leftView.snp.trailing).offset(10)
            $0.centerY.equalTo(leftView.snp.centerY)
            $0.trailing.equalToSuperview().offset(-20)
            $0.height.equalTo(45)
        }
    }


//MARK: - 알림
    func setupAlertViewUI() {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        titleLabel.text = "알림"
        
        rightView = UIView().then{
            let label = UILabel().then{
                $0.text = "전체삭제"
                $0.font = getFont(size: 16)
                $0.textColor = RGB(red: 142, green: 142, blue: 142)
                $0.textAlignment = .right
            }
            
            $0.addSubview(label)
            
            label.snp.makeConstraints{
                $0.centerY.trailing.equalToSuperview()
            }
        }
        
        addSubViews([
            leftView,
            titleLabel,
            rightView
        ])
        
        setupAlertViewConstraints()
        setupLeftBackButton()
    }
    
    func setupAlertViewConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        rightView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-24)
            $0.width.equalTo(60)
        }
    }
    
//MARK: - 상세보기
    func setupContentDetailView() {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        titleLabel.text = "상세보기"
        
        //다른 사람꺼 일 때 (프로필보기, 신고하기)
        moreView = createIconView(imageName: "ic_more"){
            let selectProfileView = AlertButtonView(title: "프로필보기", buttonType: .mainColor){
                print("프로필보기")
                self.delegate?.selectProfile?()
            }
            
            let selectReportView = AlertButtonView(title: "신고하기", buttonType: .orange){
                print("신고하기")
                self.delegate?.selectReport?()
            }
            
            AlertManager.shared.showAlertView(buttonList: [
                selectProfileView,
                selectReportView
            ])
        }
        
        addSubViews([
            leftView,
            titleLabel,
            moreView
        ])
        
        setupContentDetailViewConstraints()
        setupLeftBackButton()
    }
    
    func setupContentDetailViewConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        moreView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-16)
        }
    }
    
//MARK: - 내 글 상세보기
    func setupMyContentDetailView() {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        titleLabel.text = "상세보기"
        
        //내꺼일 때(수정하기, 삭제하기)
        moreView = createIconView(imageName: "ic_more"){
            let selectEditView = AlertButtonView(title: "수정하기", buttonType: .mainColor){
                print("수정하기")
                self.delegate?.selectEdit?()
                RefreshManager.shared.refreshALL()
            }
            
            let selectDeleteView = AlertButtonView(title: "삭제하기", buttonType: .orange){
                print("삭제하기")
                self.delegate?.selectDelete?()
                RefreshManager.shared.refreshALL()
            }
            
            AlertManager.shared.showAlertView(buttonList: [
                selectEditView,
                selectDeleteView
            ])
        }
        
        addSubViews([
            leftView,
            titleLabel,
            moreView
        ])
        
        setupMyContentDetailViewConstraints()
        setupLeftBackButton()
    }
    
    func setupMyContentDetailViewConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        moreView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-16)
        }
    }
    
//MARK: - 갤러리 편집창
    func setupGalleryEditUI(completion:(()->Void)? = nil) {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "ic_back_white")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        titleLabel.text = ""
        
        rightView = UIView().then{
            let label = UILabel().then{
                $0.text = "완료"
                $0.font = getFont(size: 18)
                $0.textColor = UIColor.white
            }
            
            lazy var button = UIButton().then{
                $0.rx.tap.bind{
                    print("완료 버튼")
                    if completion != nil {
                        completion!()
                    }
                }.disposed(by: self.disposeBag)
            }
            
            $0.addSubview(label)
            $0.addSubview(button)
            
            label.snp.makeConstraints{
                $0.centerY.centerX.equalToSuperview()
            }
            
            button.snp.makeConstraints{
                $0.top.leading.trailing.bottom.equalToSuperview()
            }
        }
        
        addSubViews([
            leftView,
            titleLabel,
            rightView
        ])
        
        setupGalleryEditUIConstraints()
        setupLeftBackButton()
    }
    
    func setupGalleryEditUIConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        rightView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-24)
            $0.width.equalTo(30)
        }
    }
    
//MARK: - 프로필
    func setupProfileViewUI(title:String? = nil) {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
//        searchView = createIconView(imageName: "search"){
//            print("검색")
//            App.push(type: .search)
//        }
        moreView = createIconView(imageName: "ic_more"){
            print("더보기")
//            let selectReportView = AlertButtonView(title: "신고하기", buttonType: .mainColor){
//                print("신고하기")
//                self.delegate?.selectReport?()
//            }
            
            let selectBlockView = AlertButtonView(title: "차단하기", buttonType: .orange){
                print("차단하기")
                self.delegate?.selectBlock?()
            }
            
            AlertManager.shared.showAlertView(buttonList: [
//                selectReportView,
                selectBlockView
            ])
        }
        
        addSubViews([
            leftView,
            titleLabel,
//            searchView,
            moreView
        ])
        
        setupProfileViewConstraints()
        
        var titleString = "프로필"
        titleLabel.text = titleString
        
        setupLeftBackButton()
    }

    func setupProfileViewConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
//        searchView.snp.makeConstraints{
//            $0.width.height.equalTo(32)
//            $0.centerY.equalToSuperview()
//            $0.trailing.equalTo(moreView.snp.leading)
//        }
        
        moreView.snp.makeConstraints{
            $0.width.height.equalTo(32)
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-16)
        }
    }
    
//MARK: - 뒤로가기 뷰
    func setupBackViewUI(title:String? = nil) {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        addSubViews([
            leftView,
            titleLabel,
        ])
        
        setupBackViewConstraints()
        
        let titleString = title
        titleLabel.text = titleString
        
        setupLeftBackButton()
    }

    func setupBackViewConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
    }

//MARK: - 일반
    func setupUI() {
        leftView = UIView().then{
            let backImageView = UIImageView()
            backImageView.image = UIImage(named: "back")
            
            $0.addSubview(backImageView)
            
            backImageView.snp.makeConstraints{
                $0.width.equalTo(32)
                $0.height.equalTo(32)
                $0.leading.equalToSuperview()
                $0.centerY.equalToSuperview()
            }
        }
        
        addSubViews([
            leftView,
            titleLabel
        ])
        
        setupConstraints()
        setupLeftBackButton()
    }
    
    func setupConstraints() {
        leftView.snp.makeConstraints{
            $0.top.bottom.equalToSuperview()
            $0.leading.equalTo(20)
            $0.width.equalTo(32)
        }
        
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
    }
    
    func setupLeftBackButton(isTabReset:Bool = false) {
        let button = UIButton()
        button.rx.tap.bind{ [weak self] in
            guard let strongSelf = self else { return }
            //앨범 새로고침
            strongSelf.galleryUpdateHandler?()
            
            if isTabReset {
                App.tabReset()
            } else {
                App.pop()
            }
        }.disposed(by: disposeBag)
        
        leftView.addSubview(button)
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func setupRightCropButton(completion:@escaping (()->Void)) {
        let button = UIButton()
        button.rx.tap.bind{
            completion()
        }.disposed(by: disposeBag)
        
        rightView.addSubview(button)
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
       
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension TopView {
    //버튼 생성
    private func createIconView(imageName:String = "", completion:(()->Void)? = nil) -> UIView {
        let view = UIView()
        let iconImageView = UIImageView()
        
        if imageName != "" {
            if let image = UIImage(named: imageName) {
                iconImageView.image = image
            } else {
                print("\(imageName) 해당 이미지 없음")
            }
        }
        lazy var button = UIButton().then{
            $0.rx.tap.bind{
                if completion != nil {
                    completion!()
                }
            }.disposed(by: disposeBag)
        }
        
        view.addSubViews([
            iconImageView,
            button
        ])
        
        iconImageView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        return view
    }
}
