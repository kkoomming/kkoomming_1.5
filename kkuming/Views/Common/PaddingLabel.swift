//
//  PaddingLabel.swift
//  kkuming
//
//  Created by 장효원 on 2022/04/25.
//

import UIKit

class PaddingLabel: UILabel {
    private var topInset: CGFloat = 0
    private var leftInset: CGFloat = 0
    private var rightInset: CGFloat = 0
    private var bottomInset: CGFloat = 0
    
    init(top:CGFloat = 0, left:CGFloat = 0, right:CGFloat = 0, bottom:CGFloat = 0) {
        super.init(frame: CGRect.zero)
        
        self.topInset = top
        self.leftInset = left
        self.rightInset = right
        self.bottomInset = bottom
    }
       
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
