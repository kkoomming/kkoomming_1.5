//
//  TextFieldView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/20.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class TextFieldView: UIView {
    private var cornerRadius:CGFloat = 0
    private var viewBackgroundColor:UIColor = .clear
    
    let textField = UITextField()
    
    init(cornerRadius:CGFloat = 0, backgroundColor:UIColor = RGB(red: 247, green: 247, blue: 247)) {
        super.init(frame: CGRect.zero)
        self.cornerRadius = cornerRadius
        self.viewBackgroundColor = backgroundColor
        
        setupUI()
    }
    
    public func getText() -> String {
        var text = ""
        
        if let temp = textField.text {
            text = temp
        }
        
        return text
    }
    
    public func setTextFieldProperties(font:UIFont, placeholder:String = "") {
        textField.font = font
        textField.placeholder = placeholder
    }
    
    private func setupUI() {
        setupProperties()
        
        addSubview(textField)
        setupConstraints()
    }
    
    private func setupProperties() {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        backgroundColor = viewBackgroundColor
    }
    
    private func setupConstraints() {
        textField.snp.makeConstraints{
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(10)
            $0.trailing.equalToSuperview().offset(-10)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
