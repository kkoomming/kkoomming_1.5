//
//  CustomTextView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/20.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class CustomTextView: UIView {
    private var cornerRadius:CGFloat = 0
    private var viewBackgroundColor:UIColor = .clear
    
    let textView = UITextView().then{
        $0.backgroundColor = .clear
    }
    
    init(cornerRadius:CGFloat = 0, backgroundColor:UIColor = RGB(red: 247, green: 247, blue: 247)) {
        super.init(frame: CGRect.zero)
        self.cornerRadius = cornerRadius
        self.viewBackgroundColor = backgroundColor
        
        setupUI()
    }
    
    public func getText() -> String {
        var text = ""
        
        if let temp = textView.text {
            text = temp
        }
        
        return text
    }
    
    public func setTextViewProperties(font:UIFont) {
        textView.font = font
    }
    
    private func setupUI() {
        setupProperties()
        
        addSubview(textView)
        setupConstraints()
    }
    
    private func setupProperties() {
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        backgroundColor = viewBackgroundColor
    }
    
    private func setupConstraints() {
        textView.snp.makeConstraints{
            $0.top.leading.equalToSuperview().offset(20)
            $0.trailing.bottom.equalToSuperview().offset(-20)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
