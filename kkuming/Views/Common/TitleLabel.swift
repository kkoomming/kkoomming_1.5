//
//  TitleLabel.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/03.
//

import UIKit

class TitleLabel: UILabel {
    init(text:String) {
        super.init(frame: CGRect.zero)
        
        commonInit()
        self.text = text
    }
    
    public func commonInit() {
        textColor = .black
        font = getFont(size: 16, type: .bold)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
