//
//  ReportView.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/08.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class ReportView: UIView {
    let disposeBag = DisposeBag()
    
    var isPost:Bool = true
    var reportItemImageViewList:[UIImageView] = []
    var selectedReport:ReportData? = nil
    
    var completion:(()->Void)? = nil
    
    lazy var dimView = UIView().then{
        $0.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        lazy var button = UIButton()
        
        button.rx.tap.bind{
            self.closeAnimation()
        }.disposed(by: disposeBag)
        
        $0.addSubview(button)
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    let alertView = UIView()
    
    let titleLabel = UILabel().then{
        $0.font = getFont(size: 16, type: .bold)
        $0.text = "신고 내용"
        $0.textColor = .black
    }
    
    let completeButton = UIButton().then{
        $0.setTitle("완료", for: .normal)
        $0.setTitleColor(RGB(red: 152, green: 152, blue: 152), for: .normal)
    }
    
    let stackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.layoutMargins = UIEdgeInsets(top: 13, left: 25, bottom: 0, right: 25)
        $0.isLayoutMarginsRelativeArrangement = true
    }
    
    init(isPost:Bool) {
        super.init(frame: CGRect.zero)
        self.isPost = isPost
        
        setupUI()
        startAnimation()
        setupRx()
        
        getListReportCategory()
    }
    
    private func setupUI() {
        setupProperties()
        
        alertView.addSubViews([
            titleLabel,
            completeButton,
            stackView
        ])
        
        addSubViews([
            dimView,
            alertView,
        ])
        
        setupConstraints()
    }
    
    private func setupProperties() {
        alertView.clipsToBounds = true
        alertView.layer.masksToBounds = true
        alertView.layer.cornerRadius = 15
        alertView.backgroundColor = RGB(red: 247, green: 247, blue: 247)
        alertView.transform = CGAffineTransform(translationX: 0, y: 520)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        alertView.addGestureRecognizer(panGesture)
    }
    
    private func setupConstraints() {
        dimView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        alertView.snp.makeConstraints{
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview().offset(20)
            $0.height.equalTo(500)
        }
        
        titleLabel.snp.makeConstraints{
            $0.top.equalToSuperview().offset(45)
            $0.leading.equalToSuperview().offset(25)
        }
        
        completeButton.snp.makeConstraints{
            $0.top.equalToSuperview().offset(39)
            $0.trailing.equalToSuperview().offset(-25)
        }
        
        stackView.snp.makeConstraints{
            $0.top.equalTo(completeButton.snp.bottom)
            $0.leading.trailing.equalToSuperview()
        }
    }
    
    private func setupRx() {
        completeButton.rx.tap.bind{
            self.completion?()
            self.closeAnimation()
        }.disposed(by: disposeBag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ReportView {
    public func startAnimation() {
        UIView.animate(withDuration: 0.5, animations: {
            self.resetAlertView()
        })
    }
    
    private func closeAnimation() {
        UIView.animate(withDuration: 0.5, animations: {
            self.alertView.transform = CGAffineTransform(translationX: 0, y: 520)
        }){ _ in
            self.removeFromSuperview()
        }
    }
    
    private func resetAlertView() {
        self.alertView.transform = CGAffineTransform(translationX: 0, y: 0)
    }
    
    @objc
    func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self)
        let y = translation.y
        
        if recognizer.state == .changed {
            if y > 0 {
                self.alertView.transform = CGAffineTransform(translationX: 0, y: translation.y)
            }
        }
        
        if recognizer.state == .ended {
            if y > 260 {
                self.removeFromSuperview()
            } else {
                startAnimation()
            }
        }
    }
    
    private func createContentView(item:ReportData){
        var isCheck:Bool = false
        
        let view = UIView().then{
            $0.clipsToBounds = true
            $0.layer.masksToBounds = true
            $0.layer.cornerRadius = 10
            $0.backgroundColor = .white
        }
        
        let titleLabel = UILabel().then{
            $0.font = getFont(size: 14)
            $0.text = item.description
        }
        
        let checkBoxImageView = UIImageView().then{
            $0.image = UIImage(named: "check_btn")
        }
        
        lazy var button = UIButton().then{
            $0.rx.tap.bind{
                self.selectedReport = item
                self.reportItemImageViewList.forEach{
                    $0.image = UIImage(named: "check_btn")
                }
                checkBoxImageView.image = UIImage(named: "check_btn_over")
//                isCheck = !isCheck
//                if isCheck {
//                    checkBoxImageView.image = UIImage(named: "check_btn_over")
//                } else {
//                    checkBoxImageView.image = UIImage(named: "check_btn")
//                }
            }.disposed(by: disposeBag)
        }
        
        view.addSubViews([
            titleLabel,
            checkBoxImageView,
            button
        ])
        
        titleLabel.snp.makeConstraints{
            $0.leading.equalToSuperview().offset(20)
            $0.centerY.equalToSuperview()
        }
        
        checkBoxImageView.snp.makeConstraints{
            $0.width.height.equalTo(20)
            $0.trailing.equalToSuperview().offset(-20)
            $0.centerY.equalToSuperview()
        }
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        stackView.addArrangedSubview(view)
        //체크시 이미지 변환을 위해 담아둠
        self.reportItemImageViewList.append(checkBoxImageView)
        
        view.snp.makeConstraints{
            $0.height.equalTo(50)
        }
        
        stackView.setCustomSpacing(20, after: view)
    }
}

//MARK: - API
extension ReportView {
    func getListReportCategory() {
        API.getListReportCategory() { [weak self] model in
            guard let strongSelf = self, let model = model, let list = model.data else { return }
            
            for item in list {
                strongSelf.createContentView(item: item)
            }
        }
    }
    
    func reportComment(commentId:Int) {
        print("reportComment")
        if let selectedReport = selectedReport {
            API.reportComment(commentId: commentId, reportCategoryId: selectedReport.id ?? 0) { model in
                guard let model = model else {
                    return
                }
                
                if model.success {
                    AlertManager.shared.show(text: "신고가 완료되었습니다.", type: .one)
                }
            }
        } else {
            AlertManager.shared.show(text: "신고 내용을 선택해 주세요.", type: .one)
            return
        }
    }
    
    func reportPost(postId:Int, completion:(()->Void)? = nil) {
        print("reportPost")
        if let selectedReport = selectedReport {
            API.reportPost(postId: postId, reportCategoryId: selectedReport.id ?? 0) { model in
                guard let model = model else {
                    return
                }

                if model.success {
                    AlertManager.shared.show(text: "신고가 완료되었습니다.", type: .one)
                    completion?()
                }
            }
        } else {
            AlertManager.shared.show(text: "신고 내용을 선택해 주세요.", type: .one)
            return
        }
    }
    
}
