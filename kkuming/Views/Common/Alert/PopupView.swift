//
//  PopupView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/11.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

enum PopupType {
    case one
    case two
}

class PopupView: UIView {
    var title:String = ""
    var subTitle:String = ""
    var leftTitle:String = ""
    var rightTitle:String = ""
    var leftCompletion:(()->Void)? = nil
    var rightCompletion:(()->Void)? = nil
    var type:PopupType = .one
    
    let dimView = BackgroundView(UIColor.gray.withAlphaComponent(0.5))
    
    let stackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.spacing = 10
    }
    
    let buttonStackView = UIStackView().then{
        $0.axis = .horizontal
        $0.distribution = .equalSpacing
        $0.alignment = .fill
    }
    
    let backgroundView = BackgroundView(UIColor.white).then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 15
    }
    
    lazy var titleLabel = UILabel().then{
        $0.text = title
        $0.font = getFont(size: 17, type: .bold)
        $0.textAlignment = .center
    }
    
    lazy var subTitleLabel = UILabel().then{
        $0.text = subTitle
        $0.font = getFont(size: 17)
        $0.numberOfLines = 0
        $0.textAlignment = .center
    }
    
    lazy var previousButtonView = AlertButtonView(title: leftTitle, buttonType: .blank){
        print("이전")
        if self.leftCompletion != nil {
            self.leftCompletion!()
        }
        
        self.removeFromSuperview()
    }.then{
        $0.setCornerRadius(17)
        $0.setTextFont(getFont(size: 17))
    }
    
    lazy var confirmButtonView = AlertButtonView(title: rightTitle, buttonType: .mainColor){
        print("확인")
        if self.rightCompletion != nil {
            self.rightCompletion!()
        }
        
        self.removeFromSuperview()
    }.then{
        $0.setCornerRadius(17)
        $0.setTextFont(getFont(size: 17))
    }
    
    init(title:String = "", subTitle:String = "", type:PopupType = .one, leftTitle:String = "이전", rightTitle:String = "확인", leftCompletion:(()->Void)? = nil, rightCompletion:(()->Void)? = nil) {
        super.init(frame: CGRect.zero)
        self.title = title
        self.subTitle = subTitle
        self.type = type
        self.leftTitle = leftTitle
        self.rightTitle = rightTitle
        self.leftCompletion = leftCompletion
        self.rightCompletion = rightCompletion
    
        switch type {
        case .one:
            setupOneUI()
        case .two:
            setupTwoUI()
        }
    }
    
    init(title:String = "", subTitle:String = "", leftTitle:String = "이전", rightTitle:String = "확인", leftCompletion:(()->Void)? = nil, rightCompletion:(()->Void)? = nil) {
        super.init(frame: CGRect.zero)
        self.title = title
        self.subTitle = subTitle
        self.leftTitle = leftTitle
        self.rightTitle = rightTitle
        self.leftCompletion = leftCompletion
        self.rightCompletion = rightCompletion
    
        setupTwoUI(isUseTitle: true)
    }
    
    private func setupOneUI() {
        addSubViews([
            dimView,
            backgroundView,
            stackView
        ])
        
        [
            confirmButtonView
        ].forEach{
            buttonStackView.addArrangedSubview($0)
        }
        
        stackView.addArrangedSubview(subTitleLabel)
        
        stackView.addArrangedSubview(buttonStackView)
        
        setupOneConstraints()
    }
    
    private func setupOneConstraints() {
        dimView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        backgroundView.snp.makeConstraints{
            $0.top.equalTo(stackView.snp.top).offset(-27)
            $0.leading.equalTo(stackView.snp.leading).offset(-17)
            $0.trailing.equalTo(stackView.snp.trailing).offset(17)
            $0.bottom.equalTo(stackView.snp.bottom).offset(27)
        }
        
        stackView.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(50)
            $0.trailing.equalToSuperview().offset(-50)
        }
        
        buttonStackView.snp.makeConstraints{
            $0.height.equalTo(35)
        }
        
        confirmButtonView.snp.makeConstraints{
            $0.top.trailing.bottom.equalToSuperview()
        }
    }
    
    private func setupTwoUI(isUseTitle:Bool = false) {
        addSubViews([
            dimView,
            backgroundView,
            stackView
        ])
        
        [
            previousButtonView,
            confirmButtonView
        ].forEach{
            buttonStackView.addArrangedSubview($0)
        }
        
        if title != "" {
            stackView.addArrangedSubview(titleLabel)
        }
        
        if subTitle != "" {
            stackView.addArrangedSubview(subTitleLabel)
        }
        
        stackView.addArrangedSubview(buttonStackView)
        
        setupTwoConstraints()
    }
    
    private func setupTwoConstraints() {
        dimView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        backgroundView.snp.makeConstraints{
            $0.top.equalTo(stackView.snp.top).offset(-27)
            $0.leading.equalTo(stackView.snp.leading).offset(-17)
            $0.trailing.equalTo(stackView.snp.trailing).offset(17)
            $0.bottom.equalTo(stackView.snp.bottom).offset(27)
        }
        
        stackView.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
            $0.leading.equalToSuperview().offset(50)
            $0.trailing.equalToSuperview().offset(-50)
        }
        
        buttonStackView.snp.makeConstraints{
            $0.height.equalTo(35)
        }
        
        previousButtonView.snp.makeConstraints{
            $0.top.leading.bottom.equalToSuperview()
            $0.trailing.equalTo(confirmButtonView.snp.leading).offset(-10)
            $0.width.equalTo(confirmButtonView.snp.width)
        }
        
        confirmButtonView.snp.makeConstraints{
            $0.top.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
