//
//  AlertView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/10.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class AlertView: UIView {
    let disposeBag = DisposeBag()
    
    var buttonList:[AlertButtonView] = []
    var stackViewHeight:CGFloat = 0
    
    lazy var backgroundView = BackgroundView(RGB(red: 0, green: 0, blue: 0).withAlphaComponent(0.5)).then{
        lazy var button = UIButton()
        button.rx.tap.bind{
            self.closeAnimation()
        }.disposed(by: disposeBag)
        $0.addSubview(button)
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    let alertView = BackgroundView(RGB(red: 247, green: 247, blue: 247)).then{
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 15
    }
    
    let stackView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.spacing = 10
    }
    
    lazy var closeButtonView = AlertButtonView(title: "닫기"){ [weak self] in
        guard let strongSelf = self else { return }
        strongSelf.closeAnimation()
    }
    
    init(buttonList:[AlertButtonView] = []) {
        super.init(frame: CGRect.zero)
        self.buttonList = buttonList
        
        setupUI()
        setupProperties()
    }
    
    private func setupUI() {
        setupStackItemView()
        
        addSubViews([
            backgroundView,
            alertView,
            stackView
        ])
        
        setupConstraints()
    }
    
    private func setupProperties() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panGesture(_:)))
        alertView.addGestureRecognizer(panGesture)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        stackViewHeight = stackView.frame.height + 20 + 20
        
        alertView.transform = CGAffineTransform(translationX: 0, y: stackViewHeight)
        stackView.transform = CGAffineTransform(translationX: 0, y: stackViewHeight)
        
        alertView.snp.updateConstraints{
            $0.height.equalTo(stackViewHeight)
        }
        
        startAnimation()
    }
    
    private func setupConstraints() {
        backgroundView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        alertView.snp.makeConstraints{
            $0.height.equalTo(0)
            $0.bottom.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
        
        stackView.snp.makeConstraints{
            $0.leading.equalToSuperview().offset(25)
            $0.trailing.equalToSuperview().offset(-25)
            $0.bottom.equalToSuperview().offset(-20)
        }
    }
    
    private func setupStackItemView() {
        buttonList.forEach{
            $0.closeHandler = {
                self.closeAnimation()
            }
            stackView.addArrangedSubview($0)
        }
        
        stackView.addArrangedSubview(createSpacingView(height: 10))
        stackView.addArrangedSubview(closeButtonView)
        
        setupStackItemViewConstraints()
    }
    
    private func setupStackItemViewConstraints() {
        buttonList.forEach{
            $0.snp.makeConstraints{
                $0.height.equalTo(50)
            }
        }
        
        closeButtonView.snp.makeConstraints{
            $0.height.equalTo(50)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AlertView {
    public func startAnimation() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alertView.transform = .identity
            self.stackView.transform = .identity
        })
    }
    
    private func closeAnimation() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alertView.transform = CGAffineTransform(translationX: 0, y: self.stackViewHeight)
            self.stackView.transform = CGAffineTransform(translationX: 0, y: self.stackViewHeight)
        }){ _ in
            self.removeFromSuperview()
        }
    }
    
    @objc
    func panGesture(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self)
        let y = translation.y
        
        if recognizer.state == .changed {
            if y > 0 {
                self.alertView.transform = CGAffineTransform(translationX: 0, y: translation.y)
                self.stackView.transform = CGAffineTransform(translationX: 0, y: translation.y)
            }
        }
        
        if recognizer.state == .ended {
            if y > stackViewHeight / 2 {
                self.removeFromSuperview()
            } else {
                startAnimation()
            }
        }
    }
}
