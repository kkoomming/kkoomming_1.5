//
//  NoticePopupView.swift
//  kkuming
//
//  Created by 장효원 on 2022/06/13.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class NoticePopupView: UIView {
    let disposeBag = DisposeBag()
    let width:CGFloat = 317
    let height:CGFloat = 317 + 45
    lazy var items:[BannerData] = [] {
        didSet{
            self.configuration()
        }
    }
    
    let dimView = UIView().then{
        $0.backgroundColor = .black.withAlphaComponent(0.7)
    }
    
    let alertView = UIStackView().then{
        $0.axis = .vertical
        $0.alignment = .fill
        $0.distribution = .fill
        $0.backgroundColor = .white
        $0.clipsToBounds = true
        $0.layer.masksToBounds = true
        $0.layer.cornerRadius = 15
    }
    
    lazy var imageListView = DirectionBannerView(width: width,
                                                 height: height
    ).then{
        $0.delegate = self
    }
    
    lazy var bottomStackView = createBottomStackView()
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
        configuration()
    }
    
    private func configuration() {
        var imageList:[UIImage] = []
        
        for item in items {
            do{
                if let popupURL = item.popupImage?.url, let url = popupURL.toURL {
                    if UIApplication.shared.canOpenURL(url) {
                        let data = try Data(contentsOf: url)
                        if let image = UIImage(data: data) {
                            imageList.append(image)
                        }
                    } else {
                        imageList.append(UIImage(named: "emptyKM")!)
                    }
                }
            } catch {
                print("error : ", error)
            }
        }
        
        imageListView.configuration(items: imageList)
    }
    
    private func setupUI() {
        
        [
            imageListView,
            bottomStackView
        ].forEach{
            alertView.addArrangedSubview($0)
        }
        
        addSubViews([
            dimView,
            alertView
        ])
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        dimView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        alertView.snp.makeConstraints{
            $0.width.equalTo(width)
            $0.height.equalTo(height)
            $0.centerX.centerY.equalToSuperview()
        }
        
        imageListView.snp.makeConstraints{
            $0.height.equalTo(width)
        }
        
        bottomStackView.snp.makeConstraints{
            $0.height.equalTo(45)
        }
    }
    
    private func setupRx() {
        
    }
    
    private func createBottomStackView() -> UIStackView {
        let stackView = UIStackView().then{
            $0.axis = .horizontal
            $0.alignment = .fill
            $0.distribution = .fill
        }
        
        let leftView = UIView()
        let leftLabel = UILabel().then{
            $0.text = "오늘 하루 그만 보기"
            $0.textColor = RGB(red: 139, green: 136, blue: 136)
            $0.font = getFont(size: 14)
        }
        lazy var leftButton = UIButton().then{
            $0.rx.tap.bind{
                self.onTouchOnlyToday()
            }.disposed(by: self.disposeBag)
        }
        
        let rightView = UIView()
        let rightLabel = UILabel().then{
            $0.text = "닫기"
            $0.textColor = .black
            $0.font = getFont(size: 14)
        }
        lazy var rightButton = UIButton().then{
            $0.rx.tap.bind{
                print("닫기")
                self.removeFromSuperview()
            }.disposed(by: self.disposeBag)
        }
        
        [
            leftLabel,
            leftButton
        ].forEach{
            leftView.addSubview($0)
        }
        
        [
            rightLabel,
            rightButton
        ].forEach{
            rightView.addSubview($0)
        }
        
        [
            leftView,
            rightView
        ].forEach{
            stackView.addArrangedSubview($0)
        }
        
        leftLabel.snp.makeConstraints{
            $0.leading.equalToSuperview().offset(20)
            $0.centerY.equalToSuperview()
        }
        
        rightLabel.snp.makeConstraints{
            $0.centerY.centerX.equalToSuperview()
        }
        
        leftButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        rightButton.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        rightView.snp.makeConstraints{
            $0.width.equalTo(100)
        }
        
        return stackView
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension NoticePopupView {
    func onTouchOnlyToday() {
        let value = Date().dateFormat()
        
        UserDefaultsUtil.save(value: value, key: .mainPopupDate)
        
        print("오늘 하루 그만 보기")
        self.removeFromSuperview()
//        Defaults[\.popupDateList][String(data.IDX)] = Date(timeIntervalSinceNow: TimeInterval(86400 * data.SHOW_DAY)).timeIntervalSince1970
    }
}

extension NoticePopupView: BannerViewDelegate {
    func didSelectItemAt(index: Int) {
        print("didSelectItemAt index : ", index)
        
        let realIndex = index % items.count
        let data = items[realIndex]
        self.selectBannerHandler(data: data)
    }
    
    func scrollViewWillEndDragging(view: BannerView, index: Int) {
        print("scrollViewWillEndDragging index : ", index)
    }
    
    /*
     메인 최상단 배너 분기처리
     1. 외부 URL -> Safari
     2. 내부 URL -> 내부 WebView
     3. 꾸밍 챌린지 상세
     4. 이벤트 상세와 같은 기능
     */
    func selectBannerHandler(data:BannerData) {
        if let dataType = data.type, let type = BannerType(rawValue: dataType) {
            switch type {
            case .BANNER_WEB_VIEW, .WEB_VIEW:
                if let linkUrl = data.linkUrl {
                    let vc = WebVC(title: data.webViewTitle ?? "", url: linkUrl)
                    AppManager.shared.push(vc)
                }
                break
            case .CHALLENGE:
                if let id = data.eventsId {
                    let vc = TagDetailVC(topTitleString: data.webViewTitle ?? "",
                                         id: id,
                                         type:.CHALLENGE,
                                         isUseBanner: true)
                    AppManager.shared.push(vc)
                }
                
                break
            case .URL:
                if let linkUrl = data.linkUrl {
                    openSafari(url: linkUrl)
                }
                break
            case .EVENT:
                if let eventsId = data.eventsId {
                    API.eventsDetail(id: eventsId, type: .EVENT) { model in
                        guard let model = model, let events = model.data?.events  else { return }
                        
                        if model.success {
                            if let linkUrl = events.linkUrl {
                                let vc = WebVC(title: data.webViewTitle ?? "", url: linkUrl)
                                AppManager.shared.push(vc)
                            }
                        }
                    }
                }
                break
            }
            
            self.removeFromSuperview()
        }
    }
}
