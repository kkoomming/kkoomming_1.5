//
//  AlertButtonView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/10.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

class AlertButtonView: UIView {
    let disposeBag = DisposeBag()
    var buttonType:AlertButtonType = .gray
    
    lazy var button = UIButton()
    let titleLabel = UILabel().then{
        $0.font = getFont(size: 16, type: .bold)
    }
    var closeHandler:(()->Void)? = nil
    
    init(title:String, buttonType:AlertButtonType = .gray, completion:(()->Void)? = nil) {
        super.init(frame: CGRect.zero)
        self.buttonType = buttonType
        
        titleLabel.text = title
        
        button.rx.tap.bind{
            if completion != nil {
                completion!()
            }
            
            if self.closeHandler != nil {
                self.closeHandler!()
            }
        }.disposed(by: disposeBag)
        
        addSubViews([
            titleLabel,
            button
        ])
        
        setupUI()
    }
    
    public func setCornerRadius(_ value:CGFloat) {
        layer.cornerRadius = value
    }
    
    public func setTextFont(_ font:UIFont) {
        titleLabel.font = font
    }
    
    private func setupUI() {
        clipsToBounds = true
        layer.masksToBounds = true
        
        switch buttonType {
        case .gray:
            layer.cornerRadius = 10
            layer.borderWidth = 2
            layer.borderColor = RGB(red: 210, green: 210, blue: 210).cgColor
            backgroundColor = .clear
            titleLabel.textColor = RGB(red: 210, green: 210, blue: 210)
        case .mainColor:
            layer.cornerRadius = 10
            layer.borderWidth = 0
            layer.borderColor = UIColor.mainColor.cgColor
            backgroundColor = .mainColor
            titleLabel.textColor = .white
        case .orange:
            layer.cornerRadius = 10
            layer.borderWidth = 0
            backgroundColor = RGB(red: 255, green: 161, blue: 92)
            titleLabel.textColor = .white
        case .blank:
            layer.cornerRadius = 10
            layer.borderColor = UIColor.mainColor.cgColor
            layer.borderWidth = 1
            backgroundColor = .white
            titleLabel.textColor = .mainColor
        }
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        titleLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
