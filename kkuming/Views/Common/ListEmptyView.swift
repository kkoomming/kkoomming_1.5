//
//  ListEmptyView.swift
//  kkuming
//
//  Created by 장효원 on 2022/07/19.
//

import UIKit
import SnapKit

class ListEmptyView: UIView {
    let image = UIImage(named: "emptyKM")!
    lazy var imageView = UIImageView(image: self.image)
    
    init() {
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    private func setupUI() {
        self.addSubview(imageView)
        
        setupConstraints()
    }
    
    private func setupConstraints() {
        imageView.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
            $0.width.height.equalTo(164)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
