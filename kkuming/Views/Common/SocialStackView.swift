//
//  SocialStackView.swift
//  kkuming
//
//  Created by 장효원 on 2022/05/06.
//

import UIKit
import SnapKit
import Then
import RxSwift
import RxCocoa

/*
 높이는 20으로 사용하고 있음
 */
class SocialStackView: UIStackView {
    var disposeBag = DisposeBag()
    var item:Post!
    var postId:Int = 0
    
    var updateItemHandler:((Post) -> ())? = nil
    
    var isLike:Bool = true {
        didSet {
            if self.isLike {
                self.likeImageView.image = UIImage(named: "ic_detail_like_over")
            } else {
                self.likeImageView.image = UIImage(named: "ic_detail_like")
            }
        }
    }
    var isScrap:Bool = false {
        didSet {
            if self.isScrap {
                self.bookmarkImageView.image = UIImage(named: "ic_detail_bookmark_over")
            } else {
                self.bookmarkImageView.image = UIImage(named: "ic_detail_bookmark")
            }
        }
    }
    
    lazy var likeImageView = UIImageView(image: UIImage(named:"ic_detail_like_over")!).then{
        $0.contentMode = .scaleAspectFit
    }
    
    lazy var likeCountLabel = UILabel().then{
        $0.textAlignment = .center
        $0.font = getFont(size: 13)
        $0.textColor = .textGrayColor
        $0.text = "99"
    }
    
    lazy var likeButton = UIButton()
    
    lazy var commentImageView = UIImageView(image: UIImage(named:"ic_detail_comment")!).then{
        $0.contentMode = .scaleAspectFit
    }
    
    lazy var commentCountLabel = UILabel().then{
        $0.textAlignment = .center
        $0.font = getFont(size: 13)
        $0.textColor = .textGrayColor
        $0.text = "99"
    }
    
    lazy var commentButton = UIButton().then{
        $0.rx.tap.bind{
            print("commentButton")
        }.disposed(by: disposeBag)
    }
    
    lazy var bookmarkImageView = UIImageView(image: UIImage(named:"ic_detail_bookmark")!).then{
        $0.contentMode = .scaleAspectFit
    }
    
    lazy var bookmarkButton = UIButton()
    
    init(){
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public func configuration(item:Post, updateItemHandler:((Post) -> ())? = nil) {
        self.item = item
        self.postId = item.id
        self.updateItemHandler = updateItemHandler
        
        //좋아요 여부
        if let isLike = item.isLike {
            self.isLike = isLike
        }
        
        //전체 좋아요 개수
        if let like = item.totalLikes {
            if like > 99 {
                likeCountLabel.text = String("99")
            } else {
                likeCountLabel.text = String("\(like)")
            }
            
            if let iconView = likeCountLabel.superview?.superview {
                iconView.snp.updateConstraints{
                    $0.width.equalTo(20 + 5 + likeCountLabel.intrinsicContentSize.width)
                }
            }
        }
        
        //전체 댓글 개수
        if let comments = item.totalComments {
            if comments > 99 {
                commentCountLabel.text = String("99")
            } else {
                commentCountLabel.text = String("\(comments)")
            }

            if let iconView = commentCountLabel.superview?.superview {
                iconView.snp.updateConstraints{
                    $0.width.equalTo(20 + 5 + commentCountLabel.intrinsicContentSize.width)
                }
            }
        }
        
        //즐겨찾기 여부
        if let isScrap = item.isScrap {
            self.isScrap = isScrap
        }
    }
    
    private func setupUI() {
        setupProperties()
        
        createIconView(type: .like)
        createIconView(type: .comment)
        createIconView(type: .bookmark)
        
        setupConstraints()
    }
    
    private func setupProperties(){
        self.axis = .horizontal
        self.alignment = .fill
        self.distribution = .fill
        self.spacing = 12
    }
    
    private func setupConstraints() {
        
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SocialStackView {
    func createIconView(type:FollowingContentType) {
        var viewList:[UIView]!
        var label:UILabel!
        var button:UIButton!
        var index = 1000
        
        switch type {
        case .like:
            viewList = [
                likeImageView,
                likeCountLabel,
            ]
            label = likeCountLabel
            button = likeButton
            index = 1000
        case .comment:
            viewList = [
                commentImageView,
                commentCountLabel,
            ]
            label = commentCountLabel
            button = commentButton
            index = 1001
        case .bookmark:
            viewList = [
                bookmarkImageView,
            ]
            button = bookmarkButton
            index = 1002
        }
        
        let stackView = UIStackView().then{
            $0.axis = .horizontal
            $0.alignment = .fill
            $0.distribution = .equalSpacing
            $0.spacing = 5
        }
        
        viewList.forEach{
            stackView.addArrangedSubview($0)
        }
        
        let iconView = UIView()
        iconView.tag = index
        
        [
            stackView,
            button
        ].forEach{
            iconView.addSubview($0)
        }
        
        self.addArrangedSubview(iconView)
        
        if label != nil {
            print("label.intrinsicContentSize.width : ", label.intrinsicContentSize.width)
            iconView.snp.makeConstraints{
                $0.width.equalTo(20 + 5 + label.intrinsicContentSize.width)
            }
        } else {
            iconView.snp.makeConstraints{
                $0.width.equalTo(20)
            }
        }
        
        stackView.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        
        button.snp.makeConstraints{
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
           
        switch type {
        case .like:
            likeImageView.snp.makeConstraints{
                $0.width.equalTo(20)
            }
        case .comment:
            commentImageView.snp.makeConstraints{
                $0.width.equalTo(20)
            }
        case .bookmark:
            bookmarkImageView.snp.makeConstraints{
                $0.width.equalTo(20)
            }
        }
    }
}

//MARK: - API
extension SocialStackView {
    func likeReact(postId:Int) {
        API.likeReact(postId: postId) { model in
            guard let model = model else { return }
            let isLiked = model.data.isLiked
            
            if model.success {
                self.isLike = isLiked
                self.item.isLike = self.isLike
                
                if let totalLikes = model.data.totalLikes {
                    self.item.totalLikes = totalLikes
                    self.likeCountLabel.text = String(totalLikes)
                    
                    self.updateItemHandler?(self.item)
                }
            } else {
                AlertManager.shared.show(text: "좋아요 에러 발생", type: .one)
            }
        }
    }
    
    func scrapReact(postId:Int) {
        API.scrapReact(postId: postId) { model in
            guard let model = model else { return }
            let isScrap = model.data.isScrap
            
            if model.success {
                self.isScrap = isScrap
                self.item.isScrap = isScrap
                
                self.updateItemHandler?(self.item)
            } else {
                AlertManager.shared.show(text: "스크랩 에러 발생", type: .one)
            }
        }
    }
}
