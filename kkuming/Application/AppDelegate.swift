//
//  AppDelegate.swift
//  Ggooming
//
//  Created by 장효원 on 2022/04/12.
//

import UIKit
import NaverThirdPartyLogin
import KakaoSDKAuth
import GoogleSignIn
import FirebaseMessaging
import Firebase

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let naviVC = UINavigationController(rootViewController: SplashVC())
        App.configNavigation(naviVC)
        window?.rootViewController = naviVC
        
        setupFCM(application)
        setupSNSLogin()
        
        return true
    }
    
    //관련링크 : https://firebase.google.com/docs/dynamic-links/ios/receive?hl=ko
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        DynamicLinks.dynamicLinks()
            .handleUniversalLink(userActivity.webpageURL!) { dynamiclink, error in
                let urlString = dynamiclink?.url?.absoluteString
                let kkoommingString = "https://kkoomming.com/init/post-detail/"
                let removedString = urlString?.replacingOccurrences(of: kkoommingString, with: "")

                DeepLinkManager.shared.openHandler = {
                    //로그인 했다면
                    if UserManager.shared.getUserId() != 0 && AppManager.shared.isRunMain {
                        DeepLinkManager.shared.openHandler = nil

                        let postId = Int(removedString ?? "0")!
                        let vc = ContentDetailVC()
                        let item = Post(id: postId)
                        vc.configuration(postData: item, postType: .NONE)
                        vc.getDetailPost() {
                            App.push(vc)
                        }
                    }
                }

                if UserManager.shared.getUserId() != 0 && AppManager.shared.isRunMain {
                    DeepLinkManager.shared.openHandler?()
                }
        }

      return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        //네이버
        NaverThirdPartyLoginConnection.getSharedInstance()?.application(app, open: url, options: options)
    
        //카카오
        if (AuthApi.isKakaoTalkLoginUrl(url)) {
            return AuthController.handleOpenUrl(url: url)
        }
        
        //구글
        GIDSignIn.sharedInstance.handle(url)
        
        //딥링크 처리
        //예제 링크 : kkoomming://?postId=38937
        /*
        if url.absoluteString.contains("kkoomming://") {
            if let params = url.params() {
                DeepLinkManager.shared.openHandler = {
                    if UserManager.shared.getUserId() == 0 {
                        print("로그인 안된 상태")
                    } else {
                        let postId = params["postId"] as! String
                        let vc = ContentDetailVC()
                        let item = Post(id: Int(postId)!)
                        vc.configuration(postData: item, postType: .NONE)
                        vc.getDetailPost() {
                            App.push(vc)
                            DeepLinkManager.shared.openHandler = nil
                        }
                    }
                }
            }
        }
        */
        
        return true
    }

    func setupSNSLogin() {
        setupNaverLogin()
        setupKakaoLogin()
    }
    
    func setupFCM(_ application: UIApplication) {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self

        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
          options: authOptions,
          completionHandler: { grant, _ in
              if grant {
                  DispatchQueue.main.async {
                      UIApplication.shared.registerForRemoteNotifications()
                  }
              }
          }
        )
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("[Log] deviceToken : ", deviceTokenString)
        
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(String(describing: fcmToken))")

        let dataDict: [String: String] = ["token": fcmToken ?? ""]
        NotificationCenter.default.post(
        name: Notification.Name("FCMToken"),
        object: nil,
        userInfo: dataDict
        )
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("willPresent")
        
        //알림 상태 변경
        API.getAlarmList(page: 1) { model in
            if let model = model, let notifications = model.data?.notifications {
                var checkRead = false
                
                for item in notifications {
                    if item.status == AlarmStatus.UNSEEN.rawValue {
                        checkRead = true
                        break
                    }
                }
                
                AlertManager.shared.status = checkRead
            }
        }
        
        completionHandler([.alert, .badge, .sound])
    }
      
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("didReceive")
        completionHandler()
    }
}
