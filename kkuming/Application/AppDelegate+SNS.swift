import NaverThirdPartyLogin
import KakaoSDKCommon
import FirebaseCore
import GoogleSignIn

extension AppDelegate {
    internal func setupNaverLogin() {
        //MARK: 네이버 로그인
        let instance = NaverThirdPartyLoginConnection.getSharedInstance()
        
        // 네이버 앱으로 인증하는 방식 활성화
        instance?.isNaverAppOauthEnable = true
        
        // SafariViewController에서 인증하는 방식 활성화
        instance?.isInAppOauthEnable = true
        
        // 인증 화면을 아이폰의 세로모드에서만 적용
        instance?.isOnlyPortraitSupportedInIphone()
        
        instance?.serviceUrlScheme = "kkumingnaverlogin" // 앱을 등록할 때 입력한 URL Scheme
        instance?.consumerKey = "71sDtrYdYeIIFyCUDNl0" // 상수 - client id
        instance?.consumerSecret = "dYtnOwJb34" // pw
        instance?.appName = "kkoomming" // app name
    }
    
    internal func setupKakaoLogin() {
        KakaoSDK.initSDK(appKey: "4d67498924a8f549a7c9cb830f3ae9dd")
    }
}
